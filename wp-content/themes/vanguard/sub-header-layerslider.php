<?php
/**
 * The sub-header slider
 */
 
global $unisphere_options, $wp_query;

$page_id = $wp_query->get_queried_object_id();

// The slider ID
$slider_id = get_post_meta($page_id, "_page_slider_layer_id", $single = true);

// The slider type
// Get WPDB Object
global $wpdb;

// Table name
$table_name = $wpdb->prefix . "layerslider";

// Get slider
$slider = $wpdb->get_row("SELECT * FROM $table_name WHERE id = ".(int)$slider_id." ORDER BY date_c DESC LIMIT 1" , ARRAY_A);
$slider = json_decode($slider['data'], true);

$slider_type = $slider['properties']['width'] == '960' ? 'normal' : 'fullwidth';

?>

 <div id="layer-slider-container" class="<?php echo $slider_type; ?>">
 	<div id="layer-slider">
 		<?php echo do_shortcode('[layerslider id="' . $slider_id . '"]'); ?>
 	</div>
 </div>
	
<?php wp_reset_query(); ?>