<?php
/**
 * unisphere framework functions
 */

/**
 * Define framework contants
 */
define( 'UNISPHERE_THEMENAME', 'Vanguard' ); // The theme name
define( 'UNISPHERE_THEMESHORTNAME', 'vanguard' ); // The theme short name
define( 'UNISPHERE_THEMEOPTIONS', 'vanguard' ); // The theme database option variable
define( 'UNISPHERE_NOTIFIER_FILE', 'http://notifier.unispheredesign.com/vanguard/notifier.xml' ); // The notifier file containing the latest version of the theme
define( 'UNISPHERE_GOOGLE_WEB_FONTS_FILE', 'http://notifier.unispheredesign.com/vanguard/update-font-list.php' ); // The remote file containing the latest Google web fonts

// Folder shortcuts
define( 'UNISPHERE_LIBRARY', get_template_directory() . '/library' ); // Shortcut to point to the /library/ dir
define( 'UNISPHERE_ADMIN', UNISPHERE_LIBRARY . '/admin' ); // Shortcut to point to the /admin/ dir
define( 'UNISPHERE_FONTS', trailingslashit(ABSPATH) . 'wp-content/uploads/fonts' );
define( 'OPTIONS_FRAMEWORK_URL', UNISPHERE_LIBRARY . '/admin/');

// URI shortcuts
define( 'UNISPHERE_CSS', get_template_directory_uri() . '/css', true ); // Shortcut to point to the /css/ URI
define( 'UNISPHERE_IMAGES', get_template_directory_uri() . '/images', true ); // Shortcut to point to the /images/ URI
define( 'UNISPHERE_JS', get_template_directory_uri() . '/js', true ); // Shortcut to point to the /js/ URI
define( 'UNISPHERE_FONTS_URL', trailingslashit(site_url()) . 'wp-content/uploads/fonts');
define( 'UNISPHERE_ADMIN_CSS', get_template_directory_uri() . '/library/admin/css', true ); // Shortcut to point to the /library/admin/css/ URI
define( 'UNISPHERE_ADMIN_IMAGES', get_template_directory_uri() . '/library/admin/images', true ); // Shortcut to point to the /library/admin/images/ URI
define( 'UNISPHERE_ADMIN_JS', get_template_directory_uri() . '/library/admin/js', true ); // Shortcut to point to the /library/admin/js/ URI
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/library/admin/');

/**
 * Include required framework files
 */
require_once(UNISPHERE_LIBRARY . '/layerslider.php');
require_once(UNISPHERE_LIBRARY . '/options.php');
require_once(UNISPHERE_LIBRARY . '/helpers.php');
require_once(UNISPHERE_LIBRARY . '/misc.php');
require_once(UNISPHERE_LIBRARY . '/breadcrumbs.php');
require_once(UNISPHERE_LIBRARY . '/post-types.php');
require_once(UNISPHERE_LIBRARY . '/semantic-classes.php');
require_once(UNISPHERE_LIBRARY . '/fonts.php');
require_once(UNISPHERE_LIBRARY . '/scripts.php');
require_once(UNISPHERE_LIBRARY . '/menus.php');
require_once(UNISPHERE_LIBRARY . '/media.php');
require_once(UNISPHERE_LIBRARY . '/widgets.php');
require_once(UNISPHERE_LIBRARY . '/gallery.php');
require_once(UNISPHERE_LIBRARY . '/custom-fields.php');
require_once(UNISPHERE_LIBRARY . '/shortcodes.php');
require_once(UNISPHERE_LIBRARY . '/comments.php');
require_once(UNISPHERE_LIBRARY . '/update-notifier.php');
require_once(UNISPHERE_LIBRARY . '/widgets/popular-posts.php');
require_once(UNISPHERE_LIBRARY . '/widgets/recent-posts.php');
require_once(UNISPHERE_LIBRARY . '/widgets/popular-portfolio.php');
require_once(UNISPHERE_LIBRARY . '/widgets/recent-portfolio.php');
require_once(UNISPHERE_LIBRARY . '/widgets/twitter.php');
require_once(UNISPHERE_LIBRARY . '/widgets/contact-form.php');
require_once(UNISPHERE_LIBRARY . '/widgets/flickr.php');
require_once(UNISPHERE_LIBRARY . '/widgets/sub-pages.php');
require_once(UNISPHERE_LIBRARY . '/widgets/social.php');
require_once(UNISPHERE_LIBRARY . '/presstrends.php');
require_once(OPTIONS_FRAMEWORK_URL . 'options-framework.php');

/**
 * Add default theme options to database on theme activation
 */
 
if ( is_admin() && isset($_GET['activated'] ) && $pagenow == 'themes.php' ) {
	
	require_once OPTIONS_FRAMEWORK_URL . 'options-interface.php';

	// Loads the options array from the theme
	require_once OPTIONS_FRAMEWORK_URL . 'options.php';
	
	// Updates the unique option id in the database if it has changed
	optionsframework_option_name();

	$optionsframework_settings = get_option('optionsframework');
	
	// Gets the unique id, returning a default if it isn't defined
	if ( isset($optionsframework_settings['id']) ) {
		$option_name = $optionsframework_settings['id'];
	}
	else {
		$option_name = 'optionsframework';
	}

	// If the option has no saved data, load the defaults
	if ( !get_option($option_name) ) {
		
		if ( isset($optionsframework_settings['knownoptions']) ) {
			$knownoptions =  $optionsframework_settings['knownoptions'];
			if ( !in_array($option_name, $knownoptions) ) {
				array_push( $knownoptions, $option_name );
				$optionsframework_settings['knownoptions'] = $knownoptions;
				update_option('optionsframework', $optionsframework_settings);
			}
		} else {
			$newoptionname = array($option_name);
			$optionsframework_settings['knownoptions'] = $newoptionname;
			update_option('optionsframework', $optionsframework_settings);
		}
		
		// Add default options to the database
		$values = array();
		$config = optionsframework_options();
		foreach ( (array) $config as $option ) {
			if ( ! isset( $option['id'] ) ) {
				continue;
			}
			if ( ! isset( $option['std'] ) ) {
				$option['std'] = '';
			}
			if ( ! isset( $option['type'] ) ) {
				continue;
			}
			if( $option['id'] != 'import_settings' 
				&& $option['id'] != 'export_settings' 
				&& $option['id'] != 'cufon_upload' 
				&& $option['id'] != 'fonts_update_google' 
				&& $option['id'] != 'sidebar_create' 
				&& $option['id'] != 'sidebar_list' ) {
				$values[$option['id']] = $option['std'];
			}			
		}

		if ( isset($values) ) {
			add_option( $option_name, $values ); // Add option with default settings
			delete_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_google_fonts' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_cufon_fonts' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-cufon_selectors' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-skin-css' );
		}
	}
	
	// Activate Presstrends
	update_option('presstrends_theme_opt', array('activated' => 'on') );

	// Update custom portfolio permalink structure
	update_option('flush_rewrite_rules', '1');
}
?>
