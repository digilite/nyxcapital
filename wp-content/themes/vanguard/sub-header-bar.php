<?php
/**
 * The sub-header bar
 */ 

global $unisphere_options;

$title = ''; 
$description = ''; 
$sub_header_container_class = '';
$sub_header_style = '';
$sub_header_container_style = ' style="';
$sub_header_height = null;
$aspect_ratio = 1;

if( !is_tag() && !is_category() && !is_author() && !is_day() && !is_month() && !is_year() && !is_404() && !is_attachment() && !is_search() ) {
	
	$post_id = $post->ID;

	$custom = get_post_custom( $post_id ); 
	$post_type = get_post_type( $post_id );

	switch( $post_type ) {
		case 'page':
		case 'portfolio_cpt':
			// Sub-header height CSS inline style based on the chosen height
			if( $custom['_page_sub_header_style'][0] == 'image-bg' ) {				
				$sub_header_height = ( isset( $custom['_page_sub_header_height'][0] ) ? $custom['_page_sub_header_height'][0] : UNISPHERE_SUB_HEADER_H );
				$sub_header_container_style .= 'height: ' . $sub_header_height . 'px;'; 
				$aspect_ratio = 960 / intval( $sub_header_height );
			}

			// Check if this page/portfolio item is overriding the Title
			if( isset( $custom['_page_sub_header_title'][0] ) ) {
				$title = $custom['_page_sub_header_title'][0];
			} else { // If not then use the post title
				$title = get_the_title($post_id);
			}

			// Check if this page/portfolio item is overriding the Description
			if( isset( $custom['_page_sub_header_desc'][0] ) ) {
				$description = $custom['_page_sub_header_desc'][0];
			}

			// Sub-header CSS inline style for an image
			$page_sub_header_image = @unserialize($custom['_page_sub_header_image'][0]);
			// If there isn't an image set, check if there's a default one
			if( $page_sub_header_image['image'] == '' ) {
				if( !empty( $unisphere_options['skin_sub_header_bgimage']['image'] ) ) {
					$page_sub_header_image = $unisphere_options['skin_sub_header_bgimage'];
				}
			}
			if( $custom['_page_sub_header_style'][0] == 'image-bg' ) {
				$sub_header_style = 'style="background-color: transparent; background-image: none; height: ' . $sub_header_height . 'px;"';
				if( $page_sub_header_image['image'] != '' ) {
					$sub_header_container_style .= 'background: url(\'' . unisphere_get_post_image_by_url( $page_sub_header_image['image'], true, "", 1920, $sub_header_height ) . '\') ' . $page_sub_header_image['repeat'] . ' ' . $page_sub_header_image['attachment'] . ' ' . $page_sub_header_image['position'] . ';';
				}
			}
			$sub_header_container_class = $custom['_page_sub_header_style'][0];
			break;

		case 'post': // the post inherits the sub-header from the parent blog page if it isn't overridden
			foreach (get_the_terms($post_id, 'category') as $term) {
				$parent_term_id = $term->term_id;
			}
			$parent_page_id = get_page_ID_by_custom_field_value('_page_blog_cat', $parent_term_id);
			$custom_parent = get_post_custom( $parent_page_id ); 

			// Sub-header height CSS inline style based on the chosen height
			if( $custom['_page_sub_header_style'][0] == 'image-bg' ) {		
				if( isset( $custom['_page_sub_header_height'][0] ) ) {		
					$sub_header_height = $custom['_page_sub_header_height'][0];
				} else { // If not then check the parent blog page
					$sub_header_height = ( isset( $custom_parent['_page_sub_header_height'][0] ) ? $custom_parent['_page_sub_header_height'][0] : UNISPHERE_SUB_HEADER_H );
				}
				$sub_header_container_style .= 'height: ' . $sub_header_height . 'px;'; 
				$aspect_ratio = 960 / intval( $sub_header_height );
			}

			// Check if this post is overriding the Title
			if( isset( $custom['_page_sub_header_title'][0] ) ) {
				$title = $custom['_page_sub_header_title'][0];
			} else { // If not then check the parent blog page
				if( isset( $custom_parent['_page_sub_header_title'][0] ) ) {
					$title = $custom_parent['_page_sub_header_title'][0];
				} else { 
					$title = get_the_title( $parent_page_id );
				}
			}

			// Check if this post is overriding the Description
			if( isset( $custom['_page_sub_header_desc'][0] ) ) {
				$description = $custom['_page_sub_header_desc'][0];
			} else { // If not then check the parent blog page
				if( isset( $custom_parent['_page_sub_header_desc'][0] ) ) {
					$description = $custom_parent['_page_sub_header_desc'][0];
				} 
			}

			// Sub-header CSS inline style for an image
			$page_sub_header_image = @unserialize($custom['_page_sub_header_image'][0]);
			// If there isn't an image set, check if there's one set for the parent page or a default one
			if( $page_sub_header_image['image'] == '' ) {
				$parent_page_sub_header_image = @unserialize($custom_parent['_page_sub_header_image'][0]);
				if( $parent_page_sub_header_image['image'] != '' ) {
					$page_sub_header_image = @unserialize($custom_parent['_page_sub_header_image'][0]);
				} elseif( !empty( $unisphere_options['skin_sub_header_bgimage']['image'] ) ) {
					$page_sub_header_image = $unisphere_options['skin_sub_header_bgimage'];
				}
			}
			if( $custom['_page_sub_header_style'][0] == 'image-bg' ) {
				$sub_header_style = 'style="background-color: transparent; background-image: none; height: ' . $sub_header_height . 'px;"';
				if( $page_sub_header_image['image'] != '' ) {
					$sub_header_container_style .= 'background: url(\'' . unisphere_get_post_image_by_url( $page_sub_header_image['image'], true, "", 1920, $sub_header_height ) . '\') ' . $page_sub_header_image['repeat'] . ' ' . $page_sub_header_image['attachment'] . ' ' . $page_sub_header_image['position'] . ';';
				}
			}
			$sub_header_container_class = $custom['_page_sub_header_style'][0];
			break;

		default:
			$title = get_the_title();
			$sub_header_container_class = isset($unisphere_options['skin_sub_header_archives_style']) ? $unisphere_options['skin_sub_header_archives_style'] : '';

			// Sub-header height CSS inline style based on the chosen height
			if( $sub_header_container_class == 'image-bg' ) {				
				$sub_header_height = UNISPHERE_SUB_HEADER_H;
				$sub_header_container_style .= 'height: ' . $sub_header_height . 'px;'; 
				$aspect_ratio = 960 / intval( $sub_header_height );
			}

			// Sub-header CSS inline style for an image
			if( !empty( $unisphere_options['skin_sub_header_bgimage']['image'] ) ) {
				$page_sub_header_image = $unisphere_options['skin_sub_header_bgimage'];
			}
			if( $sub_header_container_class == 'image-bg' ) {
				$sub_header_style = 'style="background-color: transparent; background-image: none; height: ' . $sub_header_height . 'px;"';
				if( isset($page_sub_header_image) && $page_sub_header_image['image'] != '' ) {
					$sub_header_container_style .= 'background: url(\'' . unisphere_get_post_image_by_url( $page_sub_header_image['image'], true, "", 1920, $sub_header_height ) . '\') ' . $page_sub_header_image['repeat'] . ' ' . $page_sub_header_image['attachment'] . ' ' . $page_sub_header_image['position'] . ';';
				}
			}
    		break;
	}
}

if( is_tag() || is_category() || is_author() || is_day() || is_month() || is_year() || is_404() || is_attachment() || is_search() ) {

	$sub_header_container_class = isset($unisphere_options['skin_sub_header_archives_style']) ? $unisphere_options['skin_sub_header_archives_style'] : '';

	// Sub-header height CSS inline style based on the chosen height
	if( $sub_header_container_class == 'image-bg' ) {				
		$sub_header_height = UNISPHERE_SUB_HEADER_H;
		$sub_header_container_style .= 'height: ' . $sub_header_height . 'px;'; 
		$aspect_ratio = 960 / intval( $sub_header_height );
	}

	// Sub-header CSS inline style for an image
	if( !empty( $unisphere_options['skin_sub_header_bgimage']['image'] ) ) {
		$page_sub_header_image = $unisphere_options['skin_sub_header_bgimage'];
	}
	if( $sub_header_container_class == 'image-bg' ) {
		$sub_header_style = 'style="background-color: transparent; background-image: none; height: ' . $sub_header_height . 'px;"';
		if( isset($page_sub_header_image) && $page_sub_header_image['image'] != '' ) {
			$sub_header_container_style .= 'background: url(\'' . unisphere_get_post_image_by_url( $page_sub_header_image['image'], true, "", 1920, $sub_header_height ) . '\') ' . $page_sub_header_image['repeat'] . ' ' . $page_sub_header_image['attachment'] . ' ' . $page_sub_header_image['position'] . ';';
		}
	}
}

$sub_header_container_style .= '"';

if( is_tag() ) {
	$description = __( 'Tag Archives', 'unisphere' );
	$title = single_tag_title( '', false );
} elseif( is_category() ) {
	$description = __( 'Category Archives', 'unisphere' );
	$title = single_cat_title( '', false );
} elseif( is_author() ) {
	if ( have_posts() )	the_post();
	$description = __( 'Author Archives', 'unisphere' );
	$title = get_the_author();
	rewind_posts();
} elseif( is_day() ) {
	if ( have_posts() )	the_post();
	$description = __( 'Daily Archives', 'unisphere' );
	$title = get_the_date();
	rewind_posts();
} elseif( is_month() ) {
	if ( have_posts() )	the_post();
	$description = __( 'Monthly Archives', 'unisphere' );
	$title = get_the_date('F Y');
	rewind_posts();
} elseif( is_year() ) {
	if ( have_posts() )	the_post();
	$description = __( 'Yearly Archives', 'unisphere' );
	$title = get_the_date('Y');
	rewind_posts();
} elseif( is_404() ) {					
	$title = __( 'The page could not be found', 'unisphere' );
} elseif( is_attachment() ) {
	if ( have_posts() )	the_post();
	$description = __( 'Attachment', 'unisphere' );
	$title = get_the_title();
	rewind_posts();
} elseif( is_search() ) {
	$description = __( 'Search Results', 'unisphere' );
	$title = get_search_query();
}

?>

	<!--BEGIN #sub-header-container-->
	<div id="sub-header-container" <?php echo $sub_header_container_style; ?> class="<?php echo $sub_header_container_class . ( $description == '' ? ' no-description' : '' ); ?>" data-aspect-ratio="<?php echo $aspect_ratio; ?>">

		<!--BEGIN #sub-header-->
		<div id="sub-header" <?php echo $sub_header_style; ?>>

			<div class="bar"></div>
			<h1 class="sub-header-title"><?php echo $title; ?></h1>
			<?php if( $description != '' ) : ?>
			<small><?php echo $description; ?></small>
			<?php endif; ?>
<!-- Leovic Taleon Breadcrumbs Update Solution 
			<div id="breadcrumbs"><?php echo unisphere_breadcrumbs(' / ', __('You are here: ', 'unisphere') ); ?></div> -->
	
		<!--END #sub-header-->
		</div>
		
	<!--END #sub-header-container-->
	</div>
