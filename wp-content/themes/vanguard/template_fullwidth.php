<?php 
/*
Template Name: Full Width Page
*/

get_header(); ?>

	<div class="clearfix full-width">

		<?php the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

		<?php //comments_template( '', true ); ?>

	</div>

<?php get_footer(); ?>