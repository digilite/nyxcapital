<?php 
/*
Template Name: Sitemap
*/

get_header(); ?>
    
  <div class="clearfix full-width">

    <div class="one-third">

      <h3 id="pages"><?php _e('Pages', 'unisphere'); ?></h3>
      <ul class="separator">
      <?php wp_list_pages( array( 'title_li' => '' ) ); ?>
      </ul>

    </div>

    <div class="one-third">
      <h3 id="posts"><?php _e('Posts', 'unisphere'); ?></h3>
      <ul class="separator">
      <?php
        query_posts('posts_per_page=-1');
        while(have_posts()) {
          the_post();
          echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
        }
      ?>
      </ul>

      <p>&nbsp;</p>

      <h3><?php _e( 'Category Archives', 'unisphere' ); ?></h3>
      <ul class="separator">
      <?php wp_list_categories( array( 'show_count' => 1, 'title_li' => '' ) ); ?>
      </ul>
    </div>

    <div class="one-third last">
      <?php 
      foreach( get_post_types( array('public' => true) ) as $post_type ) {
        if ( !in_array( $post_type, array('portfolio_cpt') ) )
          continue;

        $pt = get_post_type_object( $post_type );

        echo '<h3>'.$pt->labels->name.'</h3>';
        echo '<ul class="separator">';

        query_posts('post_type='.$post_type.'&posts_per_page=-1');
        while( have_posts() ) {
          the_post();
          echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
        }

        echo '</ul><p>&nbsp;</p>';
      }
      ?>
    </div>

	</div>

<?php get_footer(); ?>