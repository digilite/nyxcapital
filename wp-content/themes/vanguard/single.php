<?php
/**
 * The Template for displaying single posts.
 */

get_header(); ?>

<?php 
if( !is_attachment() && get_post_type() == 'post' ) {
	// Set the Blog as the selected menu on top
	foreach (get_the_terms($post->ID, 'category') as $term) {
	    $parent_term_id = $term->term_id;
	}
	global $parent_page_id;
	$parent_page_id = get_page_ID_by_custom_field_value('_page_blog_cat', $parent_term_id);
	$blog_root_page_ID = get_root_page($parent_page_id);
?>

<script type="text/javascript">			
	jQuery(document).ready(function() {			
		jQuery(".menu li.current_page_parent").removeClass('current_page_parent');
		jQuery(".menu li.page-item-<?php echo $blog_root_page_ID; ?>").addClass('current_page_item');
		mobileNav();					
	});			
</script>

<?php } ?>

	<div class="blog-detail-page clearfix">

		<?php if( isset($unisphere_options['show_post_navigation_links']) && $unisphere_options['show_post_navigation_links'] == '1' && get_post_type() == 'post' ) : ?>
	    <div id="single-nav">
	        <?php if( function_exists('next_post_link_plus') && function_exists('previous_post_link_plus') ) : ?>
	            <div id="single-nav-prev"><?php previous_post_link_plus( array( 'format' => '<span>&#171; ' . __('Previous', 'unisphere') . '</span>' . '%link', 'in_cats' => get_post_meta($parent_page_id, "_page_blog_cat", $single = true) ) ); ?></div>
	            <div id="single-nav-next"><?php next_post_link_plus( array( 'format' => '<span>' . __('Next', 'unisphere') . ' &#187;</span>' . '%link', 'in_cats' => get_post_meta($parent_page_id, "_page_blog_cat", $single = true) ) ); ?></div>
	        <?php else : ?>
	            <div id="single-nav-prev"><?php previous_post_link('<span>&#171; ' . __('Previous', 'unisphere') . '</span>' . '%link'); ?></div>
	            <div id="single-nav-next"><?php next_post_link('<span>' . __('Next', 'unisphere') . ' &#187;</span>' . '%link'); ?></div>
	        <?php endif; ?>
	    </div>
	    <?php endif; ?>

		<!--BEGIN #primary-->
		<div id="primary" class="blog-detail">
			<?php get_template_part( 'content', 'single' ); ?>

			<?php comments_template( '', true ); ?>

		<!--END #primary-->
		</div>

		<?php get_sidebar(); ?>
        
	</div>

<?php get_footer(); ?>
