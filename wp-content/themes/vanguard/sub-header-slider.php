<?php
/**
 * The sub-header slider
 */
 
global $unisphere_options, $wp_query;

$page_id = $wp_query->get_queried_object_id();

// The slider type
$slider_type = get_post_meta($page_id, "_page_slider_type", $single = true);
 
// The selected slider categories to show
$cats = get_post_meta($page_id, "_page_slider_cat", $single = true); 

// The number of items to display in the slider
$slider_num_items = get_post_meta($page_id, "_page_slider_num_items", $single = true); 
if( empty($slider_num_items) ) $slider_num_items = 20;

// The slider height
$slider_height_default = false;
if( $slider_type == 'fullwidth' ) {
	$slider_height = get_post_meta($page_id, "_page_slider_height", $single = true); 
	if( $slider_height == '' ) {
		$slider_height = !empty( $unisphere_options['advanced_image_size_header_full_width_slider_height'] ) ? $unisphere_options['advanced_image_size_header_full_width_slider_height'] : '450';
		$slider_height_default = true;
	}
	$slider_width = 1920;
	$aspect_ratio = 1024 / intval( $slider_height );	
} else {
	$slider_height = get_post_meta($page_id, "_page_slider_height", $single = true); 
	if( $slider_height == '' ) {
		$slider_height = !empty( $unisphere_options['advanced_image_size_header_slider_height'] ) ? $unisphere_options['advanced_image_size_header_slider_height'] : '450';
		$slider_height_default = true;
	}
	$slider_width = 960;
	$aspect_ratio = 960 / intval( $slider_height );	
}	

if( $cats == '' ) {
	echo '<div id="slider-container"><p id="no-slider-items"><strong>No slider categories have been selected for this slider.</strong><br />Please login to your Wordpress Admin area and set the slider categories you want to show by editing this page and setting one or more categories in the multi checkbox field "Slider Categories".</p></div>';
} else { ?>

	<!--BEGIN #slider-container-->
	<div id="slider-container" class="<?php echo $slider_type; ?>" style="height: <?php echo $slider_height; ?>px;">
		<ul id="slider" data-speed="<?php if ( trim( get_post_meta($post->ID, "_page_slider_transition_seconds", $single = true) ) != '' ) { echo get_post_meta($post->ID, "_page_slider_transition_seconds", $single = true); } else { echo "500"; } ?>" data-timeout="<?php if ( trim( get_post_meta($post->ID, "_page_slider_seconds", $single = true) ) != '' ) { echo get_post_meta($post->ID, "_page_slider_seconds", $single = true); } else { echo "5000"; } ?>" data-aspect-ratio="<?php echo $aspect_ratio; ?>" style="height: <?php echo $slider_height; ?>px;">
	        <?php
	        	$slider_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'slider_category');
					
				$my_query = new WP_Query( array( 'post_type' => 'slider', 'post__in' => $slider_posts_to_query, 'showposts' => $slider_num_items ) ); 
				$count = 0;
				while ($my_query->have_posts()) : $my_query->the_post();
				
				$custom = get_post_custom($post->ID);

				// Don't output HTML5/FLV videos if the SublimeVideo plugin isn't installed
				if( !(!class_exists('SublimeVideo') && isset( $custom['_slider_video'][0] ) && ( $custom['_slider_video'][0] == 'html5_flv' || $custom['_slider_video'][0] == 'html5_flv_disabled' ) ) ) :
			?>
	        	<li class="<?php echo ( isset($custom['_slider_show_description']) && $custom['_slider_show_description'][0] == '1' ? 'with-description' : 'no-description' ) ?> <?php echo ( isset($custom['_slider_video']) && $custom['_slider_video'][0] != 'disabled' ? 'has-video' : '' ) ?>" <?php echo ( $slider_type == 'fullwidth' && isset( $custom['_slider_bg_color'][0] ) && $custom['_slider_bg_color'][0] != '' ? 'style="background-color: ' . $custom['_slider_bg_color'][0] . '; background-image: none;"' : '' ); ?> style="height: <?php echo $slider_height; ?>px;">
					<?php // Display Video directly in Slider (takes precedence before everything)
					if( isset( $custom['_slider_video'][0] ) && $custom['_slider_video'][0] != 'disabled' ) :
	                    $rand = Rand();
						$data_attribute = '';
	                    switch ($custom['_slider_video'][0]) {
						    case 'youtube':
						        $data_attribute  = 'data-videourl="' . $custom['_slider_video_youtube'][0] . '"';
						        $data_attribute .= ' data-videotype="youtube"';
						        break;
						    case 'vimeo':
						        $data_attribute  = 'data-videourl="' . $custom['_slider_video_vimeo'][0] . '"';
						        $data_attribute .= ' data-videotype="vimeo"';
						        break;
						    case 'html5_flv':
						    	if( isset( $custom['_slider_video_html5_mp4'] ) ) 	{ $data_attribute .= ' data-videourl-mp4="' . $custom['_slider_video_html5_mp4'][0] . '"'; }
						    	if( isset( $custom['_slider_video_html5_mp4_hd'] ) ) { $data_attribute .= ' data-videourl-mp4-hd="' . $custom['_slider_video_html5_mp4_hd'][0] . '"'; }
						    	if( isset( $custom['_slider_video_html5_webm'] ) ) { $data_attribute .= ' data-videourl-webm="' . $custom['_slider_video_html5_webm'][0] . '"'; }
						    	if( isset( $custom['_slider_video_html5_webm_hd'] ) ) { $data_attribute .= ' data-videourl-webm-hd="' . $custom['_slider_video_html5_webm_hd'][0] . '"'; }
						    	if( isset( $custom['_slider_video_flv'] ) ) { $data_attribute .= ' data-videourl-flv="' . $custom['_slider_video_flv'][0] . '"'; }
						    	if( isset( $custom['_slider_video_flv_hd'] ) ) { $data_attribute .= ' data-videourl-flv-hd="' . $custom['_slider_video_flv_hd'][0] . '"'; }
						        $data_attribute .= ' data-videotype="html5_flv"';
						        break;
						} ?>
	                    <div class="slider-video" id="video<?php echo $rand; ?>" <?php echo $data_attribute; ?> data-autoplay="<?php echo isset($custom['_slider_autoplay']) && $custom['_slider_autoplay'][0] == '1' ? 'false' : 'true'; ?>" data-video-thumbnail="<?php echo ( isset($custom['_slider_no_autoplay_thumbnail']) ? $custom['_slider_no_autoplay_thumbnail'][0] : '' ); ?>" style="height: <?php echo $slider_height; ?>px;"><div id="objectvideo<?php echo $rand; ?>"></div></div>
	                <?php // No video directly in slider:
	                	else :  ?>
	                	<?php // ...check if there's a link for the slider item
	                		if ( isset( $custom['_slider_link'][0] ) ) : ?>
							<div class="slider-image"><a href="<?php echo $custom['_slider_link'][0]; ?>" title="<?php the_title(); ?>">
								<?php if( $slider_height_default ) : ?>
                            	<div class="slider-image"><?php echo unisphere_get_post_image( $slider_type == 'fullwidth' ? 'slider-fullwidth' : 'slider' ); ?></div>
                        	<?php else : ?>
                        		<div class="slider-image">
                        			<?php global $post;
									$attachment_id = get_post_thumbnail_id( $post->ID ); // Attachment ID
									$attachment = get_post( $attachment_id ); // The attachment object
									$attachment_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true); // The attachment (image) alt text
                        			$image = unisphere_resize( $attachment_id, null, $slider_width, $slider_height, true );
									echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />'; ?>
								</div>
                        	<?php endif; ?>
							</a></div>
	                    <?php else : ?>
	                    	<?php if( $slider_height_default ) : ?>
                            	<div class="slider-image"><?php echo unisphere_get_post_image( $slider_type == 'fullwidth' ? 'slider-fullwidth' : 'slider' ); ?></div>
                        	<?php else : ?>
                        		<div class="slider-image">
                        			<?php global $post;
									$attachment_id = get_post_thumbnail_id( $post->ID ); // Attachment ID
									$attachment = get_post( $attachment_id ); // The attachment object
									$attachment_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true); // The attachment (image) alt text
                        			$image = unisphere_resize( $attachment_id, null, $slider_width, $slider_height, true );
									echo '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />'; ?>
								</div>
                        	<?php endif; ?>
	                    <?php endif; ?>
	                <?php endif; ?>
	                    <div class="slider-description">
	                    <?php if( isset($custom['_slider_show_title']) && $custom['_slider_show_title'][0] == '1' ) : ?>	                    
	                    <h2 class="slider-title"><span class="bar"></span><?php the_title(); ?></h2>
	    	            <?php endif; ?>
	    	            <?php if( get_the_content() != '') : ?>
	        	            <div class="description"><?php the_content() ?></div>
	            	    <?php endif; ?>
	                </div>
	            </li>
	            <?php endif; ?>
			<?php $count++; endwhile; wp_reset_query(); ?>
		</ul>
	<!--END #slider-container-->
	</div>
	
	<!--BEGIN Slider jQuery initializer-->
	<script>
		// Prevent horizontal scrollbars when using the full width slider
		<?php if( $slider_type == 'fullwidth' ) : ?>
		jQuery(document).ready(function(){			
			jQuery("html").css("overflow-x", "hidden");
		});
		<?php endif; ?>
	</script>
	<!--END Slider jQuery initializer-->

	<style>
		#slider li { height: <?php echo $slider_height; ?>px!important; }
	</style>

	<?php // If the slider items is just 1 then hide the slider navigation
	if ( $count == 1 ) : ?>
	<style>
		#slider-container #slider li { display: block; }
		#slider-container #slider-next,
		#slider-container #slider-prev { display: none; }
	</style>
	<?php endif; ?>
	
<?php }
wp_reset_query(); ?>
