<?php
/**
 * The template for displaying Category Archive pages.
 */

get_header(); 

// The blog page template selected in the admin panel for the archive pages
global $unisphere_options, $current_page_template;
$current_page_template = $unisphere_options['skin_blog_archives_template']; 
$class = str_replace( 'template_', '', $current_page_template );
$class = str_replace( '.php', '', $class );
$class = str_replace( '_', '-', $class ); ?>

	<div class="clearfix">

		<!--BEGIN #primary-->
		<div id="primary" class="<?php echo $class; ?>">

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content' ); ?>

			<?php endwhile; ?>

            <?php get_template_part( 'navigation' ); ?>
        <!--END #primary-->
		</div>
        
        <?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>