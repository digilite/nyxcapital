<?php // Include WordPress core files for WP function access
$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];
require_once( $path_to_wp.'/wp-load.php' );

// Content type
header("Content-type: text/css"); 

// Theme Options
global $unisphere_options;

// Check in the cache first
if( false === ( $skin_css = get_transient( UNISPHERE_THEMEOPTIONS . '-skin-css' ) ) ) {

	$color_1 = $unisphere_options['skin_color_1']; // Main color. Also used in links and buttons
	$color_2 = $unisphere_options['skin_color_2']; // Footer bar background
	$color_3 = $unisphere_options['skin_color_3']; // Headings and footer widgets background
	$color_4 = $unisphere_options['skin_color_4']; // Body font and footer widget bars below title
	$color_5 = $unisphere_options['skin_color_5']; // Top navigation menu and breadcrumbs font
	$color_6 = $unisphere_options['skin_color_6']; // Footer widgets font
	$color_7 = $unisphere_options['skin_color_7']; // Separator lines and sub-header background
	$color_8 = $unisphere_options['skin_color_8']; // Top navigation dropdown sub-menu background
	$color_body_bgcolor = isset($unisphere_options['skin_body_bgcolor']) ? $unisphere_options['skin_body_bgcolor'] : '#f9f9f9';

	ob_start(); ?>
/* Fallback Font Family */
button, input, select, textarea { font-family: <?php echo $unisphere_options['fonts_fallback_font_family']['face']; ?>; }

/* Headings */
h1, h2, h3, h4, h5, h6 { color: <?php echo $color_3; ?>; }
<?php // Headings and blog post titles font
if( empty( $unisphere_options['fonts_headings_font']['face'] ) || startsWith( $unisphere_options['fonts_headings_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo 'h1, h2, h3, h4, h5, h6 { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . '; }' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_headings_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo 'h1, h2, h3, h4, h5, h6 { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_headings_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . '; }' . "\n";
	} else { 
		// it's a default font family stack
		echo 'h1, h2, h3, h4, h5, h6 { font-family: ' . $unisphere_options['fonts_headings_font']['face'] . '; }' . "\n";
	}
}	
?>

/* The page Body */
body { color: <?php echo $color_4; ?>; <?php echo ( !empty( $unisphere_options['skin_body_bgimage']['image'] ) ? 'background: url(\'' . $unisphere_options['skin_body_bgimage']['image'] . '\') ' . $unisphere_options['skin_body_bgimage']['repeat'] . ' ' . $unisphere_options['skin_body_bgimage']['attachment'] . ' ' . $unisphere_options['skin_body_bgimage']['position'] . ';' : '' ); ?> background-color: <?php echo $color_body_bgcolor; ?>; font-size: <?php echo $unisphere_options['fonts_body_font_family']['size']; ?>; line-height: <?php echo $unisphere_options['fonts_body_font_family']['lineheight']; ?>; }
<?php // The Body font family
	if( $unisphere_options['fonts_body_font_family']['face'] == '' ) { // Use the fallback font family
		echo 'body, h3#reply-title, h3#reply-title small, h3#comment-title, h3#related-work-title, h3#related-posts-title, h3#author-info-title, h3.info-box-title, h4.price-column-title { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . '; }' . "\n";
	} elseif( startsWith( $unisphere_options['fonts_body_font_family']['face'], 'google' ) ) { // It's a Google Web Font
		echo 'body, h3#reply-title, h3#reply-title small, h3#comment-title, h3#related-work-title, h3#related-posts-title, h3#author-info-title, h3.info-box-title, h4.price-column-title { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_body_font_family']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . '; }' . "\n";
	} else {
		echo 'body, h3#reply-title, h3#reply-title small, h3#comment-title, h3#related-work-title, h3#related-posts-title, h3#author-info-title, h3.info-box-title, h4.price-column-title { font-family: ' . $unisphere_options['fonts_body_font_family']['face'] . '; }' . "\n"; // It's a normal font stack
	}
?>

/* Links */
a { color: <?php echo $color_1; ?>; }

/* The superior-header bar */
#superior-header-container { background-color: <?php echo $color_1; ?>; }
#superior-header-left, #superior-header-left a { color: #fff; }
#superior-header-right { color: <?php echo $color_3; ?>; }
#superior-header-right .widget { background-color: #fff; }

/* The header */
#header-container { background-color: #fff; }
#header { height: <?php echo ( !empty( $unisphere_options['skin_header_height'] ) ? $unisphere_options['skin_header_height'] : '121' ); ?>px; }

/* The logo */
#logo { margin-top: <?php echo ( !empty( $unisphere_options['skin_logo_top_margin'] ) ? $unisphere_options['skin_logo_top_margin'] : '50' ); ?>px; }

/* Header menu */
#menu-bg { border-bottom: 1px solid <?php echo $color_7; ?>; top: <?php echo ( !empty( $unisphere_options['skin_menu_bar_top_margin'] ) ? $unisphere_options['skin_menu_bar_top_margin'] : '42' ); ?>px; }

.nav > li > a { color: <?php echo $color_5; ?>; }

.menu > ul > li.current_page_item > a,
.menu > ul > li.current_page_parent > a,
.menu > ul > li.current_page_ancestor > a,
.menu > ul > li.current-menu-item > a,
.menu > ul > li.current-menu-ancestor > a { color: <?php echo $color_3; ?>; border-bottom: 4px solid <?php echo $color_1; ?>; }

.nav > li > a:hover,
.nav > li.sfHover > a { color: <?php echo $color_3; ?>; border-bottom: 4px solid <?php echo $color_1; ?>; }

.nav ul { border-left: 1px solid <?php echo $color_7; ?>; border-right: 1px solid <?php echo $color_7; ?>; background-color: <?php echo $color_8; ?>; }
.nav ul a { color: <?php echo $color_3; ?>; border-bottom: 1px solid <?php echo $color_7; ?>; }
.nav ul ul { border-top: 1px solid <?php echo $color_7; ?>; }
.nav ul a:hover { background-color: <?php echo $color_7; ?>; }

<?php // Top navigation menu font
$font_size = ' font-size: ' . $unisphere_options['fonts_top_nav_menu_font']['size'] . ';';
$uppercase = ( isset( $unisphere_options['fonts_top_nav_menu_font']['uppercase']) && $unisphere_options['fonts_top_nav_menu_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_top_nav_menu_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_top_nav_menu_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $font_size . $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_top_nav_menu_font']['face'] ) || startsWith( $unisphere_options['fonts_top_nav_menu_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '.nav > li > a { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_top_nav_menu_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '.nav > li > a { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_top_nav_menu_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '.nav > li > a { font-family: ' . $unisphere_options['fonts_top_nav_menu_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Sub-header Slider */
#slider-container { background-color: #fff; }
#no-slider-items { background-color: <?php echo $color_7; ?>; }
#slider { border-bottom: 7px solid <?php echo $color_1; ?>; }
#slider .slider-description .bar { background-color: <?php echo $color_4; ?>; }
#slider .slider-description .slider-title { background-color: <?php echo $color_3; ?>; color: #fff; }
#slider-container #slider-prev { background: <?php echo $color_1; ?> url('../images/slider_prev.png') no-repeat scroll center center; }
#slider-container #slider-next { background: <?php echo $color_1; ?> url('../images/slider_next.png') no-repeat scroll center center; }

/* Sub-header Fullwidth Slider */
#slider-container.fullwidth #slider li { background-color: <?php echo $color_7; ?>; border-bottom: 7px solid <?php echo $color_1; ?>; }

<?php // Slider title font
$uppercase = ( isset( $unisphere_options['fonts_slider_title_font']['uppercase']) && $unisphere_options['fonts_slider_title_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_slider_title_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_slider_title_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_slider_title_font']['face'] ) || startsWith( $unisphere_options['fonts_slider_title_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '.slider-description .slider-title { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_slider_title_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '.slider-description .slider-title { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_slider_title_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '.slider-description .slider-title { font-family: ' . $unisphere_options['fonts_slider_title_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Sub-header LayerSlider */
#layer-slider-container.normal { background-color: #fff; }
#layer-slider-container.normal #layer-slider { border-bottom: 7px solid <?php echo $color_1; ?>; }
#layer-slider-container.fullwidth #layer-slider .ls-container { background-color: <?php echo $color_7; ?>; border-bottom: 7px solid <?php echo $color_1; ?>; }
.ls-vanguard .ls-nav-prev,
.ls-vanguard .ls-nav-next { background-color: <?php echo $color_1; ?>; }

/* Content */
#content-container { background-color: #fff; }

/* Sub-header Bar (Transparent bg) */
#sub-header-container.transparent-bg { background-color: #fff; }
#sub-header { border-bottom: 1px solid <?php echo $color_7; ?>; }
#sub-header .bar { background-color: <?php echo $color_7; ?>; }
#sub-header h1 { color: <?php echo $color_3; ?>; }
#sub-header small { color: <?php echo $color_1; ?>; }
#sub-header #breadcrumbs { background-color: <?php echo $color_7; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_breadcrumbs_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
#sub-header #breadcrumbs,
#sub-header #breadcrumbs a { color: <?php echo $color_5; ?>; }
#sub-header #breadcrumbs a:hover { color: <?php echo $color_3; ?>; }
#sub-header #breadcrumbs .prefix { color: <?php echo $color_4; ?>; }
/* Solid bg */
#sub-header-container.solid-bg,
#sub-header-container.solid-bg .bar { background-color: <?php echo $color_7; ?>; }
#sub-header-container.solid-bg #breadcrumbs { background-color: #fff!important; border-bottom: 1px solid <?php echo $color_7; ?>; }
/* Image bg */
#sub-header-container.image-bg { border-bottom: 28px solid <?php echo $color_7; ?>; }
#sub-header-container.image-bg #breadcrumbs { background-color: #fff; }
#sub-header-container.image-bg .bar { background-color: <?php echo $color_1; ?>; }
#sub-header-container.image-bg h1 { background-color: <?php echo $color_3; ?>; color: #fff; }
#sub-header-container.image-bg small { color: <?php echo $color_4; ?>; }

<?php // Sub-header title font
$uppercase = ( isset( $unisphere_options['fonts_sub_header_title_font']['uppercase']) && $unisphere_options['fonts_sub_header_title_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_sub_header_title_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_sub_header_title_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_sub_header_title_font']['face'] ) || startsWith( $unisphere_options['fonts_sub_header_title_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '#sub-header h1 { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_sub_header_title_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '#sub-header h1 { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_sub_header_title_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '#sub-header h1 { font-family: ' . $unisphere_options['fonts_sub_header_title_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

<?php // Sub-header description font
$uppercase = ( isset( $unisphere_options['fonts_sub_header_description_font']['uppercase']) && $unisphere_options['fonts_sub_header_description_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_sub_header_description_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_sub_header_description_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_sub_header_description_font']['face'] ) || startsWith( $unisphere_options['fonts_sub_header_description_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '#sub-header small { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_sub_header_description_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '#sub-header small { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_sub_header_description_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '#sub-header small { font-family: ' . $unisphere_options['fonts_sub_header_description_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Right Sidebar */
#primary { border-right: 1px solid <?php echo $color_7; ?>; }
#secondary { border-left: 1px solid <?php echo $color_7; ?>; }

/* Left Sidebar */
.left-sidebar #primary { border-left: 1px solid <?php echo $color_7; ?>; border-right: 0; }
.left-sidebar #secondary { border-right: 1px solid <?php echo $color_7; ?>; border-left: 0; }

/* Widgets */
#secondary .widget .bar { background-color: <?php echo $color_1; ?>; }
#secondary .widget ul a { color: <?php echo $color_3; ?>; }
#secondary .widget ul a:hover { color: <?php echo $color_1; ?>; }
#secondary .widget ul, #secondary .widget ul li ul, ul.separator li ul { border-top: 1px solid <?php echo $color_7; ?>; }
#secondary .widget li, ul.separator li, #secondary .widget { border-bottom: 1px solid <?php echo $color_7; ?>; }

<?php // Sidebar widget titles font
$font_size = ' font-size: ' . $unisphere_options['fonts_sidebar_widget_titles_font']['size'] . ';';
$uppercase = ( isset( $unisphere_options['fonts_sidebar_widget_titles_font']['uppercase']) && $unisphere_options['fonts_sidebar_widget_titles_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_sidebar_widget_titles_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_sidebar_widget_titles_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $font_size . $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_sidebar_widget_titles_font']['face'] ) || startsWith( $unisphere_options['fonts_sidebar_widget_titles_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '#secondary .widget > h3 { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_sidebar_widget_titles_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '#secondary .widget > h3 { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_sidebar_widget_titles_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '#secondary .widget > h3 { font-family: ' . $unisphere_options['fonts_sidebar_widget_titles_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Custom Menu and Sub-Pages Widget */
#secondary .widget-sub-pages li.current_page_item,
#secondary .widget_pages li.current_page_item,
#secondary .widget .menu li.current-menu-item { background-color: <?php echo $color_3; ?>; }
#secondary .widget-sub-pages li.current_page_item ul,
#secondary .widget_pages li.current_page_item ul,
#secondary .widget .menu li.current-menu-item ul { background-color: #fff; }
.widget-sub-pages li.current_page_item > a,
.widget_pages li.current_page_item > a,
.widget .menu li.current-menu-item > a { color: #fff!important; }

/* Recent and Popular Posts Widget */
.widget-posts li .post-title { color: <?php echo $color_3; ?>; }
.widget-posts li .post-title:hover { color: <?php echo $color_1; ?>; }

/* Blog (shared styles) */
.post-meta, .post-meta a { color: <?php echo $color_3; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_posts_meta_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
/* Blog (shared styles) */
.post-meta a:hover { color: <?php echo $color_1; ?>; }

/* Blog (wide image) */
.blog-wide-image .post-title-meta-wrapper { background-image: url('../images/transparent_black.png'); }
.blog-wide-image .bar { background-color: #fff; }
.blog-wide-image .post-title a { color: #fff; }
.blog-wide-image .post-title a:hover { color: <?php echo $color_1; ?>; }
.blog-wide-image .post-meta { background-color: #fff; }
/* No image */
.blog-wide-image .no-image .post-title-meta-wrapper { background-image: none; }
.blog-wide-image .no-image .bar { background-color: <?php echo $color_7; ?>; }
.blog-wide-image .no-image .post-title a { color: <?php echo $color_3; ?>; }
.blog-wide-image .no-image .post-title a:hover { color: <?php echo $color_1; ?>; }
.blog-wide-image .no-image .post-meta { background-color: transparent; }

/* Blog (half-image) */
.blog-half-image .bar { background-color: <?php echo $color_7; ?>; }
.blog-half-image .post-title a { color: <?php echo $color_3; ?>; }
.blog-half-image .post-title a:hover { color: <?php echo $color_1; ?>; }

/* Blog detail page */
.blog-detail .post-title-meta-wrapper { background-image: url('../images/transparent_black.png'); }
.blog-detail .bar { background-color: #fff; }
.blog-detail .post-title { color: #fff; }
.blog-detail .post-meta { background-color: #fff; }
/* No image */
.blog-detail .no-image .post-title-meta-wrapper { background-image: none; }
.blog-detail .no-image .bar { background-color: <?php echo $color_7; ?>; }
.blog-detail .no-image .post-title { color: <?php echo $color_3; ?>; }
.blog-detail .no-image .post-meta { background-color: transparent; }

<?php // Blog title font
$uppercase = ( isset( $unisphere_options['fonts_blog_title_font']['uppercase']) && $unisphere_options['fonts_blog_title_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_blog_title_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_blog_title_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_blog_title_font']['face'] ) || startsWith( $unisphere_options['fonts_blog_title_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '.blog-wide-image h2.post-title a, .blog-half-image h2.post-title a, .blog-detail h1.post-title { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_blog_title_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '.blog-wide-image h2.post-title a, .blog-half-image h2.post-title a, .blog-detail h1.post-title { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_blog_title_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '.blog-wide-image h2.post-title a, .blog-half-image h2.post-title a, .blog-detail h1.post-title { font-family: ' . $unisphere_options['fonts_blog_title_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Author Info */
#author-info-wrapper h3 { border-bottom: 1px solid <?php echo $color_7; ?>; border-top: 1px solid <?php echo $color_7; ?>; background-color: <?php echo $color_8; ?>; }
/* Related Posts */
.blog-detail .related-posts h3 { border-bottom: 1px solid <?php echo $color_7; ?>; border-top: 1px solid <?php echo $color_7; ?>; background-color: <?php echo $color_8; ?>; }
.blog-detail .related-posts li .post-title:hover { color: <?php echo $color_1; ?>; }

/* Comments */
#comment-title { border-bottom: 1px solid <?php echo $color_7; ?>; border-top: 1px solid <?php echo $color_7; ?>; background-color: <?php echo $color_8; ?>; }
#comment-title small { color: <?php echo $color_1; ?>; }
.comment-list > li.comment,
.comment-list .children > li { border-top: 1px solid <?php echo $color_7; ?>; }
.commenter, .commenter a { color: <?php echo $color_3; ?>; }
.commenter a:hover { color: <?php echo $color_1; ?>; }
.comment-meta { color: <?php echo $color_5; ?>; }
.bypostauthor > .single-comment > .comment-content { color: <?php echo $color_3; ?>; }

/* Comment Reply Form */
#reply-title { border-bottom: 1px solid <?php echo $color_7; ?>; border-top: 1px solid <?php echo $color_7; ?>; background-color: <?php echo $color_8; ?>; }
.form-section label { color: <?php echo $color_3; ?>; }

/* Portfolio */
#portfolio-filter-container { border-bottom: 1px solid <?php echo $color_7; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_sortable_portfolio_categories_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
.portfolio-browse { color: <?php echo $color_4; ?>; background-color: <?php echo $color_7; ?>; }
.portfolio-filters li a { color: <?php echo $color_5; ?>; }
.portfolio-filters li a.active,
.portfolio-filters li a:hover { color: <?php echo $color_1; ?>; }

/* Portfolio detail page */
.portfolio-detail-page .related-work h3 { border-bottom: 1px solid <?php echo $color_7; ?>; border-top: 1px solid <?php echo $color_7; ?>; background-color: <?php echo $color_8; ?>; }

/* Portfolio 1 Column */
.portfolio-1-columns-list .portfolio-item { border-bottom: 1px solid <?php echo $color_7; ?>; }
.portfolio-1-columns-list .bar { background-color: <?php echo $color_7; ?>; }
.portfolio-1-columns-list .portfolio-title {color: <?php echo $color_3; ?>; }
.portfolio-1-columns-list .portfolio-title:hover {color: <?php echo $color_1; ?>; }

<?php // Portfolio titles font in the 1 column template
$uppercase = ( isset( $unisphere_options['fonts_portfolio_title_1_column_font']['uppercase']) && $unisphere_options['fonts_portfolio_title_1_column_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_portfolio_title_1_column_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_portfolio_title_1_column_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_portfolio_title_1_column_font']['face'] ) || startsWith( $unisphere_options['fonts_portfolio_title_1_column_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '.portfolio-1-columns-list .portfolio-title { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_portfolio_title_1_column_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '.portfolio-1-columns-list .portfolio-title { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_portfolio_title_1_column_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '.portfolio-1-columns-list .portfolio-title { font-family: ' . $unisphere_options['fonts_portfolio_title_1_column_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Portfolio 2 Columns */
.portfolio-2-columns-list .long-bar { background-color: <?php echo $color_3; ?>; }
.portfolio-2-columns-list .portfolio-info { background-color: <?php echo $color_8; ?>; border-left: 1px solid <?php echo $color_7; ?>; border-bottom: 1px solid <?php echo $color_7; ?>; border-right: 1px solid <?php echo $color_7; ?>; }
.portfolio-2-columns-list .portfolio-title { color: <?php echo $color_3; ?>; }
.portfolio-2-columns-list .portfolio-title:hover { color: <?php echo $color_1; ?>; }

/* Portfolio 3 Columns */
.portfolio-3-columns-list .long-bar { background-color: <?php echo $color_3; ?>; }
.portfolio-3-columns-list .portfolio-info { background-color: <?php echo $color_8; ?>; border-left: 1px solid <?php echo $color_7; ?>; border-bottom: 1px solid <?php echo $color_7; ?>; border-right: 1px solid <?php echo $color_7; ?>; }
.portfolio-3-columns-list .portfolio-title { color: <?php echo $color_3; ?>; }
.portfolio-3-columns-list .portfolio-title:hover { color: <?php echo $color_1; ?>; }

/* Portfolio 4 Columns */
.portfolio-4-columns-list .long-bar { background-color: <?php echo $color_3; ?>; }
.portfolio-4-columns-list .portfolio-info { background-color: <?php echo $color_8; ?>; border-left: 1px solid <?php echo $color_7; ?>; border-bottom: 1px solid <?php echo $color_7; ?>; border-right: 1px solid <?php echo $color_7; ?>; }
.portfolio-4-columns-list .portfolio-title { color: <?php echo $color_3; ?>; }
.portfolio-4-columns-list .portfolio-title:hover { color: <?php echo $color_1; ?>; }

<?php // Portfolio titles font in templates with gutter
$uppercase = ( isset( $unisphere_options['fonts_portfolio_title_gutter_font']['uppercase']) && $unisphere_options['fonts_portfolio_title_gutter_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_portfolio_title_gutter_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_portfolio_title_gutter_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_portfolio_title_gutter_font']['face'] ) || startsWith( $unisphere_options['fonts_portfolio_title_gutter_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '.portfolio-2-columns-list .portfolio-title, .portfolio-3-columns-list .portfolio-title, .portfolio-4-columns-list .portfolio-title { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_portfolio_title_gutter_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '.portfolio-2-columns-list .portfolio-title, .portfolio-3-columns-list .portfolio-title, .portfolio-4-columns-list .portfolio-title { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_portfolio_title_gutter_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '.portfolio-2-columns-list .portfolio-title, .portfolio-3-columns-list .portfolio-title, .portfolio-4-columns-list .portfolio-title { font-family: ' . $unisphere_options['fonts_portfolio_title_gutter_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Portfolio 2 Columns without gutter */
.portfolio-2-columns-list-no-gutter .portfolio-info { background-image: url('../images/transparent_black.png'); }
.portfolio-2-columns-list-no-gutter .portfolio-title { color: #fff; }
.portfolio-2-columns-list-no-gutter .portfolio-title:hover { color: <?php echo $color_1; ?>; }

/* Portfolio 3 Columns without gutter */
.portfolio-3-columns-list-no-gutter .portfolio-info { background-image: url('../images/transparent_black.png'); }
.portfolio-3-columns-list-no-gutter .portfolio-title { color: #fff; }
.portfolio-3-columns-list-no-gutter .portfolio-title:hover { color: <?php echo $color_1; ?>; }

/* Portfolio 4 Columns without gutter */
.portfolio-4-columns-list-no-gutter .portfolio-info { background-image: url('../images/transparent_black.png'); }
.portfolio-4-columns-list-no-gutter .portfolio-title { color: #fff; }
.portfolio-4-columns-list-no-gutter .portfolio-title:hover { color: <?php echo $color_1; ?>; }

<?php // Portfolio titles font in templates without gutter
$uppercase = ( isset( $unisphere_options['fonts_portfolio_title_no_gutter_font']['uppercase']) && $unisphere_options['fonts_portfolio_title_no_gutter_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_portfolio_title_no_gutter_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_portfolio_title_no_gutter_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'] ) || startsWith( $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '.portfolio-2-columns-list-no-gutter .portfolio-title, .portfolio-3-columns-list-no-gutter .portfolio-title, .portfolio-4-columns-list-no-gutter .portfolio-title { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '.portfolio-2-columns-list-no-gutter .portfolio-title, .portfolio-3-columns-list-no-gutter .portfolio-title, .portfolio-4-columns-list-no-gutter .portfolio-title { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '.portfolio-2-columns-list-no-gutter .portfolio-title, .portfolio-3-columns-list-no-gutter .portfolio-title, .portfolio-4-columns-list-no-gutter .portfolio-title { font-family: ' . $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Footer Widgets */
#footer-widgets-container { background-color: <?php echo $color_3; ?>; border-top: 7px solid <?php echo $color_7; ?>; }
.footer-column { border-left: 1px solid <?php echo $color_2; ?>; }
.footer-column .bar { background-color: <?php echo $color_4; ?>; }
.footer-column .widget-title,
.footer-column a { color: #fff; }
.footer-column .widget { color: <?php echo $color_6; ?>; border-top: 1px solid <?php echo $color_2; ?>; }

<?php // Footer widget titles font
$font_size = ' font-size: ' . $unisphere_options['fonts_footer_widget_titles_font']['size'] . ';';
$uppercase = ( isset( $unisphere_options['fonts_footer_widget_titles_font']['uppercase']) && $unisphere_options['fonts_footer_widget_titles_font']['uppercase'] == 'on' ? ' text-transform: uppercase;' : '');
$font_weight = ' font-weight: ' . str_replace( 'italic', '', $unisphere_options['fonts_footer_widget_titles_font']['fontstyle'] ) . ';';
$italic = endsWith( $unisphere_options['fonts_footer_widget_titles_font']['fontstyle'], 'italic' ) ? ' font-style: italic;' : '';
$parameters = $font_size . $uppercase . $font_weight . $italic;

// The font family
if( empty( $unisphere_options['fonts_footer_widget_titles_font']['face'] ) || startsWith( $unisphere_options['fonts_footer_widget_titles_font']['face'], 'cufon' ) ) {
	// use default fallback font
	echo '.footer-column .widget > .widget-title { font-family: ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
} else {	
	if( startsWith( $unisphere_options['fonts_footer_widget_titles_font']['face'], 'google' ) ) {
		// it's a Google web font
		echo '.footer-column .widget > .widget-title { font-family: \'' . str_replace( '+', ' ', str_replace( 'google:', '', $unisphere_options['fonts_footer_widget_titles_font']['face'] ) ) . '\', ' . $unisphere_options['fonts_fallback_font_family']['face'] . ';' . $parameters . '}' . "\n";
	} else { 
		// it's a default font family stack
		echo '.footer-column .widget > .widget-title { font-family: ' . $unisphere_options['fonts_footer_widget_titles_font']['face'] . ';' . $parameters . '}' . "\n";
	}
}	
?>

/* Custom Menu Widget on the footer widget area */
.footer-column .widget-sub-pages li.current_page_item > a, 
.footer-column .widget_pages li.current_page_item > a, 
.footer-column .widget .menu li.current-menu-item > a { background-color: <?php echo $color_4; ?>; }

/* Footer */
#footer-container { background-color: <?php echo $color_2; ?>; border-bottom: 7px solid <?php echo $color_1; ?>; }
#footer { color: <?php echo $color_4; ?>; }
#footer a { color: #fff; }

/* Sharing buttons */
.post-share-buttons .post-share-title { background-color: <?php echo $color_3; ?>; color: #fff; }
.post-share-buttons .addthis_toolbox { background-color: <?php echo $color_7; ?>; }

/* Separator */
div.hr { background-color: <?php echo $color_7; ?>; }
div.hr span.to-top { color: <?php echo $color_5; ?>; background-color: <?php echo $color_7; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_separator_title_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
div.hr span.to-top:hover { color: <?php echo $color_2; ?>; }

/* Buttons */
.custom-button, button, #submit, .wp-pagenavi a, .wp-pagenavi a:link, .wp-pagenavi a:visited, .wp-pagenavi span.current { color: #fff; background-color: <?php echo $color_1; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_button_title_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
.custom-button:hover, button:hover, #submit:hover, .wp-pagenavi a:hover, .wp-pagenavi span.current { background-color: <?php echo $color_3; ?>; }
.custom-button.alt { background-color: <?php echo $color_3; ?>; }
.custom-button.alt:hover{ background-color: <?php echo $color_1; ?>; }

/* Information Box */
.info-box .info-box-title { color: #fff; background-color: <?php echo $color_1; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_info_box_title_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
.info-box .info-box-content { background-color: <?php echo $color_7; ?>; }

/* Toggle */
.toggle-container .toggle .toggle-sign.closed { background: <?php echo $color_1; ?> url('../images/toggle_icons.png') no-repeat scroll 0 0; }
.toggle-container .toggle .toggle-sign.opened { background: <?php echo $color_3; ?> url('../images/toggle_icons.png') no-repeat scroll -20px 0; }
.toggle-container .toggle .toggle-title { color: <?php echo $color_3; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_toggle_title_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }

/* Tabs */
ul.tabs li { background-color: <?php echo $color_7; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_tab_title_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
ul.tabs li a { color: <?php echo $color_5; ?>; }
ul.tabs li.active,
ul.tabs li.active a,
ul.tabs li:hover,
ul.tabs li:hover a { color: #fff; background-color: <?php echo $color_1; ?>; }

/* Testimonial */
.testimonial-meta .testimonial-company, .testimonial-meta .testimonial-person { <?php echo ( !empty( $unisphere_options['fonts_show_testimonial_title_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }

/* Pricing Table */
.price-column { background-color: <?php echo $color_8; ?>; border-bottom: 2px solid <?php echo $color_7; ?>; }
.price-column h4 { background-color: <?php echo $color_3; ?>; <?php echo ( !empty( $unisphere_options['fonts_show_price_column_title_uppercase'] ) ? 'text-transform: uppercase;' : ''); ?> }
.price-column h4,
.price-column li .custom-button { color: #fff; background-color: <?php echo $color_3; ?>; }
.price-column li .custom-button:hover { background-color: <?php echo $color_1; ?>; }
.price-column .price-tag { color: <?php echo $color_3; ?>; }

.price-column-featured { background-color: #fff; border-bottom: 0; }
.price-column-featured h4,
.price-column-featured li .custom-button { color: #fff; background-color: <?php echo $color_1; ?>; }
.price-column-featured li .custom-button:hover { background-color: <?php echo $color_3; ?>; }
.price-column-featured .price-tag { color: <?php echo $color_1; ?>; }

/* Highlight */
.highlight { color: #fff; background-color: <?php echo $color_1; ?>; }

/* Blockquote */
blockquote { color: <?php echo $color_3; ?>; border-left: 7px solid <?php echo $color_1; ?>; }

/* Call to action */
.call-to-action-big p.excerpt { color: <?php echo $color_5; ?>; }

/* Call to action bar */
.call-to-action-bar-wrapper { border-left: 7px solid <?php echo $color_1; ?>; }
.call-to-action-bar { background-color: <?php echo $color_7; ?>; } 

/* Wide bar shortcode */
.wide-bar.white-text * { color: #fff!important; }

/* Big Title */
.big-title .bar { background-color: <?php echo $color_7; ?>; }
.big-title p.sub-title { color: <?php echo $color_5; ?>; }

/* Small Title */
.small-title h3 { background-color: #fff; }
.small-title .long-bar { background-color: <?php echo $color_7; ?>; }

/* Testimonials */
.testimonial-content { background-color: <?php echo $color_7; ?>; color: <?php echo $color_3; ?>; }
.testimonial-content p { font-size: 16px; line-height: 23px; }
.testimonial-content .triangle { border-top: 20px solid <?php echo $color_7; ?>; }
.testimonial-meta .testimonial-person { display: block; font-size: 11px; }

/* Shortcode Slider */
.no-slider-items, .slider-container { background-color: <?php echo $color_7; ?>; }
.slider-container { border-bottom: 7px solid <?php echo $color_1; ?>; }
.slider .slider-description .bar { background-color: <?php echo $color_4; ?>; }
.slider .slider-description .slider-title { background-color: <?php echo $color_3; ?>; color: #fff; }
.slider-container .slider-prev { background: <?php echo $color_1; ?> url('../images/slider_prev.png') no-repeat scroll center center; }
.slider-container .slider-next { background: <?php echo $color_1; ?> url('../images/slider_next.png') no-repeat scroll center center; }

/* Single post navigation */
#single-nav { border-bottom: 1px solid <?php echo $color_7; ?>; }
#single-nav-next { border-left: 1px solid <?php echo $color_7; ?>; }
#single-nav-prev { border-right: 1px solid <?php echo $color_7; ?>; }
#single-nav span { color: <?php echo $color_3; ?>; }
#single-nav a { color: <?php echo $color_5; ?>; }
#single-nav a:hover { color: <?php echo $color_1; ?>; }

/* Single post navigation below */
#single-nav-below-title { border-bottom: 1px solid <?php echo $color_7; ?>; border-top: 1px solid <?php echo $color_7; ?>; background-color: <?php echo $color_8; ?>; }
#single-nav-below { border-bottom: 1px solid <?php echo $color_7; ?>; }
#single-nav-below-next { border-left: 1px solid <?php echo $color_7; ?>; }
#single-nav-below-prev { border-right: 1px solid <?php echo $color_7; ?>; }
#single-nav-below span { color: <?php echo $color_3; ?>; }
#single-nav-below a { color: <?php echo $color_5; ?>; }
#single-nav-below a:hover { color: <?php echo $color_1; ?>; }

/* Pre, Code and Tables border color (in main area) */
table.border, pre, code { border: 1px solid <?php echo $color_7; ?>; }
td { border-top: 1px solid <?php echo $color_7; ?>; }
table.border th + th, table.border td + td, table.border th + td { border-left: 1px solid <?php echo $color_7; ?>; }
/* Pre, Code and Tables background color (in main area) */
pre, code, .embedded-video,
table.stripes tbody tr:nth-child(odd) td, 
table.stripes tbody tr:nth-child(odd) th { background-color: <?php echo $color_8; ?>; }

/* Forms (in Main Area) */
input, textarea, select { background-color: #fff; border: 1px solid #dbdbdb; color: <?php echo $color_4; ?>; }
input:focus, textarea:focus, select:focus { border: 1px solid #cfcfcf; color: <?php echo $color_3; ?>; }
/* Forms (in Footer Area) */
#footer-widgets input, #footer-widgets textarea, #footer-widgets select { background-color: <?php echo $color_6; ?>; border: 1px solid <?php echo $color_6; ?>; color: <?php echo $color_3; ?>; }
#footer-widgets input:focus, #footer-widgets textarea:focus, #footer-widgets select:focus { border: 1px solid <?php echo $color_6; ?>; color: <?php echo $color_3; ?>; }

/* Lightbox styling */
div.pp_default .pp_content_container .pp_details { border-bottom: 7px solid <?php echo $color_1; ?>; }

<?php 
	$skin_css = ob_get_contents();
	ob_end_clean(); 

	// Cache it for 60 minutes
	set_transient( UNISPHERE_THEMEOPTIONS . '-skin-css', $skin_css, 60 * 60 );
}

echo $skin_css; ?>