<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
 
get_header(); ?>

	<div class="clearfix">

		<!--BEGIN #primary-->
		<div id="primary">

			<p><strong>NOTICE:</strong> you are seeing this page because of one of the following two things:</p>
			<ul>
				<li>if this was supposed to be your <strong>Home page</strong> go to "Settings -> Reading", select "A static page" and set your home page in the "Front page" dropdown field.<br/><br/></li>
				<li>if this was supposed to be your <strong>Blog page</strong> first create a page and assign it one of the Blog page templates. Make sure to choose what categories should be displayed for that blog page under the "Vanguard Page Settings" Blog toggle. Finally go to "Settings -> Reading", select "A static page" and set the blog page you've just created in the "Front page" dropdown field (make sure you haven't selected any page in the "Posts page" dropdown field).</li>
			</ul>
	
        <!--END #primary-->
		</div>
        
        <?php get_sidebar(); ?>

	</div>	

<?php get_footer(); ?>
