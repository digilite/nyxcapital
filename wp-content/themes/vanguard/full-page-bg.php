<?php
/**
 * The full page background image
 */ 

global $unisphere_options;
wp_reset_query();

$full_bg_image = '';

// See if there's a full page background image set in the current skin
if( isset( $unisphere_options['skin_full_page_bgimage'] ) && $unisphere_options['skin_full_page_bgimage']['image'] != '' ) {
	$full_bg_image = '<div id="full-bg-wrapper"><img src="' . $unisphere_options['skin_full_page_bgimage']['image'] . '" id="full-bg" alt="" /></div>';
}

// Check if a page, post or portfolio item has a custom full page background image set
if( !is_tag() && !is_category() && !is_author() && !is_day() && !is_month() && !is_year() && !is_404() && !is_attachment() && !is_search() ) {
	
	$post_id = $post->ID;
	$custom = get_post_custom( $post_id ); 
	$post_type = get_post_type( $post_id );

	switch( $post_type ) {
		case 'page':
		case 'portfolio_cpt':
		case 'post':
			// Sub-header height CSS inline style based on the chosen height
			if( isset( $custom['_page_full_page_bg'][0] ) ) {
				$full_bg_image = '<div id="full-bg-wrapper"><img src="' . $custom['_page_full_page_bg'][0] . '" id="full-bg" alt="" /></div>';
			}

			break;
	}
}

echo $full_bg_image;

?>
