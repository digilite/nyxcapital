<?php
/**
 * Set the theme's max width for WordPress embedded videos
 */
if ( ! isset( $content_width ) ) $content_width = 960;


/**
 * Add default posts and comments RSS feed links to head
 */
add_theme_support( 'automatic-feed-links' );


/**
 * Remove junk from head
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);


/**
 * Sets the post excerpt length to 45 words.
 */
function unisphere_excerpt_length( $length ) {
	return 45;
}
add_filter( 'excerpt_length', 'unisphere_excerpt_length' );


/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis
 */
function unisphere_auto_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter( 'excerpt_more', 'unisphere_auto_excerpt_more' );


/**
 * Styles read more link
 */
function unisphere_more_link($link) {
  $link = preg_replace('/<a(.*?)>(.*?)<\/a>/im', '<a$1>' . __( 'Read more', 'unisphere' ) . '</a>', $link);
  $link = str_replace('class="', 'class="custom-button ', $link);
  if ( comments_open() && !post_password_required() ) {
    ob_start();
    comments_popup_link( __( 'Leave a comment', 'unisphere' ), __( '1 Comment', 'unisphere' ), __( '% Comments', 'unisphere' ), 'custom-button alt comment-count' );
    $link .=  ob_get_contents();
    ob_end_clean();
  }
  
  return '<p class="more-link-wrapper">' . $link . '</p>';
}

add_filter( 'the_content_more_link', 'unisphere_more_link' );



/**
 * Gets a specific number of words from a string and appends an ellipsis.
 */
function unisphere_custom_excerpt( $excerpt, $num_words ) {
	//Delete all shortcode tags from the content.
    $excerpt = strip_shortcodes( $excerpt );
 
    $excerpt = str_replace(']]>', ']]&gt;', $excerpt);
 
    $allowed_tags = '<p>,<a>,<strong>'; /*** Add the allowed HTML tags separated by a comma. ***/
    $excerpt = strip_tags($excerpt, $allowed_tags);
 
    $words = preg_split("/[\n\r\t ]+/", $excerpt, $num_words + 1, PREG_SPLIT_NO_EMPTY);
    if ( count($words) > $num_words ) {
        array_pop($words);
        $excerpt = implode(' ', $words);
        $excerpt = $excerpt . '&hellip;';
    } else {
        $excerpt = implode(' ', $words);
    }

	return $excerpt;
}


/**
 * Exclude slider images from showing in the search results
 */
function unisphere_search_filter($query) {
	if ( !is_admin() ) {
	    if ($query->is_search) {
			$query->set('post_type', array('post', 'page', 'portfolio_cpt'));
	    }
	}
    return $query;
}
add_filter('pre_get_posts', 'unisphere_search_filter');


/**
 * Make theme available for translation
 * Translations can be filed in the /languages/ directory
 */
load_theme_textdomain( 'unisphere', get_template_directory() . '/languages' );