<?php
/*
Plugin Name: Options Framework
Plugin URI: http://www.wptheming.com
Description: A framework for building theme options.
Version: 0.9
Author: Devin Price
Author URI: http://www.wptheming.com
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* Basic plugin definitions */

define('OPTIONS_FRAMEWORK_VERSION', '0.9');
define('THEME_OPTIONS_PAGE', 'Theme Options');
define('SIDEBAR_MANAGER_PAGE', 'Sidebars Manager');
define('SKINS_MANAGER_PAGE', 'Skins Manager');
define('IMAGE_SIZES_PAGE', 'Image Sizes');
define('FONTS_MANAGER_PAGE', 'Fonts Manager');

global $current_location;

/* Make sure we don't expose any info if called directly */

if ( !function_exists( 'add_action' ) ) {
	echo "Hi there!  I'm just a little plugin, don't mind me.";
	exit;
}

/* If the user can't edit theme options, no use running this plugin */

add_action( 'init', 'optionsframework_rolescheck' );

function optionsframework_rolescheck () {
	ob_start();
	if ( current_user_can( 'edit_theme_options' ) ) {
		// If the user can edit theme options, let the fun begin!		
		add_action( 'admin_menu', 'optionsframework_add_page');
	}

	add_action( 'admin_init', 'optionsframework_init' );
	add_action( 'admin_init', 'optionsframework_mlu_init' );	
}

/* Loads the file for option sanitization */

add_action('init', 'optionsframework_load_sanitization' );

function optionsframework_load_sanitization() {
	require_once dirname( __FILE__ ) . '/options-sanitize.php';
}

/* 
 * Creates the settings in the database by looping through the array
 * we supplied in options.php.  This is a neat way to do it since
 * we won't have to save settings for headers, descriptions, or arguments.
 *
 * Read more about the Settings API in the WordPress codex:
 * http://codex.wordpress.org/Settings_API
 *
 */

function optionsframework_init() {

	// Include the required files
	require_once dirname( __FILE__ ) . '/options-interface.php';
	require_once dirname( __FILE__ ) . '/options-medialibrary-uploader.php';

	// Loads the options array from the theme
	require_once dirname( __FILE__ ) . '/options.php';
	
	$optionsframework_settings = get_option('optionsframework' );
	
	// Updates the unique option id in the database if it has changed
	optionsframework_option_name();
	
	// Gets the unique id, returning a default if it isn't defined
	if ( isset($optionsframework_settings['id']) ) {
		$option_name = $optionsframework_settings['id'];
	}
	else {
		$option_name = 'optionsframework';
	}
	
	// If the option has no saved data, load the defaults
	if ( ! get_option($option_name) ) {
		optionsframework_setdefaults();
	}
	
	// Registers the settings fields and callback
	register_setting( 'optionsframework', $option_name, 'optionsframework_validate' );
}

/* 
 * Adds default options to the database if they aren't already present.
 * May update this later to load only on plugin activation, or theme
 * activation since most people won't be editing the options.php
 * on a regular basis.
 *
 * http://codex.wordpress.org/Function_Reference/add_option
 *
 */

function optionsframework_setdefaults() {
	global $current_location;
	
	$optionsframework_settings = get_option('optionsframework');

	// Gets the unique option id
	$option_name = $optionsframework_settings['id'];
	
	/* 
	 * Each theme will hopefully have a unique id, and all of its options saved
	 * as a separate option set.  We need to track all of these option sets so
	 * it can be easily deleted if someone wishes to remove the plugin and
	 * its associated data.  No need to clutter the database.  
	 *
	 */
	
	if ( isset($optionsframework_settings['knownoptions']) ) {
		$knownoptions =  $optionsframework_settings['knownoptions'];
		if ( !in_array($option_name, $knownoptions) ) {
			array_push( $knownoptions, $option_name );
			$optionsframework_settings['knownoptions'] = $knownoptions;
			update_option('optionsframework', $optionsframework_settings);
		}
	} else {
		$newoptionname = array($option_name);
		$optionsframework_settings['knownoptions'] = $newoptionname;
		update_option('optionsframework', $optionsframework_settings);
	}
	
	// Gets the default options data from the array in options.php
	$options = optionsframework_options();
	
	// If the options haven't been added to the database yet, they are added now
	$values = of_get_default_values( $current_location );
	
	if ( isset($values) ) {
		add_option( $option_name, $values ); // Add option with default settings
	}
}

/* Add a subpage called "Theme Options" to the appearance menu. */

if ( !function_exists( 'optionsframework_add_page' ) ) {
function optionsframework_add_page() {

	// Main theme menu
	$of_page = add_menu_page(UNISPHERE_THEMENAME . ' theme options', UNISPHERE_THEMENAME, 'edit_theme_options', 'options-framework', 'optionsframework_page', UNISPHERE_ADMIN_IMAGES . '/theme_icon.png');

	// Theme options sub-menu
	add_submenu_page('options-framework', UNISPHERE_THEMENAME . ' theme options', 'Theme options', 'edit_theme_options', 'options-framework', 'optionsframework_page');

	// Adds actions to hook in the required css and javascript
	add_action("admin_print_styles-$of_page",'optionsframework_load_styles');
	add_action("admin_print_scripts-$of_page", 'optionsframework_load_scripts');

	// Image sizes settings sub-menu
	$of_page = add_submenu_page('options-framework', UNISPHERE_THEMENAME . ' image sizes', 'Image sizes', 'edit_theme_options', 'options-framework-image-sizes', 'optionsframework_imagesizes_page');

	// Adds actions to hook in the required css and javascript
	add_action("admin_print_styles-$of_page",'optionsframework_load_styles');
	add_action("admin_print_scripts-$of_page", 'optionsframework_load_scripts');

	// Skins manager sub-menu
	$of_page = add_submenu_page('options-framework', UNISPHERE_THEMENAME . ' skins manager', 'Skins manager', 'edit_theme_options', 'options-framework-skins-manager', 'optionsframework_skinsmanager_page');

	// Adds actions to hook in the required css and javascript
	add_action("admin_print_styles-$of_page",'optionsframework_load_styles');
	add_action("admin_print_scripts-$of_page", 'optionsframework_load_scripts');

	// Fonts manager sub-menu
	$of_page = add_submenu_page('options-framework', UNISPHERE_THEMENAME . ' fonts manager', 'Fonts manager', 'edit_theme_options', 'options-framework-fonts-manager', 'optionsframework_fontsmanager_page');

	// Adds actions to hook in the required css and javascript
	add_action("admin_print_styles-$of_page",'optionsframework_load_styles');
	add_action("admin_print_scripts-$of_page", 'optionsframework_load_scripts');

	// Sidebar manager sub-menu
	$of_page = add_submenu_page('options-framework', UNISPHERE_THEMENAME . ' sidebar manager', 'Sidebars manager', 'edit_theme_options', 'options-framework-sidebar-manager', 'optionsframework_sidebarmanager_page');

	// Adds actions to hook in the required css and javascript
	add_action("admin_print_styles-$of_page",'optionsframework_load_styles');
	add_action("admin_print_scripts-$of_page", 'optionsframework_load_scripts');
}
}

/* Loads the CSS */

function optionsframework_load_styles() {
	wp_enqueue_style('admin-style', OPTIONS_FRAMEWORK_DIRECTORY.'css/admin-style.css');
	wp_enqueue_style('color-picker', OPTIONS_FRAMEWORK_DIRECTORY.'css/colorpicker.css');
	wp_enqueue_style('jq-ui', 'http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css');
}	

/* Loads the javascript */

function optionsframework_load_scripts() {

	// Inline scripts from options-interface.php
	add_action('admin_head', 'of_admin_head');
	
	// Enqueued scripts
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('color-picker', OPTIONS_FRAMEWORK_DIRECTORY.'js/colorpicker.js', array('jquery'));
	wp_enqueue_script('options-custom', OPTIONS_FRAMEWORK_DIRECTORY.'js/options-custom.js', array('jquery'));
	wp_enqueue_script('jq-ui', 'http://code.jquery.com/ui/1.9.1/jquery-ui.js', array('jquery'));
}

function of_admin_head() {

	// Hook to add custom scripts
	do_action( 'optionsframework_custom_scripts' );
}

/* 
 * Builds out the options panel.
 *
 * If we were using the Settings API as it was likely intended we would use
 * do_settings_sections here.  But as we don't want the settings wrapped in a table,
 * we'll call our own custom optionsframework_fields.  See options-interface.php
 * for specifics on how each individual field is generated.
 *
 * Nonces are provided using the settings_fields()
 *
 */

function optionsframework_page() {
	global $current_location;
	$current_location = THEME_OPTIONS_PAGE;
	optionsframework_buildpage();
}

function optionsframework_sidebarmanager_page() {
	global $current_location;
	$current_location = SIDEBAR_MANAGER_PAGE;
	optionsframework_buildpage();
}

function optionsframework_skinsmanager_page() {
	global $current_location;
	$current_location = SKINS_MANAGER_PAGE;
	optionsframework_buildpage();
}

function optionsframework_fontsmanager_page() {
	global $current_location;
	$current_location = FONTS_MANAGER_PAGE;
	optionsframework_buildpage();
}

function optionsframework_imagesizes_page() {
	global $current_location;
	$current_location = IMAGE_SIZES_PAGE;
	optionsframework_buildpage();
}

if ( !function_exists( 'optionsframework_buildpage' ) ) {
function optionsframework_buildpage() {
	global $current_location;
	global $current_user;
	get_currentuserinfo();

	// Save the uploaded cufon font to the "fonts" folder
	if ($current_location == FONTS_MANAGER_PAGE) {

		// Check if there's a new cufon font upload
		$cufon_upload_file = get_option('unisphere_cufon_upload_' . $current_user->ID);
		if( !empty( $cufon_upload_file ) ) {
			if( unisphere_check_credentials( $current_location ) ) {
				global $wp_filesystem;
				// If the fonts folder doesn't exist, create it
				if ( !$wp_filesystem->mkdir(UNISPHERE_FONTS, FS_CHMOD_DIR) && !$wp_filesystem->is_dir(UNISPHERE_FONTS) ) { // Only check to see if the Dir exists upon creation failure. Less I/O this way.
					// let's mitigate some damage if there aren't enough permissions
					delete_option('unisphere_cufon_upload_' . $current_user->ID);
					wp_die( "Could not create directory " . UNISPHERE_FONTS . "\n" );
				}

				if( $wp_filesystem->copy($cufon_upload_file['source_path'], $cufon_upload_file['dest_path'], true, FS_CHMOD_FILE ) ) {
					delete_transient('cufon_fonts');
					delete_option('unisphere_cufon_upload_' . $current_user->ID);
					$cufon_upload_file = null;
					add_settings_error( 'options-framework', 'cufon_upload', __( 'New Cufon font added.', 'optionsframework' ), 'updated fade' );
				} else {
					// let's mitigate some damage if there aren't enough permissions
					delete_option('unisphere_cufon_upload_' . $current_user->ID);
					add_settings_error( 'options-framework', 'cufon_upload', __( 'An error occurred when adding the new Cufon font.', 'optionsframework' ), 'error' );
				}
			}
		}
	}

	// No page-load operations to be made, so show the options panel
	if( empty( $cufon_upload_file ) ) :

		$return = optionsframework_fields( $current_location );
		settings_errors();	?>
	    
		<div class="wrap">
		    <?php screen_icon( 'themes' ); ?>
		    <h2><?php echo $current_location; ?></h2>
		    <h2 class="nav-tab-wrapper">
		        <?php echo $return[1]; ?>
		    </h2>
		    
		    <div class="metabox-holder">
			    <div id="optionsframework" class="postbox optionsframework-<?php echo sanitize_title( $current_location ); ?>">
					<form action="options.php" method="post">
						<?php settings_fields('optionsframework'); ?>
						<input type="hidden" name="options_framework_page" value="<?php echo $current_location; ?>" />

						<?php echo $return[0]; /* Settings */  ?>
				        
				        <div id="optionsframework-submit">
							<input type="submit" class="button-primary" name="update" value="<?php esc_attr_e( 'Save Options' ); ?>" />
							<?php if( $current_location != SIDEBAR_MANAGER_PAGE ) : ?>
				            <input type="submit" class="reset-button button-secondary" name="reset" value="<?php esc_attr_e( 'Restore Defaults' ); ?>" onclick="return confirm( '<?php print esc_js( __( 'Click OK to reset. Any theme settings will be lost!', 'optionsframework' ) ); ?>' );" />
				            <?php endif; ?>
				            <div class="clear"></div>
						</div>
					</form>
				</div> <!-- / #container -->
			</div>
		</div> <!-- / .wrap -->

<?php 
	endif;
}
}


function unisphere_check_credentials( $current_location ) {
	if( $current_location != FONTS_MANAGER_PAGE )
		return false;

	$page_url = 'options-framework-fonts-manager';

	$help  = '<div id="message" class="updated below-h2" style="margin-left: 0;"><p><strong>Important Note</strong> (please read)</p></div>';
	$help .= '<p>This particular section of the theme admin panel needs to upload and move Cufon font files but your server configuration doesn\'t allow direct access to the filesystem.</p>';
	$help .= '<p>WordPress has detected this and requires FTP credentials. In order to procede you can either provide the FTP credentials everytime you want to change any setting in this section or provide your FTP details in your <strong>wp-config.php</strong> file just one time.</p>';
	$help .= '<br /><p><strong>These settings work on most servers:</strong><br /><small>(adjust FTP_USER, FTP_PASS and FTP_HOST according to your server config)</small></p>';
	$help .= "<pre>define('FS_METHOD', 'ftpext');\n";
	$help .= "define('FTP_USER', 'username');\n";
	$help .= "define('FTP_PASS', 'password');\n";
	$help .= "define('FTP_HOST', 'ftp.example.org');</pre>";
	$help .= "<br /><p>Sometimes WordPress requires some extra information. If the above settings aren't enough try adding the following also:<br /><small>(adjust FTP_BASE according to your server config)</small></p>";
	$help .= "<pre>define('FTP_BASE', '/path/to/wordpress/');\n";
	$help .= "define('FTP_SSL', false);</pre>";

	@ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
    @set_time_limit( 0 );

	// okay, let's see about getting credentials
	$url = wp_nonce_url('admin.php?page=' . $page_url);
	if (false === ($creds = request_filesystem_credentials($url, '', false, false, null) ) ) {
	
		// if we get here, then we don't have credentials yet,
		// but have just produced a form for the user to fill in, 
		// so stop processing for now
		echo $help;
		return false;
	}
		
	// now we have some credentials, try to get the wp_filesystem running
	if ( ! WP_Filesystem($creds) ) {
		// our credentials were no good, ask the user for them again
		request_filesystem_credentials($url, '', true, false, null);
		echo $help;
		return false;
	}
	
	return true;
}


/** 
 * Validate Options.
 *
 * This runs after the submit/reset button has been clicked and
 * validates the inputs.
 */
function optionsframework_validate( $input ) {
	$location = $_POST['options_framework_page'];
	global $current_user;
	get_currentuserinfo();

	/*
	 * Create a new sidebar
	 */

	if ( isset( $_POST['sidebar_create'] ) ) {
		$new_sidebar_name = $input['sidebar_create_text'];

		// Check for an existing sidebar with the same name
		$sidebar_exists = false;
		if( !empty( $input['sidebar_list'] ) ) {
			foreach($input['sidebar_list'] as $elementKey => $element) {
			    foreach($element as $valueKey => $value) {
			        if($valueKey == 'name' && $value == $new_sidebar_name)
			            $sidebar_exists = true;
			    }
			}
		}

		if( trim( $new_sidebar_name ) != '' && !$sidebar_exists ) 
			$input['sidebar_list'][] = Array( 'name' => $new_sidebar_name, 'id' => sanitize_title( $new_sidebar_name ) );
		else {
			$_POST['sidebar_create'] = null;
			$_POST['sidebar_exists'] = 'true';
		}
		
		$_POST['update'] = 'true';
	}

	/*
	 * Delete a sidebar
	 */

	if ( isset( $_POST['sidebar_delete'] ) ) {
		foreach($input['sidebar_list'] as $elementKey => $element) {
		    foreach($element as $valueKey => $value) {
		        if($valueKey == 'id' && $value == $_POST['sidebar_delete']) {
		            unset($input['sidebar_list'][$elementKey]);
		        } 
		    }
		}
		$_POST['update'] = 'true';
	}

	/*
	 * Update Google Web Fonts from the remote list
	 */
	if( isset( $_POST['fonts_update_google'] ) ) {
		getGoogleFonts(true);
		$_POST['update'] = 'true';
	}

	/*
	 * Upload a new cufon font script
	 */

	if ( isset( $_POST['cufon_upload'] ) ) {
		$source_path = truepath(ABSPATH . '/wp-content/uploads') . substr( $input['cufon_upload_file'], strpos( $input['cufon_upload_file'], '/wp-content/uploads/' ) + 19 );
		$dest_path = truepath( UNISPHERE_FONTS . '/' . end( explode( '/', str_replace('\\', '/', $input['cufon_upload_file']))));

		$cufon_upload_data = array('source_path' => $source_path, 'dest_path' => $dest_path);

		// Flag this cufon script file to be copied to the "fonts" folder on the next page load
		update_option('unisphere_cufon_upload_' . $current_user->ID, $cufon_upload_data);

		$_POST['update'] = 'true';
	}

	/*
	 * Restore Defaults.
	 *
	 * In the event that the user clicked the "Restore Defaults"
	 * button, the options defined in the theme's options.php
	 * file will be added to the option for the active theme.
	 */
	 
	if ( isset( $_POST['reset'] ) ) {
		// Any changes to the Skins manager or Fonts manager pages deletes the cached data for the fonts
		if( $location == SKINS_MANAGER_PAGE || $location == FONTS_MANAGER_PAGE ) {
			delete_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_google_fonts' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_cufon_fonts' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-cufon_selectors' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-skin-css' );
		}
		// If the portfolio detail page permalink is changed, flag it for WordPress permalink rewrite
		if( $location == THEME_OPTIONS_PAGE ) {
			update_option('flush_rewrite_rules', '1');
		}
		add_settings_error( 'options-framework', 'restore_defaults', __( 'Default options restored.', 'optionsframework' ), 'updated fade' );
		return of_get_default_values( $location );
	}

	/*
	 * Update Settings.
	 */
	 
	if ( isset( $_POST['update'] ) ) {
		$clean = array();

		// Let's see if the user is importing theme settings
		if ( isset( $input['import_settings'] ) && $input['import_settings'] != '' ) {
			$settings_to_import = base64_decode( $input['import_settings'] );
			$settings_to_import = @unserialize($settings_to_import);

			if( $settings_to_import )
				return $settings_to_import;
		}

		$options = optionsframework_options();

		// If the portfolio detail page permalink is changed, flag it for WordPress permalink rewrite
		if( $location == THEME_OPTIONS_PAGE ) {
			update_option('flush_rewrite_rules', '1');
		}

		// Any changes to the Skins manager or Fonts manager pages deletes the cached data for the fonts
		if( $location == SKINS_MANAGER_PAGE || $location == FONTS_MANAGER_PAGE ) {
			delete_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_google_fonts' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_cufon_fonts' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-cufon_selectors' );
			delete_transient( UNISPHERE_THEMEOPTIONS . '-skin-css' );
		}

		foreach ( $options as $option ) {

			if ( ! isset( $option['id'] ) ) {
				continue;
			}

			if ( ! isset( $option['type'] ) ) {
				continue;
			}

			$id = preg_replace( '/[^a-zA-Z0-9._\-]/', '', strtolower( $option['id'] ) );
			
			if ( isset( $option['location'] ) && $option['location'] == $location ) {

				// Set checkbox to false if it wasn't sent in the $_POST
				if ( 'checkbox' == $option['type'] && ! isset( $input[$id] ) ) {
					$input[$id] = '0'; $clean[$id] = '0';
				}

				// Set each item in the multicheck to false if it wasn't sent in the $_POST
				if ( 'multicheck' == $option['type'] && ! isset( $input[$id] ) ) {
					foreach ( $option['options'] as $key => $value ) {
						$input[$id][$key] = '0';
					}
				}

				// For a value to be submitted to database it must pass through a sanitization filter
				if ( has_filter( 'of_sanitize_' . $option['type'] ) && !empty( $input[$id] ) ) {
					$clean[$id] = apply_filters( 'of_sanitize_' . $option['type'], $input[$id], $option );
				}

				if ( $option['type'] == 'text' && $input[$id] == '0' ) {
					$clean[$id] = '0';
				}

				if ( $id == 'sidebar_list' ) {
					$clean[$id] = $input[$id];
				}
			} else {
				// Keep the other theme admin pages fields intact
				$option_value = of_get_option( $option['id'] );
				if( $option_value != '') {
					$clean[$id] = of_get_option( $option['id'] );
				}
			}

			// Always clean the import and export settings field as we don't want it to be stored in the database
			if( $id == "export_settings" || $id == "import_settings" ) {
				unset($clean[$id]);
			}
		}

		if( isset( $_POST['sidebar_create'] ) )
			add_settings_error( 'options-framework', 'sidebar_create', __( 'New custom sidebar created.', 'optionsframework' ), 'updated fade' );

		if( isset( $_POST['sidebar_delete'] ) )
			add_settings_error( 'options-framework', 'sidebar_delete', __( 'Custom sidebar deleted.', 'optionsframework' ), 'updated fade' );

		if( isset( $_POST['sidebar_exists'] ) )
			add_settings_error( 'options-framework', 'sidebar_exists', __( 'Error: there\'s already a sidebar with that name or the field is empty.', 'optionsframework' ), 'error' );

		if( isset( $_POST['fonts_update_google'] ) )
			add_settings_error( 'options-framework', 'fonts_update_google', __( 'Google web fonts list updated successfully.', 'optionsframework' ), 'updated fade' );

		if( !isset( $_POST['sidebar_create'] ) && !isset( $_POST['sidebar_delete'] ) && !isset( $_POST['sidebar_exists'] ) && !isset( $_POST['fonts_update_google'] ) )
			add_settings_error( 'options-framework', 'save_options', __( 'Options saved.', 'optionsframework' ), 'updated fade' );

		return $clean;
	}

	/*
	 * Request Not Recognized.
	 */
	
	return of_get_default_values( $location );
}

/**
 * Format Configuration Array.
 *
 * Get an array of all default values as set in
 * options.php. The 'id','std' and 'type' keys need
 * to be defined in the configuration array. In the
 * event that these keys are not present the option
 * will not be included in this function's output.
 *
 * @return    array     Rey-keyed options configuration array.
 *
 * @access    private
 */
 
function of_get_default_values( $location ) {
	$output = array();
	$config = optionsframework_options();
	foreach ( (array) $config as $option ) {
		if ( ! isset( $option['id'] ) ) {
			continue;
		}
		if ( ! isset( $option['std'] ) ) {
			continue;
		}
		if ( ! isset( $option['type'] ) ) {
			continue;
		}
		if ( isset( $option['location'] ) && $option['location'] == $location ) {
			if ( has_filter( 'of_sanitize_' . $option['type'] ) ) {
				$output[$option['id']] = apply_filters( 'of_sanitize_' . $option['type'], $option['std'], $option );
			} 
		} else {
			// Keep the other theme admin pages fields intact
			$output[$option['id']] = of_get_option( $option['id'] );
		}
	}
	return $output;
}

/**
 * Add Theme Options menu item to Admin Bar.
 */
 
add_action( 'wp_before_admin_bar_render', 'optionsframework_adminbar' );

function optionsframework_adminbar() {
	
	global $wp_admin_bar;
	
	$wp_admin_bar->add_menu( array(
		'parent' => 'appearance',
		'id' => 'of_theme_options',
		'title' => __( 'Theme Options', 'optionsframework' ),
		'href' => admin_url( 'themes.php?page=options-framework' )
  ));
}

if ( ! function_exists( 'of_get_option' ) ) {

	/**
	 * Get Option.
	 *
	 * Helper function to return the theme option value.
	 * If no value has been saved, it returns $default.
	 * Needed because options are saved as serialized strings.
	 */
	 
	function of_get_option( $name, $default = false ) {
		$config = get_option( 'optionsframework' );

		if ( ! isset( $config['id'] ) ) {
			return $default;
		}

		$options = get_option( $config['id'] );

		if ( isset( $options[$name] ) ) {
			return $options[$name];
		}

		return $default;
	}
}

if ( ! function_exists( 'of_update_option' ) ) {
	function of_update_option( $name, $value, $array = '' ) {
		$config = get_option( 'optionsframework' );

		if ( ! isset( $config['id'] ) ) {
			return false;
		}

		$options = get_option( $config['id'] );

		if( $array != '' ) {
			$options[$name][$array] = $value;
		} else {
			$options[$name] = $value;
		}

		if( $value == '' ) {
			unset($options[$name]);
		}

		$optionsframework_settings = get_option('optionsframework' );
		
		// Updates the unique option id in the database if it has changed
		optionsframework_option_name();
		
		// Gets the unique id, returning a default if it isn't defined
		if ( isset($optionsframework_settings['id']) ) {
			$option_name = $optionsframework_settings['id'];
		}
		else {
			$option_name = 'optionsframework';
		}
		
		unregister_setting( 'optionsframework', $option_name, 'optionsframework_validate' );
		update_option( $config['id'], $options );
		unregister_setting( 'optionsframework', $option_name, 'optionsframework_validate' );

		$options = get_option( $config['id'] );
	}
}


/**
 * This function is to replace PHP's extremely buggy realpath().
 */
function truepath($path){

	global $wp_filesystem;
	if($wp_filesystem && $wp_filesystem->method == 'ftpext') {
		$ftp_abspath = $wp_filesystem->abspath();
		if($ftp_abspath == '/')
			$ftp_abspath = '/wp-content';
		if( !startsWith( $path, $ftp_abspath ) && !endsWith( $path, '.tmp' ) && !endsWith( $path, '.zip' ) ) {
	    	$path_parts = explode($ftp_abspath, $path);
			$path = $ftp_abspath . $path_parts[1];
	    }
	}

    // whether $path is unix or not
    $unipath=strlen($path)==0 || $path{0}!='/';
    // attempts to detect if path is relative in which case, add cwd
    if(strpos($path,':')===false && $unipath)
        $path=getcwd().DIRECTORY_SEPARATOR.$path;
    // resolve path parts (single dot, double dot and double delimiters)
    $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
    $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
    $absolutes = array();
    foreach ($parts as $part) {
        if ('.'  == $part) continue;
        if ('..' == $part) {
            array_pop($absolutes);
        } else {
            $absolutes[] = $part;
        }
    }
    $path=implode(DIRECTORY_SEPARATOR, $absolutes);
    // resolve any symlinks
    if( function_exists('readlink') ) {
    	if(file_exists($path) && linkinfo($path)>0)$path=readlink($path);
    }
    // put initial separator that could have been lost
    $path=!$unipath ? '/'.$path : $path;
    return $path;
}
