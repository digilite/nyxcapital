<?php

/**
 * Generates the options fields that are used in the form.
 */

function optionsframework_fields( $location ) {

	global $allowedtags;
	$optionsframework_settings = get_option('optionsframework');
	
	// Gets the unique option id
	if (isset($optionsframework_settings['id'])) {
		$option_name = $optionsframework_settings['id'];
	}
	else {
		$option_name = 'optionsframework';
	};

	$settings = get_option($option_name);
    $options = optionsframework_options();
        
    $counter = 0;
	$menu = '';
	$output = '';
	
	foreach ($options as $value) {
	   
		if( isset( $value['location'] ) && $value['location'] == $location ) {
			
			$counter++;
			$val = '';
			$select_value = '';
			$checked = '';
			
			// Wrap all options
			if ( ($value['type'] != "heading") && ($value['type'] != "info") && ($value['type'] != "toggle-start") && ($value['type'] != "toggle-end" ) ) {

				// Keep all ids lowercase with no spaces
				$value['id'] = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($value['id']) );

				$id = 'section-' . $value['id'];

				$class = 'section ';
				if ( isset( $value['type'] ) ) {
					$class .= ' section-' . $value['type'];
				}
				if ( isset( $value['class'] ) ) {
					$class .= ' ' . $value['class'];
				}

				$output .= '<div id="' . esc_attr( $id ) .'" class="' . esc_attr( $class ) . '">'."\n";
				$output .= '<h4 class="heading">' . esc_html( $value['name'] ) . '</h4>' . "\n";
				$output .= '<div class="option">' . "\n" . '<div class="controls">' . "\n";
			 }
			
			// Set default value to $val
			if ( isset( $value['std']) ) {
				$val = $value['std'];
			}
			
			// If the option is already saved, override $val
			if ( ($value['type'] != 'heading') && ($value['type'] != 'info') && ($value['type'] != "toggle-start") && ($value['type'] != "toggle-end") ) {
				if ( isset($settings[($value['id'])]) ) {
					$val = $settings[($value['id'])];
					// Striping slashes of non-array options
					if (!is_array($val)) {
						$val = stripslashes($val);
					}
				}
			}
			
			// If there is a description save it for labels
			$explain_value = '';
			if ( isset( $value['desc'] ) ) {
				$explain_value = $value['desc'];
			}
			                                
			switch ( $value['type'] ) {
			
			// Basic text input
			case 'text':
				$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="of-input" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" type="text" value="' . esc_attr( $val ) . '" />';
			break;
			
			// Textarea
			case 'textarea':
				$cols = '8';
				$ta_value = '';
				
				if(isset($value['options'])){
					$ta_options = $value['options'];
					if(isset($ta_options['cols'])){
						$cols = $ta_options['cols'];
					} else { $cols = '8'; }
				}
				
				$val = stripslashes( $val );
				
				$output .= '<textarea id="' . esc_attr( $value['id'] ) . '" class="of-input" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" cols="'. esc_attr( $cols ) . '" rows="8">' . esc_textarea( $val ) . '</textarea>';
			break;
			
			// Select Box
			case ($value['type'] == 'select'):
				$output .= '<select class="of-input" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '">';
				
				foreach ($value['options'] as $key => $option ) {
					$selected = '';
					 if( $val != '' ) {
						 if ( $val == $key) { $selected = ' selected="selected"';} 
				     }
					 $output .= '<option'. $selected .' value="' . esc_attr( $key ) . '">' . esc_html( $option ) . '</option>';
				 } 
				 $output .= '</select>';
			break;

			
			// Radio Box
			case "radio":
				$name = $option_name .'['. $value['id'] .']';
				foreach ($value['options'] as $key => $option) {
					$id = $option_name . '-' . $value['id'] .'-'. $key;
					$output .= '<input class="of-input of-radio" type="radio" name="' . esc_attr( $name ) . '" id="' . esc_attr( $id ) . '" value="'. esc_attr( $key ) . '" '. checked( $val, $key, false) .' /><label for="' . esc_attr( $id ) . '">' . esc_html( $option ) . '</label>';
				}
			break;
			
			// Image Selectors
			case "images":
				$name = $option_name .'['. $value['id'] .']';
				foreach ( $value['options'] as $key => $option ) {
					$selected = '';
					$checked = '';
					if ( $val != '' ) {
						if ( $val == $key ) {
							$selected = ' of-radio-img-selected';
							$checked = ' checked="checked"';
						}
					}
					$output .= '<input type="radio" id="' . esc_attr( $value['id'] .'_'. $key) . '" class="of-radio-img-radio" value="' . esc_attr( $key ) . '" name="' . esc_attr( $name ) . '" '. $checked .' />';
					$output .= '<div class="of-radio-img-label">' . esc_html( $key ) . '</div>';
					$output .= '<img src="' . esc_url( $option ) . '" alt="' . $option .'" class="of-radio-img-img' . $selected .'" onclick="document.getElementById(\''. esc_attr($value['id'] .'_'. $key) .'\').checked=true;" />';
				}
			break;
			
			// Checkbox
			case "checkbox":
				$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="checkbox of-input" type="checkbox" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" '. checked( $val, 1, false) .' />';
				$output .= '<label class="explain" for="' . esc_attr( $value['id'] ) . '">' . wp_kses( $explain_value, $allowedtags) . '</label>';
			break;
			
			// Multicheck
			case "multicheck":
				foreach ($value['options'] as $key => $option) {
					$checked = '';
					$label = $option;
					$option = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($key));

					$id = $option_name . '-' . $value['id'] . '-'. $option;
					$name = $option_name . '[' . $value['id'] . '][' . $option .']';

				    if ( isset($val[$option]) ) {
						$checked = checked($val[$option], 1, false);
					}

					$output .= '<input id="' . esc_attr( $id ) . '" class="checkbox of-input" type="checkbox" name="' . esc_attr( $name ) . '" ' . $checked . ' /><label for="' . esc_attr( $id ) . '">' . esc_html( $label ) . '</label>';
				}
			break;
			
			// Color picker
			case "color":
				$output .= '<div id="' . esc_attr( $value['id'] . '_picker' ) . '" class="colorSelector"><div style="' . esc_attr( 'background-color:' . $val ) . '"></div></div>';
				$output .= '<input class="of-color" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '" type="text" value="' . esc_attr( $val ) . '" />';
				$output .= '<input type="checkbox" class="hidden of-color-checkbox" name="' . esc_attr( $option_name . '[' . $value['id'] . '_enable_hsl]' ) . '" id="' . esc_attr( $value['id'] . '_enable_hsl' ) . '" checked="checked" />';
			break; 
			
			// Uploader
			case "upload":
				$output .= optionsframework_medialibrary_uploader( $value['id'], $val, null ); // New AJAX Uploader using Media Library	
			break;
			
			// Typography
			case 'typography':	
			
				$typography_stored = $val;
				if( !isset( $typography_stored['uppercase'] ) )
					$typography_stored['uppercase'] = 'off';

				// Font Face
				$output .= '<select class="of-typography of-typography-face" name="' . esc_attr( $option_name . '[' . $value['id'] . '][face]' ) . '" id="' . esc_attr( $value['id'] . '_face' ) . '">';

				if( $value['advanced'] == true ) {
					// Option to inherit fallback font family
					if( $typography_stored['face'] == '' ) {
						$output .= '<option selected="selected" value="">Use fallback font family</option>';					
				    } else {
				    	$output .= '<option value="">Use fallback font family</option>';
				    }

    				$output .= '<optgroup label="Default font family stacks" style="margin-top: 15px;">';
			    }
				
				$faces = of_recognized_font_faces();
				foreach ( $faces as $key => $face ) {
					$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $typography_stored['face'], $key, false ) . '>' . esc_html( $face ) . '</option>';
				}			

				if( $value['advanced'] == true ) {
					$output .= '</optgroup>';

					if( $value['cufon_fonts'] != false ) {
						// Cufon fonts
						$output .= '<optgroup label="Cufon fonts" style="margin-top: 15px;">';
						foreach( $value['cufon_fonts'] as $cufon_font) {
					     	foreach( $cufon_font as $key => $option) {
					     		if( $key == 'value' )
						        	$select_value = $option;
						        else
						        	$select_text = $option;
							}
							$selected = '';
							if( $typography_stored['face'] != '' ) {
								if ( $typography_stored['face'] == esc_attr( $select_value ) ) { $selected = ' selected="selected"';} 
							}
							$output .= '<option'. $selected .' value="' . esc_attr( $select_value ) . '">' . esc_html( $select_text ) . '</option>';
					    }
						$output .= '</optgroup>';
					}

					// Google web fonts
					$output .= '<optgroup label="Google web fonts" style="margin-top: 15px;">';
					foreach( $value['google_fonts'] as $google_font) {
				     	foreach( $google_font as $key => $option) {
				     		if( $key == 'value' )
					        	$select_value = $option;
					        else
					        	$select_text = $option;
						}
						$selected = '';
						if( $typography_stored['face'] != '' ) {
							if ( $typography_stored['face'] == esc_attr( $select_value ) ) { $selected = ' selected="selected"';} 
						}
						$output .= '<option'. $selected .' value="' . esc_attr( $select_value ) . '">' . esc_html( $select_text ) . '</option>';
				    }
					$output .= '</optgroup>';
				}
				
				$output .= '</select>';	

				// Font Size
				if( $value['size'] != false ) {					
					$output .= '<label class="typography-size">Font size:</label><select class="of-typography of-typography-size" name="' . esc_attr( $option_name . '[' . $value['id'] . '][size]' ) . '" id="' . esc_attr( $value['id'] . '_size' ) . '">';
					for ($i = 8; $i < 71; $i++) { 
						$size = $i . 'px';
						$output .= '<option value="' . esc_attr( $size ) . '" ' . selected( $typography_stored['size'], $size, false ) . '>' . esc_html( $size ) . '</option>';
					}
					$output .= '</select>';
				}

				// Line Height
				if( $value['lineheight'] != false ) {					
					$output .= '<label class="typography-lineheight">Line height:</label><select class="of-typography of-typography-lineheight" name="' . esc_attr( $option_name . '[' . $value['id'] . '][lineheight]' ) . '" id="' . esc_attr( $value['id'] . '_lineheight' ) . '">';
					for ($i = 8; $i < 71; $i++) { 
						$lineheight = $i . 'px';
						$output .= '<option value="' . esc_attr( $lineheight ) . '" ' . selected( $typography_stored['lineheight'], $lineheight, false ) . '>' . esc_html( $lineheight ) . '</option>';
					}
					$output .= '</select>';
				}

				// Font Style
				if( $value['fontstyle'] != false ) {					
					$output .= '<label class="typography-fontstyle">Font style:</label><select class="of-typography of-typography-fontstyle" name="' . esc_attr( $option_name . '[' . $value['id'] . '][fontstyle]' ) . '" id="' . esc_attr( $value['id'] . '_fontstyle' ) . '">';
					$font_styles_defaults = array(  '100' => 'Ultra-Light 100',
									'100italic' => 'Ultra-Light 100 Italic',
									'200' => 'Light 200',
									'200italic' => 'Light 200 Italic',
									'300' => 'Book 300',
									'300italic' => 'Book 300 Italic',
									'400' => 'Normal 400',
									'400italic' => 'Normal 400 Italic',
									'500' => 'Medium 500',
									'500italic' => 'Medium 500 Italic',
									'600' => 'Semi-Bold 600',
									'600italic' => 'Semi-Bold 600 Italic',
									'700' => 'Bold 700',
									'700italic' => 'Bold 700 Italic',
									'800' => 'Extra-Bold 800',
									'800italic' => 'Extra-Bold 800 Italic',
									'900' => 'Ultra-Bold 900',
									'900italic' => 'Ultra-Bold 900 Italic' );

					foreach ($font_styles_defaults as $key => $option) {
						$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $typography_stored['fontstyle'], $key, false ) . '>' . esc_html( $option ) . '</option>';
					}
					$output .= '</select>';
				}

				// Uppercase
				if( $value['uppercase'] != false ) {
					$output .= '<label class="typography-uppercase">Uppercase:</label>';
					$output .= '<input type="checkbox" class="of-typography of-typography-uppercase" name="' . esc_attr( $option_name . '[' . $value['id'] . '][uppercase]' ) . '" id="' . esc_attr( $value['id'] . '_uppercase' ) . '" '. checked( $typography_stored['uppercase'], 'on', false) .' />';
				}

			break;

			// Background
			case 'background':
				
				$background = $val;

				if( !isset( $background['repeat'] )) $background['repeat'] = 'repeat';
				if( !isset( $background['position'] )) $background['position'] = 'top center';
				if( !isset( $background['attachment'] )) $background['attachment'] = 'scroll';
				
				// Background Image - New AJAX Uploader using Media Library
				if (!isset($background['image'])) {
					$background['image'] = '';
				}
				
				$output .= optionsframework_medialibrary_uploader( $value['id'], $background['image'], null, '',0,'image');
				$class = 'of-background-properties';
				if ( '' == $background['image'] ) {
					$class .= ' hide';
				}
				$output .= '<div class="' . esc_attr( $class ) . '">';
				
				// Background Repeat
				$output .= '<select class="of-background of-background-repeat" name="' . esc_attr( $option_name . '[' . $value['id'] . '][repeat]'  ) . '" id="' . esc_attr( $value['id'] . '_repeat' ) . '">';
				$repeats = of_recognized_background_repeat();
				
				foreach ($repeats as $key => $repeat) {
					$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $background['repeat'], $key, false ) . '>'. esc_html( $repeat ) . '</option>';
				}
				$output .= '</select>';
				
				// Background Position
				$output .= '<select class="of-background of-background-position" name="' . esc_attr( $option_name . '[' . $value['id'] . '][position]' ) . '" id="' . esc_attr( $value['id'] . '_position' ) . '">';
				$positions = of_recognized_background_position();
				
				foreach ($positions as $key=>$position) {
					$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $background['position'], $key, false ) . '>'. esc_html( $position ) . '</option>';
				}
				$output .= '</select>';
				
				// Background Attachment
				$output .= '<select class="of-background of-background-attachment" name="' . esc_attr( $option_name . '[' . $value['id'] . '][attachment]' ) . '" id="' . esc_attr( $value['id'] . '_attachment' ) . '">';
				$attachments = of_recognized_background_attachment();
				
				foreach ($attachments as $key => $attachment) {
					$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $background['attachment'], $key, false ) . '>' . esc_html( $attachment ) . '</option>';
				}
				$output .= '</select>';
				$output .= '</div>';
			
			break;  
			
			// Info
			case "info":
				$class = 'section';
				if ( isset( $value['type'] ) ) {
					$class .= ' section-' . $value['type'];
				}
				if ( isset( $value['class'] ) ) {
					$class .= ' ' . $value['class'];
				}

				$output .= '<div class="' . esc_attr( $class ) . '">' . "\n";
				if ( isset($value['name']) ) {
					$output .= '<h4 class="heading">' . esc_html( $value['name'] ) . '</h4>' . "\n";
				}
				if ( $value['desc'] ) {
					$output .= apply_filters('of_sanitize_info', $value['desc'] ) . "\n";
				}
				$output .= '<div class="clear"></div></div>' . "\n";
			break;                       
			
			// toggle start
			case "toggle-start":
				$output .= '<div class="toggle-container">' . "\n";
				$output .= '<a class="toggle" href="javascript:void(0);"><span class="toggle-sign">+</span><span class="toggle-title">' . esc_html( $value['name'] ) . '</span></a>' . "\n";
				$output .= '<div class="toggle-content" style="display: none;">' . "\n";
				break;

			// toggle end
			case "toggle-end":
				$output .= '</div></div>' . "\n";
				break;

			case "hsl":
				$output .= '<div id="hsl_wrapper">';
				$output .= '<div id="slider-h"></div>';
				$output .= '<div id="slider-s"></div>';
				$output .= '<div id="slider-l"></div>';
				$output .= '</div>';
				break;

			// create sidebar
			case "sidebar_create":
				$output .= '<input id="' . esc_attr( $value['id'] ) . '_text" class="of-input" name="' . esc_attr( $option_name . '[' . $value['id'] . '_text]' ) . '" type="text" value="' . esc_attr( $val ) . '" />';
				$output .= '<input type="submit" class="button2" name="sidebar_create" value="Create" />';
				break;

			// list sidebars
			case "sidebar_list":
				$i = 0;
				if( !empty( $settings['sidebar_list'] ) ) {
					$output .= '<table class="wp-list-table widefat fixed">';
					$output .= '<thead>';
					$output .= '<tr><th>Name</th><th>CSS Class</th><th class="manage-column column-cb check-column">&nbsp;</th></tr>';
					$output .= '</thead>';
					$output .= '<tbody>';
					foreach ($settings['sidebar_list'] as $item) {
						foreach ($item as $key => $option) {
							if( $key == 'name' ) {
								$name_sidebar = esc_attr( $option );
								$name_hidden_field = '<input name="' . esc_attr( $option_name . '[sidebar_list][' . $i . '][name]' ) . '" type="hidden" value="' . esc_attr( $option ) . '" />';
							}

							if( $key == 'id' ) {
								$id_sidebar = esc_attr( $option );
								$id_hidden_field = '<input name="' . esc_attr( $option_name . '[sidebar_list][' . $i . '][id]' ) . '" type="hidden" value="' . esc_attr( $option ) . '" />';
							}
						}
						$output .= '<tr class="' . ($i % 2 == 0 ? 'alternate' : '') . '"><td style="padding-top: 9px;">' . $name_sidebar . $id_hidden_field . '</td><td style="padding-top: 9px;">sidebar-' . $id_sidebar . $name_hidden_field . '</td><td><input class="remove-button" type="submit" name="sidebar_delete" value="' . $id_sidebar . '" /></td></tr>';
						$i++;
					}
					$output .= '</tbody>';
					$output .= '</table>';
				} else {
					$output .= '<p>No custom sidebars have been created yet.</p>';
				}
				break;

			// google fonts update
			case "fonts_update_google":
				$output .= '<input type="submit" class="button2" name="fonts_update_google" value="Update" />';
				break;

			// cufon upload
			case "cufon_upload":
				$output .= optionsframework_medialibrary_uploader( $value['id'] . '_file', $val, null ); // New AJAX Uploader using Media Library	
				$output .= '<input type="submit" class="button2" name="cufon_upload" value="Add to font list" />';
				break;
				
			// Heading for Navigation
			case "heading":
				if ($counter >= 2) {
				   $output .= '</div>'."\n";
				}
				$jquery_click_hook = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($value['name']) );
				$jquery_click_hook = "of-option-" . $jquery_click_hook;
				$menu .= '<a id="'.  esc_attr( $jquery_click_hook ) . '-tab" class="nav-tab' . ( esc_attr( $value['name'] ) == '' ? ' nav-tab-empty' : '' ) . '" title="' . esc_attr( $value['name'] ) . '" href="' . esc_attr( '#'.  $jquery_click_hook ) . '">' . esc_html( $value['name'] ) . '</a>';
				$output .= '<div class="group" id="' . esc_attr( $jquery_click_hook ) . '">';
				$output .= '<h3>' . esc_html( $value['name'] ) . '</h3>' . "\n";
				break;
			}

			if ( ( $value['type'] != "heading" ) && ( $value['type'] != "info" ) && ( $value['type'] != "toggle-start" ) && ( $value['type'] != "toggle-end" ) ) {
				if ( $value['type'] != "checkbox" ) {
					$output .= '<br/>';
				}
				$output .= '</div>';
				if ( $value['type'] != "checkbox" ) {
					$output .= '<div class="explain explain-' . $value['type'] . '">' . wp_kses( $explain_value, $allowedtags) . '</div>'."\n";
				}
				$output .= '<div class="clear"></div></div></div>'."\n";
			}
		}
	}
    $output .= '</div>';
    return array($output,$menu);
}


/**
 * get Cufon fonts in the /fonts/ folder
 */
function getCufonFonts() {
	// Get any existing copy of our transient data
	if ( false === ( $fonts = get_transient( 'cufon_fonts' ) ) ) {
	    $fonts = array();
	    $files = glob(UNISPHERE_FONTS . "/*");
	    if( $files ) {
			foreach ($files as $path_to_file)
			{
				$file_name = basename( $path_to_file );
				$delimeterLeft = 'font-family":"';
				$delimeterRight = '"';
				
				$filename = truepath( UNISPHERE_FONTS . '/' . $file_name );
				if ($fh = fopen($filename, "r")) {
					$file_content = fread($fh, filesize($filename));
					$font_name = cufonFontName($file_content, $delimeterLeft, $delimeterRight);	
					$fonts[] = array( 'value' => 'cufon:' . $font_name . ':' . basename( $path_to_file ), 'text' => $font_name );
					fclose($fh);
				}
			}
			// Cache it for 60 minutes
		    set_transient( 'cufon_fonts', $fonts, 60 * 60 );
		}
	}

	return $fonts;
}

/**
 * extract name of the Cufon font
 */
function cufonFontName($inputStr, $delimeterLeft, $delimeterRight)
{
    $posLeft = strpos($inputStr, $delimeterLeft);
    if ($posLeft === false)
    {
        return false;
    }
    $posLeft += strlen($delimeterLeft);
    $posRight = strpos($inputStr, $delimeterRight, $posLeft);
    if ($posRight === false)
    {
        return false;
    }
    return substr($inputStr, $posLeft, $posRight - $posLeft);
}

/**
 * get Cufon fonts in the /fonts/ folder
 */
function getGoogleFonts( $remote_update = false ) {

    // Check if the Google web fonts list has already been imported
    if( false === ( $fonts = get_option('unisphere_google_fonts') ) || $remote_update ) {
    	// Import the font list
    	$response = wp_remote_get( UNISPHERE_GOOGLE_WEB_FONTS_FILE );
		if( is_wp_error( $response ) ) {
		   return null;
		} else {
			// Parse the JSON data into a PHP array
			$json_data = json_decode( $response['body'], true );
			
			$fonts = array();
			foreach( $json_data['items'] as $font ) {
				if( false === array_search( 'khmer', $font['subsets'] ) ) {
					$fonts[] = array( 'value' => 'google:' . str_replace(' ', '+', $font['family']), 'text' => $font['family'] );
				}
			}
		}

		update_option( 'unisphere_google_fonts', $fonts );
    }

    return $fonts;
}
