/**
 * Prints out the inline javascript needed for the colorpicker and choosing
 * the tabs in the panel.
 */

jQuery(document).ready(function($) {
	
	// Fade out the save message
	$('.fade').delay(1000).fadeOut(1000);
	
	// Color Picker
	$('.colorSelector').each(function(){
		var Othis = this; //cache a copy of the this variable for use inside nested function
		var initialColor = $(Othis).next('input').attr('value');
		$(this).ColorPicker({
		color: initialColor,
		onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
		},
		onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
		},
		onChange: function (hsb, hex, rgb) {
		$(Othis).children('div').css('backgroundColor', '#' + hex);
		$(Othis).next('input').attr('value','#' + hex);

		// if( $(Othis).attr('id').indexOf('skin_color_') != -1 ) {
		// 	var index = parseInt($(Othis).attr('id').replace('skin_color_', '').replace('_picker', ''));
		// 	orig_colors[index-1] = '#' + hex;	
		// }		
	}
	});
	}); //end color picker
	
	// Switches option sections
	$('.group').hide();
	var activetab = '';
	if (typeof(localStorage) != 'undefined' ) {
		activetab = localStorage.getItem("activetab");
	}
	if (activetab != '' && $(activetab).length ) {
		$(activetab).fadeIn();
	} else {
		$('.group:first').fadeIn();
	}
	$('.group .collapsed').each(function(){
		$(this).find('input:checked').parent().parent().parent().nextAll().each( 
			function(){
				if ($(this).hasClass('last')) {
					$(this).removeClass('hidden');
						return false;
					}
				$(this).filter('.hidden').removeClass('hidden');
			});
	});
	
	if (activetab != '' && $(activetab + '-tab').length ) {
		$(activetab + '-tab').addClass('nav-tab-active');
	}
	else {
		$('.nav-tab-wrapper a:first').addClass('nav-tab-active');
	}
	$('.nav-tab-wrapper a').click(function(evt) {
		$('.nav-tab-wrapper a').removeClass('nav-tab-active');
		$(this).addClass('nav-tab-active').blur();
		var clicked_group = $(this).attr('href');
		if (typeof(localStorage) != 'undefined' ) {
			localStorage.setItem("activetab", $(this).attr('href'));
		}
		$('.group').hide();
		$(clicked_group).fadeIn();
		evt.preventDefault();
	});
           					
	$('.group .collapsed input:checkbox').click(unhideHidden);
				
	function unhideHidden(){
		if ($(this).attr('checked')) {
			$(this).parent().parent().parent().nextAll().removeClass('hidden');
		}
		else {
			$(this).parent().parent().parent().nextAll().each( 
			function(){
				if ($(this).filter('.last').length) {
					$(this).addClass('hidden');
					return false;		
					}
				$(this).addClass('hidden');
			});
           					
		}
	}
	
	// Image Options
	$('.of-radio-img-img').click(function(){
		$(this).parent().parent().find('.of-radio-img-img').removeClass('of-radio-img-selected');
		$(this).addClass('of-radio-img-selected');		
	});
		
	$('.of-radio-img-label').hide();
	$('.of-radio-img-img').show();
	$('.of-radio-img-radio').hide();
	
	/* Toggle */
	$(".toggle-container .toggle-content").hide(); //Hide (Collapse) the toggle containers on load
	$(".toggle-container .toggle-sign").text('+'); //Add the + sign on load

	$(".toggle-container .toggle").toggle(function() {
		$(this).addClass('selected');
		$(this).find('.toggle-sign').text('-');
		$(this).next(".toggle-content").slideToggle();
	}, function() {
		$(this).removeClass('selected');
		$(this).find('.toggle-sign').text('+');
		$(this).next(".toggle-content").slideToggle();
	});

	// Add confirmation dialog when deleting a sidebar
	$('#section-sidebar_list .remove-button').click(function(e) {
		if(!confirm("Are you sure you want to delete this sidebar?")) 	{
		    e.preventDefault();
		} else {
			$(this).closest("form").submit();
		}
	});

	// Custom fields multi-checkbox sorting
	if( $.isFunction($.fn.sortable) ) {
		$('.sort-children').sortable();
		$('.sort-children').disableSelection();
	}

	// Custom field parent container margin and padding adjust
	if( $('#custom-fields-form-wrap') )
		$('#custom-fields-form-wrap').parent().css('margin', '0').css('padding', '0');

	// Custom fields hide/show fields based on options ( SLIDER ITEM )
	if( $('#_slider_video') ) {
		// on screen load
		$('#section_slider_video_no_sublimevideo, #section_slider_video_youtube, #section_slider_video_vimeo, #section_slider_video_html5_mp4, #section_slider_video_html5_mp4_hd, #section_slider_video_html5_webm, #section_slider_video_html5_webm_hd, #section_slider_video_flv, #section_slider_video_flv_hd, #section_slider_autoplay, #section_slider_no_autoplay_thumbnail').hide();
		if( $('#_slider_video').val() == 'youtube' ) { $('#section_slider_video_youtube, #section_slider_autoplay').show(); }
		if( $('#_slider_video').val() == 'vimeo' ) { $('#section_slider_video_vimeo, #section_slider_autoplay').show(); }
		if( $('#_slider_video').val() == 'html5_flv' ) { $('#section_slider_video_html5_mp4, #section_slider_video_html5_mp4_hd, #section_slider_video_html5_webm, #section_slider_video_html5_webm_hd, #section_slider_video_flv, #section_slider_video_flv_hd, #section_slider_autoplay, #section_slider_no_autoplay_thumbnail').show(); }
		if( $('#_slider_video').val() == 'html5_flv_disabled' ) { $('#section_slider_video_no_sublimevideo').show(); }

		// on option change
		$('#_slider_video').change(function(){
			$('#section_slider_video_no_sublimevideo, #section_slider_video_youtube, #section_slider_video_vimeo, #section_slider_video_html5_mp4, #section_slider_video_html5_mp4_hd, #section_slider_video_html5_webm, #section_slider_video_html5_webm_hd, #section_slider_video_flv, #section_slider_video_flv_hd, #section_slider_autoplay, #section_slider_no_autoplay_thumbnail').hide();
			if( $(this).val() == 'youtube' ) { $('#section_slider_video_youtube, #section_slider_autoplay').show(250); }
			if( $(this).val() == 'vimeo' ) { $('#section_slider_video_vimeo, #section_slider_autoplay').show(250); }
			if( $(this).val() == 'html5_flv' ) { $('#section_slider_video_html5_mp4, #section_slider_video_html5_mp4_hd, #section_slider_video_html5_webm, #section_slider_video_html5_webm_hd, #section_slider_video_flv, #section_slider_video_flv_hd, #section_slider_autoplay, #section_slider_no_autoplay_thumbnail').show(); }
			if( $(this).val() == 'html5_flv_disabled' ) { $('#section_slider_video_no_sublimevideo').show(); }
		});
	}

	// Custom fields hide/show fields based on options ( SUB-HEADER BAR )
	if( $('#_page_sub_header_style') ) {		
		// on screen load
		$('#section_page_sub_header_title, #section_page_sub_header_desc, #section_page_sub_header_height, #section_page_sub_header_image').hide();
		if( $('#_page_sub_header_style').val() == 'solid-bg' || $('#_page_sub_header_style').val() == 'transparent-bg' ) { $('#section_page_sub_header_title, #section_page_sub_header_desc').show(); }
		if( $('#_page_sub_header_style').val() == 'image-bg' ) { $('#section_page_sub_header_title, #section_page_sub_header_desc, #section_page_sub_header_height, #section_page_sub_header_image').show(); }

		// on option change
		$('#_page_sub_header_style').change(function(){
			$('#section_page_sub_header_title, #section_page_sub_header_desc, #section_page_sub_header_height, #section_page_sub_header_image').hide();
			if( $(this).val() == 'solid-bg' || $(this).val() == 'transparent-bg' ) { $('#section_page_sub_header_title, #section_page_sub_header_desc').show(250); }
			if( $(this).val() == 'image-bg' ) { $('#section_page_sub_header_title, #section_page_sub_header_desc, #section_page_sub_header_height, #section_page_sub_header_image').show(250); }
		});
	}

	// Custom fields hide/show fields based on options ( SUB-HEADER SLIDER )
	if( $('#_page_show_slider') ) {
		// on screen load
		$('#section_page_slider_height, #section_page_slider_type, #section_page_slider_no_layerslider, #section_page_slider_layer_id, #section_page_slider_num_items, #section_page_slider_seconds, #section_page_slider_transition_seconds, #section_page_slider_cat').hide();
		
		if( $('#_page_show_slider').is(':checked') ) {
			$('#section_page_slider_type').show();
			if( $('#_page_slider_type').val() == '' || $('#_page_slider_type').val() == 'fullwidth' ) { $('#section_page_slider_height, #section_page_slider_num_items, #section_page_slider_seconds, #section_page_slider_transition_seconds, #section_page_slider_cat').show(); }
			if( $('#_page_slider_type').val() == 'layer_slider' ) { $('#section_page_slider_layer_id').show(); }
			if( $('#_page_slider_type').val() == 'layer_slider_disabled' ) { $('#section_page_slider_no_layerslider').show(); }
		}

		// on option change
		$('#_page_show_slider').change(function(){
			if( $(this).is(':checked') ) {
				$('#section_page_slider_type').show();
				if( $('#_page_slider_type').val() == '' || $('#_page_slider_type').val() == 'fullwidth' ) { $('#section_page_slider_height, #section_page_slider_num_items, #section_page_slider_seconds, #section_page_slider_transition_seconds, #section_page_slider_cat').show(); }
			} else {
				$('#section_page_slider_height, #section_page_slider_type, #section_page_slider_no_layerslider, #section_page_slider_layer_id, #section_page_slider_num_items, #section_page_slider_seconds, #section_page_slider_transition_seconds, #section_page_slider_cat').hide(250);
			}
		});

		// on video type change
		$('#_page_slider_type').change(function(){
			$('#section_page_slider_height, #section_page_slider_no_layerslider, #section_page_slider_layer_id, #section_page_slider_num_items, #section_page_slider_seconds, #section_page_slider_transition_seconds, #section_page_slider_cat').hide(250);
			if( $(this).val() == '' || $(this).val() == 'fullwidth' ) { $('#section_page_slider_height, #section_page_slider_num_items, #section_page_slider_seconds, #section_page_slider_transition_seconds, #section_page_slider_cat').show(250); }
			if( $(this).val() == 'layer_slider' ) { $('#section_page_slider_layer_id').show(250); }
			if( $(this).val() == 'layer_slider_disabled' ) { $('#section_page_slider_no_layerslider').show(250); }
		});
	}

	// Custom fields hide/show fields based on options ( PORTFOLIO ITEM )
	if( $('#_portfolio_video') ) {
		// on screen load
		$('#section_portfolio_video_no_sublimevideo, #section_portfolio_video_youtube, #section_portfolio_video_vimeo, #section_portfolio_video_html5_mp4, #section_portfolio_video_html5_mp4_hd, #section_portfolio_video_html5_webm, #section_portfolio_video_html5_webm_hd, #section_portfolio_video_flv, #section_portfolio_video_flv_hd, #section_portfolio_autoplay, #section_portfolio_no_autoplay_thumbnail, #section_portfolio_video_lightbox_width, #section_portfolio_video_lightbox_height').hide();
		if( $('#_portfolio_video').val() == 'youtube' ) { $('#section_portfolio_video_youtube, #section_portfolio_autoplay').show(); }
		if( $('#_portfolio_video').val() == 'vimeo' ) { $('#section_portfolio_video_vimeo, #section_portfolio_autoplay').show(); }
		if( $('#_portfolio_video').val() == 'html5_flv' ) { $('#section_portfolio_video_html5_mp4, #section_portfolio_video_html5_mp4_hd, #section_portfolio_video_html5_webm, #section_portfolio_video_html5_webm_hd, #section_portfolio_video_flv, #section_portfolio_video_flv_hd, #section_portfolio_autoplay, #section_portfolio_no_autoplay_thumbnail, #section_portfolio_video_lightbox_width, #section_portfolio_video_lightbox_height').show(); }
		if( $('#_portfolio_video').val() == 'html5_flv_disabled' ) { $('#section_portfolio_video_no_sublimevideo').show(); }

		// on option change
		$('#_portfolio_video').change(function(){
			$('#section_portfolio_video_no_sublimevideo, #section_portfolio_video_youtube, #section_portfolio_video_vimeo, #section_portfolio_video_html5_mp4, #section_portfolio_video_html5_mp4_hd, #section_portfolio_video_html5_webm, #section_portfolio_video_html5_webm_hd, #section_portfolio_video_flv, #section_portfolio_video_flv_hd, #section_portfolio_autoplay, #section_portfolio_no_autoplay_thumbnail, #section_portfolio_video_lightbox_width, #section_portfolio_video_lightbox_height').hide();
			if( $(this).val() == 'youtube' ) { $('#section_portfolio_video_youtube, #section_portfolio_autoplay').show(250); }
			if( $(this).val() == 'vimeo' ) { $('#section_portfolio_video_vimeo, #section_portfolio_autoplay').show(250); }
			if( $(this).val() == 'html5_flv' ) { $('#section_portfolio_video_html5_mp4, #section_portfolio_video_html5_mp4_hd, #section_portfolio_video_html5_webm, #section_portfolio_video_html5_webm_hd, #section_portfolio_video_flv, #section_portfolio_video_flv_hd, #section_portfolio_autoplay, #section_portfolio_no_autoplay_thumbnail, #section_portfolio_video_lightbox_width, #section_portfolio_video_lightbox_height').show(); }
			if( $(this).val() == 'html5_flv_disabled' ) { $('#section_portfolio_video_no_sublimevideo').show(); }
		});
	}

	if( $('#_portfolio_detail_big_images') ) {
		// on screen load
		$('#section_portfolio_slider_height').hide();
		if( $('#_portfolio_detail_big_images').val() == 'slider' ) {
			$('#section_portfolio_slider_height').show();
		}

		// on option change
		$('#_portfolio_detail_big_images').change(function(){
			$('#section_portfolio_slider_height').hide();
			if( $(this).val() == 'slider' ) { $('#section_portfolio_slider_height').show(250); }
		});
	}

	if( $('#_blog_post_media') ) {
		// on screen load
		$('#section_blog_post_media_overlay_off, #section_blog_post_slider_height, #section_blog_post_video, #section_blog_post_video_no_sublimevideo, #section_blog_post_video_youtube, #section_blog_post_video_vimeo, #section_blog_post_video_html5_mp4, #section_blog_post_video_html5_mp4_hd, #section_blog_post_video_html5_webm, #section_blog_post_video_html5_webm_hd, #section_blog_post_video_flv, #section_blog_post_video_flv_hd, #section_blog_post_autoplay, #section_blog_post_no_autoplay_thumbnail').hide();
		if( $('#_blog_post_media').val() == 'image' ) {
			$('#section_blog_post_media_overlay_off').show();
		}
		if( $('#_blog_post_media').val() == 'video' ) {
			$('#section_blog_post_video').show();
			if( $('#_blog_post_video').val() == 'youtube' ) { $('#section_blog_post_video_youtube, #section_blog_post_autoplay').show(); }
			if( $('#_blog_post_video').val() == 'vimeo' ) { $('#section_blog_post_video_vimeo, #section_blog_post_autoplay').show(); }
			if( $('#_blog_post_video').val() == 'html5_flv' ) { $('#section_blog_post_video_html5_mp4, #section_blog_post_video_html5_mp4_hd, #section_blog_post_video_html5_webm, #section_blog_post_video_html5_webm_hd, #section_blog_post_video_flv, #section_blog_post_video_flv_hd, #section_blog_post_autoplay, #section_blog_post_no_autoplay_thumbnail').show(); }
			if( $('#_blog_post_video').val() == 'html5_flv_disabled' ) { $('#section_blog_post_video_no_sublimevideo').show(); }
		}
		if( $('#_blog_post_media').val() == 'slider' ) {
			$('#section_blog_post_slider_height').show();
		}

		// on media option change
		$('#_blog_post_media').change(function(){
			$('#section_blog_post_media_overlay_off, #section_blog_post_slider_height, #section_blog_post_video, #section_blog_post_video_no_sublimevideo, #section_blog_post_video_youtube, #section_blog_post_video_vimeo, #section_blog_post_video_html5_mp4, #section_blog_post_video_html5_mp4_hd, #section_blog_post_video_html5_webm, #section_blog_post_video_html5_webm_hd, #section_blog_post_video_flv, #section_blog_post_video_flv_hd, #section_blog_post_autoplay, #section_blog_post_no_autoplay_thumbnail').hide();
			if( $(this).val() == 'image' ) {
				$('#section_blog_post_media_overlay_off').show();
			}
			if( $(this).val() == 'video' ) {
				$('#section_blog_post_video').show();
				if( $('#_blog_post_video').val() == 'youtube' ) { $('#section_blog_post_video_youtube, #section_blog_post_autoplay').show(250); }
				if( $('#_blog_post_video').val() == 'vimeo' ) { $('#section_blog_post_video_vimeo, #section_blog_post_autoplay').show(250); }
				if( $('#_blog_post_video').val() == 'html5_flv' ) { $('#section_blog_post_video_html5_mp4, #section_blog_post_video_html5_mp4_hd, #section_blog_post_video_html5_webm, #section_blog_post_video_html5_webm_hd, #section_blog_post_video_flv, #section_blog_post_video_flv_hd, #section_blog_post_autoplay, #section_blog_post_no_autoplay_thumbnail').show(); }
				if( $('#_blog_post_video').val() == 'html5_flv_disabled' ) { $('#section_blog_post_video_no_sublimevideo').show(); }
			}
			if( $(this).val() == 'slider' ) {
				$('#section_blog_post_slider_height').show();
			}
		});

		// on video option change
		$('#_blog_post_video').change(function(){
			$('#section_blog_post_video_no_sublimevideo, #section_blog_post_video_youtube, #section_blog_post_video_vimeo, #section_blog_post_video_html5_mp4, #section_blog_post_video_html5_mp4_hd, #section_blog_post_video_html5_webm, #section_blog_post_video_html5_webm_hd, #section_blog_post_video_flv, #section_blog_post_video_flv_hd, #section_blog_post_autoplay, #section_blog_post_no_autoplay_thumbnail').hide();
			if( $(this).val() == 'youtube' ) { $('#section_blog_post_video_youtube, #section_blog_post_autoplay').show(250); }
			if( $(this).val() == 'vimeo' ) { $('#section_blog_post_video_vimeo, #section_blog_post_autoplay').show(250); }
			if( $(this).val() == 'html5_flv' ) { $('#section_blog_post_video_html5_mp4, #section_blog_post_video_html5_mp4_hd, #section_blog_post_video_html5_webm, #section_blog_post_video_html5_webm_hd, #section_blog_post_video_flv, #section_blog_post_video_flv_hd, #section_blog_post_autoplay, #section_blog_post_no_autoplay_thumbnail').show(); }
			if( $(this).val() == 'html5_flv_disabled' ) { $('#section_blog_post_video_no_sublimevideo').show(); }
		});
	}

	/**
	 * Converts an RGB color value to HSL. Conversion formula
	 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
	 * Assumes r, g, and b are contained in the set [0, 255] and
	 * returns h, s, and l in the set [0, 1].
	 *
	 * @param   Number  r       The red color value
	 * @param   Number  g       The green color value
	 * @param   Number  b       The blue color value
	 * @return  Array           The HSL representation
	 */
	function rgbToHsl(r, g, b){
	    r /= 255, g /= 255, b /= 255;
	    var max = Math.max(r, g, b), min = Math.min(r, g, b);
	    var h, s, l = (max + min) / 2;

	    if(max == min){
	        h = s = 0; // achromatic
	    }else{
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max){
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }

	    return [h, s, l];
	}

	function hexToHsl(hex) {
		var r = parseInt(hex.substr(1,2), 16);
		var g = parseInt(hex.substr(3,2), 16);
		var b = parseInt(hex.substr(5,2), 16);

		return rgbToHsl(r, g, b);
	}

	/**
	 * Converts an HSL color value to RGB. Conversion formula
	 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
	 * Assumes h, s, and l are contained in the set [0, 1] and
	 * returns r, g, and b in the set [0, 255].
	 *
	 * @param   Number  h       The hue
	 * @param   Number  s       The saturation
	 * @param   Number  l       The lightness
	 * @return  Array           The RGB representation
	 */
	function hslToRgb(h, s, l){
	    var r, g, b;

	    if(s == 0){
	        r = g = b = l; // achromatic
	    }else{
	        function hue2rgb(p, q, t){
	            if(t < 0) t += 1;
	            if(t > 1) t -= 1;
	            if(t < 1/6) return p + (q - p) * 6 * t;
	            if(t < 1/2) return q;
	            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
	            return p;
	        }

	        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	        var p = 2 * l - q;
	        r = hue2rgb(p, q, h + 1/3);
	        g = hue2rgb(p, q, h);
	        b = hue2rgb(p, q, h - 1/3);
	    }

	    return [r * 255, g * 255, b * 255];
	}

	function componentToHex(c) {
	    var hex = c.toString(16);
	    hex = hex.replace('-', '');
	    return hex.length == 1 ? "0" + hex : hex;
	}

	function hslToHex(h, s, l) {
		var hsl = hslToRgb(h, s, l);
		return '#' + componentToHex(parseInt(hsl[0])) + componentToHex(parseInt(hsl[1])) + componentToHex(parseInt(hsl[2]));
	}

	if( $("#slider-h").length > 0 ) {
		$("#slider-h").slider({
	        orientation: "vertical",
	        range: "min",
	        min: -180,
	        max: 180,
	        value: 0,
	        step: 1,
	        slide: refreshColors
	    });

	    $("#slider-s, #slider-l").slider({
	        orientation: "vertical",
	        range: "min",
	        min: -100,
	        max: 100,
	        value: 0,
	        step: 1,
	        slide: refreshColors
	    });
		
		var orig_colors = new Array();
	    for (var i=0;i<8;i++) {
			orig_colors[i] = $('#skin_color_'+(i+1)).val();
		}
	}

    function refreshColors() {
    	for (var i=0;i<8;i++) {
    		if( $('#skin_color_'+(i+1)+'_enable_hsl').is(':checked') ) {
				new_color = transformColor(orig_colors[i]);
				$('#skin_color_'+(i+1)+'_picker div').css('background-color', new_color);
				$('#skin_color_'+(i+1)).val(new_color);
			}
		}
    }

    function transformColor(orig_color) {
		var orig_color_hsl = hexToHsl(orig_color);

		// Hue
		var h = parseFloat(orig_color_hsl[0])*360;
		h += parseInt($("#slider-h").slider("value"));
		if(h > 360) h -= 360;

		// Saturation
		var s = parseFloat(orig_color_hsl[1])*100;
		var added_s = parseInt($("#slider-s").slider("value"));

		if( added_s > 0 )
			s += ( (100 - s) * added_s / 100 );
		else
			s += ( s * added_s / 100 );

		if(s > 100) s = 100;
		if(s < 0) s = 0;

		// Lightness
		var l = parseFloat(orig_color_hsl[2])*100;
		var added_l = parseInt($("#slider-l").slider("value"));

		if( added_l > 0 )
			l += ( (100 - l) * added_l / 100 );
		else
			l += ( l * added_l / 100 );

		if(l > 100) l = 100;
		if(l < 0) l = 0;

		return hslToHex(h/360, s/100, l/100);
    }
});