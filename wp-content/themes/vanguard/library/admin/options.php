<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 * 
 */
function optionsframework_option_name() {

	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = UNISPHERE_THEMEOPTIONS;
	update_option('optionsframework', $optionsframework_settings);
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */
function optionsframework_options() {
	
	// Background Defaults	
	$background_defaults = array( 'image' => '', 'repeat' => 'repeat', 'position' => 'top center', 'attachment'=>'scroll' );

	// Font subsets defaults
	$font_subsets_defaults = array( 'cyrillic' => 'Cyrillic', 'cyrillic-ext' => 'Cyrillic Extended', 'greek' => 'Greek', 'greek-ext' => 'Greek Extended', 'latin' => 'Latin', 'latin-ext' => 'Latin Extended', 'vietnamese' => 'Vietnamese' );

	// Font styles defaults
	$font_styles_defaults = array(  '100' => 'Ultra-Light 100',
									'100italic' => 'Ultra-Light 100 Italic',
									'200' => 'Light 200',
									'200italic' => 'Light 200 Italic',
									'300' => 'Book 300',
									'300italic' => 'Book 300 Italic',
									'400' => 'Normal 400',
									'400italic' => 'Normal 400 Italic',
									'500' => 'Medium 500',
									'500italic' => 'Medium 500 Italic',
									'600' => 'Semi-Bold 600',
									'600italic' => 'Semi-Bold 600 Italic',
									'700' => 'Bold 700',
									'700italic' => 'Bold 700 Italic',
									'800' => 'Extra-Bold 800',
									'800italic' => 'Extra-Bold 800 Italic',
									'900' => 'Ultra-Bold 900',
									'900italic' => 'Ultra-Bold 900 Italic' );

	$optionsframework_settings = get_option('optionsframework' );
	
	// Updates the unique option id in the database if it has changed
	optionsframework_option_name();
	
	$export_settings = base64_encode(serialize(get_option($optionsframework_settings['id'])));

	// Cufon fonts in the /fonts/ folder
	$cufon_fonts = getCufonFonts();

	// Google Web Fonts list
	$google_fonts = getGoogleFonts(false);

	$options = array();
	
	// GENERAL -------------------------------------------------------------------
	$options[] = array( "name" => "General",
						"type" => "heading",
						"location" => THEME_OPTIONS_PAGE);
						
		$options[] = array( "name" => "Logo",
							"desc" => "Set your header logo image here.",
							"id" => "logo",
							"type" => "upload",
							"std" => "",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Favicon",
							"desc" => "Set your favicon here.",
							"id" => "favicon",
							"type" => "upload",
							"std" => "",
							"location" => THEME_OPTIONS_PAGE);
				
		$options[] = array( "name" => "Custom CSS",
							"desc" => "Add your custom css entries here.",
							"id" => "custom_css",
							"std" => "",
							"type" => "textarea",
							"location" => THEME_OPTIONS_PAGE); 
							
		$options[] = array( "name" => "Custom Scripts",
							"desc" => "Add your custom scripts here like for example your Google Analytics code to track your visitors.",
							"id" => "custom_js",
							"std" => "",
							"type" => "textarea",
							"location" => THEME_OPTIONS_PAGE); 

	// BLOG -------------------------------------------------------------------
	$options[] = array( "name" => "Blog",
						"type" => "heading",
						"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Blog template displayed in the Archives",
							"desc" => "Select the blog template displayed in the categories, tags, author and date archives.",
							"id" => "skin_blog_archives_template",
							"std" => "template_blog.php",
							"type" => "select",
							"class" => "mini",
							"options" => array( "template_blog_wide_image.php" => "Wide Image", "template_blog_half_image.php" => "Half Image" ),
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Sub-header style displayed in the Archives",
							"desc" => "Select the sub-header style displayed displayed in the categories, tags, author and date archives.",
							"id" => "skin_sub_header_archives_style",
							"std" => "solid-bg",
							"type" => "select",
							"class" => "mini",
							"options" => array( "hide" => "Don't display", "solid-bg" => "Solid background", "transparent-bg" => "Transparent background", "image-bg" => "Background image" ),
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show \"Next\" and \"Previous\" navigation links on the top of post detail?",
							"desc" => "Check this if you want to show the \"Next\" and \"Previous\" links on the top of post detail page.",
							"id" => "show_post_navigation_links",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show \"Next\" and \"Previous\" navigation links on the bottom of post detail?",
							"desc" => "Check this if you want to show the \"Next\" and \"Previous\" links on the bottom of post detail page.",
							"id" => "show_post_navigation_links_below",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the social sharing buttons on post detail?",
							"desc" => "Check this if you want to show the social sharing buttons on the post detail page.",
							"id" => "show_blog_share_buttons",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Author?",
							"desc" => "Check this if you want to show the Author meta information on blog posts.",
							"id" => "show_meta_author",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Date?",
							"desc" => "Check this if you want to show the Date meta information on blog posts.",
							"id" => "show_meta_date",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Categories?",
							"desc" => "Check this if you want to show the Categories meta information on blog posts.",
							"id" => "show_meta_categories",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);
		
		$options[] = array( "name" => "Show the Tags?",
							"desc" => "Check this if you want to show the Tags meta information on blog posts.",
							"id" => "show_meta_tags",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);
		
		$options[] = array( "name" => "Show the Comment Count?",
							"desc" => "Check this if you want to show the Comment Count meta information on blog posts.",
							"id" => "show_meta_comment_count",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Author Info section?",
							"desc" => "Check this if you want to show the Author Info section on the blog post detail page.",
							"id" => "show_blog_author_info",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Related Posts section?",
							"desc" => "Check this if you want to show the Related Posts section on the blog post detail page.",
							"id" => "show_blog_related_posts",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

	// PORTFOLIO -------------------------------------------------------------------
	$options[] = array( "name" => "Portfolio",
						"type" => "heading",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Portfolio Detail Page Permalink",
							"desc" => "This is the portfolio detail page permalink if using custom permalink settings in WordPress.",
							"id" => "portfolio_permalink",
							"std" => "portfolio/detail",
							"type" => "text",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show \"Next\" and \"Previous\" navigation links on the top of portfolio detail?",
							"desc" => "Check this if you want to show the \"Next\" and \"Previous\" links on the top of portfolio item detail page.",
							"id" => "show_portfolio_navigation_links",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show \"Next\" and \"Previous\" navigation links on the bottom of portfolio detail?",
							"desc" => "Check this if you want to show the \"Next\" and \"Previous\" links on the bottom of portfolio item detail page.",
							"id" => "show_portfolio_navigation_links_below",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the social sharing buttons on portfolio detail?",
							"desc" => "Check this if you want to show the social sharing buttons on the portfolio detail page.",
							"id" => "show_portfolio_share_buttons",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Date?",
							"desc" => "Check this if you want to show the Date meta information on the portfolio item detail page.",
							"id" => "show_portfolio_meta_date",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Comment Count?",
							"desc" => "Check this if you want to show the Comment Count meta information on the portfolio item detail page.",
							"id" => "show_portfolio_meta_comment_count",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Show the Related Work section?",
							"desc" => "Check this if you want to show the Related Work section on the portfolio item detail page.",
							"id" => "show_portfolio_related_work",
							"std" => "1",
							"type" => "checkbox",
							"location" => THEME_OPTIONS_PAGE);

	// CONTACT -------------------------------------------------------------------
	$options[] = array( "name" => "Contact",
						"type" => "heading",
						"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Destination E-mail",
							"desc" => "This is the e-mail account you want your messages to be sent to.",
							"id" => "destination_email",
							"std" => "",
							"type" => "text",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "Success Message",
							"desc" => "This is the message displayed when someone uses the contact form and the email is sent successfully.",
							"id" => "email_success",
							"std" => "<strong>Thanks!</strong> Your email was successfully sent. I check my email all the time, so I should be in touch soon.",
							"type" => "text",
							"location" => THEME_OPTIONS_PAGE);
							
		$options[] = array( "name" => "Error Message",
							"desc" => "This is the message displayed when someone uses the contact form and an error occurs when sending the email.",
							"id" => "email_error",
							"std" => "<strong>There was an error sending your message.</strong> Please try again later.",
							"type" => "text",
							"location" => THEME_OPTIONS_PAGE);

	// ADDTHIS -------------------------------------------------------------------
	$options[] = array( "name" => "AddThis",
						"type" => "heading",
							"location" => THEME_OPTIONS_PAGE);

		$options[] = array( "name" => "AddThis Profile ID",
							"desc" => "Please set your <a href=\"http://www.addthis.com\">AddThis</a> Profile ID in order to display social sharing buttons on the blog and portfolio posts.",
							"id" => "addthis_profile_id",
							"std" => "",
							"type" => "text",
							"location" => THEME_OPTIONS_PAGE);

	// IMPORT/EXPORT -------------------------------------------------------------------
	$options[] = array( "name" => "Import/Export",
						"type" => "heading",
						"location" => THEME_OPTIONS_PAGE);
						
		$options[] = array( "name" => "Import Settings",
							"desc" => "Paste an exported encoded field from another theme installation into this field in order to import the other theme installation settings.",
							"id" => "import_settings",
							"std" => "",
							"type" => "textarea",
							"location" => THEME_OPTIONS_PAGE); 

		$options[] = array( "name" => "Export Settings",
							"desc" => "Copy this encoded field into the \"Import Settings\" field in another theme installation to import this installation settings.",
							"id" => "export_settings",
							"std" => $export_settings,
							"type" => "textarea",
							"location" => THEME_OPTIONS_PAGE); 

	// IMAGE SIZES -------------------------------------------------------------------
	$options[] = array( "name" => "",
						"type" => "heading",
						"location" => IMAGE_SIZES_PAGE);

		// sub-header slider
		$options[] = array( "name" => "Sub-header slider",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_header_slider_height",
								"std" => "450",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_sub_header_slider_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// sub-header full width slider
		$options[] = array( "name" => "Sub-header full width slider",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_header_full_width_slider_height",
								"std" => "450",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_sub_header_full_width_slider_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// sub-header bar
		$options[] = array( "name" => "Sub-header bar",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_sub_header_bar_height",
								"std" => "120",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_sub_header_bar_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// 1 column portfolio
		$options[] = array( "name" => "1 column portfolio",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_1_column_portfolio_height",
								"std" => "300",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_1_column_portfolio_crop",
								"std" => "1",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// 2 column portfolio
		$options[] = array( "name" => "2 column portfolio",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_2_column_portfolio_height",
								"std" => "280",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_2_column_portfolio_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// 2 column portfolio without gutter
		$options[] = array( "name" => "2 column portfolio without gutter",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_2_column_portfolio_no_gutter_height",
								"std" => "300",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_2_column_portfolio_no_gutter_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// 3 column portfolio
		$options[] = array( "name" => "3 column portfolio",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_3_column_portfolio_height",
								"std" => "190",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_3_column_portfolio_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// 3 column portfolio without gutter
		$options[] = array( "name" => "3 column portfolio without gutter",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_3_column_portfolio_no_gutter_height",
								"std" => "200",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_3_column_portfolio_no_gutter_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// 4 column portfolio
		$options[] = array( "name" => "4 column portfolio",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_4_column_portfolio_height",
								"std" => "140",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_4_column_portfolio_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// 4 column portfolio without gutter
		$options[] = array( "name" => "4 column portfolio without gutter",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_4_column_portfolio_no_gutter_height",
								"std" => "150",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_4_column_portfolio_no_gutter_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// portfolio detail big images
		$options[] = array( "name" => "Portfolio detail big images",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_portfolio_detail_big_images_height",
								"std" => "9999",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_portfolio_detail_big_images_crop",
								"std" => "0",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// blog (wide image)
		$options[] = array( "name" => "Blog (wide image)",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_blog_wide_image_height",
								"std" => "9999",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_blog_wide_image_crop",
								"std" => "0",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// blog (half image)
		$options[] = array( "name" => "Blog (half image)",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_blog_half_image_height",
								"std" => "240",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_blog_half_image_crop",
								"std" => "1",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// blog detail
		$options[] = array( "name" => "Blog detail",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_blog_detail_height",
								"std" => "9999",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_blog_detail_crop",
								"std" => "0",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// sidebar posts (recent/popular posts from blog/portfolio)
		$options[] = array( "name" => "Posts (recent/popular/related posts from blog/portfolio)",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_sidebar_posts_height",
								"std" => "105",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_sidebar_posts_crop",
								"std" => "1",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// WordPress default galleries
		$options[] = array( "name" => "WordPress default galleries",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_gallery_height",
								"std" => "192",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_gallery_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

		// slider shortcode
		$options[] = array( "name" => "Slider shortcode",
							"type" => "toggle-start",
							"location" => IMAGE_SIZES_PAGE);
						
			$options[] = array( "name" => "Image height",
								"desc" => "",
								"id" => "advanced_image_size_slider_shortcode_height",
								"std" => "540",
								"type" => "text",
								"class" => "image-size",
								"location" => IMAGE_SIZES_PAGE);

			$options[] = array( "name" => "Crop the image?",
								"desc" => "",
								"id" => "advanced_image_size_slider_shortcode_crop",
								"std" => "1",
								"class" => "hidden",
								"type" => "checkbox",
								"location" => IMAGE_SIZES_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => IMAGE_SIZES_PAGE);

	// SKINS MANAGER -------------------------------------------------------------------
	$options[] = array( "name" => "General",
						"type" => "heading",
						"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Layout",
							"desc" => "",
							"id" => "skin_layout",
							"std" => "wide",
							"type" => "select",
							"class" => "mini",
							"options" => array( "wide" => "Wide", "narrow" => "Narrow" ),
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Body background color",
							"desc" => "",
							"id" => "skin_body_bgcolor",
							"std" => "#ffffff",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Body background image",
							"desc" => "This image is set as the background body of all pages. (only displayed when using the 'Narrow' layout)",
							"id" => "skin_body_bgimage",
							"std" => $background_defaults, 
							"type" => "background",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Full page default background image",
							"desc" => "This image is centered and covers all the page background while maintaining aspect ratio. (only displayed when using the 'Narrow' layout)",
							"id" => "skin_full_page_bgimage",
							"std" => $background_defaults, 
							"type" => "background",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Sub-header default background image",
							"desc" => "This image is set by default when using the \"image background\" sub-header style.",
							"id" => "skin_sub_header_bgimage",
							"std" => $background_defaults, 
							"type" => "background",
							"location" => SKINS_MANAGER_PAGE);

	$options[] = array( "name" => "Colors",
						"type" => "heading",
						"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "H&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L",
							"desc" => "",
							"id" => "skin_hsl",
							"std" => "",
							"type" => "hsl",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Main color. Also used in links and buttons.",
							"desc" => "(default: #208ac7)",
							"id" => "skin_color_1",
							"std" => "#208ac7",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);
							
		$options[] = array( "name" => "Footer bar background.",
							"desc" => "(default: #33292d)",
							"id" => "skin_color_2",
							"std" => "#33292d",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Headings and footer widgets background.",
							"desc" => "(default: #403438)",
							"id" => "skin_color_3",
							"std" => "#403438",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Body font and footer widget bars below title.",
							"desc" => "(default: #857672)",
							"id" => "skin_color_4",
							"std" => "#857672",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Top navigation menu and breadcrumbs font.",
							"desc" => "(default: #ad9c9d)",
							"id" => "skin_color_5",
							"std" => "#ad9c9d",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Footer widgets font.",
							"desc" => "(default: #e5d7d3)",
							"id" => "skin_color_6",
							"std" => "#e5d7d3",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Separator lines and sub-header background.",
							"desc" => "(default: #f0eced)",
							"id" => "skin_color_7",
							"std" => "#f0eced",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Top navigation dropdown sub-menu background.",
							"desc" => "(default: #fcfcfc)",
							"id" => "skin_color_8",
							"std" => "#fcfcfc",
							"type" => "color",
							"location" => SKINS_MANAGER_PAGE);

	// FONTS
	$options[] = array( "name" => "Fonts",
						"type" => "heading",
						"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "IMPORTANT NOTICE:",
							"type" => "info",
							"desc" => "Not all fonts support all font weights and styles, for more information please check the <a href=\"http://www.google.com/webfonts\">Google web fonts directory</a>.",
							"location" => SKINS_MANAGER_PAGE);

		// FALLBACK FONT FAMILY
		$options[] = array( "name" => "Fallback font family",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This is the default fallback font family applied when for any particular reason the Cufon or Google web fonts don't load.",
								"id" => "fonts_fallback_font_family",
								"std" => array('face' => '"Helvetica Neue", Helvetica, Arial, sans-serif'),
								"type" => "typography",
								"advanced" => false,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => false,
								"uppercase" => false,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// BODY FONT
		$options[] = array( "name" => "Body font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applyed to the body text. You can't set a Cufon font for the body because it will probably crash your User's browser.",
								"id" => "fonts_body_font_family",
								"std" => array('face' => 'google:Open+Sans', 'size' => '14px', 'lineheight' => '26px'),
								"type" => "typography",
								"advanced" => true,
								"size" => true,
								"lineheight" => true,
								"fontstyle" => false,
								"uppercase" => false,
								"cufon_fonts" => false,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// TOP NAVIGATION MENU FONT
		$options[] = array( "name" => "Top navigation menu font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the top navigation menu parent elements.",
								"id" => "fonts_top_nav_menu_font",
								"std" => array('face' => 'google:Open+Sans', 'size' => '14px', 'fontstyle' => '600', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => true,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// SUB-HEADER TITLE FONT
		$options[] = array( "name" => "Sub-header title font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the sub-header title.",
								"id" => "fonts_sub_header_title_font",
								"std" => array('face' => 'google:Open+Sans', 'fontstyle' => '700', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// SUB-HEADER DESCRIPTION FONT
		$options[] = array( "name" => "Sub-header description font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the sub-header description.",
								"id" => "fonts_sub_header_description_font",
								"std" => array('face' => 'google:Open+Sans', 'fontstyle' => '400', 'uppercase' => 'off'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// SLIDER TITLE FONT
		$options[] = array( "name" => "Slider title font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the slider title font.",
								"id" => "fonts_slider_title_font",
								"std" => array('face' => 'google:Open+Sans', 'fontstyle' => '700', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// HEADINGS FONT
		$options[] = array( "name" => "Headings font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the headings (h1, h2, h3, h4, h5, h6, call-to-action and titles shortcodes).",
								"id" => "fonts_headings_font",
								"std" => array('face' => 'google:Open+Sans'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => false,
								"uppercase" => false,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// BLOG POST TITLES FONT
		$options[] = array( "name" => "Blog post titles font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the blog post titles.",
								"id" => "fonts_blog_title_font",
								"std" => array('face' => 'google:Open+Sans', 'fontstyle' => '700', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);

		// SIDEBAR WIDGET TITLES FONT
		$options[] = array( "name" => "Sidebar widget titles font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the widget titles in the sidebar.",
								"id" => "fonts_sidebar_widget_titles_font",
								"std" => array('face' => 'google:Open+Sans', 'size' => '20px', 'lineheight' => '26px', 'fontstyle' => '600', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => true,
								"lineheight" => true,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// FOOTER WIDGET TITLES FONT
		$options[] = array( "name" => "Footer widget titles font",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the widget titles in the footer.",
								"id" => "fonts_footer_widget_titles_font",
								"std" => array('face' => 'google:Open+Sans', 'size' => '16px', 'lineheight' => '16px', 'fontstyle' => '400', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => true,
								"lineheight" => true,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// PORTFOLIO TITLES FONT IN THE 1 COLUMN TEMPLATE
		$options[] = array( "name" => "Portfolio titles font in the 1 column template",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the titles in portfolio items using the 1 column portfolio template.",
								"id" => "fonts_portfolio_title_1_column_font",
								"std" => array('face' => 'google:Open+Sans', 'fontstyle' => '700', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

		// PORTFOLIO TITLES FONT IN TEMPLATES WITH GUTTER
		$options[] = array( "name" => "Portfolio titles font in templates with gutter",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the titles in portfolio items using the portfolio templates with gutter.",
								"id" => "fonts_portfolio_title_gutter_font",
								"std" => array('face' => 'google:Open+Sans', 'fontstyle' => '600', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	
							
		// PORTFOLIO TITLES FONT IN TEMPLATES WITHOUT GUTTER
		$options[] = array( "name" => "Portfolio titles font in templates without gutter",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "This font is applied to the titles in portfolio items using the portfolio templates without gutter.",
								"id" => "fonts_portfolio_title_no_gutter_font",
								"std" => array('face' => 'google:Open+Sans', 'fontstyle' => '700', 'uppercase' => 'on'),
								"type" => "typography",
								"advanced" => true,
								"size" => false,
								"lineheight" => false,
								"fontstyle" => true,
								"uppercase" => true,
								"cufon_fonts" => $cufon_fonts,
								"google_fonts" => $google_fonts,
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);		

		// OTHER FONT OPTIONS
		$options[] = array( "name" => "Other font options",
							"type" => "toggle-start",
							"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show post meta information in uppercase?",
								"id" => "fonts_show_posts_meta_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show sortable portfolio categories in uppercase?",
								"id" => "fonts_show_sortable_portfolio_categories_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show info box shortcode titles in uppercase?",
								"id" => "fonts_show_info_box_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show price table column titles in uppercase?",
								"id" => "fonts_show_price_column_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show breadcrumbs in uppercase?",
								"id" => "fonts_show_breadcrumbs_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show toggle shortcode titles in uppercase?",
								"id" => "fonts_show_toggle_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show separator \"to top\" title in uppercase?",
								"id" => "fonts_show_separator_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show button shortcode titles in uppercase?",
								"id" => "fonts_show_button_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show bar graph shortcode titles in uppercase?",
								"id" => "fonts_show_bar_graph_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show tab shortcode titles in uppercase?",
								"id" => "fonts_show_tab_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show call-to-action bar shortcode title in uppercase?",
								"id" => "fonts_show_call_to_action_bar_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

			$options[] = array( "name" => "",
								"desc" => "Show testimonial shortcode person title in uppercase?",
								"id" => "fonts_show_testimonial_title_uppercase",
								"std" => "1",
								"type" => "checkbox",
								"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "type" => "toggle-end",
							"location" => SKINS_MANAGER_PAGE);	

	$options[] = array( "name" => "Header positioning",
						"type" => "heading",
						"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Header height",
							"desc" => "px",
							"id" => "skin_header_height",
							"std" => "121",
							"type" => "text",
							"class" => "mini",
							"location" => SKINS_MANAGER_PAGE);

		$options[] = array( "name" => "Logo top margin",
							"desc" => "px",
							"id" => "skin_logo_top_margin",
							"std" => "50",
							"type" => "text",
							"class" => "mini",
							"location" => SKINS_MANAGER_PAGE);
			
		$options[] = array( "name" => "Navigation menu top margin",
							"desc" => "px",
							"id" => "skin_menu_bar_top_margin",
							"std" => "42",
							"type" => "text",
							"class" => "mini",
							"location" => SKINS_MANAGER_PAGE);

	// SIDEBAR MANAGER -------------------------------------------------------------------
	$options[] = array( "name" => "",
						"type" => "heading",
						"location" => SIDEBAR_MANAGER_PAGE);
							
		$options[] = array( "name" => "Create new custom sidebar:",
							"desc" => "",
							"id" => "sidebar_create",
							"type" => "sidebar_create",
							"class" => "mini",
							"std" => "",
							"location" => SIDEBAR_MANAGER_PAGE);

		$options[] = array( "name" => "Available custom sidebars:",
							"desc" => "",
							"id" => "sidebar_list",
							"type" => "sidebar_list",
							"std" => "",
							"location" => SIDEBAR_MANAGER_PAGE);

	// FONTS MANAGER -------------------------------------------------------------------
	$options[] = array( "name" => "Google Web Fonts Settings",
						"type" => "heading",
						"location" => FONTS_MANAGER_PAGE);

		$options[] = array( "name" => "Update available Google web fonts",
							"desc" => "The number of available fonts in the Google web font directory is constantly being updated, hit the \"Update\" button to grab the latest list of fonts. You can preview the available fonts in the <a href=\"http://www.google.com/webfonts\">Google web fonts directory</a>.",
							"id" => "fonts_update_google",
							"type" => "fonts_update_google",
							"std" => "",
							"location" => FONTS_MANAGER_PAGE);

		$options[] = array( "name" => "Include the following font subsets:",
							"desc" => "NOTE: not all fonts support all subsets, for more information please check the <a href=\"http://www.google.com/webfonts\">Google web fonts directory</a>.",
							"id" => "fonts_subsets_google",
							"std" => "",
							"type" => "multicheck",
							"options" => $font_subsets_defaults,
							"location" => FONTS_MANAGER_PAGE);

	$options[] = array( "name" => "Cufon Fonts Settings",
						"type" => "heading",
						"location" => FONTS_MANAGER_PAGE);

		$options[] = array( "name" => "Add new Cufon font:",
							"desc" => "",
							"id" => "cufon_upload",
							"type" => "cufon_upload",
							"class" => "mini",
							"location" => FONTS_MANAGER_PAGE);

	return $options;
}
?>
