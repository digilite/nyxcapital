<?php
/**
 * The theme's image sizes definitions and helper functions
 */

global $unisphere_options;


/**
 * Add thumbnail support
 */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
}
 

/**
 * Set the different image sizes for the images used in several places across the theme
 */

// sub-header slider image size
define( 'UNISPHERE_SLIDER_W', 960 ); 
define( 'UNISPHERE_SLIDER_H', !empty( $unisphere_options['advanced_image_size_header_slider_height'] ) ? $unisphere_options['advanced_image_size_header_slider_height'] : 450 ); 
define( 'UNISPHERE_SLIDER_CROP', $unisphere_options['advanced_image_size_sub_header_slider_crop'] ); 

// sub-header fullwidth slider image size
define( 'UNISPHERE_SLIDER_FULLWIDTH_W', 1920 );
define( 'UNISPHERE_SLIDER_FULLWIDTH_H', !empty( $unisphere_options['advanced_image_size_header_full_width_slider_height'] ) ? $unisphere_options['advanced_image_size_header_full_width_slider_height'] : 450 ); 
define( 'UNISPHERE_SLIDER_FULLWIDTH_CROP', $unisphere_options['advanced_image_size_sub_header_full_width_slider_crop'] ); 

// sub-header image size
define( 'UNISPHERE_SUB_HEADER_W', 1920 ); 
define( 'UNISPHERE_SUB_HEADER_H', !empty( $unisphere_options['advanced_image_size_sub_header_bar_height'] ) ? $unisphere_options['advanced_image_size_sub_header_bar_height'] : 120 ); 
define( 'UNISPHERE_SUB_HEADER_CROP', $unisphere_options['advanced_image_size_sub_header_bar_crop'] ); 

// 1 column portfolio thumb size
define( 'UNISPHERE_PORTFOLIO1_W', 480 );
define( 'UNISPHERE_PORTFOLIO1_H', $unisphere_options['advanced_image_size_1_column_portfolio_height'] ); 
define( 'UNISPHERE_PORTFOLIO1_CROP', $unisphere_options['advanced_image_size_1_column_portfolio_crop'] ); 

// 2 column portfolio thumb size
define( 'UNISPHERE_PORTFOLIO2_W', 470 );
define( 'UNISPHERE_PORTFOLIO2_H', $unisphere_options['advanced_image_size_2_column_portfolio_height'] ); 
define( 'UNISPHERE_PORTFOLIO2_CROP', $unisphere_options['advanced_image_size_2_column_portfolio_crop'] ); 

// 2 column portfolio without gutter thumb size
define( 'UNISPHERE_PORTFOLIO2_NO_GUTTER_W', 480 );
define( 'UNISPHERE_PORTFOLIO2_NO_GUTTER_H', $unisphere_options['advanced_image_size_2_column_portfolio_no_gutter_height'] ); 
define( 'UNISPHERE_PORTFOLIO2_NO_GUTTER_CROP', $unisphere_options['advanced_image_size_2_column_portfolio_no_gutter_crop'] ); 

// 3 column portfolio thumb size
define( 'UNISPHERE_PORTFOLIO3_W', 300 );
define( 'UNISPHERE_PORTFOLIO3_H', $unisphere_options['advanced_image_size_3_column_portfolio_height'] );
define( 'UNISPHERE_PORTFOLIO3_CROP', $unisphere_options['advanced_image_size_3_column_portfolio_crop'] ); 

// 3 column portfolio without gutter thumb size
define( 'UNISPHERE_PORTFOLIO3_NO_GUTTER_W', 320 );
define( 'UNISPHERE_PORTFOLIO3_NO_GUTTER_H', $unisphere_options['advanced_image_size_3_column_portfolio_no_gutter_height'] );
define( 'UNISPHERE_PORTFOLIO3_NO_GUTTER_CROP', $unisphere_options['advanced_image_size_3_column_portfolio_no_gutter_crop'] ); 

// 4 column portfolio thumb size
define( 'UNISPHERE_PORTFOLIO4_W', 225 );
define( 'UNISPHERE_PORTFOLIO4_H', $unisphere_options['advanced_image_size_4_column_portfolio_height'] );
define( 'UNISPHERE_PORTFOLIO4_CROP', $unisphere_options['advanced_image_size_4_column_portfolio_crop'] ); 

// 4 column portfolio without gutter thumb size
define( 'UNISPHERE_PORTFOLIO4_NO_GUTTER_W', 240 );
define( 'UNISPHERE_PORTFOLIO4_NO_GUTTER_H', $unisphere_options['advanced_image_size_4_column_portfolio_no_gutter_height'] );
define( 'UNISPHERE_PORTFOLIO4_NO_GUTTER_CROP', $unisphere_options['advanced_image_size_4_column_portfolio_no_gutter_crop'] ); 

// portfolio detail image size
define( 'UNISPHERE_PORTFOLIO_DETAIL_W', 960 );
define( 'UNISPHERE_PORTFOLIO_DETAIL_H', $unisphere_options['advanced_image_size_portfolio_detail_big_images_height'] ); 
define( 'UNISPHERE_PORTFOLIO_DETAIL_CROP', $unisphere_options['advanced_image_size_portfolio_detail_big_images_crop'] ); 

// blog (wide image) thumb size
define( 'UNISPHERE_BLOG_WIDE_IMAGE_W', 710 );
define( 'UNISPHERE_BLOG_WIDE_IMAGE_H', $unisphere_options['advanced_image_size_blog_wide_image_height'] );
define( 'UNISPHERE_BLOG_WIDE_IMAGE_CROP', $unisphere_options['advanced_image_size_blog_wide_image_crop'] ); 

// blog (half image) thumb size
define( 'UNISPHERE_BLOG_HALF_IMAGE_W', 340 );
define( 'UNISPHERE_BLOG_HALF_IMAGE_H', $unisphere_options['advanced_image_size_blog_half_image_height'] );
define( 'UNISPHERE_BLOG_HALF_IMAGE_CROP', $unisphere_options['advanced_image_size_blog_half_image_crop'] );

// blog detail page thumb size
define( 'UNISPHERE_BLOG_DETAIL_W', 710 );
define( 'UNISPHERE_BLOG_DETAIL_H', $unisphere_options['advanced_image_size_blog_detail_height'] );
define( 'UNISPHERE_BLOG_DETAIL_CROP', $unisphere_options['advanced_image_size_blog_detail_crop'] ); 

// sidebar posts thumb size
define( 'UNISPHERE_SIDEBAR_POSTS_W', 210 );
define( 'UNISPHERE_SIDEBAR_POSTS_H', $unisphere_options['advanced_image_size_sidebar_posts_height'] );
define( 'UNISPHERE_SIDEBAR_POSTS_CROP', $unisphere_options['advanced_image_size_sidebar_posts_crop'] ); 

// gallery thumb size
define( 'UNISPHERE_GALLERY_W', 192 );
define( 'UNISPHERE_GALLERY_H', $unisphere_options['advanced_image_size_gallery_height'] );
define( 'UNISPHERE_GALLERY_CROP', $unisphere_options['advanced_image_size_gallery_crop'] ); 

// shortcode slider image size
define( 'UNISPHERE_SLIDER_SHORTCODE_W', 960 );
define( 'UNISPHERE_SLIDER_SHORTCODE_H', $unisphere_options['advanced_image_size_slider_shortcode_height'] );
define( 'UNISPHERE_SLIDER_SHORTCODE_CROP', $unisphere_options['advanced_image_size_slider_shortcode_crop'] ); 

/**
 * Get thumbnail based on the context passed as a parameter
 * If there are images defined in custom fields, return these instead of the default thumbnail
 */
function unisphere_get_post_image( $context, $post_id = -1 )
{
	global $post;
	
	if( $post_id != -1 ) {
		$current_post = get_post( $post_id );
	} else {
		$current_post = $post;
	}
	
	$attachment_id = get_post_thumbnail_id( $current_post->ID ); // Attachment ID
	$attachment = get_post( $attachment_id ); // The attachment object
	$attachment_title = $attachment->post_title; // The attachment (image) title
	$attachment_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true); // The attachment (image) alt text
	$attachment_caption = $attachment->post_excerpt;
	$post_title = $current_post->post_title; // The current post title
	
	switch ( $context ) {
		
		case "slider":
			$image = unisphere_resize( $attachment_id, null, UNISPHERE_SLIDER_W, UNISPHERE_SLIDER_H, UNISPHERE_SLIDER_CROP );
			return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			break;

		case "slider-fullwidth":
			$image = unisphere_resize( $attachment_id, null, UNISPHERE_SLIDER_FULLWIDTH_W, UNISPHERE_SLIDER_FULLWIDTH_H, UNISPHERE_SLIDER_FULLWIDTH_CROP );
			return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			break;

		case "portfolio1":
			if( get_post_meta($current_post->ID, "_portfolio_thumb_1", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_thumb_1", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_PORTFOLIO1_W, UNISPHERE_PORTFOLIO1_H, UNISPHERE_PORTFOLIO1_CROP );
				return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			}
			break;

		case "portfolio2":
			if( get_post_meta($current_post->ID, "_portfolio_thumb_2", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_thumb_2", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_PORTFOLIO2_W, UNISPHERE_PORTFOLIO2_H, UNISPHERE_PORTFOLIO2_CROP );
				return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			}
			break;
		
		case "portfolio2-no-gutter":
			if( get_post_meta($current_post->ID, "_portfolio_thumb_2_no_gutter", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_thumb_2_no_gutter", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_PORTFOLIO2_NO_GUTTER_W, UNISPHERE_PORTFOLIO2_NO_GUTTER_H, UNISPHERE_PORTFOLIO2_NO_GUTTER_CROP );
				return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			}
			break;

		case "portfolio3":
			if( get_post_meta($current_post->ID, "_portfolio_thumb_3", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_thumb_3", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_PORTFOLIO3_W, UNISPHERE_PORTFOLIO3_H, UNISPHERE_PORTFOLIO3_CROP );
				return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			}
			break;

		case "portfolio3-no-gutter":
			if( get_post_meta($current_post->ID, "_portfolio_thumb_3_no_gutter", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_thumb_3_no_gutter", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_PORTFOLIO3_NO_GUTTER_W, UNISPHERE_PORTFOLIO3_NO_GUTTER_H, UNISPHERE_PORTFOLIO3_NO_GUTTER_CROP );
				return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			}
			break;

		case "portfolio4":
			if( get_post_meta($current_post->ID, "_portfolio_thumb_4", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_thumb_4", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_PORTFOLIO4_W, UNISPHERE_PORTFOLIO4_H, UNISPHERE_PORTFOLIO4_CROP );
				return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			}
			break;

		case "portfolio4-no-gutter":
			if( get_post_meta($current_post->ID, "_portfolio_thumb_4_no_gutter", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_thumb_4_no_gutter", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_PORTFOLIO4_NO_GUTTER_W, UNISPHERE_PORTFOLIO4_NO_GUTTER_H, UNISPHERE_PORTFOLIO4_NO_GUTTER_CROP );
				return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
			}
			break;

		case "blog-wide-image":
			if( get_post_meta($current_post->ID, "_blog_wide_image_thumb", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_blog_wide_image_thumb", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_BLOG_WIDE_IMAGE_W, UNISPHERE_BLOG_WIDE_IMAGE_H, UNISPHERE_BLOG_WIDE_IMAGE_CROP );
				if( $image['url'] != '' )
					return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
				else
					return '';
			}
			break;

		case "blog-half-image":
			if( get_post_meta($current_post->ID, "_blog_half_image_thumb", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_blog_half_image_thumb", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_BLOG_HALF_IMAGE_W, UNISPHERE_BLOG_HALF_IMAGE_H, UNISPHERE_BLOG_HALF_IMAGE_CROP );
				if( $image['url'] != '' )
					return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
				else
					return '';
			}
			break;

		case "blog-detail":
			if( get_post_meta($current_post->ID, "_blog_detail_thumb", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_blog_detail_thumb", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_BLOG_DETAIL_W, UNISPHERE_BLOG_DETAIL_H, UNISPHERE_BLOG_DETAIL_CROP );
				if( $image['url'] != '' )
					return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
				else
					return '';
			}
			break;

		case "sidebar-post":
			if( get_post_meta($current_post->ID, "_blog_sidebar_posts_thumb", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_blog_sidebar_posts_thumb", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_SIDEBAR_POSTS_W, UNISPHERE_SIDEBAR_POSTS_H, UNISPHERE_SIDEBAR_POSTS_CROP );
				if( $image['url'] != '' )
					return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
				else
					return '';
			}
			break;

		case "sidebar-portfolio-item":
			if( get_post_meta($current_post->ID, "_portfolio_sidebar_posts_thumb", $single = true) != '') { 
				return '<img alt="' . $current_post->post_title . '" src="' . get_post_meta($current_post->ID, "_portfolio_sidebar_posts_thumb", $single = true) . '" />'; 
			} else {
				$image = unisphere_resize( $attachment_id, null, UNISPHERE_SIDEBAR_POSTS_W, UNISPHERE_SIDEBAR_POSTS_H, UNISPHERE_SIDEBAR_POSTS_CROP );
				if( $image['url'] != '' )
					return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';
				else
					return '';
			}
			break;
	}
}


/**
 * Get thumbnail based on the URL passed as a parameter
 */
function unisphere_get_post_image_by_url( $url, $url_only = false, $alt = "", $width, $height )
{
	$image = unisphere_resize( null, $url, $width, $height, UNISPHERE_SUB_HEADER_CROP );

	if( $url_only )
		return $image['url'];
	else
		return '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $alt . '" />';
}


/*
 * Resize images dynamically using wp built in functions
 * Victor Teixeira
 * http://core.trac.wordpress.org/ticket/15311#comment:13
 *
 * php 5.2+
 *
 * Example usage:
 * 
 * <?php 
 * $thumb = get_post_thumbnail_id(); 
 * $image = vt_resize( $thumb, '', 140, 110, true );
 * ?>
 * <img src="<?php echo $image[url]; ?>" width="<?php echo $image[width]; ?>" height="<?php echo $image[height]; ?>" />
 *
 * @param int $attach_id
 * @param string $img_url
 * @param int $width
 * @param int $height
 * @param bool $crop
 * @return array
 */
function unisphere_resize( $attach_id = null, $img_url = null, $width, $height, $crop = false ) {
	
	if ( !$attach_id && !$img_url )
		return null;

	// this is an attachment, so we have the ID
	if ( $attach_id ) {
	
		$image_src = wp_get_attachment_image_src( $attach_id, 'full' );
		$file_path = get_attached_file( $attach_id );
	
	// this is not an attachment, let's use the image url
	} else if ( $img_url ) {
		
		$file_path = parse_url( $img_url );
		$file_path = $_SERVER['DOCUMENT_ROOT'] . $file_path['path'];

		$orig_size = @getimagesize( $file_path );
		$image_src[0] = $img_url;
		$image_src[1] = $orig_size[0];
		$image_src[2] = $orig_size[1];
	}
	
	$file_info = pathinfo( $file_path );
	$extension = '.'. $file_info['extension'];

	// the image path without the extension
	$no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];

	$cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.($crop?'-crop':'').$extension;

	// checking if the file size is larger than the target size
	// if it is smaller or the same size, stop right here and return
	if ( $image_src[1] > $width || $image_src[2] > $height ) {

		// the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
		if ( file_exists( $cropped_img_path ) ) {

			$cropped_img_url = str_replace( basename( $image_src[0] ), basename( $cropped_img_path ), $image_src[0] );
			$img_size = getimagesize( $cropped_img_path ); 
			$vt_image = array (
				'url' => $cropped_img_url,
				'width' => $img_size[0], 
				'height' => $img_size[1] 
			);
			
			return $vt_image;
		}

		if ( $crop == false ) {
		
			// calculate the size proportionaly
			$proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
			$resized_img_path = $no_ext_path.'-'.$proportional_size[0].'x'.$proportional_size[1].$extension;			

			// checking if the file already exists
			if ( file_exists( $resized_img_path ) ) {
			
				$resized_img_url = str_replace( basename( $image_src[0] ), basename( $resized_img_path ), $image_src[0] );

				$vt_image = array (
					'url' => $resized_img_url,
					'width' => $proportional_size[0],
					'height' => $proportional_size[1]
				);
				
				return $vt_image;
			}
		}

		// no cache files - let's finally resize it
		$new_img_path = image_resize( $file_path, $width, $height, $crop, ($crop ? $width.'x'.$height.'-crop' : '') );
		$new_img_size = getimagesize( $new_img_path );
		$new_img = str_replace( basename( $image_src[0] ), basename( $new_img_path ), $image_src[0] );

		// resized output
		$vt_image = array (
			'url' => $new_img,
			'width' => $new_img_size[0],
			'height' => $new_img_size[1]
		);
		
		return $vt_image;
	}

	// default output - without resizing
	$vt_image = array (
		'url' => $image_src[0],
		'width' => $image_src[1],
		'height' => $image_src[2]
	);
	
	return $vt_image;
}

?>
