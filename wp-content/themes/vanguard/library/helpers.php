<?php
/**
 * Several helper functions used in the theme
 */


/**
 * Get root parent of a page
 */
function get_root_page($page_id) 
{
	global $wpdb;
	
	$parent = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE post_type='page' AND ID = '$page_id'");
	
	if ($parent == 0) 
		return $page_id;
	else 
		return get_root_page($parent);
}


/**
 * Get page ID by Custom Field Value
 */
function get_page_ID_by_custom_field_value($custom_field, $value)
{
	global $wpdb;
	$page_ID = $wpdb->get_var("
	    SELECT wposts.ID
    	FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
	    WHERE wposts.ID = wpostmeta.post_id 
    	AND wpostmeta.meta_key = '$custom_field' 
	    AND (wpostmeta.meta_value like '$value,%' OR wpostmeta.meta_value like '%,$value,%' OR wpostmeta.meta_value like '%,$value' OR wpostmeta.meta_value = '$value')		
    	AND wposts.post_status = 'publish' 
	    AND wposts.post_type = 'page'
		LIMIT 0, 1");

	return $page_ID;
}

function startsWith( $str, $sub ) {
    $length = strlen($sub);
    return (substr($str, 0, $length) === $sub);
}

function endsWith( $str, $sub ) {
   return ( substr( $str, strlen( $str ) - strlen( $sub ) ) === $sub );
}

function remove_item_by_value($array, $val = '', $preserve_keys = true) {
	if (empty($array) || !is_array($array)) return false;
	if (!in_array($val, $array)) return $array;

	foreach($array as $key => $value) {
		if ($value == $val) unset($array[$key]);
	}

	return ($preserve_keys === true) ? $array : array_values($array);
}

function contains($substring, $string) {
    $pos = strpos($string, $substring);

    if($pos === false) {
        // string needle NOT found in haystack
        return false;
    }
    else {
        // string needle found in haystack
        return true;
    }
}

function unisphere_wp_get_theme() {
	if (function_exists('wp_get_theme')) {
		$theme_data = wp_get_theme();
	} else {
		$theme_data = get_theme_data(get_template_directory() . '/style.css');	
	}

	return $theme_data;
}
?>
