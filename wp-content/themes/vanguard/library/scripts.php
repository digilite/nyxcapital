<?php
/**
 * unisphere_scripts() enqueues scripts and styles
 */
if ( !function_exists( 'unisphere_scripts' ) ) {
function unisphere_scripts() {
	
	global $unisphere_options;
	
	if( is_admin() || in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php')) ) return;

	// The skin CSS file
	wp_register_style( 'skin', UNISPHERE_CSS . '/skin.php' ); 
	wp_enqueue_style( 'skin' );

	// The Mobile CSS file
	wp_register_style( 'mobile', UNISPHERE_CSS . '/mobile.css', array(), false, 'screen' ); 
	wp_enqueue_style( 'mobile');  

	// Google maps api
	wp_register_script( 'gmaps', ("http://maps.google.com/maps/api/js?sensor=false"), false, ''); 
	wp_enqueue_script( 'gmaps' );
	
	// swfobject (required for slider in IE)
	wp_enqueue_script( 'swfobject' );

	// Theme script dependencies
	wp_enqueue_script( 'scripts', UNISPHERE_JS . '/scripts.js', array( 'jquery' ) ); 

	// Load Cufon fonts if any (from /library/fonts.php)
	unisphere_enqueue_google_fonts();

	// Load Google Web fonts if any (from /library/fonts.php)
	unisphere_enqueue_cufon_fonts();
	
	// Theme custom scripts loaded in the footer
	wp_enqueue_script( 'screen', UNISPHERE_JS . '/screen.js', array( 'jquery' ), '1.0', true ); 
}
}

add_action( 'wp_enqueue_scripts', 'unisphere_scripts' );