<?php
/**
 * Provides a notification everytime the theme is updated
 */
 
function unisphere_update_notifier_menu() {  
	if (function_exists('simplexml_load_string')) {
	    $xml = get_latest_theme_version(21600);
		$theme_data = unisphere_wp_get_theme();

		if( version_compare( $theme_data['Version'], $xml->latest ) < 0 ) {
			add_dashboard_page( UNISPHERE_THEMENAME . ' Theme Updates', UNISPHERE_THEMENAME . ' <span class="update-plugins count-1"><span class="update-count">New Updates</span></span>', 'administrator', UNISPHERE_THEMESHORTNAME . '-updates', 'unisphere_update_notifier');
		}
	}	
}

add_action('admin_menu', 'unisphere_update_notifier_menu');  

function unisphere_update_notifier() { 
	$xml = get_latest_theme_version(21600);
	$theme_data = unisphere_wp_get_theme(); ?>
	
	<style>
		.update-nag { display: none; }
		#instructions {max-width: 670px;}
		h3.title {margin: 30px 0 0 0; padding: 30px 0 0 0; border-top: 1px solid #ddd;}
	</style>

	<div class="wrap">
	
		<div id="icon-tools" class="icon32"></div>
		<h2><?php echo UNISPHERE_THEMENAME ?> Theme Updates</h2>
	    <div id="message" class="updated below-h2"><p><strong>There is a new version of the <?php echo UNISPHERE_THEMENAME; ?> theme available.</strong> You have version <?php echo $theme_data['Version']; ?> installed. Update to version <?php echo $xml->latest; ?></p></div>

		<img style="float: left; margin: 0 20px 20px 0; border: 1px solid #ddd;" src="<?php echo get_template_directory_uri() . '/screenshot.png'; ?>" />
		
		<div id="instructions">
		    <h3>Update Download and Instructions</h3>
		    <p style="background-color: #FFEBE8; border: 1px solid #CC0000; margin-left: 323px; padding: 12px; border-radius: 3px;"><strong>Important:</strong> make sure to create a <strong>backup</strong> of the theme folder found in your WordPress installation folder <strong>/wp-content/themes/<?php echo UNISPHERE_THEMESHORTNAME; ?>/</strong> in case something goes wrong during the update process.</p>
		    <p>To update the Theme, login to <a href="http://www.themeforest.net/">ThemeForest</a>, head over to your <strong>downloads</strong> section and re-download the theme like you did when you first purchased it.</p>
		    <p>Extract the zip's contents, look for the extracted theme folder, and after you have all the new files upload them using FTP to the <strong>/wp-content/themes/<?php echo UNISPHERE_THEMESHORTNAME; ?>/</strong> folder overwriting the old ones (this is why it's important to backup any changes you've made to the theme files).</p>
		    <p>If you didn't make any changes to the theme files, you are free to overwrite them with the new ones without the risk of losing theme settings, pages, posts, etc, and backwards compatibility is guaranteed.</p>
		</div>
	    
	    <h3 class="title">Changelog</h3>
	    <?php echo $xml->changelog; ?>

	</div>
    
<?php } 

function get_latest_theme_version($interval) {
	if( false === ( $notifier_data = get_transient( UNISPHERE_THEMEOPTIONS . '-notifier-cache' ) ) ) {
    	$response = wp_remote_get( UNISPHERE_NOTIFIER_FILE );
		if( is_wp_error( $response ) ) {
		    $notifier_data = '<?xml version="1.0" encoding="UTF-8"?><notifier><latest>1.0</latest><changelog></changelog></notifier>';
		} else {
			$notifier_data = $response['body'];
		}

		set_transient( UNISPHERE_THEMEOPTIONS . '-notifier-cache', $notifier_data, $interval );
	}

	// Let's see if the $xml data was returned as we expected it to
	if( strpos((string)$notifier_data, '<notifier>') === false ) {
		$notifier_data = '<?xml version="1.0" encoding="UTF-8"?><notifier><latest>1.0</latest><changelog></changelog></notifier>';
	}
	
	$xml = simplexml_load_string($notifier_data); 
	
	return $xml;
}

?>
