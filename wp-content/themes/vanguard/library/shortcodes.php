<?php
/**
 * This function pushes shortcodes earlier in the queue so their content doesn't get texturized
 */
function unisphere_run_shortcode( $content ) {
    global $shortcode_tags;
 
    // Backup current registered shortcodes and clear them all out
    $orig_shortcode_tags = $shortcode_tags;

    $shortcode_tags = array();

	add_shortcode('one_half', 'unisphere_one_half');
	add_shortcode('one_half_last', 'unisphere_one_half_last');
	add_shortcode('one_third', 'unisphere_one_third');
	add_shortcode('one_third_last', 'unisphere_one_third_last');
	add_shortcode('two_third', 'unisphere_two_third');
	add_shortcode('two_third_last', 'unisphere_two_third_last');
	add_shortcode('one_fourth', 'unisphere_one_fourth');
	add_shortcode('one_fourth_last', 'unisphere_one_fourth_last');
	add_shortcode('three_fourth', 'unisphere_three_fourth');
	add_shortcode('three_fourth_last', 'unisphere_three_fourth_last');	
	add_shortcode('one_fifth', 'unisphere_one_fifth');
	add_shortcode('one_fifth_last', 'unisphere_one_fifth_last');
	add_shortcode('two_fifth', 'unisphere_two_fifth');
	add_shortcode('two_fifth_last', 'unisphere_two_fifth_last');
	add_shortcode('three_fifth', 'unisphere_three_fifth');
	add_shortcode('three_fifth_last', 'unisphere_three_fifth_last');
	add_shortcode('four_fifth', 'unisphere_four_fifth');
	add_shortcode('four_fifth_last', 'unisphere_four_fifth_last');	
	add_shortcode('one_sixth', 'unisphere_one_sixth');
	add_shortcode('one_sixth_last', 'unisphere_one_sixth_last');	
	add_shortcode('five_sixth', 'unisphere_five_sixth');
	add_shortcode('five_sixth_last', 'unisphere_five_sixth_last');	
	add_shortcode('hr', 'unisphere_hr_shortcode');	
	add_shortcode('dropcap', 'unisphere_dropcap_shortcode');
	add_shortcode('list', 'unisphere_list_shortcode');	
	add_shortcode('table', 'unisphere_table_shortcode');	
	add_shortcode('image', 'unisphere_image_shortcode');
	add_shortcode('slider', 'unisphere_slider_shortcode');
	add_shortcode('testimonial', 'unisphere_testimonial_shortcode');
	add_shortcode('button', 'unisphere_button_shortcode');
	add_shortcode('blockquote', 'unisphere_blockquote_shortcode');
	add_shortcode('call_to_action_big', 'unisphere_call_to_action_big_shortcode');
	add_shortcode('call_to_action_small', 'unisphere_call_to_action_small_shortcode');
	add_shortcode('call_to_action_bar', 'unisphere_call_to_action_bar_shortcode');
	add_shortcode('info_box', 'unisphere_info_box_shortcode');
	add_shortcode('tabs', 'unisphere_tabs_shortcode');
	add_shortcode('tab', 'unisphere_tab_shortcode');
	add_shortcode('toggle', 'unisphere_toggle_shortcode');
	add_shortcode('price_table', 'unisphere_price_table_shortcode');
	add_shortcode('price_column', 'unisphere_price_column_shortcode');
	add_shortcode('price_tag', 'unisphere_price_tag_shortcode');
	add_shortcode('highlight', 'unisphere_highlight_shortcode');
	add_shortcode('video', 'unisphere_video_shortcode');
	add_shortcode('video_lightbox', 'unisphere_video_lightbox_shortcode');	
	add_shortcode('lightbox', 'unisphere_lightbox_shortcode');
	add_shortcode('recent_posts', 'unisphere_recent_posts_shortcode');
	add_shortcode('popular_posts', 'unisphere_popular_posts_shortcode');
	add_shortcode('recent_portfolio', 'unisphere_recent_portfolio_shortcode');
	add_shortcode('popular_portfolio', 'unisphere_popular_portfolio_shortcode');
	add_shortcode('portfolio', 'unisphere_portfolio_shortcode');	
	add_shortcode('twitter', 'unisphere_twitter_shortcode');
	add_shortcode('flickr', 'unisphere_flickr_shortcode');
	add_shortcode('map', 'unisphere_map_shortcode');
	add_shortcode('title_big', 'unisphere_title_big_shortcode');
	add_shortcode('title_small', 'unisphere_title_small_shortcode');
	add_shortcode('wide_bar', 'unisphere_wide_bar_shortcode');
	add_shortcode('icon', 'unisphere_icon_shortcode');

	// Creates a space between two consecutive shortcodes (otherwise causes unexpected bugs in shortcode parsing)
	$content = preg_replace('/\]\[/im', "] [", $content);

    // Do the shortcode (only the one above is registered)
    $content = do_shortcode( $content );
 
    // Put the original shortcodes back
    $shortcode_tags = $orig_shortcode_tags;
 
    return $content;
} 
add_filter( 'the_content', 'unisphere_run_shortcode', 7 );
add_filter( 'widget_text', 'unisphere_run_shortcode', 7 );
add_filter( 'widget_text', 'do_shortcode', 8 );

/**
 * Column Shortcodes
 */

// Half
function unisphere_one_half($atts, $content=null) {
   return '<div class="one-half">' . do_shortcode($content) . '</div>';
}

function unisphere_one_half_last($atts, $content=null) {
   return '<div class="one-half last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}


// Third
function unisphere_one_third($atts, $content=null) {
   return '<div class="one-third">' . do_shortcode($content) . '</div>';
}

function unisphere_one_third_last($atts, $content=null) {
   return '<div class="one-third last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}

function unisphere_two_third($atts, $content=null) {
   return '<div class="two-third">' . do_shortcode($content) . '</div>';
}

function unisphere_two_third_last($atts, $content=null) {
   return '<div class="two-third last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}


// Fourth
function unisphere_one_fourth($atts, $content=null) {
   return '<div class="one-fourth">' . do_shortcode($content) . '</div>';
}

function unisphere_one_fourth_last($atts, $content=null) {
   return '<div class="one-fourth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}

function unisphere_three_fourth($atts, $content=null) {
   return '<div class="three-fourth">' . do_shortcode($content) . '</div>';
}

function unisphere_three_fourth_last($atts, $content=null) {
   return '<div class="three-fourth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}


// Fifth
function unisphere_one_fifth($atts, $content=null) {
   return '<div class="one-fifth">' . do_shortcode($content) . '</div>';
}

function unisphere_one_fifth_last($atts, $content=null) {
   return '<div class="one-fifth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}

function unisphere_two_fifth($atts, $content=null) {
   return '<div class="two-fifth">' . do_shortcode($content) . '</div>';
}

function unisphere_two_fifth_last($atts, $content=null) {
   return '<div class="two-fifth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}

function unisphere_three_fifth($atts, $content=null) {
   return '<div class="three-fifth">' . do_shortcode($content) . '</div>';
}

function unisphere_three_fifth_last($atts, $content=null) {
   return '<div class="three-fifth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}

function unisphere_four_fifth($atts, $content=null) {
   return '<div class="four-fifth">' . do_shortcode($content) . '</div>';
}

function unisphere_four_fifth_last($atts, $content=null) {
   return '<div class="four-fifth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}


// Sixth
function unisphere_one_sixth($atts, $content=null) {
   return '<div class="one-sixth">' . do_shortcode($content) . '</div>';
}

function unisphere_one_sixth_last($atts, $content=null) {
   return '<div class="one-sixth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}

function unisphere_five_sixth($atts, $content=null) {
   return '<div class="five-sixth">' . do_shortcode($content) . '</div>';
}

function unisphere_five_sixth_last($atts, $content=null) {
   return '<div class="five-sixth last">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}


/**
 * Horizontal Separator Shortcode
 */
function unisphere_hr_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "totop" => "true"
	), $atts));

	if( $totop == 'true' )
   		return '<div class="hr to-top"><hr /><span class="to-top">' . __('To Top', 'unisphere') . '</span></div>';
   	else
   		return '<div class="hr"><hr /></div>';
}


/**
 * Dropcaps Shortcode
 */
function unisphere_dropcap_shortcode($atts, $content = null) {	
	return '<span class="dropcap">' . do_shortcode($content) . '</span>';
}


/**
 * Lists Shortcodes
 */
function unisphere_list_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "bullet" => ""
	), $atts));

	return '<ul class="list separator bullet-' . $bullet . '">' . $content . '</ul>';
}

/**
 * Table Shortcodes
 */
function unisphere_table_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "style" => ""
	), $atts));

	return '<table class="' . $style . '">' . $content . '</table>';
}


/**
 * Image Shortcode
 */
function unisphere_image_shortcode($atts) {
	extract(shortcode_atts(array(		
		"url" => "",
		"img" => "",
		"alt" => "",
		"lightbox" => "true",
		"group" => "",
		"align" => ""
	), $atts));
	
	if ( $img == '' )
		return NULL;
	
	$img_rel = '';
	if( $lightbox == 'true' )
		$img_rel = 'data-rel="lightbox"';
		
	if( $group != '' && $lightbox == 'true')
		$img_rel = 'data-rel="lightbox[' . $group . ']"';
	
	$align != '' ? $align = 'image-' . $align  : $align = '';
	
	$imageclass = '';	
	if( $align != '') {
		$imageclass = 'class="' . $align . '" ';
	}
	
	$linkclass = 'class="';
	if( $align != '') {
		$linkclass .= $align;
	}
	$linkclass .= '" ';
	
	$output = '';
		
	if( $url != '' ) {
		$output  .=  '<a href="' . $url . '" title="' . $alt . '" ' . $img_rel . ' ' . $linkclass . ' style="overflow: hidden; display: inline-block;"><img src="' . $img . '" alt="' . $alt . '" title="' . $alt . '" ' . $imageclass . ' style="display: block;" /></a>';
	} else {
		$output  .=  '<img src="' . $img . '" alt="' . $alt . '" title="' . $alt . '" ' . $imageclass . '/>';
	}
	
	return $output;
}


/**
 * Slider Shortcode
 */
function unisphere_slider_shortcode($atts) {
	global $post, $unisphere_options;
	extract(shortcode_atts(array(
		"animspeed" => 500,
		"pausetime" => 5000,
		"numberslides" => '10',
		"height" => '',
		"cats" => ''
	), $atts));

	$height = ($height != '' ? intval($height) : intval( !empty( $unisphere_options['advanced_image_size_slider_shortcode_height'] ) ? $unisphere_options['advanced_image_size_slider_shortcode_height'] : 540 ) );
	
	$aspect_ratio = 960 / $height;	

	$sliderId = rand();
	
	if( $cats == '' ) {
		return '<div class="slider-container ' . $size . '"><p class="no-slider-items"><strong>No slider categories have been set for this slider.</strong></p></div>';
	} else {
		$output  = '<div class="slider-container">';
		$output .= '<ul class="slider" id="slider' . $sliderId . '" data-speed="' . $animspeed . '" data-timeout="' . $pausetime . '" data-aspect-ratio="' . $aspect_ratio . '">';

       	$slider_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'slider_category');
					
		$my_query = new WP_Query( array( 'post_type' => 'slider', 'post__in' => $slider_posts_to_query, 'showposts' => $numberslides ) ); 
		while ($my_query->have_posts()) : $my_query->the_post();
				
			$custom = get_post_custom($post->ID);

			// Don't output HTML5/FLV videos if the SublimeVideo plugin isn't installed
			if( !(!class_exists('SublimeVideo') && isset( $custom['_slider_video'][0] ) && ( $custom['_slider_video'][0] == 'html5_flv' || $custom['_slider_video'][0] == 'html5_flv_disabled' ) ) ) {
				$output .= '<li class="' . ( isset($custom['_slider_show_description']) && $custom['_slider_show_description'][0] == '1' ? 'with-description' : 'no-description' ) . ( isset($custom['_slider_video']) && $custom['_slider_video'][0] != 'disabled' ? ' has-video' : '' ) . '">';
				
				// Display Video directly in Slider (takes precedence before everything)
				if ( isset( $custom['_slider_video'][0] ) && $custom['_slider_video'][0] != 'disabled' ) {
					$rand = rand();
					$data_attribute = '';
	                switch ($custom['_slider_video'][0]) {
					    case 'youtube':
					        $data_attribute  = 'data-videourl="' . $custom['_slider_video_youtube'][0] . '"';
					        $data_attribute .= ' data-videotype="youtube"';
					        break;
					    case 'vimeo':
					        $data_attribute  = 'data-videourl="' . $custom['_slider_video_vimeo'][0] . '"';
					        $data_attribute .= ' data-videotype="vimeo"';
					        break;
						case 'html5_flv':
					    	if( isset( $custom['_slider_video_html5_mp4'] ) ) 	{ $data_attribute .= ' data-videourl-mp4="' . $custom['_slider_video_html5_mp4'][0] . '"'; }
					    	if( isset( $custom['_slider_video_html5_mp4_hd'] ) ) { $data_attribute .= ' data-videourl-mp4-hd="' . $custom['_slider_video_html5_mp4_hd'][0] . '"'; }
					    	if( isset( $custom['_slider_video_html5_webm'] ) ) { $data_attribute .= ' data-videourl-webm="' . $custom['_slider_video_html5_webm'][0] . '"'; }
					    	if( isset( $custom['_slider_video_html5_webm_hd'] ) ) { $data_attribute .= ' data-videourl-webm-hd="' . $custom['_slider_video_html5_webm_hd'][0] . '"'; }
					    	if( isset( $custom['_slider_video_flv'] ) ) { $data_attribute .= ' data-videourl-flv="' . $custom['_slider_video_flv'][0] . '"'; }
					    	if( isset( $custom['_slider_video_flv_hd'] ) ) { $data_attribute .= ' data-videourl-flv-hd="' . $custom['_slider_video_flv_hd'][0] . '"'; }
					        $data_attribute .= ' data-videotype="html5_flv"';
					        break;
					}
					$output .= '<div class="slider-video" id="video' . $rand . '" ' . $data_attribute . ' data-autoplay="' . ( isset($custom['_slider_autoplay']) && $custom['_slider_autoplay'][0] == '1' ? 'false' : 'true' ) . '" data-video-thumbnail="' . ( isset($custom['_slider_no_autoplay_thumbnail']) ? $custom['_slider_no_autoplay_thumbnail'][0] : '' ) . '"><div id="objectvideo' . $rand . '"></div></div>';
				} else {

					global $post;
					$attachment_id = get_post_thumbnail_id( $post->ID ); // Attachment ID
					$attachment = get_post( $attachment_id ); // The attachment object
					$attachment_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true); // The attachment (image) alt text
        			$image = unisphere_resize( $attachment_id, null, 960, $height, true );
					$imageHtml = '<img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="' . $attachment_alt . '" />';

					// ...check if there's a link for the slider item
					if ( isset( $custom['_slider_link'][0] ) ) {
						$output .= '<div class="slider-image"><a href="' . $custom['_slider_link'][0] . '" title="' . get_the_title() . '">' . $imageHtml . '</a></div>';
					} else {
						$output .= '<div class="slider-image">' . $imageHtml . '</div>';
					}
				}
				
				$output .= '<div class="slider-description">';
				if( isset($custom['_slider_show_title']) && $custom['_slider_show_title'][0] == '1' ) {
					$output .= '<div class="slider-title"><div class="bar"></div>' . get_the_title() . '</div>';
				}
				if( get_the_content() != '') {
					$output .= '<div class="description">' . do_shortcode( get_the_content() ) . '</div>';
				}
				$output .= '</div>';
				$output .= '</li>';
			}
		endwhile;

		if( is_single() )
			wp_reset_query();
		
		$output .= '	</ul>';
		$output .= '</div>';
		
		return $output;
	}
}


/**
 * Testimonials Shortcode
 */
function unisphere_testimonial_shortcode($atts, $content = null) {
	global $post;
	extract(shortcode_atts(array(		
		"person" => "",
		"bgcolor" => "",
		"txtcolor" => ""
	), $atts));
	
	$output  = "<div class=\"testimonial-container\">";
	$output .= "<div class=\"testimonial-content\" style=\"" . ($bgcolor != '' ? 'background-color: ' . $bgcolor . ';' : '') . ($txtcolor != '' ? 'color: ' . $txtcolor . ';' : '') . "\">";
	$output .= "<p>" . do_shortcode($content) . "</p>";
	$output .= "<div class=\"triangle\" style=\"" . ($bgcolor != '' ? 'border-top: 20px solid ' . $bgcolor . ';' : '') . "\"></div>";
	$output .= "</div>";
	$output .= "<div class=\"testimonial-meta\">";
	$output .= "<span class=\"testimonial-person\">" . $person . "</span>";
	$output .= "</div>";
	$output .= "</div>";

	return $output;
}


/**
 * Button Shortcodes
 */
function unisphere_button_shortcode($atts) {
	extract(shortcode_atts(array(		
		"text" => "",
		"url" => "",
		"size" => "small", // small, medium, large
		"newwindow" => "",
		"txtcolor" => "",
		"bgcolor" => "",
		"txtcolorhover" => "",
		"bgcolorhover" => ""		
	), $atts));
	
	if ( $newwindow == 'true' ) { $newwindow = ' data-newwindow="true"'; } else { $newwindow = ''; }
	if ( $txtcolor != '' ) { $txtcolor = ' data-txtcolor="' . $txtcolor . '"'; } else { $txtcolor = ''; }
	if ( $bgcolor != '' ) { $bgcolor = ' data-bgcolor="' . $bgcolor . '"'; } else { $bgcolor = ''; }	
	if ( $txtcolorhover != '' ) { $txtcolorhover = ' data-txtcolorhover="' . $txtcolorhover . '"'; } else { $txtcolorhover = ''; }
	if ( $bgcolorhover != '' ) { $bgcolorhover = ' data-bgcolorhover="' . $bgcolorhover . '"'; } else { $bgcolorhover = ''; }

	if( $url != '' ) {
		return '<a href="' . $url . '" class="custom-button ' . $size . '"' . $newwindow . $txtcolor . $bgcolor . $txtcolorhover . $bgcolorhover . '>' . $text . '</a>';
	} else {
		return '<button class="custom-button ' . $size . '"' . $newwindow . $txtcolor . $bgcolor . $txtcolorhover . $bgcolorhover . '>' . $text . '</button>';
	}
}


/**
 * Blockquote Shortcodes
 */
function unisphere_blockquote_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "align" => ""
	), $atts));
	
	if ( $align != '' ) {
		return '<blockquote class="align' . $align . '">' . $content . '</blockquote>';
	} else {
		return '<blockquote>' . $content . '</blockquote>';
	}	
}


/**
 * Call to action Big Shortcode
 */
function unisphere_call_to_action_big_shortcode($atts) {
	extract(shortcode_atts(array(		
        "title" => "",
        "excerpt" => "",
        "buttonurl" => "",
		"buttontext" => "",
		"buttonnewwindow" => "",
		"buttontxtcolor" => "",
		"buttonbgcolor" => "",
		"buttontxtcolorhover" => "",
		"buttonbgcolorhover" => ""
	), $atts));

	$output  = '<div class="call-to-action-big">';
	$output .= '<h2>' . $title . '</h2>';
	
	if ( $excerpt != '' )
		$output .= '<p class="excerpt">' . $excerpt . '</p>';

	if ( $buttonurl != '' && $buttontext != '' )
		$output .= '<p>' . do_shortcode('[button text="' . $buttontext . '" url="' . $buttonurl . '" corners="flat" type="normal" newwindow="' . $buttonnewwindow . '" txtcolor="' . $buttontxtcolor . '" bgcolor="' . $buttonbgcolor . '" txtcolorhover="' . $buttontxtcolorhover . '" bgcolorhover="' . $buttonbgcolorhover . '" size="large" /]') . '</p>';
		
	$output .= '</div>';
	
	return $output;
}


/**
 * Call to action Small Shortcode
 */
function unisphere_call_to_action_small_shortcode($atts) {
	extract(shortcode_atts(array(		
        "title" => "",
        "excerpt" => "",
        "buttonurl" => "",
		"buttontext" => "",
		"buttonnewwindow" => "",
		"buttontxtcolor" => "",
		"buttonbgcolor" => "",
		"buttontxtcolorhover" => "",
		"buttonbgcolorhover" => ""
	), $atts));

	$output  = '<div class="call-to-action-small">';
	$output .= '<h2>' . $title . '</h2>';
	
	if ( $excerpt != '' )
		$output .= '<p class="excerpt">' . $excerpt . '</p>';

	if ( $buttonurl != '' && $buttontext != '' )
		$output .= '<p>' . do_shortcode('[button text="' . $buttontext . '" url="' . $buttonurl . '" corners="flat" type="normal" newwindow="' . $buttonnewwindow . '" txtcolor="' . $buttontxtcolor . '" bgcolor="' . $buttonbgcolor . '" txtcolorhover="' . $buttontxtcolorhover . '" bgcolorhover="' . $buttonbgcolorhover . '" size="medium" /]') . '</p>';
		
	$output .= '</div>';
	
	return $output;
}


/**
 * Call to action Bar Shortcode
 */
function unisphere_call_to_action_bar_shortcode($atts) {
	extract(shortcode_atts(array(		
        "title" => "",
        "excerpt" => "",
        "buttonurl" => "",
		"buttontext" => "",
		"buttonnewwindow" => ""
	), $atts));

	$output  = '<div class="call-to-action-bar-wrapper' . ( $excerpt == '' ? ' no-excerpt' : '' ) . '">';
	$output .= '<div class="call-to-action-bar">';
	$output .= '<h3>' . $title . '</h3>';

	if( $excerpt != '' )
		$output .= '<span>' . $excerpt . '</span>';
	
	if ( $buttonurl != '' && $buttontext != '' )
		$output .= do_shortcode('[button text="' . $buttontext . '" url="' . $buttonurl . '" newwindow="' . $buttonnewwindow . '" size="medium" /]');
		
	$output .= '</div>';
	$output .= '</div>';
	
	return $output;
}


/**
 * Big Title Shortcode
 */
function unisphere_title_big_shortcode($atts) {
	extract(shortcode_atts(array(		
        "title" => "",
        "subtitle" => ""
	), $atts));

	$output  = '<div class="big-title">';
	$output .= '<div class="bar"></div>';
	$output .= '<h2>' . $title . '</h2>';
	
	if ( $subtitle != '' )
		$output .= '<p class="sub-title">' . $subtitle . '</p>';
	else
		$output .= '<p class="sub-title-empty">&nbsp;</p>';

	$output .= '</div>';
	
	return $output;
}


/**
 * Small Title Shortcode
 */
function unisphere_title_small_shortcode($atts) {
	extract(shortcode_atts(array(		
        "title" => ""
	), $atts));

	$output  = '<div class="small-title">';
	$output .= '<h3>' . $title . '</h3>';
	$output .= '<div class="long-bar"></div>';
	$output .= '</div>';
	
	return $output;
}


/**
 * Information Box Shortcode
 */
function unisphere_info_box_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "title" => "",
        "titlecolor" => "",
        "titlebgcolor" => "",
        "txtcolor" => "",
        "txtbgcolor" => ""
	), $atts));

	$title_style = 'style="';
	if ( $titlecolor != '' ) { $title_style .= 'color:' . $titlecolor . ';'; }
	if ( $titlebgcolor != '' ) { $title_style .= 'background:' . $titlebgcolor . ';'; }
	$title_style .= '"';

	$text_style = 'style="';
	if ( $txtcolor != '' ) { $text_style .= 'color:' . $txtcolor . ';'; }
	if ( $txtbgcolor != '' ) { $text_style .= 'background:' . $txtbgcolor . ';'; }
	$text_style .= '"';
	
	$output  = '<div class="info-box">';
	$output .= '<h3 class="info-box-title" ' . $title_style . '>' . $title . '</h3>';
	$output .= '<div class="info-box-content" ' . $text_style . '><p>' . do_shortcode($content) . '</p></div>';
	$output .= '</div>';
	
	return $output;
}


/**
 * Tab Shortcode
 */
function unisphere_tabs_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "titles" => "",
        "id" => ""
	), $atts));
	
	$tabs = explode( ",", $titles );
	$tab_number = 1;
	
	$output  = '<ul class="tabs" data-tab-id="' . $id . '">';
	foreach ($tabs as $tab) {
		$output .= '<li><a href="#tab' . $tab_number . '">' . trim( $tab ) . '</a></li>';
		$tab_number++;
	}
	$output .= '</ul>';

	$output .= '<div class="tab-container">';
	$output .= do_shortcode( $content );
	$output .= '</div>';
	$output .= '<div class="clearfix"></div>';
	
	return $output;
}

function unisphere_tab_shortcode($atts, $content = null) {
   return '<div class="tab-content">' . do_shortcode($content) . '</div>';
}


/**
 * Toggle Shortcode
 */
function unisphere_toggle_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "title" => "",
        "open" => ""
	), $atts));

	$output  = '<div class="toggle-container' . ($open == 'true' ? ' toggle-container-open' : '') . '"></span>';
	$output .= '<a href="#" class="toggle"><span class="toggle-sign"></span><span class="toggle-title">' . $title . '</span></a>';
	$output .= '<div class="toggle-content">';
	$output .= do_shortcode( $content );
	$output .= '</div>';
	$output .= '</div>';
	
	return $output;
}


/**
 * Price Table Shortcode
 */
function unisphere_price_table_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "columns" => ""
	), $atts));
	
	if( $columns == '' ) {
		
		return '<strong>Please set a number of columns for the pricing table.</strong>';
		
	} else {
	
		$semantic_columns = array(2 => "two", 3 => "three", 4 => "four", 5 => "five", 6 => "six");
		
		$output  = '<div class="price-table price-table-' . $semantic_columns[$columns] . '">';
		$output .= do_shortcode( $content );
		$output .= '</div>';
		$output .= '<div class="clearfix"></div>';
		
		return $output;
	}
}

function unisphere_price_column_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
		"featured" => "",
		"title" => "",
		"color" => ""
	), $atts));
	
	if( $featured == "true") {
		$featured = 'price-column-featured';
	}

	$output  = '<div class="price-column ' . $featured . '"' . ($color != '' ? ' data-color="' . $color . '"' : '') . '>';
	$output .= '<h4 class="price-column-title"' . ($color != '' ? ' style="background-color: ' . $color . '"' : '') . '>' . $title . '</h4>';
	$output .= '<ul>';
	$output .= do_shortcode($content);
	$output .= '</ul>';
	$output .= '</div>';
	
	return $output;
}

function unisphere_price_tag_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "value" => "",
		"period" => ""
	), $atts));
	
	$output  = '<div class="price-tag">';
	$output .= '<span class="price-value' . ($period == "" ? ' big' : '') . '">' . $value . '</span>';
	
	if( $period != "" ) {
		$output .= '<span class="price-period">' . $period . '</span>';
	}
	
	$output .= '</div>';
	
	return $output;
}


/**
 * Highlight Shortcodes
 */
function unisphere_highlight_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "txtcolor" => "",
		"bgcolor" => ""
	), $atts));
	
	if( $txtcolor != '' )
		$txtcolor = 'color: ' . $txtcolor . ';';
	
	if( $bgcolor != '' )
		$bgcolor = 'background-color: ' . $bgcolor . ';';
	
	return '<span class="highlight" style="' . $txtcolor . $bgcolor . '">' . do_shortcode($content) . '</span>';
}


/**
 * Video Shortcodes
 */
function unisphere_video_shortcode($atts) {
	extract(shortcode_atts(array(		
        "type" => "",
        "url" => "",
        "autoplay" => "false",
        "thumbnail" => "",
        "mp4" => "",
        "mp4hd" => "",
        "webmogg" => "",
        "webmogghd" => "",
        "flv" => "",
        "flvhd" => "",
        "width" => "",
        "height" => ""
	), $atts));
	
	$output = '';	
	$iframe_width = '';
	$iframe_height = '';
	
	if( $width != '' ) { $iframe_width = 'width="'. $width .'"'; $width = "width: " . $width . "px;"; }
	if( $height != '' ) { $iframe_height = 'width="'. $height .'"'; $height = "height: " . $height . "px;"; }
	
	if( $width != '' || $height != '' ) { $style = 'style="' . $width . $height . '"'; } else { $style = ''; }
	
	$rand = Rand();

	switch ( $type ) {
		case 'youtube':
			preg_match('"^http://(?<domain>([^./]+\\.)*youtube\\.com)(/v/|/watch\\?v=)(?<videoId>[A-Za-z0-9_-]{11})"', $url, $matches);
			$videoId = $matches['videoId'];
			// Alternate approach
			if( empty($videoId) ) {
				$parsed_url = parse_url($url);
				parse_str($parsed_url['query'], $parsed_query_string);
				$videoId = $parsed_query_string['v'];
			}
			$output  = '<iframe id="objectvideo' . $rand . '" class="embedded-video embedded-video-' . $type . '" type="text/html" ' . $style . ' ' . $iframe_width . ' ' . $iframe_height . ' src="http://www.youtube.com/embed/' . $videoId . '?rel=0&amp;autohide=1&amp;showinfo=0' . ($autoplay == "true" ? '&amp;autoplay=1' : '') . '" frameborder="0" scrolling="no"></iframe>';
			break;
		case 'vimeo':
			preg_match('"^http://vimeo\\.com/(?<videoId>[A-Za-z0-9_-]{7,8})"', $url, $matches);
			$videoId = $matches['videoId'];
			// Alternate approach (uncomment)
			if( empty($videoId) ) {
				$parsed_url = parse_url($url);
				$videoId = str_replace('/', '', $parsed_url['path']);
			}
			$output  = '<iframe id="objectvideo' . $rand . '" class="embedded-video embedded-video-' . $type . '" type="text/html" ' . $style . ' ' . $iframe_width . ' ' . $iframe_height . ' src="http://player.vimeo.com/video/' . $videoId . '?show_portrait=0&amp;show_byline=0&amp;show_title=0' . ($autoplay == "true" ? '&amp;autoplay=1' : '') . '" frameborder="0"></iframe>';
			break;
		case 'flv':
			$output  = '<div class="embedded-video embedded-video-' . $type . '" data-videourl-flv="' . $flv . '" data-videourl-flv-hd="' . $flvhd . '" data-autoplay="' . $autoplay . '" ' . $style . ' data-video-thumbnail="' . $thumbnail . '" id="videowrapper' . $rand . '"><div id="objectvideo' . $rand . '" ' . $style . '></div></div>';
			break;
		case 'html5':
			$output  = '<div class="embedded-video embedded-video-' . $type . '" data-videourl-mp4="' . $mp4 . '" data-videourl-mp4-hd="' . $mp4hd . '" data-videourl-webm="' . $webmogg . '" data-videourl-webm-hd="' . $webmogghd . '" data-autoplay="' . $autoplay . '" ' . $style . ' data-video-thumbnail="' . $thumbnail . '" id="videowrapper' . $rand . '"><div id="objectvideo' . $rand . '" ' . $style . '></div></div>';
			break;
		default:
			return NULL;		
	}

	return $output;
}


/**
 * Lightbox Video Shortcode
 */
function unisphere_video_lightbox_shortcode($atts) {
	extract(shortcode_atts(array(		
        "type" => "",
        "mp4" => "",
        "mp4hd" => "",
        "webmogg" => "",
        "webmogghd" => "",
        "flv" => "",
        "flvhd" => "",
        "width" => "",
        "height" => ""
	), $atts));

	$rand = Rand();
	
	$output = '<video id="lightbox-video-' . $rand . '" class="sublime zoom" preload="none" width="' . $width . '" height="' . $height . '">';

	switch ( $type ) {
		case 'flv':
			$output .= '<source src="' . $flv . '" />';
			if( $flvhd != '') { $output .= '<source src="' . $flvhd . '" />'; };
			break;
		case 'html5':
			$output .= '<source src="' . $mp4 . '" />';
			if( $mp4hd != '') { $output .= '<source src="' . $mp4hd . '" />'; };
			if( $webmogg != '') { $output .= '<source src="' . $webmogg . '" />'; };
			if( $webmogghd != '') { $output .= '<source src="' . $webmogghd . '" />'; };
			break;
		default:
			return NULL;		
	}

	$output .= '</video>';

	return $output;
}


/**
 * Lightbox Shortcodes
 */
function unisphere_lightbox_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
		"url" => "",
		"title" => "",
		"group" => "",
		"iframe" => "",
		"width" => "",
		"height" => ""
	), $atts));
	
	if ( $url == '' )
		return NULL;
	
	if( $group != '') { $rel = 'data-rel="lightbox[' . $group . ']"'; } else { $rel = 'data-rel="lightbox"'; }		

	if( $iframe == 'true') { 
		$url .= '?iframe=true'; 
		if( $width != '' && $height != '' )
			$url .= '&width=' . $width . '&height=' . $height;
	} else if( $width != '' && $height != '' ) {
		$url .= '?width=' . $width . '&height=' . $height; 
	}
		
	$output =  '<a class="lightbox" href="' . $url . '" title="' . $title . '" ' . $rel . '>' . do_shortcode($content) . '</a>';
	
	return $output;
}


/**
 * Recent Posts Shortcode
 */
function unisphere_recent_posts_shortcode($atts) {
	extract(shortcode_atts(array(		
		"title" => "",
		"numberposts" => "3",
		"cats" => "",
		"numberwords" => "29"
	), $atts));
	
	if( $cats != '' )
		$cats = explode( ',', $cats );

	$recent = new WP_Query( array( 'posts_per_page' => $numberposts, 'category__in' => $cats ) );

	if( $title != "" )
		$output = '<h3>' . $title . '</h3>';
	else
		$output = '';
	$output .= '<div class="widget-posts widget-posts-off-sidebar">';
	$output .= '<ul>';

	while ($recent->have_posts()) : $recent->the_post();

		$image = unisphere_get_post_image( 'sidebar-post' );
		$output .= '<li' . ( !$image ? ' class="no-image"' : '' ) .  '>';
		if( $image )
			$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-image">' . $image . '</a>';
		$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-title">' . get_the_title() . '</a>';
		if( intval( $numberwords) > 0 )
			$output .= '<p class="excerpt">' . unisphere_custom_excerpt( get_the_content(), $numberwords ) . '</p>';
		$output .= '</li>';

	endwhile;
	
	$output .= '</ul>';
	$output .= '</div>';
	
	wp_reset_query();
				
	return $output;
}


/**
 * Popular Posts Shortcode
 */
function unisphere_popular_posts_shortcode($atts) {
	extract(shortcode_atts(array(		
		"title" => "",
		"numberposts" => "3",
		"cats" => "",
		"numberwords" => "29"
	), $atts));
	
	if( $cats != '' )
		$cats = explode( ',', $cats );

	$popular = new WP_Query( array( 'orderby' => 'comment_count', 'posts_per_page' => $numberposts, 'category__in' => $cats ) );
	
	if( $title != "" )
		$output = '<h3>' . $title . '</h3>';
	else
		$output = '';
	$output .= '<div class="widget-posts widget-posts-off-sidebar">';
	$output .= '<ul>';

	while ($popular->have_posts()) : $popular->the_post();

		$image = unisphere_get_post_image( 'sidebar-post' );
		$output .= '<li' . ( !$image ? ' class="no-image"' : '' ) .  '>';
		if( $image )
			$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-image">' . $image . '</a>';
		$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-title">' . get_the_title() . '</a>';
		if( intval( $numberwords) > 0 )
			$output .= '<p class="excerpt">' . unisphere_custom_excerpt( get_the_content(), $numberwords ) . '</p>';
		$output .= '</li>';

	endwhile;
	
	$output .= '</ul>';
	$output .= '</div>';
	
	wp_reset_query();
				
	return $output;
}


/**
 * Recent Portfolio Items Shortcode
 */
function unisphere_recent_portfolio_shortcode($atts) {
	extract(shortcode_atts(array(		
		"title" => "",
		"numberposts" => "3",
		"cats" => "",
		"numberwords" => "29"
	), $atts));
	
	if( $cats != '' )
		$cats = explode( ',', $cats );

	$portfolio_posts_to_query = get_objects_in_term( $cats, 'portfolio_category');
	$recent = new WP_Query( array( 'post_type' => 'portfolio_cpt', 'posts_per_page' => $numberposts, 'post__in' => $portfolio_posts_to_query ) );

	if( $title != "" )
		$output = '<h3>' . $title . '</h3>';
	else
		$output = '';
	$output .= '<div class="widget-posts widget-posts-off-sidebar">';
	$output .= '<ul>';

	while ($recent->have_posts()) : $recent->the_post();

		$image = unisphere_get_post_image( 'sidebar-portfolio-item' );
		$output .= '<li' . ( !$image ? ' class="no-image"' : '' ) .  '>';
		if( $image )
			$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-image">' . $image . '</a>';
		$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-title">' . get_the_title() . '</a>';
		if( intval( $numberwords) > 0 )
			$output .= '<p class="excerpt">' . unisphere_custom_excerpt( get_the_content(), $numberwords ) . '</p>';
		$output .= '</li>';

	endwhile;
	
	$output .= '</ul>';
	$output .= '</div>';
	
	wp_reset_query();
				
	return $output;
}


/**
 * Popular Portfolio Items Shortcode
 */
function unisphere_popular_portfolio_shortcode($atts) {
	extract(shortcode_atts(array(		
		"title" => "",
		"numberposts" => "3",
		"cats" => "",
		"numberwords" => "29"
	), $atts));
	
	if( $cats != '' )
		$cats = explode( ',', $cats );

	$portfolio_posts_to_query = get_objects_in_term( $cats, 'portfolio_category');
	$popular = new WP_Query( array( 'orderby' => 'comment_count', 'post_type' => 'portfolio_cpt', 'posts_per_page' => $numberposts, 'post__in' => $portfolio_posts_to_query ) );
		
	if( $title != "" )
		$output = '<h3>' . $title . '</h3>';
	else
		$output = '';
	$output .= '<div class="widget-posts widget-posts-off-sidebar">';
	$output .= '<ul>';

	while ($popular->have_posts()) : $popular->the_post();

		$image = unisphere_get_post_image( 'sidebar-portfolio-item' );
		$output .= '<li' . ( !$image ? ' class="no-image"' : '' ) .  '>';
		if( $image )
			$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-image">' . $image . '</a>';
		$output .= '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-title">' . get_the_title() . '</a>';
		if( intval( $numberwords) > 0 )
			$output .= '<p class="excerpt">' . unisphere_custom_excerpt( get_the_content(), $numberwords ) . '</p>';
		$output .= '</li>';

	endwhile;
	
	$output .= '</ul>';
	$output .= '</div>';
	
	wp_reset_query();
				
	return $output;
}


/**
 * Portfolio Items Shortcode
 */
function unisphere_portfolio_shortcode($atts) {
	extract(shortcode_atts(array(
		"columns" => 4,
		"numberposts" => 4,
		"withgutter" => 'true',
		"cats" => ""
	), $atts));
	
	if( $cats != '' )
		$cats = explode( ',', $cats );

	$portfolio_posts_to_query = get_objects_in_term( $cats, 'portfolio_category');
	$portfolio = new WP_Query( array( 'post_type' => 'portfolio_cpt', 'post__in' => $portfolio_posts_to_query, 'showposts' => $numberposts ) ); 
		
	$output = '<div class="portfolio-shortcode">';
	$output .= '<ul class="portfolio-' . $columns . '-columns-list' . ( $withgutter == 'false' && intval($columns) != 1 ? '-no-gutter' : '' ) . ' portfolio-' . ( $withgutter == 'true' || intval( $columns ) == 1 ? 'scale' : 'fade' ) . ' clearfix">';

	while ($portfolio->have_posts()) : $portfolio->the_post();

		$custom = get_post_custom(get_the_ID());

		$output .= '<li class="portfolio-item">';
	        if( !empty( $custom['_portfolio_link'][0]) ) : // User has set a custom destination link for this portfolio item
	            $output .= '<a class="portfolio-image" href="' . $custom['_portfolio_link'][0] . '">';
	        elseif( isset($custom['_portfolio_no_lightbox'][0]) && $custom['_portfolio_no_lightbox'][0] == '1' ) : // User has selected to link the thumb directly to the portfolio item detail page or the custom url
	            $output .= '<a class="portfolio-image" href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '">';
	        elseif( !empty( $custom['_portfolio_video'][0] ) && $custom['_portfolio_video'][0] != 'disabled' && $custom['_portfolio_video'][0] != 'html5_flv_disabled' ) : // Check if there's a video to be displayed in the lightbox when clicking the thumb
	            switch( $custom['_portfolio_video'][0] ) {
	                case 'youtube':
	                    $output .= '<a class="portfolio-image" href="' . $custom['_portfolio_video_youtube'][0] . '" title="' . get_the_title() . '" data-rel="lightbox[portfolio]">';
	                    break;
	                case 'vimeo':
	                    $output .= '<a class="portfolio-image" href="' . $custom['_portfolio_video_vimeo'][0] . '" title="' . get_the_title() . '" data-rel="lightbox[portfolio]">';
	                    break;
	                case 'html5_flv':
	                    $output .= '<a class="portfolio-image sublime" href="' . $custom['_portfolio_video_html5_mp4'][0] . '" title="' . get_the_title() . '">';
	                    break;
	            }
	        else : // just open the full image in the lightbox 
	        	$full_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false);
	            $output .= '<a class="portfolio-image" href="' . $full_image[0] . '" title="' . get_the_title() . '" data-rel="lightbox[portfolio]">';
	        endif;
			$output .= unisphere_get_post_image("portfolio" . $columns . ( $withgutter == 'false' && intval($columns) != 1 ? '-no-gutter' : '' ));
	        $output .= '</a>';
	        // If there's an HTML5 or FLV video to display in the lightbox add it here
	        if( !empty( $custom['_portfolio_video'][0] ) && $custom['_portfolio_video'][0] == 'html5_flv' ) {
	            if( isset( $custom['_portfolio_video_flv'] ) ) { // FLV
	                $output .= unisphere_run_shortcode('[video_lightbox ' . ( isset( $custom['_portfolio_video_lightbox_width'] ) ? ' width="' . $custom['_portfolio_video_lightbox_width'][0] . '"' : '' ) . ( isset( $custom['_portfolio_video_lightbox_height'] ) ? ' height="' . $custom['_portfolio_video_lightbox_height'][0] . '"' : '' ) . ' type="flv" flv="' . $custom['_portfolio_video_flv'][0] . '" ' . ( isset( $custom['_portfolio_video_flv_hd'] ) ? ' flvhd="' . $custom['_portfolio_video_flv_hd'][0] . '"' : '' ) . ' /]');
	            } else { // HTML5
	                $output .= unisphere_run_shortcode('[video_lightbox ' . ( isset( $custom['_portfolio_video_lightbox_width'] ) ? ' width="' . $custom['_portfolio_video_lightbox_width'][0] . '"' : '' ) . ( isset( $custom['_portfolio_video_lightbox_height'] ) ? ' height="' . $custom['_portfolio_video_lightbox_height'][0] . '"' : '' ) . ' type="html5" mp4="' . $custom['_portfolio_video_html5_mp4'][0] . '" ' . ( isset( $custom['_portfolio_video_html5_mp4_hd'] ) ? ' mp4hd="' . $custom['_portfolio_video_html5_mp4_hd'][0] . '"' : '' ) . ' ' . ( isset( $custom['_portfolio_video_html5_webm'] ) ? ' webmogg="' . $custom['_portfolio_video_html5_webm'][0] . '"' : '' ) . ' ' . ( isset( $custom['_portfolio_video_html5_webm_hd'] ) ? ' webmogghd="' . $custom['_portfolio_video_html5_webm_hd'][0] . '"' : '' ) . ' /]');
	            }
	        }

	        if( $withgutter == 'true' && intval($columns) != 1 )
	        	$output .= '<div class="long-bar"></div>';

	        if( intval($columns) != 1 ) {
	        	$output .= '<div class="portfolio-info">';
	            	$output .= '<a class="portfolio-title" href="' . (!empty( $custom['_portfolio_link'][0] ) ? $custom['_portfolio_link'][0] : get_permalink(get_the_ID())) . '">' . get_the_title() . '</a>';
	        	$output .= '</div>';
	        } else {
	        	$output .= '<div class="portfolio-info">';
                    $output .= '<div class="bar"></div>';
                    $output .= '<a class="portfolio-title" href="' . (!empty( $custom['_portfolio_link'][0] ) ? $custom['_portfolio_link'][0] : get_permalink(get_the_ID())) . '">' . get_the_title() . '</a>';
                    $output .= '<div class="portfolio-excerpt">' . get_the_excerpt() . '</div>';
                    $output .= '<a class="custom-button more-link" href="' . (!empty( $custom['_portfolio_link'][0] ) ? $custom['_portfolio_link'][0] : get_permalink(get_the_ID())) . '">' . __( 'Read more', 'unisphere' ) . '</a>';
                $output .= '</div>';
	        }
	    $output .= '</li>';

	endwhile;

	$output .= '</ul>';
	$output .= '</div>';
	
	wp_reset_query();
				
	return $output;
}


/**
 * Twitter Shortcode
 */
function unisphere_twitter_shortcode($atts) {
	extract(shortcode_atts(array(		
		"title" => "",
		"numbertweets" => "3",
		"username" => "",
		'excludereplies' => "",
		'includeretweets' => "true",
		"interval" => "1800",
		'usejavascript' => ""
	), $atts));

	if( $usejavascript != 'true' ) {
		if( false === ( $tweets = get_transient( UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $numbertweets . '-' . $excludereplies . '-' . $includeretweets ) ) ) {
			$api_url = "https://api.twitter.com/1/statuses/user_timeline.json?screen_name=" . $username . "&count=" . $numbertweets . "&trim_user=1";

			if( $excludereplies == 'true' )
				$api_url .= '&exclude_replies=1';

			if( $includeretweets == 'true' )
				$api_url .= '&include_rts=1';

	    	$response = wp_remote_get( $api_url, array( 'sslverify' => false) );
			if( is_wp_error( $response ) || $response['response']['code'] != 200 ) {
				if( get_option(UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $numbertweets . '-' . $excludereplies . '-' . $includeretweets) != '' ) {
					$tweets = get_option(UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $numbertweets . '-' . $excludereplies . '-' . $includeretweets);
				} else {
					$output = '<h3>' . $title . '</h3>';
					$output .= '<div class="widget-twitter widget-twitter-off-sidebar">';
				   	$output .= '<p class="tweet">' . __( 'No tweets were found.', 'unisphere' ) . '</p>';
				   	$output .= '</div>';
				   	return $output;
				}
			} else {
				// Parse the JSON data into a PHP array
				$tweets = json_decode( $response['body'], true );
			}

			set_transient( UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $numbertweets . '-' . $excludereplies . '-' . $includeretweets, $tweets, $interval );
			update_option( UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $numbertweets . '-' . $excludereplies . '-' . $includeretweets, $tweets );
		}

		$output = '<h3>' . $title . '</h3>';
		$output .= '<div class="widget-twitter widget-twitter-off-sidebar">';
		$output .= '<ul>';

		foreach( $tweets as $tweet ) {
			$output .= '<li>';
	        $output .= '<p class="tweet">' . twitterize( $tweet['text'] ) . '</p>';
	        $output .= '<small><a href="http://twitter.com/' . $username . '/status/' . $tweet['id_str'] . '">' . relativeTime( strtotime( $tweet['created_at'] ) ) . '</a></small>';
			$output .= '</li>';
		}

		$output .= '</ul>';
		$output .= '<a href="https://twitter.com/' . $username . '" class="twitter-follow-button" data-show-count="false">Follow @' . $username . '</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
		$output .= '</div>';
	} else {
		// Use the javascript solution
		$twitterid = rand();
		$output = '<h3>' . $title . '</h3>';
		$output .= '<div class="widget-twitter widget-twitter-off-sidebar">';
		$output .= '<ul id="jtwt_' . $twitterid . '"></ul>';
		$output .= '<a href="https://twitter.com/' . $username . '" class="twitter-follow-button" data-show-count="false">Follow @' . $username . '</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
		$output .= '</div>';

		$output .= "<script>";
		$output .= "jQuery(window).load(function() {";
		$output .= 		"jQuery('#jtwt_" . $twitterid . "').jtwt({";
		$output .= 			"username: '" . $username . "',";
		$output .= 			"count: '" . $numbertweets . "',";
		$output .= 			"excludereplies: '" . ( $excludereplies == 'true' ? 'true' : 'false' ) . "',";
		$output .= 			"includeretweets: '" . ( $includeretweets == 'true' ? 'true' : 'false' ) . "'";
		$output .= 		"});";
		$output .= "});";
		$output .= "</script>";
	}
	return $output;
}


/**
 * Flickr Shortcode
 */
function unisphere_flickr_shortcode($atts) {
	extract(shortcode_atts(array(		
		"title" => "",
		"numberimages" => "9",
		"flickrid" => "",
		"lightbox" => "false",
		"interval" => "1800"
		
	), $atts));
	
	if( trim( $flickrid ) == '' ) {
		$output = '<h3>' . $title . '</h3>';
		$output .= '<div class="widget-flickr widget-flickr-off-sidebar">';
		$output .= '<p>' . __( 'No flickr photos were found.', 'unisphere' ) . '</p>';
	} else {		
		if( false === ( $photos = get_transient( UNISPHERE_THEMEOPTIONS . '-flickr-' . $flickrid . '-' . $numberimages ) ) ) {
			$feed = 'http://api.flickr.com/services/feeds/photos_public.gne?id=' . $flickrid . '&format=php_serial';
			
			$response = wp_remote_get( $feed );
			if( is_wp_error( $response ) ) {
				$output = '<h3>' . $title . '</h3>';
				$output .= '<div class="widget-flickr widget-flickr-off-sidebar">';
			   	$output .= '<p>' . __( 'No flickr photos were found.', 'unisphere' ) . '</p>';
			   	$output .= '</div>';
			   	return $output;
			} else {
				$photos = unserialize( $response['body'] );
			}

			set_transient( UNISPHERE_THEMEOPTIONS . '-flickr-' . $flickrid . '-' . $numberimages, $photos, $interval );
		}
		
		$lightboxGalleryID = rand();
		
		$output = '<h3>' . $title . '</h3>';
		$output .= '<div class="widget-flickr widget-flickr-off-sidebar">';
		$output .= '<ul>';
		
		for ($i = 0; $i < $numberimages; $i++) {
			$thumb_url = $photos['items'][$i]['thumb_url'];
			$photo_title = $photos['items'][$i]['title'];
			$photo_url = $photos['items'][$i]['photo_url'];
			$url = $photos['items'][$i]['url'];			
			
			if( $lightbox ) {
				$output .= '<li><a href="' . $photo_url . '" title="' . $photo_title . '" data-rel="lightbox[' . $lightboxGalleryID .  ']"><img src="' . $thumb_url . '" alt="' . $photo_title . '" /></a></li>';
			} else {
				$output .= '<li><a href="' . $url . '" title="' . $photo_title . '"><img src="' . $thumb_url . '" alt="' . $photo_title . '" /></a></li>';
			}
		}
		
		$output .= '</ul>';
		$output .= '</div>';
	}
				
	return $output;
}


/**
 * Google Map Shortcode
 */
function unisphere_map_shortcode($atts) {
	global $post;
	extract(shortcode_atts(array(		
		"type" => "ROADMAP",
		"width" => "210",
		"height" => "210",
		"address" => "",
		"latitude" => "",
		"longitude" => "",
		"popuptext" => "",
		"zoom" => "8",
		"showmaptypecontrol" => "true",
		"showzoomcontrol" => "true",
		"showpancontrol" => "true",
		"showscalecontrol" => "true",
		"showstreetviewcontrol" => "true"
	), $atts));

	$size = 'style="width: ' . $width . 'px; height: ' . $height . 'px;"';
	
	if( $popuptext != '' )
		$popuptext = ', html: "' . $popuptext . '", popup: false';

	$rand = Rand();

	$output  = '<div class="gmap" id="gmap' . $rand . '" ' . $size . '></div>';
	
	$output .= '<script>';
	$output .= 'unisphere_gmap();';
	$output .= 'jQuery(window).load(function(){';
	$output .= 'jQuery("#gmap' . $rand . '").gMap( {';
	$output .= 'maptype: google.maps.MapTypeId.' . $type . ',';
	$output .= 'zoom: ' . $zoom . ',';

	if( $latitude != '' && $longitude != '' )
		$output .= 'markers: [ { latitude: ' . $latitude . ', longitude: ' . $longitude . $popuptext . ' } ],';
	
	if( $latitude == '' && $longitude == '' && $address != '' )
		$output .= 'markers: [ { address: "' . $address . '"' . $popuptext . ' } ],';
		
	$output .= 'mapTypeControl: ' . $showmaptypecontrol . ',';
	$output .= 'zoomControl: ' . $showzoomcontrol . ',';
	$output .= 'panControl: ' . $showpancontrol . ',';
	$output .= 'scaleControl: ' . $showscalecontrol . ',';
	$output .= 'streetViewControl: ' . $showstreetviewcontrol;		
	$output .= '});';

	$output .= '});';
	$output .= '</script>';
	
	return $output;
}

/**
 * Wide Bar Shortcode
 */
function unisphere_wide_bar_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		"whitetext" => "false",
        "bgcolor" => "",
        "bgimage" => "",
        "bgposition" => "center center",
        "bgrepeat" => "no-repeat",
        "paddingtop" => "20",
        "paddingbottom" => "20",
        "marginbottom" => "0"
	), $atts));

	global $unisphere_options;

	$style = '';

	if( $bgcolor != '' )
		$style .= 'background-color: ' . $bgcolor . '; ';
	else
		$style .= 'background-color: ' . $unisphere_options['skin_color_7'] . '; ';

	if( $bgimage != '' )
		$style .= 'background-image: url(\'' . $bgimage . '\'); ';

	$style .= 'background-position: ' . $bgposition . '; ';
	$style .= 'background-repeat: ' . $bgrepeat . '; ';
	$style .= 'padding-top: ' . $paddingtop . 'px; ';
	$style .= 'padding-bottom: ' . $paddingbottom . 'px; ';
	$style .= 'margin-bottom: ' . $marginbottom . 'px; ';

	$output  = '<div class="wide-bar' . ($whitetext == 'true' ? ' white-text' : '' ) . '" style="' . $style . '">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	
	return $output;
}

/**
 * Icon Shortcode
 */
function unisphere_icon_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(		
        "id" => "001",
        "floatleft" => "true"
	), $atts));

   	return '<img class="icon" src="' . UNISPHERE_IMAGES . '/icons/icon_' . $id . '.png" alt="" ' . ($floatleft == 'true' ? 'style="float: left;"' : '') . ' />';
}


/**
 * Add Shortcode Button to the Rich Editor
 */
function unisphere_shortcode_button() {
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )	return;
	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "unisphere_add_shortcode_tinymce_plugin");
		add_filter('mce_buttons', 'unisphere_register_shortcode_button');
	}
}
 
/**
 * Register the TinyMCE Shortcode Button
 */
function unisphere_register_shortcode_button($buttons) {
	array_push($buttons, "|", "unisphere_shortcodes_button");
	return $buttons;
}

/**
 * Load the TinyMCE plugin: shortcode_plugin.js
 */
function unisphere_add_shortcode_tinymce_plugin($plugin_array) {
   $plugin_array['UniSphereShortcodes'] = get_template_directory_uri() . '/js/shortcode_plugin.js';
   return $plugin_array;
}
 
function unisphere_refresh_mce($ver) {
  $ver += 3;
  return $ver;
}

/**
 * Init process for button control
 */
add_filter( 'tiny_mce_version', 'unisphere_refresh_mce');
add_action( 'init', 'unisphere_shortcode_button' );

?>
