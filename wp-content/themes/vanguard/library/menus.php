<?php
/**
 * unisphere_menu_ulclass() adds css class to the <ul> tag in wp_page_menu.
 */
function unisphere_menu_ulclass( $ulclass ) {
	return preg_replace( '/<ul>/', '<ul class="nav">', $ulclass, 1 );
}

add_filter( 'wp_page_menu', 'unisphere_menu_ulclass' ); // adds a .nav class to the ul wp_page_menu generates


/**
 * Add support for the WP 3.0 menu system
 */
if ( function_exists( 'register_nav_menus' ) ) {	
	register_nav_menus( array(
		'header_menu' => 'Header Menu'
	) );
}


/**
 * Function that renders Header Navigation as a fallback of the WP 3.0 menu system
 */
function unisphere_header_navigation() {
	wp_page_menu( 'show_home=0&sort_column=menu_order&menu_class=menu' );
}


/**
 * Custom Walker function that adds the page/post ID as a class in each menu LI
 */
class unisphere_menu_walker extends Walker_Nav_Menu{
    function start_el(&$output, $item, $depth, $args) {

		global $wp_query;

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		$classes[] = 'page-item-' . $item->object_id;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="'. esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title )   ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="'  . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )  ? ' rel="' . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="'  . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
?>