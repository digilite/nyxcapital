<?php
 
/**************************/
/* Include LayerSlider WP */
/**************************/
 
// Path for LayerSlider WP main PHP file
$layerslider = get_stylesheet_directory() . '/LayerSlider/layerslider.php';
 
// Check if the file is available to prevent warnings
if(file_exists($layerslider)) {
 
    // Include the file
    include $layerslider;
 
    // Activate the plugin if necessary
    if(get_option( UNISPHERE_THEMEOPTIONS . '_layerslider_activated', '0') == '0') {
 
        // Run activation script
        layerslider_activation_scripts();
 
        // Save a flag that it is activated, so this won't run again
        update_option(UNISPHERE_THEMEOPTIONS . '_layerslider_activated', '1');
    }
}
 
?>