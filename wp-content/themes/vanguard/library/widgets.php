<?php
/**
 * Register the theme's widget areas
 */
if ( function_exists('register_sidebar') )	{
	
	register_sidebar(array(
		'name'=>'Main Sidebar',
		'id'=>'main-sidebar',
		'description' => 'The main widget sidebar area',
		'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="bar"></div>',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name'=>'Superior-header Bar (left)',
		'id'=>'superior-header-bar-left',
		'description' => 'A widget area shown in the Superior-header Bar (left) of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	));

	register_sidebar(array(
		'name'=>'Superior-header Bar (right)',
		'id'=>'superior-header-bar-right',
		'description' => 'A widget area shown in the Superior-header Bar (right) of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	));

	register_sidebar(array(
		'name'=>'Footer Column 1',
		'id'=>'footer-column-1',
		'description' => 'A widget area shown in the Footer of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><div class="bar"></div>',
	));
	
	register_sidebar(array(
		'name'=>'Footer Column 2',
		'id'=>'footer-column-2',
		'description' => 'A widget area shown in the Footer of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><div class="bar"></div>',
	));
	
	register_sidebar(array(
		'name'=>'Footer Column 3',
		'id'=>'footer-column-3',
		'description' => 'A widget area shown in the Footer of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><div class="bar"></div>',
	));
	
	register_sidebar(array(
		'name'=>'Footer Column 4',
		'id'=>'footer-column-4',
		'description' => 'A widget area shown in the Footer of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><div class="bar"></div>',
	));

	register_sidebar(array(
		'name'=>'Footer Bar (left)',
		'id'=>'footer-bar-left',
		'description' => 'A widget area shown in the Footer Bar (left) of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	));

	register_sidebar(array(
		'name'=>'Footer Bar (right)',
		'id'=>'footer-bar-right',
		'description' => 'A widget area shown in the Footer Bar (right) of all pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	));

	// Register custom sidebards
	global $unisphere_options;
	if( isset($unisphere_options['sidebar_list']) ) {
		$sidebars = $unisphere_options['sidebar_list'];
		if( !empty($sidebars) ) {
			foreach ($sidebars as $sidebar) {
				register_sidebar(array(
					'name'=>$sidebar['name'],
					'id'=>$sidebar['id'],
					'description' => 'Custom created widget area',
					'before_widget' => '<div id="%1$s" class="widget sidebar-' . $sidebar['id'] . ' %2$s"><div class="bar"></div>',
					'after_widget' => '</div>',
					'before_title' => '<h3 class="widget-title">',
					'after_title' => '</h3>',
				));
			}
		}
	}

	/**
	 * Get the custom sidebar for a page/post if exists, else display default
	 * (adapted from http://wordpress.org/extend/plugins/sidebar-generator/)
	 */
	function unisphere_dynamic_sidebar( $name = '' ) {

		if( !is_singular() ) {
			if( $name != '' ) {
				dynamic_sidebar( $name );
			} else {
				dynamic_sidebar();
			}
			return; //dont do anything
		}

		global $wp_query, $unisphere_options;

		$post = $wp_query->get_queried_object();

		$post_meta = get_post_meta($post->ID, '_page_custom_sidebar', true);

		if( $post_meta != '' )
			$custom_sidebars = explode( ",", $post_meta );
		else
			$custom_sidebars = '';

		$did_sidebar = false;
		//this page uses a custom sidebar
		if( $custom_sidebars != '' ) {
			foreach ($custom_sidebars as $custom_sidebar) { // $custom_sidebar equals 'default_sidebar;custom_sidebar' 				
				$custom_sidebar = explode( ";", $custom_sidebar );
				if( $name == $custom_sidebar[0] ) {
					if( $custom_sidebar[1] == 'unisphere_empty' ) {
						$did_sidebar = true;
						break;
					}
					// Check if the custom sidebar is still available (wasn't deleted)
					$of_deleted = true;
					if( isset($unisphere_options['sidebar_list']) ) {
						foreach( $unisphere_options['sidebar_list'] as $of_sidebar ) {
							if( $of_sidebar['id'] == $custom_sidebar[1] )
								$of_deleted = false;
						}
					}
					if( !$of_deleted )
						dynamic_sidebar( $custom_sidebar[1] );
					else
						dynamic_sidebar( $custom_sidebar[0] );
					$did_sidebar = true;
					break;
				} 
			}			

			if( $did_sidebar == true ) {
				return;
			}

			// go through without finding any replacements, lets just send them what they asked for
			if( $name != '' ) {
				dynamic_sidebar( $name );
			} else {
				dynamic_sidebar();
			}
			return;			
		} else {
			if( $name != '' ) {
				dynamic_sidebar( $name );
			} else {
				dynamic_sidebar();
			}
		}
	}

	/**
	 * Custom is_active_sidebar() function to properly suite the theme's custom sidebars
	 */
	function unisphere_is_active_sidebar( $name = '' ) {

		global $wp_query, $unisphere_options;

		$post = $wp_query->get_queried_object();
		
		if( isset($post->ID) ) {
			$post_meta = get_post_meta($post->ID, '_page_custom_sidebar', true);
			if( $post_meta != '' )
				$custom_sidebars = explode( ",", $post_meta );
			else
				$custom_sidebars = '';
		} else
			$custom_sidebars = '';

		$did_sidebar = false;
		//this page uses a custom sidebar
		if( $custom_sidebars != '' ) {
			foreach ($custom_sidebars as $custom_sidebar) { // $custom_sidebar equals 'default_sidebar;custom_sidebar' 				
				$custom_sidebar = explode( ";", $custom_sidebar );
				if( $name == $custom_sidebar[0] ) {
					if( $custom_sidebar[1] == 'unisphere_empty' ) {
						return false;
						break;
					}
					// Check if the custom sidebar is still available (wasn't deleted)
					$of_deleted = true;
					if( isset($unisphere_options['sidebar_list']) ) {
						foreach( $unisphere_options['sidebar_list'] as $of_sidebar ) {
							if( $of_sidebar['id'] == $custom_sidebar[1] )
								$of_deleted = false;
						}
					}
					if( !$of_deleted )
						return is_active_sidebar( $custom_sidebar[1] );
					else
						return is_active_sidebar( $custom_sidebar[0] );
				} 
			}			

			// go through without finding any replacements, lets just send them what they asked for
			if( $name != '' ) {
				return is_active_sidebar( $name );
			} else {
				return is_active_sidebar();
			}
			return;			
		} else {
			if( $name != '' ) {
				return is_active_sidebar( $name );
			} else {
				return is_active_sidebar();
			}
		}
	}
}
?>