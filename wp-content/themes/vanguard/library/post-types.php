<?php
/**
 * The Portfolio custom post type
 */
add_action('init', 'portfolio_cpt_register');

function portfolio_cpt_register() {	

	global $unisphere_options;
	
	register_post_type( 'portfolio_cpt', 
						array(
							'labels' => array(
								'name' => 'Portfolio',
								'singular_name' => 'Portfolio Item',
								'add_new' => 'Add New Item',
								'add_new_item' => 'Add New Portfolio Item',
								'edit_item' => 'Edit Portfolio Item',
								'new_item' => 'Add New Portfolio Item',
								'view_item' => 'View Item',
								'search_items' => 'Search Portfolio Items',
								'not_found' => 'No portfolio items found',
								'not_found_in_trash' => 'No portfolio items found in trash' ),
							'public' => true,
							'capability_type' => 'post',
							'has_archive' => true,
							'menu_position' => 5,
							'rewrite' => array( 'slug' => trim( isset( $unisphere_options['portfolio_permalink'] ) ? $unisphere_options['portfolio_permalink'] : 'portfolio/detail' ), 'with_front' => false ),
							'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'comments', 'revisions')
						)
					);
	
	register_taxonomy(  'portfolio_category', 
						'portfolio_cpt', 
						array(
							'labels' => array(
								'name' => 'Portfolio Categories',
								'singular_name' => 'Portfolio Category',
								'search_items' => 'Search Portfolio Categories',
								'popular_items' => 'Popular Portfolio Categories',
								'all_items' => 'All Portfolio Categories',
								'parent_item' => 'Parent Portfolio Category',
								'parent_item_colon' => 'Parent Portfolio Category:',
								'edit_item' => 'Edit Portfolio Category',
								'update_item' => 'Update Portfolio Category',
								'add_new_item' => 'Add New Portfolio Category',
								'new_item_name' => 'New Portfolio Category Name',
								'separate_items_with_commas' => 'Separate portfolio categories with commas',
								'add_or_remove_items' => 'Add or remove portfolio categories',
								'choose_from_most_used' => 'Choose from the most used portfolio categories',
								'menu_name' => 'Portfolio Categories' ),
							'public' => true,
							'hierarchical' => true,
							'rewrite' => array( 'slug' => 'portfolio-category' )
						)
					);
					
	// portfolio permalink 404 error fix
	if( get_option('flush_rewrite_rules') == '1' ) {
		flush_rewrite_rules( false );
		update_option('flush_rewrite_rules', '0');
	}
	
	function portfolio_cpt_edit_columns($columns) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'portfolio_thumbnail' => 'Image',
			'title' => 'Title',			
			'portfolio_category' => 'Category',
			"author" => 'Author',
			"comments" => 'Comments',
			"date" => 'Date'
		);
		$columns['comments'] = '<div class="vers"><img alt="Comments" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></div>';
		return $columns;
	}	
	add_filter('manage_edit-portfolio_cpt_columns', 'portfolio_cpt_edit_columns');
	
	
	function portfolio_cpt_custom_columns($column, $post_id) {
		switch ($column)
		{
			case 'portfolio_thumbnail':
				$width = (int) 35;
				$height = (int) 35;
				// Display the featured image in the column view if possible
				if ( has_post_thumbnail()) {
					the_post_thumbnail( array($width, $height) );
				} else {
					echo 'None';
				}
				break;
				
			case "portfolio_category":  
				if ( $category_list = get_the_term_list( $post_id, 'portfolio_category', '', ', ', '' ) ) {
					echo $category_list;
				} else {
					echo 'None';
				}
				break; 
		}
	}	
	add_action('manage_posts_custom_column', 'portfolio_cpt_custom_columns', 10, 2);
	
	
	function portfolio_cpt_icons() { ?>
		<style type="text/css" media="screen">
			#menu-posts-portfolio_cpt .wp-menu-image {
				background: url(<?php echo UNISPHERE_ADMIN_IMAGES; ?>/portfolio-icon.png) no-repeat 6px 6px !important;
			}
			#menu-posts-portfolio_cpt:hover .wp-menu-image, #menu-posts-portfolio_cpt.wp-has-current-submenu .wp-menu-image {
				background-position:6px -16px !important;
			}
			#icon-edit.icon32-posts-portfolio_cpt {background: url(<?php echo UNISPHERE_ADMIN_IMAGES; ?>/portfolio-32x32.png) no-repeat;}
		</style>
	<?php }	
	add_action( 'admin_head', 'portfolio_cpt_icons' );
	
	
	function restrict_portfolio_by_portfolio_categories() {
		global $typenow;
		global $wp_query;
		if ($typenow == 'portfolio_cpt') {
			$taxonomy = 'portfolio_category';
			$business_taxonomy = get_taxonomy($taxonomy);
			wp_dropdown_categories(array(
				'show_option_all' =>  "Show All {$business_taxonomy->label}",
				'taxonomy'        =>  $taxonomy,
				'name'            =>  'portfolio_category',
				'orderby'         =>  'name',
				'selected'        =>  isset($wp_query->query['portfolio_category']) ? $wp_query->query['portfolio_category'] : false,
				'hierarchical'    =>  true,
				'depth'           =>  3,
				'show_count'      =>  true,
				'hide_empty'      =>  true
			));
		}
	}
	add_action('restrict_manage_posts','restrict_portfolio_by_portfolio_categories');	


	function convert_portfolio_category_id_to_taxonomy_term_in_query($query) {
		global $pagenow;
		$qv = &$query->query_vars;
		if ($pagenow == 'edit.php' && isset($qv['portfolio_category']) && is_numeric($qv['portfolio_category']) && $qv['portfolio_category'] != '0') {
			$term = get_term_by('id', $qv['portfolio_category'], 'portfolio_category');
			$qv['portfolio_category'] = $term->slug;
		}
	}
	add_filter( 'parse_query', 'convert_portfolio_category_id_to_taxonomy_term_in_query' );
}


//-----------------------------------------------------------------------


/**
 * The Slider Images custom post type
 */
add_action('init', 'slider_register');

function slider_register() {	

	register_post_type( 'slider', 
						array(
							'labels' => array(
								'name' => 'Slider',
								'singular_name' => 'Slider Item',
								'add_new' => 'Add New Item',
								'add_new_item' => 'Add New Slider Item',
								'edit_item' => 'Edit Slider Item',
								'new_item' => 'Add New Slider Item',
								'view_item' => 'View Item',
								'search_items' => 'Search Slider Items',
								'not_found' => 'No slider items found',
								'not_found_in_trash' => 'No slider items found in trash' ),
							'public' => true,
							'capability_type' => 'post',
							'menu_position' => 5,
							'rewrite' => false,
							'supports' => array('title', 'editor', 'thumbnail', 'custom-fields')
						)
					);
	
	register_taxonomy(  'slider_category', 
						'slider', 
						array(
							'labels' => array(
								'name' => 'Slider Categories',
								'singular_name' => 'Slider Category',
								'search_items' => 'Search Slider Categories',
								'popular_items' => 'Popular Slider Categories',
								'all_items' => 'All Slider Categories',
								'parent_item' => 'Parent Slider Category',
								'parent_item_colon' => 'Parent Slider Category:',
								'edit_item' => 'Edit Slider Category',
								'update_item' => 'Update Slider Category',
								'add_new_item' => 'Add New Slider Category',
								'new_item_name' => 'New Slider Category Name',
								'separate_items_with_commas' => 'Separate slider categories with commas',
								'add_or_remove_items' => 'Add or remove slider categories',
								'choose_from_most_used' => 'Choose from the most used slider categories',
								'menu_name' => 'Slider Categories' ),
							'public' => true,
							'hierarchical' => true,
							'rewrite' => false
						)
					);

	function slider_edit_columns($columns) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'slider_thumbnail' => 'Image',
			'title' => 'Title',
			'slider_category' => 'Category',
			"author" => 'Author',
			"date" => 'Date'
		);
		return $columns;
	}
	add_filter('manage_edit-slider_columns', 'slider_edit_columns');
	

	function slider_custom_columns($column, $post_id) {
		switch ($column)
		{
			case 'slider_thumbnail':
				$width = (int) 35;
				$height = (int) 35;
				// Display the featured image in the column view if possible
				if ( has_post_thumbnail()) {
					the_post_thumbnail( array($width, $height) );
				} else {
					echo 'None';
				}
				break;
				
			case "slider_category":  
				if ( $category_list = get_the_term_list( $post_id, 'slider_category', '', ', ', '' ) ) {
					echo $category_list;
				} else {
					echo 'None';
				}
				break; 
		}
	}	
	add_action('manage_posts_custom_column',  'slider_custom_columns', 10, 2);
	
	
	function slider_icons() { ?>
		<style type="text/css" media="screen">
			#menu-posts-slider .wp-menu-image {
				background: url(<?php echo UNISPHERE_ADMIN_IMAGES; ?>/slider-icon.png) no-repeat 6px 6px !important;
			}
			#menu-posts-slider:hover .wp-menu-image, #menu-posts-slider.wp-has-current-submenu .wp-menu-image {
				background-position:6px -16px !important;
			}
			#icon-edit.icon32-posts-slider {background: url(<?php echo UNISPHERE_ADMIN_IMAGES; ?>/slider-32x32.png) no-repeat;}
		</style>
	<?php }	
	add_action( 'admin_head', 'slider_icons' );
	

	function restrict_slider_by_slider_categories() {
		global $typenow;
		global $wp_query;
		if ($typenow == 'slider') {
			$taxonomy = 'slider_category';
			$business_taxonomy = get_taxonomy($taxonomy);
			wp_dropdown_categories(array(
				'show_option_all' =>  "Show All {$business_taxonomy->label}",
				'taxonomy'        =>  $taxonomy,
				'name'            =>  'slider_category',
				'orderby'         =>  'name',
				'selected'        =>  isset($wp_query->query['slider_category']) ? $wp_query->query['slider_category'] : false,
				'hierarchical'    =>  true,
				'depth'           =>  3,
				'show_count'      =>  true,
				'hide_empty'      =>  true
			));
		}
	}
	add_action('restrict_manage_posts','restrict_slider_by_slider_categories');	


	function convert_slider_category_id_to_taxonomy_term_in_query($query) {
		global $pagenow;
		$qv = &$query->query_vars;
		if ($pagenow == 'edit.php' && isset($qv['slider_category']) && is_numeric($qv['slider_category']) && $qv['slider_category'] != '0') {
			$term = get_term_by('id', $qv['slider_category'], 'slider_category');
			$qv['slider_category'] = $term->slug;
		}
	}
	add_filter( 'parse_query', 'convert_slider_category_id_to_taxonomy_term_in_query' );

}
?>