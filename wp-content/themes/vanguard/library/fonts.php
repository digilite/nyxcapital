<?php

// See if any Google web font has been selected for use and enqueue the style
if ( !function_exists( 'unisphere_enqueue_google_fonts' ) ) {
function unisphere_enqueue_google_fonts() {	

	global $unisphere_options;

	// Check in the cache first
	if( false === ( $parameters = get_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_google_fonts' ) ) ) {
		$font_family = '';
		if( !empty($unisphere_options['fonts_body_font_family']) && startsWith( $unisphere_options['fonts_body_font_family']['face'], 'google' ) )
			$font_family .= str_replace( 'google:', '', $unisphere_options['fonts_body_font_family']['face'] ) . ':300,300italic,400,400italic,600,600italic,700,700italic';

		if( !empty($unisphere_options['fonts_top_nav_menu_font']) && startsWith( $unisphere_options['fonts_top_nav_menu_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_top_nav_menu_font']['face'] ) . ':' . $unisphere_options['fonts_top_nav_menu_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_sub_header_title_font']) && startsWith( $unisphere_options['fonts_sub_header_title_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_sub_header_title_font']['face'] ) . ':' . $unisphere_options['fonts_sub_header_title_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_sub_header_description_font']) && startsWith( $unisphere_options['fonts_sub_header_description_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_sub_header_description_font']['face'] ) . ':' . $unisphere_options['fonts_sub_header_description_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_slider_title_font']) && startsWith( $unisphere_options['fonts_slider_title_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_slider_title_font']['face'] ) . ':' . $unisphere_options['fonts_slider_title_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_headings_font']) && startsWith( $unisphere_options['fonts_headings_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_headings_font']['face'] ) . ':300,400,600,700';

		if( !empty($unisphere_options['fonts_blog_title_font']) && startsWith( $unisphere_options['fonts_blog_title_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_blog_title_font']['face'] ) . ':' . $unisphere_options['fonts_blog_title_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_sidebar_widget_titles_font']) && startsWith( $unisphere_options['fonts_sidebar_widget_titles_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_sidebar_widget_titles_font']['face'] ) . ':' . $unisphere_options['fonts_sidebar_widget_titles_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_footer_widget_titles_font']) && startsWith( $unisphere_options['fonts_footer_widget_titles_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_footer_widget_titles_font']['face'] ) . ':' . $unisphere_options['fonts_footer_widget_titles_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_portfolio_title_1_column_font']) && startsWith( $unisphere_options['fonts_portfolio_title_1_column_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_portfolio_title_1_column_font']['face'] ) . ':' . $unisphere_options['fonts_portfolio_title_1_column_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_portfolio_title_gutter_font']) && startsWith( $unisphere_options['fonts_portfolio_title_gutter_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_portfolio_title_gutter_font']['face'] ) . ':' . $unisphere_options['fonts_portfolio_title_gutter_font']['fontstyle'];

		if( !empty($unisphere_options['fonts_portfolio_title_no_gutter_font']) && startsWith( $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'], 'google' ) )
			$font_family .= '|' . str_replace( 'google:', '', $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'] ) . ':' . $unisphere_options['fonts_portfolio_title_no_gutter_font']['fontstyle'];

		// Remove any repeated pipes and also single pipe at the start of the string
		while( strpos( $font_family, '||' ) !== false )
			$font_family = str_replace( '||', '|', $font_family );
			
		if( startsWith( $font_family, '|') )
			$font_family = substr($font_family, 1);

		// Subsets to use
		$subsets = '';
		if( !empty( $unisphere_options['fonts_subsets_google'] ) ) {
			foreach( $unisphere_options['fonts_subsets_google'] as $key => $option ) {
				if( $option == 1 )
					$subsets .= ',' . $key;
			}
		}

		// Remove single comma at the start of the string
		if( startsWith( $subsets, ',') )
			$subsets = substr($subsets, 1);

		if( $subsets != '')
			$subsets = '&amp;subset=' . $subsets;

		$parameters = $font_family . $subsets;

		// Cache it for 60 minutes
		set_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_google_fonts', $parameters, 60 * 60 );
	}

	// Finally add the link to the Google web font CSS file
	if( !empty( $parameters ) ) {
		wp_register_style( 'google_fonts', 'http://fonts.googleapis.com/css?family=' . $parameters ); 
		wp_enqueue_style( 'google_fonts');  
	}
}
}

 
// See if any Cufon font has been selected for use and enqueue the scripts
if ( !function_exists( 'unisphere_enqueue_cufon_fonts' ) ) {
function unisphere_enqueue_cufon_fonts() {	
	global $unisphere_options;

	// Check in the cache first
	if( false === ( $cufon_fonts = get_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_cufon_fonts' ) ) ) {
		$cufon_fonts = array();
		if( !empty($unisphere_options['fonts_top_nav_menu_font']) && startsWith( $unisphere_options['fonts_top_nav_menu_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_top_nav_menu_font']['face'];

		if( !empty($unisphere_options['fonts_sub_header_title_font']) && startsWith( $unisphere_options['fonts_sub_header_title_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_sub_header_title_font']['face'];

		if( !empty($unisphere_options['fonts_sub_header_description_font']) && startsWith( $unisphere_options['fonts_sub_header_description_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_sub_header_description_font']['face'];

		if( !empty($unisphere_options['fonts_slider_title_font']) && startsWith( $unisphere_options['fonts_slider_title_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_slider_title_font']['face'];

		if( !empty($unisphere_options['fonts_headings_font']) && startsWith( $unisphere_options['fonts_headings_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_headings_font']['face'];

		if( !empty($unisphere_options['fonts_blog_title_font']) && startsWith( $unisphere_options['fonts_blog_title_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_blog_title_font']['face'];

		if( !empty($unisphere_options['fonts_sidebar_widget_titles_font']) && startsWith( $unisphere_options['fonts_sidebar_widget_titles_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_sidebar_widget_titles_font']['face'];

		if( !empty($unisphere_options['fonts_footer_widget_titles_font']) && startsWith( $unisphere_options['fonts_footer_widget_titles_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_footer_widget_titles_font']['face'];

		if( !empty($unisphere_options['fonts_portfolio_title_1_column_font']) && startsWith( $unisphere_options['fonts_portfolio_title_1_column_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_portfolio_title_1_column_font']['face'];

		if( !empty($unisphere_options['fonts_portfolio_title_gutter_font']) && startsWith( $unisphere_options['fonts_portfolio_title_gutter_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_portfolio_title_gutter_font']['face'];

		if( !empty($unisphere_options['fonts_portfolio_title_no_gutter_font']) && startsWith( $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'], 'cufon' ) )
			$cufon_fonts[] = $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'];

		// Cache it for 60 minutes
		set_transient( UNISPHERE_THEMEOPTIONS . '-enqueue_cufon_fonts', $cufon_fonts, 60 * 60 );
	}

	foreach( $cufon_fonts as $cufon_font ) {
		$pieces = explode( ':', $cufon_font );
		wp_enqueue_script( $pieces[1], UNISPHERE_FONTS_URL . '/' . $pieces[2] );
	}
}
}


// Get the Cufon font CSS selectors in JSON format
if ( !function_exists( 'unisphere_get_cufon_selectors' ) ) {
function unisphere_get_cufon_selectors() {	
	global $unisphere_options;

	// Check in the cache first
	if( false === ( $cufon_fonts = get_transient( UNISPHERE_THEMEOPTIONS . '-cufon_selectors' ) ) ) {
		$cufon_fonts = '';

		if( !empty($unisphere_options['fonts_top_nav_menu_font']) && startsWith( $unisphere_options['fonts_top_nav_menu_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_top_nav_menu_font']['face'] );
			$cufon_fonts .= ',{"selector": "#header .menu .nav > li > a", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_sub_header_title_font']) && startsWith( $unisphere_options['fonts_sub_header_title_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_sub_header_title_font']['face'] );
			$cufon_fonts .= ',{"selector": "#sub-header h1", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_sub_header_description_font']) && startsWith( $unisphere_options['fonts_sub_header_description_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_sub_header_description_font']['face'] );
			$cufon_fonts .= ',{"selector": "#sub-header small", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_slider_title_font']) && startsWith( $unisphere_options['fonts_slider_title_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_slider_title_font']['face'] );
			$cufon_fonts .= ',{"selector": ".slider-description .slider-title", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_headings_font']) && startsWith( $unisphere_options['fonts_headings_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_headings_font']['face'] );
			$cufon_fonts .= ',{"selector": "h1:not(.sub-header-title,.post-title), h2:not(.slider-title,.post-title), h3:not(.widget-title,#reply-title,#comment-title,#related-work-title,#related-posts-title,#author-info-title,.info-box-title), h4:not(.price-column-title), h5, h6", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_blog_title_font']) && startsWith( $unisphere_options['fonts_blog_title_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_blog_title_font']['face'] );
			$cufon_fonts .= ',{"selector": ".blog-wide-image h2.post-title a, .blog-half-image h2.post-title a, .blog-detail h1.post-title", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_sidebar_widget_titles_font']) && startsWith( $unisphere_options['fonts_sidebar_widget_titles_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_sidebar_widget_titles_font']['face'] );
			$cufon_fonts .= ',{"selector": "#secondary .widget > h3", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_footer_widget_titles_font']) && startsWith( $unisphere_options['fonts_footer_widget_titles_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_footer_widget_titles_font']['face'] );
			$cufon_fonts .= ',{"selector": ".footer-column .widget > .widget-title", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_portfolio_title_1_column_font']) && startsWith( $unisphere_options['fonts_portfolio_title_1_column_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_portfolio_title_1_column_font']['face'] );
			$cufon_fonts .= ',{"selector": ".portfolio-1-columns-list .portfolio-title", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_portfolio_title_gutter_font']) && startsWith( $unisphere_options['fonts_portfolio_title_gutter_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_portfolio_title_gutter_font']['face'] );
			$cufon_fonts .= ',{"selector": ".portfolio-2-columns-list .portfolio-title, .portfolio-3-columns-list .portfolio-title, .portfolio-4-columns-list .portfolio-title", "fontFamily": "' . $pieces[1] . '"}';
		}

		if( !empty($unisphere_options['fonts_portfolio_title_no_gutter_font']) && startsWith( $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'], 'cufon' ) ) {
			$pieces = explode( ':', $unisphere_options['fonts_portfolio_title_no_gutter_font']['face'] );
			$cufon_fonts .= ',{"selector": ".portfolio-2-columns-list-no-gutter .portfolio-title, .portfolio-3-columns-list-no-gutter .portfolio-title, .portfolio-4-columns-list-no-gutter .portfolio-title", "fontFamily": "' . $pieces[1] . '"}';
		}

		// Remove single comma at the start of the string
		if( startsWith( $cufon_fonts, ',') )
			$cufon_fonts = substr($cufon_fonts, 1);

		if( $cufon_fonts != '' ) {
			$cufon_fonts = '{"fonts": [' . $cufon_fonts;
			$cufon_fonts .= ']}';
		}

		// Cache it for 60 minutes
		set_transient( UNISPHERE_THEMEOPTIONS . '-cufon_selectors', $cufon_fonts, 60 * 60 );
	}

	return $cufon_fonts;
}
}

?>