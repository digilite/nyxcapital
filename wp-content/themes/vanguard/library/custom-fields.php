<?php
/**
 * Post/Page Custom Fields Meta Boxes
 */

global $new_meta_boxes, $unisphere_options;

$video_options = array( array( "value" => "disabled", "text" => "Don't display"),
						array( "value" => "youtube", "text" => "YouTube"),
						array( "value" => "vimeo", "text" => "Vimeo") );

$video_options_blog = array( array( "value" => "youtube", "text" => "YouTube"),
						     array( "value" => "vimeo", "text" => "Vimeo") );

if( class_exists('SublimeVideo') ) {
	$video_options[] = array( "value" => "html5_flv", "text" => "HTML5/FLV");
	$video_options_blog[] = array( "value" => "html5_flv", "text" => "HTML5/FLV");
} else {
	$video_options[] = array( "value" => "html5_flv_disabled", "text" => "HTML5/FLV");
	$video_options_blog[] = array( "value" => "html5_flv_disabled", "text" => "HTML5/FLV");
}

$slider_options = array( array( "value" => "", "text" => "Normal"),
						 array( "value" => "fullwidth", "text" => "Full-width") );

if( function_exists('layerslider_init') ) {
	$slider_options[] = array( "value" => "layer_slider", "text" => "LayerSlider");
} else {
	$slider_options[] = array( "value" => "layer_slider_disabled", "text" => "LayerSlider");
}

$new_meta_boxes =
array(

	// PORTFOLIO ITEM
	"_portfolio_item_toggle_start" => array(
	"name" => "_portfolio_item_toggle_start",
	"std" => "",
	"title" => "Portfolio Item Settings",
	"description" => "",
	"type" => "toggle-start",
	"location" => "Portfolio"),

		"_portfolio_detail_big_images" => array(
		"name" => "_portfolio_detail_big_images",
		"std" => "",
		"title" => "How to display images in detail page?",
		"description" => "",
		"type" => "select",
		"class" => "mini",
		"options" => array( array( "value" => "none", "text" => "Don't display"),
							array( "value" => "big", "text" => "Big Images&nbsp;&nbsp;"),
							array( "value" => "slider", "text" => "Slider"),
							array( "value" => "gallery", "text" => "Gallery"),
							array( "value" => "featured", "text" => "Featured image only") ),
		"location" => "Portfolio"),

		"_portfolio_slider_height" => array(
		"name" => "_portfolio_slider_height",
		"std" => "",
		"title" => "Override the Slider height",
		"description" => "",
		"type" => "text",
		"location" => "Portfolio"),
		
		"_portfolio_exclude_featured_image" => array(
		"name" => "_portfolio_exclude_featured_image",
		"std" => "",
		"title" => "Exclude the thumbnail (featured image) from the image listing in the Portfolio Item detail page?",
		"description" => "Check this if you want to exclude the thumbnail (featured image) from the listing in the portfolio item detail page",
		"type" => "checkbox",
		"location" => "Portfolio"),

		"_portfolio_no_lightbox" => array(
		"name" => "_portfolio_no_lightbox",
		"std" => "",
		"title" => "Thumbnail links to Portfolio Item Detail?",
		"description" => "Check this if you want the thumbnail to link directly to the portfolio item detail or your custom defined URL below instead of opening the full image in the lightbox",
		"type" => "checkbox",
		"location" => "Portfolio"),
		
		"_portfolio_link" => array(
		"name" => "_portfolio_link",
		"std" => "",
		"title" => "Portfolio Item custom destination URL",
		"description" => "Set the destination link of the Portfolio Item when a user clicks it, leave blank to link to the default Portfolio Item detail page.<br />Example: http://www.google.com/",
		"type" => "text",
		"location" => "Portfolio"),

		"_portfolio_video" => array(
		"name" => "_portfolio_video",
		"std" => "",
		"title" => "Portfolio Video",
		"description" => "",
		"type" => "select",
		"class" => "mini",
		"options" => $video_options,
		"location" => "Portfolio"),

		"_portfolio_video_no_sublimevideo" => array(
		"name" => "_portfolio_video_no_sublimevideo",
		"std" => "",
		"title" => "The SublimeVideo plugin isn't installed",
		"description" => "In order to use self-hosted HTML5 and/or FLV videos you need to install the <a href=\"http://wordpress.org/extend/plugins/sublimevideo-official/\">SublimeVideo plugin</a>. Install the plugin and follow the instructions on how to create a free account.",
		"type" => "info",
		"location" => "Portfolio"),

		"_portfolio_video_youtube" => array(
		"name" => "_portfolio_video_youtube",
		"std" => "",
		"title" => "YouTube video URL",
		"description" => "Example: http://www.youtube.com/watch?v=B0ky-VMi9fI<br/>Example (lightbox size): http://www.youtube.com/watch?v=B0ky-VMi9fI&width=1280&height=720",
		"type" => "text",
		"location" => "Portfolio"),

		"_portfolio_video_vimeo" => array(
		"name" => "_portfolio_video_vimeo",
		"std" => "",
		"title" => "Vimeo video URL",
		"description" => "Example: http://vimeo.com/14824441<br/>Example (lightbox size): http://vimeo.com/14824441?width=1280&height=720",
		"type" => "text",
		"location" => "Portfolio"),

		"_portfolio_video_html5_mp4" => array(
		"name" => "_portfolio_video_html5_mp4",
		"std" => "",
		"title" => "MP4",
		"description" => "The MP4 format is required when adding an HTML5 video.",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_video_html5_mp4_hd" => array(
		"name" => "_portfolio_video_html5_mp4_hd",
		"std" => "",
		"title" => "MP4 HD version (Optional)",
		"description" => "",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_video_html5_webm" => array(
		"name" => "_portfolio_video_html5_webm",
		"std" => "",
		"title" => "WebM or Ogg (Optional)",
		"description" => "The WebM or Ogg file is optional but recommend for a better cross browser compatibility of HTML5 video playback. If this field is not provided the player will display the MP4 video using Flash.",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_video_html5_webm_hd" => array(
		"name" => "_portfolio_video_html5_webm_hd",
		"std" => "",
		"title" => "WebM or Ogg HD version (Optional)",
		"description" => "",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_video_flv" => array(
		"name" => "_portfolio_video_flv",
		"std" => "",
		"title" => "FLV",
		"description" => "Ignore the FLV video format if you set a MP4 video file above. Use this field when you have a video only in FLV format (keep in mind that mobile devices that do not support flash playback won't be able to play this format).",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_video_flv_hd" => array(
		"name" => "_portfolio_video_flv_hd",
		"std" => "",
		"title" => "FLV HD version (Optional)",
		"description" => "",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_autoplay" => array(
		"name" => "_portfolio_autoplay",
		"std" => "",
		"title" => "Don't Autoplay Video?",
		"description" => "Check this if you don't want the video to autoplay.",
		"type" => "checkbox",
		"location" => "Portfolio"),

		"_portfolio_no_autoplay_thumbnail" => array(
		"name" => "_portfolio_no_autoplay_thumbnail",
		"std" => "",
		"title" => "Set the video splash image when adding a video without autoplay",
		"description" => "Set the path of the splash image to display in this video if not using the autoplay feature.",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_video_lightbox_width" => array(
		"name" => "_portfolio_video_lightbox_width",
		"std" => "",
		"title" => "Video width in lightbox",
		"description" => "Set the video width when displaying it in the lightbox.",
		"type" => "text",
		"location" => "Portfolio"),

		"_portfolio_video_lightbox_height" => array(
		"name" => "_portfolio_video_lightbox_height",
		"std" => "",
		"title" => "Video height in lightbox",
		"description" => "Set the video height when displaying it in the lightbox.",
		"type" => "text",
		"location" => "Portfolio"),

		"_portfolio_item_toggle_stop" => array(
		"name" => "_portfolio_item_toggle_stop",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => "Portfolio"),

	// PORTFOLIO ITEM OVERRIDE IMAGES
	"_portfolio_override_images_toggle_start" => array(
	"name" => "_portfolio_override_images_toggle_start",
	"std" => "",
	"title" => "Portfolio Item (override auto generated thumbnails)",
	"description" => "",
	"type" => "toggle-start",
	"location" => "Portfolio"),
		
		"_portfolio_thumb_1" => array(
		"name" => "_portfolio_thumb_1",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Portfolio items (1 Column view)",
		"description" => "<strong>Size:</strong> 480x[ANY-HEIGHT]",
		"type" => "image",
		"location" => "Portfolio"),	

		"_portfolio_thumb_2" => array(
		"name" => "_portfolio_thumb_2",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Portfolio items (2 Column view)",
		"description" => "<strong>Size:</strong> 470x" . $unisphere_options['advanced_image_size_2_column_portfolio_height'],
		"type" => "image",
		"location" => "Portfolio"),	

		"_portfolio_thumb_2_no_gutter" => array(
		"name" => "_portfolio_thumb_2_no_gutter",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Portfolio items (2 Column view without gutter)",
		"description" => "<strong>Size:</strong> 480x" . $unisphere_options['advanced_image_size_2_column_portfolio_no_gutter_height'],
		"type" => "image",
		"location" => "Portfolio"),	

		"_portfolio_thumb_3" => array(
		"name" => "_portfolio_thumb_3",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Portfolio items (3 Column view)",
		"description" => "<strong>Size:</strong> 300x" . $unisphere_options['advanced_image_size_3_column_portfolio_height'],
		"type" => "image",
		"location" => "Portfolio"),	

		"_portfolio_thumb_3_no_gutter" => array(
		"name" => "_portfolio_thumb_3_no_gutter",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Portfolio items (3 Column view without gutter)",
		"description" => "<strong>Size:</strong> 320x" . $unisphere_options['advanced_image_size_3_column_portfolio_no_gutter_height'],
		"type" => "image",
		"location" => "Portfolio"),	

		"_portfolio_thumb_4" => array(
		"name" => "_portfolio_thumb_4",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Portfolio items (4 Column view)",
		"description" => "<strong>Size:</strong> 225x" . $unisphere_options['advanced_image_size_4_column_portfolio_height'],
		"type" => "image",
		"location" => "Portfolio"),	

		"_portfolio_thumb_4_no_gutter" => array(
		"name" => "_portfolio_thumb_4_no_gutter",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Portfolio items (4 Column view without gutter)",
		"description" => "<strong>Size:</strong> 240x" . $unisphere_options['advanced_image_size_4_column_portfolio_no_gutter_height'],
		"type" => "image",
		"location" => "Portfolio"),	

		"_portfolio_sidebar_posts_thumb" => array(
		"name" => "_portfolio_sidebar_posts_thumb",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Popular and Recent portfolio items widget",
		"description" => "<strong>Size:</strong> 210x[ANY-HEIGHT]",
		"type" => "image",
		"location" => "Portfolio"),

		"_portfolio_override_images_toggle_stop" => array(
		"name" => "_portfolio_override_images_toggle_stop",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => "Portfolio"),

	// BLOG POST
	"_blog_post_toggle_start" => array(
	"name" => "_blog_post_toggle_start",
	"std" => "",
	"title" => "Blog Post Settings",
	"description" => "",
	"type" => "toggle-start",
	"location" => "Post"),

		"_blog_post_media" => array(
		"name" => "_blog_post_media",
		"std" => "",
		"title" => "What media should be displayed in this blog post?",
		"description" => "",
		"type" => "select",
		"class" => "mini",
		"options" => array( array( "value" => "image", "text" => "Image"),
							array( "value" => "slider", "text" => "Slider"),
							array( "value" => "video", "text" => "Video") ),
		"location" => "Post"),

		"_blog_post_media_overlay_off" => array(
		"name" => "_blog_post_media_overlay_off",
		"std" => "",
		"title" => "Prevent post title and meta from overlaying on top of the image?",
		"description" => "Check this if you don't want to prevent the post title and meta from overlaying on top of the image (wide blog template only).",
		"type" => "checkbox",
		"location" => "Post"),

		"_blog_post_slider_height" => array(
		"name" => "_blog_post_slider_height",
		"std" => "",
		"title" => "Override the Slider height",
		"description" => "",
		"type" => "text",
		"location" => "Post"),
		
		"_blog_post_video" => array(
		"name" => "_blog_post_video",
		"std" => "",
		"title" => "Video type",
		"description" => "",
		"type" => "select",
		"class" => "mini",
		"options" => $video_options_blog,
		"location" => "Post"),

		"_blog_post_video_no_sublimevideo" => array(
		"name" => "_blog_post_video_no_sublimevideo",
		"std" => "",
		"title" => "The SublimeVideo plugin isn't installed",
		"description" => "In order to use self-hosted HTML5 and/or FLV videos you need to install the <a href=\"http://wordpress.org/extend/plugins/sublimevideo-official/\">SublimeVideo plugin</a>. Install the plugin and follow the instructions on how to create a free account.",
		"type" => "info",
		"location" => "Post"),

		"_blog_post_video_youtube" => array(
		"name" => "_blog_post_video_youtube",
		"std" => "",
		"title" => "YouTube video URL",
		"description" => "Example: http://www.youtube.com/watch?v=B0ky-VMi9fI",
		"type" => "text",
		"location" => "Post"),

		"_blog_post_video_vimeo" => array(
		"name" => "_blog_post_video_vimeo",
		"std" => "",
		"title" => "Vimeo video URL",
		"description" => "Example: http://vimeo.com/14824441",
		"type" => "text",
		"location" => "Post"),

		"_blog_post_video_html5_mp4" => array(
		"name" => "_blog_post_video_html5_mp4",
		"std" => "",
		"title" => "MP4",
		"description" => "The MP4 format is required when adding an HTML5 video.",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_video_html5_mp4_hd" => array(
		"name" => "_blog_post_video_html5_mp4_hd",
		"std" => "",
		"title" => "MP4 HD version (Optional)",
		"description" => "",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_video_html5_webm" => array(
		"name" => "_blog_post_video_html5_webm",
		"std" => "",
		"title" => "WebM or Ogg (Optional)",
		"description" => "The WebM or Ogg file is optional but recommend for a better cross browser compatibility of HTML5 video playback. If this field is not provided the player will display the MP4 video using Flash.",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_video_html5_webm_hd" => array(
		"name" => "_blog_post_video_html5_webm_hd",
		"std" => "",
		"title" => "WebM or Ogg HD version (Optional)",
		"description" => "",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_video_flv" => array(
		"name" => "_blog_post_video_flv",
		"std" => "",
		"title" => "FLV",
		"description" => "Ignore the FLV video format if you set a MP4 video file above. Use this field when you have a video only in FLV format (keep in mind that mobile devices that do not support flash playback won't be able to play this format).",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_video_flv_hd" => array(
		"name" => "_blog_post_video_flv_hd",
		"std" => "",
		"title" => "FLV HD version (Optional)",
		"description" => "",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_autoplay" => array(
		"name" => "_blog_post_autoplay",
		"std" => "",
		"title" => "Don't Autoplay Video?",
		"description" => "Check this if you don't want the video to autoplay.",
		"type" => "checkbox",
		"location" => "Post"),

		"_blog_post_no_autoplay_thumbnail" => array(
		"name" => "_blog_post_no_autoplay_thumbnail",
		"std" => "",
		"title" => "Set the video splash image when adding a video without autoplay",
		"description" => "Set the path of the splash image to display in this video if not using the autoplay feature.",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_toggle_stop" => array(
		"name" => "_blog_post_toggle_stop",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => "Post"),

	// BLOG POST OVERRIDE IMAGES
	"_blog_post_override_images_toggle_start" => array(
	"name" => "_blog_post_override_images_toggle_start",
	"std" => "",
	"title" => "Blog Post (override auto generated thumbnails)",
	"description" => "",
	"type" => "toggle-start",
	"location" => "Post"),

		"_blog_wide_image_thumb" => array(
		"name" => "_blog_wide_image_thumb",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Blog post (wide image blog template)",
		"description" => "<strong>Size:</strong> 710x[ANY-HEIGHT]",
		"type" => "image",
		"location" => "Post"),

		"_blog_half_image_thumb" => array(
		"name" => "_blog_half_image_thumb",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Blog post (half image blog template)",
		"description" => "<strong>Size:</strong> 340x[ANY-HEIGHT]",
		"type" => "image",
		"location" => "Post"),

		"_blog_detail_thumb" => array(
		"name" => "_blog_detail_thumb",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Blog post (detail page)",
		"description" => "<strong>Size:</strong> 710x[ANY-HEIGHT]",
		"type" => "image",
		"location" => "Post"),

		"_blog_sidebar_posts_thumb" => array(
		"name" => "_blog_sidebar_posts_thumb",
		"std" => "",
		"title" => "Override the default thumbnail generated for the Popular and Recent posts widget",
		"description" => "<strong>Size:</strong> 210x[ANY-HEIGHT]",
		"type" => "image",
		"location" => "Post"),

		"_blog_post_override_images_toggle_stop" => array(
		"name" => "_blog_post_override_images_toggle_stop",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => "Post"),

	// SUB-HEADER BAR
	"_page_toggle_start_sub_header_bar" => array(
	"name" => "_page_toggle_start_sub_header_bar",
	"std" => "",
	"title" => "Sub-header Bar",
	"description" => "",
	"type" => "toggle-start",
	"location" => array( "Page", "Post", "Portfolio") ),

		"_page_sub_header_style" => array(
		"name" => "_page_sub_header_style",
		"std" => "",
		"title" => "Sub-header style",
		"description" => "",
		"type" => "select",
		"class" => "mini",
		"options" => array( array( "value" => "", "text" => "Don't display"),
							array( "value" => "solid-bg", "text" => "Solid background"),
							array( "value" => "transparent-bg", "text" => "Transparent background"),
							array( "value" => "image-bg", "text" => "Background image") ),
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_sub_header_title" => array(
		"name" => "_page_sub_header_title",
		"std" => "",
		"title" => "Override the sub-header title",
		"description" => "",
		"type" => "text",
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_sub_header_desc" => array(
		"name" => "_page_sub_header_desc",
		"std" => "",
		"title" => "Sub-header description",
		"description" => "",
		"type" => "text",
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_sub_header_height" => array(
		"name" => "_page_sub_header_height",
		"std" => "",
		"title" => "Override the sub-header height",
		"description" => "",
		"type" => "text",
		"location" => array( "Page", "Post", "Portfolio") ),
		
		"_page_sub_header_image" => array(
		"name" => "_page_sub_header_image",
		"std" => "",
		"title" => "Sub-header background image",
		"description" => "",
		"type" => "background",
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_toggle_stop_sub_header_bar" => array(
		"name" => "_page_toggle_stop_sub_header_bar",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => array( "Page", "Post", "Portfolio") ),

	// SUB-HEADER SLIDER
	"_page_toggle_start_sub_header_slider" => array(
	"name" => "_page_toggle_start_sub_header_slider",
	"std" => "",
	"title" => "Sub-header Slider",
	"description" => "",
	"type" => "toggle-start",
	"location" => array( "Page", "Post", "Portfolio") ),

		"_page_show_slider" => array(
		"name" => "_page_show_slider",
		"std" => "",
		"title" => "Show a slider?",
		"description" => "Check this if you want to show a slider in this page.",
		"type" => "checkbox",
		"location" => array( "Page", "Post", "Portfolio")),

		"_page_slider_type" => array(
		"name" => "_page_slider_type",
		"std" => "",
		"title" => "Choose Slider",
		"description" => "",
		"type" => "select",
		"class" => "mini",
		"options" => $slider_options,
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_slider_no_layerslider" => array(
		"name" => "_page_slider_no_layerslider",
		"std" => "",
		"title" => "The LayerSlider WP plugin isn't installed",
		"description" => "In order to use the LayerSlider please install the plugin provided with the theme.",
		"type" => "info",
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_slider_layer_id" => array(
		"name" => "_page_slider_layer_id",
		"std" => "",
		"title" => "LayerSlider to display",
		"description" => "Please select which LayerSlider you want to display in this page/post",
		"type" => "slider_layer_id",
		"class" => "medium",
		"location" => array( "Page", "Post", "Portfolio")),

		"_page_slider_height" => array(
		"name" => "_page_slider_height",
		"std" => "",
		"title" => "Override the Slider height",
		"description" => "",
		"type" => "text",
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_slider_num_items" => array(
		"name" => "_page_slider_num_items",
		"std" => "",
		"title" => "Number of items in this slider",
		"description" => "",
		"type" => "text",
		"location" => array( "Page", "Post", "Portfolio")),
		
		"_page_slider_seconds" => array(
		"name" => "_page_slider_seconds",
		"std" => "5000",
		"title" => "Slider speed between transitions",
		"description" => "The number of milliseconds between transitions. (1 second equals 1000 milliseconds)",
		"type" => "text",
		"location" => array( "Page", "Post", "Portfolio")),
			
		"_page_slider_transition_seconds" => array(
		"name" => "_page_slider_transition_seconds",
		"std" => "500",
		"title" => "The number of milliseconds of each transition animation. (1 second equals 1000 milliseconds)",
		"description" => "",
		"type" => "text",
		"location" => array( "Page", "Post", "Portfolio")),

		"_page_slider_cat" => array(
		"name" => "_page_slider_cat",
		"std" => "",
		"title" => "Slider Categories",
		"description" => "If you want to display a slider in this page/post, then set the slider categories to be displayed",
		"type" => "slider_cat",
		"location" => array( "Page", "Post", "Portfolio")),
	
		"_page_toggle_stop_sub_header_slider" => array(
		"name" => "_page_toggle_stop_sub_header_slider",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => array( "Page", "Post", "Portfolio") ),

	// PORTFOLIO
	"_page_toggle_start_portfolio" => array(
	"name" => "_page_toggle_start_portfolio",
	"std" => "",
	"title" => "Portfolio",
	"description" => "",
	"type" => "toggle-start",
	"location" => "Page"),
	
		"_page_portfolio_cat" => array(
		"name" => "_page_portfolio_cat",
		"std" => "",
		"title" => "Portfolio Categories",
		"description" => "If this page uses a Portfolio page template, then set the categories to be displayed",
		"type" => "portfolio_cat",
		"location" => "Page"),
		
		"_page_portfolio_num_items_page" => array(
		"name" => "_page_portfolio_num_items_page",
		"std" => "",
		"title" => "Number of Portfolio items per Page",
		"description" => "If this page uses a Portfolio page template, you can set the number of items displayed per page and the template will paginate the items<br />Leave blank if you don't want to paginate the portfolio items",
		"type" => "text",
		"location" => "Page"),

		"_page_toggle_stop_portfolio" => array(
		"name" => "_page_toggle_stop_portfolio",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => "Page"),

	// BLOG
	"_page_toggle_start_blog" => array(
	"name" => "_page_toggle_start_blog",
	"std" => "",
	"title" => "Blog",
	"description" => "",
	"type" => "toggle-start",
	"location" => "Page"),

		"_page_blog_cat" => array(
		"name" => "_page_blog_cat",
		"std" => "",
		"title" => "Blog Categories",
		"description" => "If this page uses a Blog page template, then set the categories to be displayed",
		"type" => "blog_cat",
		"location" => "Page"),

		"_page_blog_num_items_page" => array(
		"name" => "_page_blog_num_items_page",
		"std" => "",
		"title" => "Number of Blog Posts per Page",
		"description" => "If this page uses a Blog page template, you can set the number of items displayed per page and the template will paginate the posts",
		"type" => "text",
		"location" => "Page"),

		"_page_toggle_stop_blog" => array(
		"name" => "_page_toggle_stop_blog",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => "Page"),

	// SIDEBAR
	"_page_toggle_start_sidebar" => array(
	"name" => "_page_toggle_start_sidebar",
	"std" => "",
	"title" => "Sidebar",
	"description" => "",
	"type" => "toggle-start",
	"location" => array( "Page", "Post", "Portfolio" ) ),

		"_page_custom_sidebar" => array(
		"name" => "_page_custom_sidebar",
		"std" => "",
		"title" => "Custom sidebar",
		"description" => "Select the sidebar you wish to display on this page, and which sidebar it will replace.<br/>If you haven't created custom sidebars yet, you can do it now: <a href='admin.php?page=options-framework-sidebar-manager'>create custom sidebar +</a>",
		"type" => "sidebar",
		"location" => array( "Page", "Post", "Portfolio" ) ),

		"_page_toggle_stop_sidebar" => array(
		"name" => "_page_toggle_stop_sidebar",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => array( "Page", "Post", "Portfolio" ) ),

	// FULL PAGE BACKGROUND
	"_page_toggle_start_full_page_bg" => array(
	"name" => "_page_toggle_start_full_page_bg",
	"std" => "",
	"title" => "Full page background image",
	"description" => "",
	"type" => "toggle-start",
	"location" => array( "Page", "Post", "Portfolio") ),

		"_page_full_page_bg" => array(
		"name" => "_page_full_page_bg",
		"std" => "",
		"title" => "Full page background image",
		"description" => "This image is centered and covers all the page background while maintaining aspect ratio (overlaps the body background image).",
		"type" => "image",
		"location" => array( "Page", "Post", "Portfolio") ),

		"_page_toggle_stop_full_page_bg" => array(
		"name" => "_page_toggle_stop_full_page_bg",
		"std" => "",
		"title" => "",
		"description" => "",
		"type" => "toggle-end",
		"location" => array( "Page", "Post", "Portfolio") ),
	
	// SLIDER ITEM
	"_slider_link" => array(
	"name" => "_slider_link",
	"std" => "",
	"title" => "Slider Item custom destination URL",
	"description" => "Set the destination link of the slider item when a user clicks it (doesn't work with embedded videos), leave blank if you don't want to link anywhere.<br />Example: http://www.google.com/",
	"type" => "text",
	"location" => "Slider"),	
	
	"_slider_show_title" => array(
	"name" => "_slider_show_title",
	"std" => "",
	"title" => "Show slider title?",
	"description" => "Check this if you want to show a title in this slider item.",
	"type" => "checkbox",
	"location" => "Slider"),

	"_slider_video" => array(
	"name" => "_slider_video",
	"std" => "",
	"title" => "Display embedded video directly in this Slider Item",
	"description" => "",
	"type" => "select",
	"class" => "mini",
	"options" => $video_options,
	"location" => "Slider"),

	"_slider_video_no_sublimevideo" => array(
	"name" => "_slider_video_no_sublimevideo",
	"std" => "",
	"title" => "The SublimeVideo plugin isn't installed",
	"description" => "In order to use self-hosted HTML5 and/or FLV videos you need to install the <a href=\"http://wordpress.org/extend/plugins/sublimevideo-official/\">SublimeVideo plugin</a>. Install the plugin and follow the instructions on how to create a free account.",
	"type" => "info",
	"location" => "Slider"),

	"_slider_video_youtube" => array(
	"name" => "_slider_video_youtube",
	"std" => "",
	"title" => "YouTube video URL",
	"description" => "Example: http://www.youtube.com/watch?v=B0ky-VMi9fI",
	"type" => "text",
	"location" => "Slider"),

	"_slider_video_vimeo" => array(
	"name" => "_slider_video_vimeo",
	"std" => "",
	"title" => "Vimeo video URL",
	"description" => "Example: http://vimeo.com/14824441",
	"type" => "text",
	"location" => "Slider"),

	"_slider_video_html5_mp4" => array(
	"name" => "_slider_video_html5_mp4",
	"std" => "",
	"title" => "MP4",
	"description" => "The MP4 format is required when adding an HTML5 video.",
	"type" => "image",
	"location" => "Slider"),

	"_slider_video_html5_mp4_hd" => array(
	"name" => "_slider_video_html5_mp4_hd",
	"std" => "",
	"title" => "MP4 HD version (Optional)",
	"description" => "",
	"type" => "image",
	"location" => "Slider"),

	"_slider_video_html5_webm" => array(
	"name" => "_slider_video_html5_webm",
	"std" => "",
	"title" => "WebM or Ogg (Optional)",
	"description" => "The WebM or Ogg file is optional but recommend for a better cross browser compatibility of HTML5 video playback. If this field is not provided the player will display the MP4 video using Flash.",
	"type" => "image",
	"location" => "Slider"),

	"_slider_video_html5_webm_hd" => array(
	"name" => "_slider_video_html5_webm_hd",
	"std" => "",
	"title" => "WebM or Ogg HD version (Optional)",
	"description" => "",
	"type" => "image",
	"location" => "Slider"),

	"_slider_video_flv" => array(
	"name" => "_slider_video_flv",
	"std" => "",
	"title" => "FLV",
	"description" => "Ignore the FLV video format if you set a MP4 video file above. Use this field when you have a video only in FLV format (keep in mind that mobile devices that do not support flash playback won't be able to play this format).",
	"type" => "image",
	"location" => "Slider"),

	"_slider_video_flv_hd" => array(
	"name" => "_slider_video_flv_hd",
	"std" => "",
	"title" => "FLV HD version (Optional)",
	"description" => "",
	"type" => "image",
	"location" => "Slider"),

	"_slider_autoplay" => array(
	"name" => "_slider_autoplay",
	"std" => "",
	"title" => "Don't Autoplay Video?",
	"description" => "Check this if you don't want the Stage Slider video to autoplay.",
	"type" => "checkbox",
	"location" => "Slider"),

	"_slider_no_autoplay_thumbnail" => array(
	"name" => "_slider_no_autoplay_thumbnail",
	"std" => "",
	"title" => "Set the video splash image when adding a video without autoplay",
	"description" => "Set the path of the splash image to display in this video if not using the autoplay feature.",
	"type" => "image",
	"location" => "Slider"),
	
	"_slider_bg_color" => array(
	"name" => "_slider_bg_color",
	"std" => "",
	"title" => "Full-width slider item background color",
	"description" => "If this slider item is used in a full-width slider with images smaller than 1920px, you can set the background color for the slider item so it blends well with your image.",
	"type" => "color",
	"location" => "Slider")

);

function new_meta_boxes_page() {
	new_meta_boxes('Page');
}

function new_meta_boxes_post() {
	new_meta_boxes('Post');
}

function new_meta_boxes_slider() {
	new_meta_boxes('Slider');
}

function new_meta_boxes_portfolio() {
	new_meta_boxes('Portfolio');
}

function new_meta_boxes( $type ) {

	global $post, $new_meta_boxes;
	
	// Use nonce for verification
    echo '<input type="hidden" name="unisphere_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

 	echo '<div id="custom-fields-form-wrap">';

	foreach($new_meta_boxes as $meta_box) {
		if( ( is_array( $meta_box['location'] ) && in_array( $type, $meta_box['location'] ) ) || ( !is_array( $meta_box['location'] ) && $meta_box['location'] == $type ) ) {
			
			if ( $meta_box['type'] == 'title' ) {
				echo '<p style="font-size: 18px; font-weight: bold; font-style: normal; color: #e5e5e5; text-shadow: 0 1px 0 #111; line-height: 40px; background-color: #464646; border: 1px solid #111; padding: 0 10px; -moz-border-radius: 6px;">' . $meta_box[ 'title' ] . '</p>';
			} elseif ( $meta_box['type'] == 'toggle-start' ) {
				echo '<div class="toggle-container">';
				echo '<a class="toggle" href="javascript:void(0);"><span class="toggle-sign">+</span><span class="toggle-title">' . $meta_box[ 'title' ] . '</span></a>';
				echo '<div class="toggle-content" style="display: none;">';
			} elseif ( $meta_box['type'] == 'toggle-end' ) {
				echo '</div></div>';
			} else {
				$meta_box_value = get_post_meta($post->ID, $meta_box['name'], true);
		
				if($meta_box_value == "")
					$meta_box_value = $meta_box['std'];

				echo '<div id="section' . $meta_box['name'] . '" class="section section-' . $meta_box['type'] . ' ' . ( !empty($meta_box[ 'class' ]) ? $meta_box[ 'class' ] : '' ) . '">';
				
				switch ( $meta_box['type'] ) {
					case 'text':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
						echo '<input type="text" id="' . $meta_box[ 'name' ] . '" name="' . $meta_box[ 'name' ] . '" value="' . htmlspecialchars( $meta_box_value ) . '" class="of-input" />';
						echo '</div>';
						echo '<div class="explain explain-' . $meta_box['type'] . '">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;
						
					case 'checkbox':
						if($meta_box_value == '1'){ $checked = "checked=\"checked\""; }else{ $checked = "";} 
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
						echo '<input class="checkbox of-input" type="checkbox" id="' . $meta_box[ 'name' ] . '" name="' . $meta_box[ 'name' ] . '" value="1" ' . $checked . ' />';
						echo '<label class="explain" for="' . $meta_box[ 'name' ] .'">' . $meta_box[ 'description' ] . '</label>';
						echo '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;

					case 'info':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
						echo '<p class="explain">' . $meta_box[ 'description' ] . '</p>';
						echo '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;
						
					case 'select':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
                        echo '<select id="' . $meta_box[ 'name' ] . '" name="' . $meta_box[ 'name' ] . '" class="of-input">';
						// Loop through each option in the array
						foreach ($meta_box[ 'options' ] as $option) {
							if(is_array($option)) {
								echo '<option ' . ( $meta_box_value == $option['value'] ? 'selected="selected"' : '' ) . ' value="' . $option['value'] . '">' . $option['text'] . '</option>';
							} else {
   								echo '<option ' . ( $meta_box_value == $option ? 'selected="selected"' : '' ) . ' value="' . $option['value'] . '">' . $option['text'] . '</option>';
							}
						}                        
						echo '</select>';
						echo '</div>';
						echo '<div class="explain explain-' . $meta_box['type'] . '">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
                        break;

                    case 'slider_layer_id':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';

						// Get WPDB Object
						global $wpdb;

						// Table name
						$table_name = $wpdb->prefix . "layerslider";

						// Get sliders
						$sliders = $wpdb->get_results( "SELECT * FROM $table_name
														WHERE flag_hidden = '0' AND flag_deleted = '0'
														ORDER BY date_c ASC LIMIT 100" );

						echo '<select id="' . $meta_box[ 'name' ] . '" name="' . $meta_box[ 'name' ] . '" class="of-input">';
						// Loop through each option in the array
						foreach ($sliders as $key => $item) {
							$name = empty($item->name) ? 'Unnamed' : $item->name;
							$slider_id = $item->id;
   							echo '<option ' . ( $meta_box_value == $slider_id ? 'selected="selected"' : '' ) . ' value="' . $slider_id . '">' . $name . '</option>';
						}                        
						echo '</select>';
						echo '</div>';
						echo '<div class="explain explain-' . $meta_box['type'] . '">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
                        break;
						
					case 'portfolio_cat':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
						echo '<ul class="sort-children">';
						
						// If building the portfolio categories list, bring the already selected and ordered cats to the top					
						$selected_cats = explode( ",", $meta_box_value );
						foreach ($selected_cats as $selected_cat) { 
							if ($selected_cat != ' ' && $selected_cat != '') {
								$tax_term = get_term( $selected_cat, 'portfolio_category' );
		                		echo '<li class="sortable"><input id="' . $meta_box[ 'name' ] . '_' . $selected_cat . '" class="checkbox of-input" type="checkbox" name="' . $meta_box[ 'name' ] . '[]" value="' . $selected_cat . '" checked="checked" /><label for="' . $meta_box[ 'name' ] . '_' . $selected_cat . '">' . $tax_term->name . '</label></li>';
		                	}
						}
						
						$unselected_args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'exclude' => $selected_cats );
						$unselected_cats = get_categories( $unselected_args );
		                foreach($unselected_cats as $unselected_cat) { 
		                    echo '<li class="sortable"><input id="' . $meta_box[ 'name' ] . '_' . $unselected_cat->cat_ID . '" class="checkbox of-input" type="checkbox" name="' . $meta_box[ 'name' ] . '[]" value="' . $unselected_cat->cat_ID . '" /><label for="' . $meta_box[ 'name' ] . '_' . $unselected_cat->cat_ID . '">' . $unselected_cat->name . '</label></li>';
		                } 
													
						echo '</ul>';						
						echo '</div>';
						echo '<div class="explain explain-multicheck">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;
					
					case 'blog_cat':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
						echo '<ul>';
						
						// If building the blog categories list, bring the already selected and ordered cats to the top					
						$selected_cats = explode( ",", $meta_box_value );
						foreach ($selected_cats as $selected_cat) { 
							if ($selected_cat != ' ' && $selected_cat != '') {
								$tax_term = get_term( $selected_cat, 'category' );
		                		echo '<li><input id="' . $meta_box[ 'name' ] . '_' . $selected_cat . '" class="checkbox of-input" type="checkbox" name="' . $meta_box[ 'name' ] . '[]" value="' . $selected_cat . '" checked="checked" /><label for="' . $meta_box[ 'name' ] . '_' . $selected_cat . '">' . $tax_term->name . '</label></li>';
		                	}
						}
						
						$unselected_args = array( 'taxonomy' => 'category', 'hide_empty' => '0', 'exclude' => $selected_cats );
						$unselected_cats = get_categories( $unselected_args );
		                foreach($unselected_cats as $unselected_cat) { 
		                    echo '<li><input id="' . $meta_box[ 'name' ] . '_' . $unselected_cat->cat_ID . '" class="checkbox of-input" type="checkbox" name="' . $meta_box[ 'name' ] . '[]" value="' . $unselected_cat->cat_ID . '" /><label for="' . $meta_box[ 'name' ] . '_' . $unselected_cat->cat_ID . '">' . $unselected_cat->name . '</label></li>';
		                } 
													
						echo '</ul>';
						echo '</div>';
						echo '<div class="explain explain-multicheck">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;
						
					case 'slider_cat':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
						echo '<ul>';
						
						// If building the slider categories list, bring the already selected and ordered cats to the top					
						$selected_cats = explode( ",", $meta_box_value );
						foreach ($selected_cats as $selected_cat) { 
							if ($selected_cat != ' ' && $selected_cat != '') {
								$tax_term = get_term( $selected_cat, 'slider_category' );
		                		echo '<li><input id="' . $meta_box[ 'name' ] . '_' . $selected_cat . '" class="checkbox of-input" type="checkbox" name="' . $meta_box[ 'name' ] . '[]" value="' . $selected_cat . '" checked="checked" /><label for="' . $meta_box[ 'name' ] . '_' . $selected_cat . '">' . $tax_term->name . '</label></li>';
		                	}
						}
						
						$unselected_args = array( 'taxonomy' => 'slider_category', 'hide_empty' => '0', 'exclude' => $selected_cats );
						$unselected_cats = get_categories( $unselected_args );
		                foreach($unselected_cats as $unselected_cat) { 
		                    echo '<li><input id="' . $meta_box[ 'name' ] . '_' . $unselected_cat->cat_ID . '" class="checkbox of-input" type="checkbox" name="' . $meta_box[ 'name' ] . '[]" value="' . $unselected_cat->cat_ID . '" /><label for="' . $meta_box[ 'name' ] . '_' . $unselected_cat->cat_ID . '">' . $unselected_cat->name . '</label></li>';
		                } 
													
						echo '</ul>';
						echo '</div>';
						echo '<div class="explain explain-multicheck">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;
						
					case 'image':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';						
						echo optionsframework_medialibrary_uploader( $meta_box['name'], $meta_box_value, null ); // New AJAX Uploader using Media Library
						echo '</div>';
						echo '<div class="explain explain-background">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;

					case 'background':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';											

						// Gets the unique option id
						$optionsframework_settings = get_option('optionsframework');
						if (isset($optionsframework_settings['id'])) {
							$option_name = $optionsframework_settings['id'];
						}
						else {
							$option_name = 'optionsframework';
						};

						$background = $meta_box_value;

						if( !isset( $background['repeat'] )) $background['repeat'] = 'repeat';
						if( !isset( $background['position'] )) $background['position'] = 'top center';
						if( !isset( $background['attachment'] )) $background['attachment'] = 'scroll';
						
						// Background Image - New AJAX Uploader using Media Library
						if (!isset($background['image'])) {
							$background['image'] = '';
						}
						
						echo optionsframework_medialibrary_uploader( $meta_box['name'], $background['image'], null, '',0,'image' ); // New AJAX Uploader using Media Library
						$class = 'of-background-properties';
						if ( '' == $background['image'] ) {
							$class .= ' hide';
						}
						echo '<div class="' . esc_attr( $class ) . '">';
						
						// Background Repeat
						echo '<select class="of-background of-background-repeat" name="' . esc_attr( $option_name . '[' . $meta_box['name'] . '][repeat]'  ) . '" id="' . esc_attr( $meta_box['name'] . '_repeat' ) . '">';
						$repeats = of_recognized_background_repeat();
						
						foreach ($repeats as $key => $repeat) {
							echo '<option value="' . esc_attr( $key ) . '" ' . selected( $background['repeat'], $key, false ) . '>'. esc_html( $repeat ) . '</option>';
						}
						echo '</select>';
						
						// Background Position
						echo '<select class="of-background of-background-position" name="' . esc_attr( $option_name . '[' . $meta_box['name'] . '][position]' ) . '" id="' . esc_attr( $meta_box['name'] . '_position' ) . '">';
						$positions = of_recognized_background_position();
						
						foreach ($positions as $key=>$position) {
							echo '<option value="' . esc_attr( $key ) . '" ' . selected( $background['position'], $key, false ) . '>'. esc_html( $position ) . '</option>';
						}
						echo '</select>';
						
						// Background Attachment
						echo '<select class="of-background of-background-attachment" name="' . esc_attr( $option_name . '[' . $meta_box['name'] . '][attachment]' ) . '" id="' . esc_attr( $meta_box['name'] . '_attachment' ) . '">';
						$attachments = of_recognized_background_attachment();
						
						foreach ($attachments as $key => $attachment) {
							echo '<option value="' . esc_attr( $key ) . '" ' . selected( $background['attachment'], $key, false ) . '>' . esc_html( $attachment ) . '</option>';
						}
						echo '</select>';
						echo '</div>';
						echo '</div>';
						echo '<div class="explain explain-background">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;

					case 'color':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';
						echo '<div id="' . $meta_box['name'] . '_picker" class="colorSelector"><div style="background-color:' . $meta_box_value . '"></div></div>';
						echo '<input class="of-color" name="' . $meta_box['name'] . '" id="' . $meta_box['name'] . '" type="text" value="' . $meta_box_value . '" />';
						echo '</div>';
						echo '<div class="explain explain-color">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;

					case 'sidebar':
						echo '<h4 class="heading">' . $meta_box[ 'title' ] . '</h4>';
						echo '<div class="option">';
						echo '<div class="controls">';

						$sidebars = of_get_option('sidebar_list');
						if( !$sidebars )
							$sidebars = array();
						array_unshift( $sidebars, Array( 'id' => 'unisphere_empty', 'name' => 'Empty sidebar' ) );

						$default_sidebars = Array();
						if( $type != 'Portfolio' ) {
							$default_sidebars[] = Array( 'id' => 'main-sidebar', 'name' => 'Main Sidebar' );
						}
						$default_sidebars[] = Array( 'id' => 'superior-header-bar-left', 'name' => 'Superior-header Bar (left)' );
						$default_sidebars[] = Array( 'id' => 'superior-header-bar-right', 'name' => 'Superior-header Bar (right)' );
						$default_sidebars[] = Array( 'id' => 'footer-column-1', 'name' => 'Footer Column 1' );
						$default_sidebars[] = Array( 'id' => 'footer-column-2', 'name' => 'Footer Column 2' );
						$default_sidebars[] = Array( 'id' => 'footer-column-3', 'name' => 'Footer Column 3' );
						$default_sidebars[] = Array( 'id' => 'footer-column-4', 'name' => 'Footer Column 4' );
						$default_sidebars[] = Array( 'id' => 'footer-bar-left', 'name' => 'Footer Bar (left)' );
						$default_sidebars[] = Array( 'id' => 'footer-bar-right', 'name' => 'Footer Bar (right)' );

						$meta_box_values = explode( ",", $meta_box_value );

						foreach( $default_sidebars as $default_sidebar ) {
							echo 'Replace <strong>' . $default_sidebar['name'] . '</strong> with ';
							echo '<select name="' . $meta_box[ 'name' ] . '[]">';
							echo '<option value="">None</option>';							
							foreach ($sidebars as $sidebar) {
								if( !($default_sidebar['id'] == 'main-sidebar' && $sidebar['id'] == 'unisphere_empty') ) {
									$selected = '';
									foreach ($meta_box_values as $meta_box_val) {
										if( $meta_box_val == $default_sidebar['id'] . ';' . $sidebar['id'] ) {
											$selected = 'selected="selected"';
										}
									}
									echo '<option ' . $selected . ' value="' . $default_sidebar['id'] . ';' . $sidebar['id'] . '">' . $sidebar['name'] . '</option>';
								}
							}
							echo '</select>';
							echo '<br />';
						}
						echo '</div>';
						echo '<div class="explain explain-color">' . $meta_box['description'] . '</div>';
						echo '<div class="clear"></div>';
						echo '</div>';
						break;
				}

				echo '</div>';
			}
		}
	}
	
	echo '</div>';
}

function create_meta_box() {
	global $theme_name;
	if ( function_exists('add_meta_box') ) {
		add_meta_box( 'new_meta_boxes_post', UNISPHERE_THEMENAME . ' Post Settings', 'new_meta_boxes_post', 'post', 'normal', 'high' );
		add_meta_box( 'new_meta_boxes_page', UNISPHERE_THEMENAME . ' Page Settings', 'new_meta_boxes_page', 'page', 'normal', 'high' );
		add_meta_box( 'new_meta_boxes_slider', UNISPHERE_THEMENAME . ' Slider Settings', 'new_meta_boxes_slider', 'slider', 'normal', 'high' );
		add_meta_box( 'new_meta_boxes_portfolio', UNISPHERE_THEMENAME . ' Portfolio Settings', 'new_meta_boxes_portfolio', 'portfolio_cpt', 'normal', 'high' );
	}
}

function save_postdata( $post_id ) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !isset( $_POST['unisphere_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['unisphere_meta_box_nonce'], basename(__FILE__)) ) {
		return $post_id;
	}
	
	if ( wp_is_post_revision( $post_id ) or wp_is_post_autosave( $post_id ) )
		return $post_id;
		
	global $post, $new_meta_boxes;

	foreach($new_meta_boxes as $meta_box) {

		if ( $meta_box['type'] != 'title' && $meta_box['type'] != 'toggle-start' && $meta_box['type'] != 'toggle-end' ) {
		
			if ( 'page' == $_POST['post_type'] ) {
				if ( !current_user_can( 'edit_page', $post_id ))
					return $post_id;
			} else {
				if ( !current_user_can( 'edit_post', $post_id ))
					return $post_id;
			}
			
			if ( isset($_POST[$meta_box['name']]) && is_array($_POST[$meta_box['name']]) ) {
				
				$cats = '';
				foreach($_POST[$meta_box['name']] as $cat){
					$cats .= $cat . ",";
				}
				while( strpos( $cats, ',,' ) !== false )
					$cats = str_replace( ',,', ',', $cats );
					
				if( endsWith( $cats, ',') )
					$cats = substr($cats, 0, -1);
				
				if( startsWith( $cats, ',') )
					$cats = substr($cats, 1);

				$data = $cats;
			} elseif( isset($_POST[UNISPHERE_THEMEOPTIONS][$meta_box['name']]) ) {
				$data = ( isset($_POST[UNISPHERE_THEMEOPTIONS][$meta_box['name']]) && $_POST[UNISPHERE_THEMEOPTIONS][$meta_box['name']] != '' ? $_POST[UNISPHERE_THEMEOPTIONS][$meta_box['name']] : '' ); 
			} else { 
				$data = ( isset($_POST[$meta_box['name']]) && $_POST[$meta_box['name']] != '' ? $_POST[$meta_box['name']] : '' ); 
			}

			if(get_post_meta($post_id, $meta_box['name']) == "")
				add_post_meta($post_id, $meta_box['name'], $data, true);
			elseif($data == "")
				delete_post_meta($post_id, $meta_box['name'], get_post_meta($post_id, $meta_box['name'], true));
			elseif($data != get_post_meta($post_id, $meta_box['name'], true))
				update_post_meta($post_id, $meta_box['name'], $data);
		}
	}
}

add_action('admin_menu', 'create_meta_box');
add_action('save_post', 'save_postdata');

function unisphere_custom_field_admin_scripts() {
	global $post;
	if(isset($post) && ($post->post_type == 'page' || $post->post_type == 'post' || $post->post_type == 'portfolio_cpt' || $post->post_type == 'slider')) {
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_register_script('of-medialibrary-uploader', UNISPHERE_ADMIN_JS . '/of-medialibrary-uploader.js', array('jquery','media-upload','thickbox'));
		wp_enqueue_script('of-medialibrary-uploader');
		wp_enqueue_script('colorpicker_js', UNISPHERE_ADMIN_JS . '/colorpicker.js', array('jquery'));  
		wp_enqueue_script('options_custom_js', UNISPHERE_ADMIN_JS . '/options-custom.js', array('jquery'));  
	}
}

function unisphere_custom_field_admin_styles() {
	global $post;
	if(isset($post) && ($post->post_type == 'page' || $post->post_type == 'post' || $post->post_type == 'portfolio_cpt' || $post->post_type == 'slider')) {
		wp_enqueue_style('thickbox');
		wp_enqueue_style('admin_style_css', UNISPHERE_ADMIN_CSS . '/custom-fields-style.css');  
		wp_enqueue_style('colorpicker_css', UNISPHERE_ADMIN_CSS . '/colorpicker.css');  
	}
}

add_action('admin_print_scripts', 'unisphere_custom_field_admin_scripts');
add_action('admin_print_styles', 'unisphere_custom_field_admin_styles');

?>
