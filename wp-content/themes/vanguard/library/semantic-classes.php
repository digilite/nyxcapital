<?php
/**
 * Adds several useful classes tp <body> element
 */
add_filter( 'body_class', 'semantic_body' );
function semantic_body( $classes ) {
	global $unisphere_options, $post;

	// set the "wide" or "narrow" layout class
	if( isset($unisphere_options['skin_layout']) )
		$classes[] = $unisphere_options['skin_layout'];
	else
		$classes[] = 'wide';
	
	return $classes;
}
?>