<?php 

/**
 * Function responsible for displaying breadcrumbs
 */
function unisphere_breadcrumbs($delimiter, $prefix_message) {
	$delimiter = '<span class="delimiter">' . $delimiter . '</span>';
	$prefix_message = '<span class="prefix">' . $prefix_message . '</span>';
	$currentBefore = '<span class="current">';
	$currentAfter = '</span>';
	
	if ( is_front_page() ) {
		$front_page_id = get_option('page_on_front');
		if( $front_page_id == 0 ) return;
		echo $prefix_message . get_the_title(get_option('page_on_front'));

	} else {
		global $post;
		/*$home = home_url();*/
                $home = "";
		echo $prefix_message . '<a href="' . $home . '">' . get_the_title(get_option('page_on_front')) . '</a> ' . $delimiter . ' ';
		 
		if ( is_category() ) {
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
			echo $currentBefore;
			printf( '%s', single_cat_title( '', false ) );
			echo $currentAfter;
		
		} elseif ( is_day() ) {
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
			echo $currentBefore . get_the_time('d') . $currentAfter;
		
		} elseif ( is_month() ) {
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo $currentBefore . get_the_time('F') . $currentAfter;
		
		} elseif ( is_year() ) {
			echo $currentBefore . get_the_time('Y') . $currentAfter;
		
		} elseif ( is_single() && !is_attachment() ) {
			// Check current post's post type
			$post_type = get_post_type( $post->ID );

			switch( $post_type ) {
				case 'post':
					// Get the post category (if has more than one use the first)
					if ( have_posts() )	the_post();
					$cat = wp_get_object_terms(get_the_ID(), 'category');
					$cat = $cat[0];

					// Check in what page this category is being displayed
					$page_id = get_page_ID_by_custom_field_value('_page_blog_cat', $cat->term_id);
					
					rewind_posts();
					
					$blog_page = get_page( $page_id );

					$parent_id  = $blog_page;
					$breadcrumbs = array();
					while ($parent_id) {
						$page = get_page($parent_id);
						if( get_permalink($page->ID) == '#' )
							$breadcrumbs[] = get_the_title($page->ID);
						else
							$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
						$parent_id  = $page->post_parent;
					}
					$breadcrumbs = array_reverse($breadcrumbs);
					foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
					echo $currentBefore;
					the_title();
					echo $currentAfter;
					break;

				case 'portfolio_cpt':
					// Get the portfolio item category (if has more than one use the first)
					if ( have_posts() )	the_post();
					$cat = wp_get_object_terms(get_the_ID(), 'portfolio_category');
					$cat = $cat[0];

					// Check in what page this category is being displayed
					$page_id = get_page_ID_by_custom_field_value('_page_portfolio_cat', $cat->term_id);
					
					rewind_posts();
					
					$portfolio_page = get_page( $page_id );
					
					$parent_id  = $portfolio_page;
					$breadcrumbs = array();
					while ($parent_id) {
						$page = get_page($parent_id);
						if( get_permalink($page->ID) == '#' )
							$breadcrumbs[] = get_the_title($page->ID);
						else
							$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
						$parent_id  = $page->post_parent;
					}
					$breadcrumbs = array_reverse($breadcrumbs);
					foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
					echo $currentBefore;
					the_title();
					echo $currentAfter;
					break;

				default:
					echo $currentBefore;
					the_title();
					echo $currentAfter;
					break;
			}
		
		} elseif ( is_page() && !$post->post_parent ) {
			echo $currentBefore;
			the_title();
			echo $currentAfter;
		
		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			$page_on_front = get_option('page_on_front');
			while ($parent_id) {
				if($parent_id == $page_on_front) { break; }
				$page = get_page($parent_id);
				if( get_permalink($page->ID) == '#' )
					$breadcrumbs[] = get_the_title($page->ID);
				else
					$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
			echo $currentBefore;
			the_title();
			echo $currentAfter;
		
		} elseif ( is_search() ) {
			echo $currentBefore . sprintf( __( 'Search results for \'%s\'', 'unisphere' ), get_search_query() ) . $currentAfter;
		
		} elseif ( is_tag() ) {
			echo $currentBefore . sprintf( '%s', single_tag_title( '', false ) ) . $currentAfter;
		
		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			echo $currentBefore . sprintf( __( 'Articles by: %s', 'unisphere' ), $userdata->display_name ) . $currentAfter;
			
		} elseif ( is_attachment() ) {
			echo $currentBefore . sprintf( '%s', get_the_title() ) . $currentAfter;                
		
		} elseif ( is_404() ) {
			echo $currentBefore . '404' . $currentAfter;
		} 
	}
}