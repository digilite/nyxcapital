<?php
/**
 * Twitter Widget
 */


/**
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'unisphere_load_twitter_widget' );


/**
 * Register our widget.
 * 'UniSphere_Twitter_Widget' is the widget class used below.
 */
function unisphere_load_twitter_widget() {
	register_widget( 'UniSphere_Twitter_Widget' );
}


/**
 * UniSphere_Twitter_Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 */
class UniSphere_Twitter_Widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function UniSphere_Twitter_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget-twitter', 'description' => 'The most recent tweets from your Twitter account' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'unisphere-twitter-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'unisphere-twitter-widget', 'UniSphere Twitter', $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$username = $instance['username'];
		$numbertweets = $instance['numbertweets'];
		$excludereplies = $instance['excludereplies'];
		$includeretweets = $instance['includeretweets'];
		$interval = $instance['interval'];
		$usejavascript = (isset($instance['usejavascript']) ? $instance['usejavascript'] : '');

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by theme). */
		if ( $title )
			echo $before_title . $title . $after_title;

		/* Display tweets. */

		getTweets( $username, $numbertweets, $interval, $excludereplies, $includeretweets, $usejavascript );

		/* After widget (defined by theme). */
		echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['username'] = strip_tags( $new_instance['username'] );
		$instance['numbertweets'] = strip_tags( $new_instance['numbertweets'] );
		$instance['excludereplies'] = $new_instance['excludereplies'];
		$instance['includeretweets'] = $new_instance['includeretweets'];
		$instance['interval'] = strip_tags( $new_instance['interval'] );
		$instance['usejavascript'] = strip_tags( $new_instance['usejavascript'] );

		// Delete the transient data on widget settings update
		delete_transient( UNISPHERE_THEMEOPTIONS . '-twitter-' . $instance['username'] . '-' . $instance['numbertweets'] );

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 	'title' => '',
							'numbertweets' => 3,
							'username' => '',
							'excludereplies' => '',
							'includeretweets' => 'on',
							'interval' => 1800,
							'usejavascript' => '' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
        
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
        
        <!-- Widget Username: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'username' ); ?>">Username:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'username' ); ?>" name="<?php echo $this->get_field_name( 'username' ); ?>" value="<?php echo $instance['username']; ?>" class="widefat" />
		</p>
        
		<!-- Widget Number of Tweets: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'numbertweets' ); ?>">Number of tweets to show:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'numbertweets' ); ?>" name="<?php echo $this->get_field_name( 'numbertweets' ); ?>" value="<?php echo $instance['numbertweets']; ?>" size="3" />
		</p>

		<!-- Widget Exclude Replies: Checkbox -->
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id( 'excludereplies' ); ?>" name="<?php echo $this->get_field_name( 'excludereplies' ); ?>" <?php echo ( !empty($instance['excludereplies']) ? 'checked="checked"': '' ); ?> />&nbsp;Exclude replies?
		</p>

		<!-- Widget Include Native Retweets: Checkbox -->
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id( 'includeretweets' ); ?>" name="<?php echo $this->get_field_name( 'includeretweets' ); ?>" <?php echo ( !empty($instance['includeretweets']) ? 'checked="checked"': '' ); ?> />&nbsp;Include native retweets?
		</p>
        
        <!-- Widget Cache Interval: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'interval' ); ?>">Cache interval in seconds:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'interval' ); ?>" name="<?php echo $this->get_field_name( 'interval' ); ?>" value="<?php echo $instance['interval']; ?>" size="3" />
		</p>

		<!-- Widget use javascript: Checkbox -->
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id( 'usejavascript' ); ?>" name="<?php echo $this->get_field_name( 'usejavascript' ); ?>" <?php echo ( !empty($instance['usejavascript']) ? 'checked="checked"': '' ); ?> />&nbsp;Use a javascript solution?<br/><small>(useful when you are on a shared hosting environment and the Twitter API is limited resulting in no tweets)</small>
		</p>

	<?php
	}
}

function getTweets($username, $limit, $interval, $excludereplies, $includeretweets, $usejavascript) {
	if( empty( $usejavascript ) ) {
		if( false === ( $tweets = get_transient( UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $limit . '-' . $excludereplies . '-' . $includeretweets ) ) ) {
			$api_url = "https://api.twitter.com/1/statuses/user_timeline.json?screen_name=" . $username . "&count=" . $limit . "&trim_user=1";

			if( !empty( $excludereplies ) )
				$api_url .= '&exclude_replies=1';

			if( !empty( $includeretweets ) )
				$api_url .= '&include_rts=1';

	    	$response = wp_remote_get( $api_url, array( 'sslverify' => false) );
			if( is_wp_error( $response ) || $response['response']['code'] != 200 ) {
				if( get_option(UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $limit . '-' . $excludereplies . '-' . $includeretweets) != '' ) {
					$tweets = get_option(UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $limit . '-' . $excludereplies . '-' . $includeretweets);
				} else {
			   		echo '<ul><li><p class="tweet">' . _e( 'No tweets were found.', 'unisphere' ) . '</p></li></ul>';
			   		return;
			   	}
			} else {
				// Parse the JSON data into a PHP array
				$tweets = json_decode( $response['body'], true );
			}

			set_transient( UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $limit . '-' . $excludereplies . '-' . $includeretweets, $tweets, $interval );
			update_option( UNISPHERE_THEMEOPTIONS . '-twitter-' . $username . '-' . $limit . '-' . $excludereplies . '-' . $includeretweets, $tweets );
		}

		echo '<ul>';
		foreach( $tweets as $tweet ) { ?>
			<li>
	        	<p class="tweet"><?php echo twitterize( $tweet['text'] ); ?></p>
	            <small><a href="<?php echo 'http://twitter.com/' . $username . '/status/' . $tweet['id_str']; ?>"><?php echo relativeTime( strtotime( $tweet['created_at'] ) ); ?></a></small>
			</li>
		<?php }
		echo '</ul>';
		echo '<a href="https://twitter.com/' . $username . '" class="twitter-follow-button" data-show-count="false">Follow @' . $username . '</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
	} else {
		// Use the javascript solution
		$twitterid = rand();
		echo '<ul id="jtwt_' . $twitterid . '"></ul>';
		echo '<a href="https://twitter.com/' . $username . '" class="twitter-follow-button" data-show-count="false">Follow @' . $username . '</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';

		echo "<script>";
		echo "jQuery(window).load(function() {";
		echo 	"jQuery('#jtwt_" . $twitterid . "').jtwt({";
		echo 		"username: '" . $username . "',";
		echo 		"count: '" . $limit . "',";
		echo 		"excludereplies: '" . ( !empty( $excludereplies ) ? 'true' : 'false' ) . "',";
		echo 		"includeretweets: '" . ( !empty( $includeretweets ) ? 'true' : 'false' ) . "'";
		echo 	"});";
		echo "});";
		echo "</script>";
	}
}


/*
	Relative Time Function
	based on code from http://stackoverflow.com/questions/11/how-do-i-calculate-relative-time/501415#501415
	For use in the "Parse Twitter Feeds" code above
*/
define("SECOND", 1);
define("MINUTE", 60 * SECOND);
define("HOUR", 60 * MINUTE);
define("DAY", 24 * HOUR);
define("MONTH", 30 * DAY);
function relativeTime($time)
{   
    $delta = time() - $time;

    if ($delta < 1 * MINUTE)
    {
        return $delta == 1 ? __('one second ago', 'unisphere') : sprintf( __('%s seconds ago', 'unisphere'), $delta );
    }
    if ($delta < 2 * MINUTE)
    {
      return  __('a minute ago', 'unisphere');
    }
    if ($delta < 45 * MINUTE)
    {
        return sprintf( __('%s minutes ago', 'unisphere'), floor($delta / MINUTE) );
    }
    if ($delta < 90 * MINUTE)
    {
      return __('an hour ago', 'unisphere');
    }
    if ($delta < 24 * HOUR)
    {
      return sprintf( __('%s hours ago', 'unisphere'), floor($delta / HOUR) );
    }
    if ($delta < 48 * HOUR)
    {
      return __('yesterday', 'unisphere');
    }
    if ($delta < 30 * DAY)
    {
        return sprintf( __('%s days ago', 'unisphere'), floor($delta / DAY) );
    }
    if ($delta < 12 * MONTH)
    {
      $months = floor($delta / DAY / 30);
      return $months <= 1 ? __('one month ago', 'unisphere') : sprintf( __('%s months ago', 'unisphere'), $months );
    }
    else
    {
        $years = floor($delta / DAY / 365);
        return $years <= 1 ? __('one year ago', 'unisphere') : sprintf( __('%s years ago', 'unisphere'), $years );
    }
}


/**
 * Add links to Hashtags, Accounts and URLs in a string
 */
function twitterize($raw_text) {
	$output = $raw_text;
	
	// parse urls
	$pattern = '/([A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+)/i';
	$replacement = '<a href="$1">$1</a>';
	$output = preg_replace($pattern, $replacement, $output);
	
	// parse usernames
	$pattern = '/[@]+([A-Za-z0-9-_]+)/';
	$replacement = '<a href="http://twitter.com/$1">@$1</a>';
	$output = preg_replace($pattern, $replacement, $output);
	
	// parse hashtags
	$pattern = '/(?<!&)[#]+([A-Za-z0-9-_]+)/';
	$replacement = '<a href="http://search.twitter.com/search?q=%23$1">#$1</a>';
	$output = preg_replace($pattern, $replacement, $output);
	
	return $output;
}
?>
