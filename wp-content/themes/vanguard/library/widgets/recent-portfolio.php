<?php
/**
 * Recent Portfolio Items Widget
 */


/**
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'unisphere_load_recent_portfolio_widget' );


/**
 * Register our widget.
 * 'UniSphere_Recent_Portfolio_Widget' is the widget class used below.
 */
function unisphere_load_recent_portfolio_widget() {
	register_widget( 'UniSphere_Recent_Portfolio_Widget' );
}


/**
 * UniSphere_Recent_Portfolio_Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 */
class UniSphere_Recent_Portfolio_Widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function UniSphere_Recent_Portfolio_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget-posts widget-recent-portfolio-items', 'description' => 'The most recent portfolio items on your site' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'unisphere-recent-portfolio-items-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'unisphere-recent-portfolio-items-widget', 'UniSphere Recent Portfolio Items', $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$numberposts = $instance['numberposts'];
		$cats = ( isset( $instance['cats'] ) ? $instance['cats'] : null );
		$numberwords = $instance['numberwords'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by theme). */
		if ( $title )
			echo $before_title . $title . $after_title;

		/* Display recent portfolio items. */
		$portfolio_posts_to_query = get_objects_in_term( $cats, 'portfolio_category');
		$recent = new WP_Query( array( 'post_type' => 'portfolio_cpt', 'posts_per_page' => $numberposts, 'post__in' => $portfolio_posts_to_query ) );
		
		echo '<ul>';

		while ($recent->have_posts()) : $recent->the_post();

			$image = unisphere_get_post_image( 'sidebar-portfolio-item' );
			echo '<li class="clearfix' . ( !$image ? ' no-image' : '' ) .  '">';
			if( $image )
				echo '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-image">' . $image . '</a>';
			echo '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-title">' . get_the_title() . '</a>';
			if( intval( $numberwords) > 0 )
				echo '<p class="excerpt">' . unisphere_custom_excerpt( get_the_excerpt(), $numberwords ) . '</p>';
			echo '</li>';
    
		endwhile;
		
		echo '</ul>';

		/* After widget (defined by theme). */
		echo $after_widget;
		wp_reset_query();
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['numberposts'] = strip_tags( $new_instance['numberposts'] );
		$instance['cats'] = $new_instance['cats'];
		$instance['numberwords'] = strip_tags( $new_instance['numberwords'] );

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'numberposts' => 3, 'numberwords' => 29 );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
        
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if( isset( $instance['title'] ) ) echo $instance['title']; ?>" class="widefat" />
		</p>
        
		<!-- Widget Number of Portfolio Items: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'numberposts' ); ?>">Number of portfolio items to show:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'numberposts' ); ?>" name="<?php echo $this->get_field_name( 'numberposts' ); ?>" value="<?php echo $instance['numberposts']; ?>" size="3" />
		</p>

		<!-- Widget Number of Words in Excerpt: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'numberwords' ); ?>">Number of words to show in the excerpt:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'numberwords' ); ?>" name="<?php echo $this->get_field_name( 'numberwords' ); ?>" value="<?php echo $instance['numberwords']; ?>" size="3" />
		</p>

		<!-- Widget Categories: Multi-checkbox -->
		<p>
			<label>Categories to select from:</label>
			<ul>
				<?php // If building the portfolio categories list, bring the already selected and ordered cats to the top					
				if( isset( $instance['cats'] ) ) {
					$selected_cats = $instance['cats'];
					foreach ($selected_cats as $selected_cat) { 
						if ($selected_cat != ' ' && $selected_cat != '') {
							$tax_term = get_term( $selected_cat, 'portfolio_category' ); ?>
		               		<li style="margin-bottom: 0"><input type="checkbox" name="<?php echo $this->get_field_name( 'cats' ); ?>[]" value="<?php echo $selected_cat; ?>" checked="checked" />&nbsp;<?php echo $tax_term->name; ?></li>
		            <?php  	}
					}
				}

				if( !isset( $selected_cats ) ) $selected_cats = array();

				$unselected_args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'exclude' => $selected_cats );
				$unselected_cats = get_categories( $unselected_args );
	            foreach($unselected_cats as $unselected_cat) { ?>
	                <li style="margin-bottom: 0"><input type="checkbox" name="<?php echo $this->get_field_name( 'cats' ); ?>[]" value="<?php echo $unselected_cat->cat_ID; ?>" />&nbsp;<?php echo $unselected_cat->name; ?></li>
	            <?php } ?>
			</ul>
		</p>

	<?php
	}
}
?>