<?php
/**
 * Sub-pages Widget
 */


/**
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'unisphere_load_sub_pages_widget' );


/**
 * Register our widget.
 * 'UniSphere_Sub_Pages_Widget' is the widget class used below.
 */
function unisphere_load_sub_pages_widget() {
	register_widget( 'UniSphere_Sub_Pages_Widget' );
}


/**
 * UniSphere_Sub_Pages_Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 */
class UniSphere_Sub_Pages_Widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function UniSphere_Sub_Pages_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget-sub-pages', 'description' => 'A menu displaying sub-pages of the current top parent page' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'unisphere-sub-pages-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'unisphere-sub-pages-widget', 'UniSphere Sub-pages', $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		global $post;

		if( isset($post) && is_page($post->ID) ) {
			$children = wp_list_pages( 'title_li=&echo=0&child_of=' . get_root_page($post->ID) );
			if( $children ) {
				$postdata = get_post($post->ID);

				/* Our variables from the widget settings. */
				$title = apply_filters('widget_title', $instance['title'] );

				/* Before widget (defined by themes). */
				echo $before_widget;

				/* Display the widget title if one was input (before and after defined by theme). */
				echo $before_title . $title . $after_title;

				/* Display ad */
				echo '<div class="widget-sub-pages">';
				echo '<ul>';
		        echo $children;
		        echo '</ul>';
				echo '</div>';

				/* After widget (defined by theme). */
				echo $after_widget;
			}
		}
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array();
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>A menu displaying sub-pages of the current top parent page.</p>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if( isset( $instance['title'] ) ) echo $instance['title']; ?>" class="widefat" />
		</p>
        
	<?php
	}
}
?>