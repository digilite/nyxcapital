<?php
/**
 * Social Icons Widget
 */


/**
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'unisphere_load_social_widget' );


/**
 * Register our widget.
 * 'UniSphere_Social_Widget' is the widget class used below.
 */
function unisphere_load_social_widget() {
	register_widget( 'UniSphere_Social_Widget' );
}


/**
 * UniSphere_Social_Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 */
class UniSphere_Social_Widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function UniSphere_Social_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget-social', 'description' => 'Social Icons' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'unisphere-social-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'unisphere-social-widget', 'UniSphere Social', $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$newwindow = $instance['newwindow'];
		$rss = $instance['rss'];
		$twitter = $instance['twitter'];
		$facebook = $instance['facebook'];
		$googleplus = $instance['googleplus'];
		$flickr = $instance['flickr'];
		$dribbble = $instance['dribbble'];
		$forrst = $instance['forrst'];
		$behance = $instance['behance'];
		$youtube = $instance['youtube'];
		$vimeo = $instance['vimeo'];
		$linkedin = $instance['linkedin'];
		$skype = $instance['skype'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by theme). */
		if ( $title )
			echo $before_title . $title . $after_title;

		if( !empty( $newwindow ) )
			$newwindow = 'target="_blank"';

		/* Display ad */
		echo '<div class="widget-social-container">';
		echo '<ul>';
		if( $rss != '' ) { echo '<li><a ' . $newwindow . ' href="' . $rss . '" class="rss">&nbsp;</a></li>'; }
		if( $twitter != '' ) { echo '<li><a ' . $newwindow . ' href="' . $twitter . '" class="twitter">&nbsp;</a></li>'; }
		if( $facebook != '' ) { echo '<li><a ' . $newwindow . ' href="' . $facebook . '" class="facebook">&nbsp;</a></li>'; }
		if( $googleplus != '' ) { echo '<li><a ' . $newwindow . ' href="' . $googleplus . '" class="googleplus">&nbsp;</a></li>'; }
		if( $flickr != '' ) { echo '<li><a ' . $newwindow . ' href="' . $flickr . '" class="flickr">&nbsp;</a></li>'; }
		if( $dribbble != '' ) { echo '<li><a ' . $newwindow . ' href="' . $dribbble . '" class="dribbble">&nbsp;</a></li>'; }
		if( $forrst != '' ) { echo '<li><a ' . $newwindow . ' href="' . $forrst . '" class="forrst">&nbsp;</a></li>'; }
		if( $behance != '' ) { echo '<li><a ' . $newwindow . ' href="' . $behance . '" class="behance">&nbsp;</a></li>'; }
		if( $youtube != '' ) { echo '<li><a ' . $newwindow . ' href="' . $youtube . '" class="youtube">&nbsp;</a></li>'; }
		if( $vimeo != '' ) { echo '<li><a ' . $newwindow . ' href="' . $vimeo . '" class="vimeo">&nbsp;</a></li>'; }
		if( $linkedin != '' ) { echo '<li><a ' . $newwindow . ' href="' . $linkedin . '" class="linkedin">&nbsp;</a></li>'; }
		if( $skype != '' ) { echo '<li><a ' . $newwindow . ' href="' . $skype . '" class="skype">&nbsp;</a></li>'; }
		echo '</ul>';
		echo '</div>';

		/* After widget (defined by theme). */
		echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['newwindow'] = $new_instance['newwindow'];
		$instance['rss'] = strip_tags( $new_instance['rss'] );
		$instance['twitter'] = strip_tags( $new_instance['twitter'] );
		$instance['facebook'] = strip_tags( $new_instance['facebook'] );
		$instance['googleplus'] = strip_tags( $new_instance['googleplus'] );
		$instance['flickr'] = strip_tags( $new_instance['flickr'] );
		$instance['dribbble'] = strip_tags( $new_instance['dribbble'] );
		$instance['forrst'] = strip_tags( $new_instance['forrst'] );
		$instance['behance'] = strip_tags( $new_instance['behance'] );
		$instance['youtube'] = strip_tags( $new_instance['youtube'] );
		$instance['vimeo'] = strip_tags( $new_instance['vimeo'] );
		$instance['linkedin'] = strip_tags( $new_instance['linkedin'] );
		$instance['skype'] = strip_tags( $new_instance['skype'] );

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'rss' => get_bloginfo( 'rss2_url' ) );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
        
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if( isset( $instance['title'] ) ) echo $instance['title']; ?>" class="widefat" />
		</p>

		<!-- Widget Open in new window: Checkbox -->
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id( 'newwindow' ); ?>" name="<?php echo $this->get_field_name( 'newwindow' ); ?>" <?php echo ( !empty($instance['newwindow']) ? 'checked="checked"': '' ); ?> />&nbsp;Open in new window/tab?
		</p>
        
		<!-- Widget rss URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'rss' ); ?>">RSS feed:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'rss' ); ?>" name="<?php echo $this->get_field_name( 'rss' ); ?>" value="<?php if( isset( $instance['rss'] ) ) echo $instance['rss']; ?>" class="widefat" />
		</p>

		<!-- Widget twitter URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>">Twitter:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php if( isset( $instance['twitter'] ) ) echo $instance['twitter']; ?>" class="widefat" />
		</p>

		<!-- Widget facebook URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook' ); ?>">Facebook:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="<?php if( isset( $instance['facebook'] ) ) echo $instance['facebook']; ?>" class="widefat" />
		</p>

		<!-- Widget googleplus URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'googleplus' ); ?>">Google+:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'googleplus' ); ?>" name="<?php echo $this->get_field_name( 'googleplus' ); ?>" value="<?php if( isset( $instance['googleplus'] ) ) echo $instance['googleplus']; ?>" class="widefat" />
		</p>

		<!-- Widget flickr URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'flickr' ); ?>">Flickr:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'flickr' ); ?>" name="<?php echo $this->get_field_name( 'flickr' ); ?>" value="<?php if( isset( $instance['flickr'] ) ) echo $instance['flickr']; ?>" class="widefat" />
		</p>

		<!-- Widget dribbble URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'dribbble' ); ?>">Dribbble:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'dribbble' ); ?>" name="<?php echo $this->get_field_name( 'dribbble' ); ?>" value="<?php if( isset( $instance['dribbble'] ) ) echo $instance['dribbble']; ?>" class="widefat" />
		</p>

		<!-- Widget forrst URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'forrst' ); ?>">Forrst:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'forrst' ); ?>" name="<?php echo $this->get_field_name( 'forrst' ); ?>" value="<?php if( isset( $instance['forrst'] ) ) echo $instance['forrst']; ?>" class="widefat" />
		</p>

		<!-- Widget behance URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'behance' ); ?>">Behance:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'behance' ); ?>" name="<?php echo $this->get_field_name( 'behance' ); ?>" value="<?php if( isset( $instance['behance'] ) ) echo $instance['behance']; ?>" class="widefat" />
		</p>

		<!-- Widget youtube URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'youtube' ); ?>">YouTube:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" value="<?php if( isset( $instance['youtube'] ) ) echo $instance['youtube']; ?>" class="widefat" />
		</p>

		<!-- Widget vimeo URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'vimeo' ); ?>">Vimeo:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'vimeo' ); ?>" name="<?php echo $this->get_field_name( 'vimeo' ); ?>" value="<?php if( isset( $instance['vimeo'] ) ) echo $instance['vimeo']; ?>" class="widefat" />
		</p>

		<!-- Widget linkedin URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'linkedin' ); ?>">LinkedIn:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" value="<?php if( isset( $instance['linkedin'] ) ) echo $instance['linkedin']; ?>" class="widefat" />
		</p>

		<!-- Widget skype URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'skype' ); ?>">Skype:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'skype' ); ?>" name="<?php echo $this->get_field_name( 'skype' ); ?>" value="<?php if( isset( $instance['skype'] ) ) echo $instance['skype']; ?>" class="widefat" />
		</p>

	<?php
	}
}
?>