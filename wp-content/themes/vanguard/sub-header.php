<?php
/**
 * The sub-header
 */ 
global $unisphere_options;

wp_reset_query();

if( is_tag() || is_category() || is_author() || is_day() || is_month() || is_year() || is_404() || is_attachment() || is_search() ) {
	if( isset($unisphere_options['skin_sub_header_archives_style']) && $unisphere_options['skin_sub_header_archives_style'] != 'hide' )
		get_template_part( 'sub-header-bar' );
} else {
	$custom = get_post_custom($post->ID);
	if( isset( $custom['_page_sub_header_style'][0] ) && $custom['_page_sub_header_style'][0] != '' && !( isset( $custom['_page_show_slider'][0] ) && $custom['_page_show_slider'][0] == '1' ) )
		get_template_part( 'sub-header-bar' );

	if( isset( $custom['_page_show_slider'][0] ) && $custom['_page_show_slider'][0] == '1' ) {
		if( empty($custom['_page_slider_type'][0]) || ( isset( $custom['_page_slider_type'][0] ) && $custom['_page_slider_type'][0] == 'fullwidth' ) )
			get_template_part( 'sub-header-slider' );
		elseif( isset( $custom['_page_slider_type'][0] ) && $custom['_page_slider_type'][0] == 'layer_slider' )
			get_template_part( 'sub-header-layerslider' );
	}

	$post_type = get_post_type($post->ID);
	if( $post_type != 'page' && $post_type != 'portfolio_cpt' && $post_type != 'post' )
		if( isset($unisphere_options['skin_sub_header_archives_style']) && $unisphere_options['skin_sub_header_archives_style'] != 'hide' )
			get_template_part( 'sub-header-bar' );
}

?>