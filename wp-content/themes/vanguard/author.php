<?php
/**
 * The template for displaying Author Archive pages.
 */

get_header(); 

// The blog page template selected in the admin panel for the archive pages
global $unisphere_options, $current_page_template;
$current_page_template = $unisphere_options['skin_blog_archives_template']; 
$class = str_replace( 'template_', '', $current_page_template );
$class = str_replace( '.php', '', $class );
$class = str_replace( '_', '-', $class ); ?>

	<div class="clearfix">

		<!--BEGIN #primary-->
		<div id="primary" class="<?php echo $class; ?>">

			<?php if ( have_posts() ) : ?>
				
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content' ); ?>

				<?php endwhile; ?>

			<?php else : ?>

				<div id="post-0" class="post no-results not-found">
					<h2 class="post-title"><?php _e( 'Not Found', 'unisphere' ); ?></h2>
					<div class="post-text">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'unisphere' ); ?></p>
						<?php get_search_form(); ?>
					</div>
				</div>

			<?php endif; ?>
	
            <?php get_template_part( 'navigation' ); ?>
        <!--END #primary-->
		</div>
        
        <?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>