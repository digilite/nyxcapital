<?php
/**
 * Search form shown when the function get_search_form() is called
 */

global $unisphere_options;
?>

<form method="get" class="searchform" action="<?php echo home_url(); ?>">
	<input class="search" name="s" type="text" value="<?php if( isset( $_GET['s'] ) ) esc_attr($_GET['s']); ?>" />
    <button class="search-btn" type="submit"><?php _e('Search', 'unisphere'); ?></button>
</form>
