<?php
/*
Template Name: Left Sidebar Page
*/

get_header(); ?>

	<div class="clearfix left-sidebar">

		<!--BEGIN #primary-->
		<div id="primary">
            
			<?php the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

			<?php comments_template( '', true ); ?>

		<!--END #primary-->
		</div>

		<?php get_sidebar(); ?>
            
	</div>

<?php get_footer(); ?>