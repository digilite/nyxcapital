<?php 
/*
Template Name: Portfolio 2 Columns
*/

get_header();

// If the user as set a number of items per page...
if( get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = true) != '' ) { 
	$items_per_page = get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = true);
} else { // else don't paginate
	$items_per_page = 9999;
}
	
// The selected categories to show
$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);

if( get_the_content() != '' ) { echo '<div class="portfolio-content">'; the_content(); echo '</div>'; }

if( $cats == '' ) {
	echo '<div class="portfolio-content"><p>No categories have been selected for this portfolio page. Please login to your Wordpress Admin area and set the categories you want to show by editing this page and setting one or more categories in the multi checkbox field "Portfolio Categories".</p></div>';
} else { ?>
    <div id="portfolio-2-columns" class="portfolio-page clearfix">
<?php
	// If the user hasn't set a number of items per page, then use JavaScript filtering
	if( $items_per_page == 9999 ) : ?>
    
			<div id="portfolio-filter-container" class="no-sidebar clearfix">
                <span class="portfolio-browse"><?php _e( 'Browse:', 'unisphere' ); ?></span>
                <ul class="portfolio-filters">
                    <li class="all"><a href="#" class="active"><?php _e( 'All', 'unisphere' ); ?></a></li>
                    <?php
                    // display only the selected portfolio categories
                    $cats_array = explode( ",", $cats );
                        
                    // list selected terms in the portfolio_category taxonomy
                    foreach ($cats_array as $cat) {
                        $tax_term = get_term( $cat, 'portfolio_category' );
                        echo '<li class="' . $tax_term->slug . '">' . '<a href="#">' . $tax_term->name . '</a></li>';
                    } ?>
                </ul>
            </div>
            
	<?php endif; ?>

            <ul class="portfolio-2-columns-list portfolio-sortable portfolio-scale clearfix">
                <?php
                    $portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
                    
                    $wp_query = new WP_Query( array( 'post_type' => 'portfolio_cpt', 'post__in' => $portfolio_posts_to_query, 'paged' => $paged, 'showposts' => $items_per_page ) ); 
                    while ($wp_query->have_posts()) : $wp_query->the_post();
                        
                        $custom = get_post_custom($post->ID);
                        
                        // Get the portfolio item categories
                        $cats = wp_get_object_terms($post->ID, 'portfolio_category');
                        
                        // If no category was assigned, don't show the item
                        if ( $cats ) :
							$cat_slugs = '';
							foreach( $cats as $cat ) {
								$cat_slugs .= $cat->slug . ",";
							}
							$cat_slugs = substr($cat_slugs, 0, -1);
                ?>
                        <li class="portfolio-item" data-type="<?php echo $cat_slugs; ?>" data-id="<?php echo $post->ID; ?>">
                            <?php if( !empty( $custom['_portfolio_link'][0]) ) : // User has set a custom destination link for this portfolio item ?>
                                <a class="portfolio-image" href="<?php echo $custom['_portfolio_link'][0]; ?>">
                            <?php elseif( isset($custom['_portfolio_no_lightbox'][0]) && $custom['_portfolio_no_lightbox'][0] == '1' ) : // User has selected to link the thumb directly to the portfolio item detail page or the custom url ?>
                                <a class="portfolio-image" href="<?php echo the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php elseif( !empty( $custom['_portfolio_video'][0] ) && $custom['_portfolio_video'][0] != 'disabled' && $custom['_portfolio_video'][0] != 'html5_flv_disabled' ) : // Check if there's a video to be displayed in the lightbox when clicking the thumb ?>
                                <?php switch( $custom['_portfolio_video'][0] ) {
                                    case 'youtube': ?>
                                        <a class="portfolio-image" href="<?php echo $custom['_portfolio_video_youtube'][0]; ?>" title="<?php the_title(); ?>" data-rel="lightbox[portfolio]">
                                        <?php break;
                                    case 'vimeo': ?>
                                        <a class="portfolio-image" href="<?php echo $custom['_portfolio_video_vimeo'][0]; ?>" title="<?php the_title(); ?>" data-rel="lightbox[portfolio]">
                                        <?php break;
                                    case 'html5_flv': ?>
                                        <a class="portfolio-image sublime" href="<?php echo $custom['_portfolio_video_html5_mp4'][0]; ?>" title="<?php the_title(); ?>">
                                        <?php break;
                                } ?>
                            <?php else : // just open the full image in the lightbox ?>
                                <a class="portfolio-image" href="<?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); echo $full_image[0]; ?>" title="<?php the_title(); ?>" data-rel="lightbox[portfolio]">
                            <?php endif; ?>
                                <?php echo unisphere_get_post_image("portfolio2"); ?>
                            </a>
                            <?php // If there's an HTML5 or FLV video to display in the lightbox add it here
                            if( !empty( $custom['_portfolio_video'][0] ) && $custom['_portfolio_video'][0] == 'html5_flv' ) {
                                if( isset( $custom['_portfolio_video_flv'] ) ) { // FLV
                                    echo unisphere_run_shortcode('[video_lightbox ' . ( isset( $custom['_portfolio_video_lightbox_width'] ) ? ' width="' . $custom['_portfolio_video_lightbox_width'][0] . '"' : '' ) . ( isset( $custom['_portfolio_video_lightbox_height'] ) ? ' height="' . $custom['_portfolio_video_lightbox_height'][0] . '"' : '' ) . ' type="flv" flv="' . $custom['_portfolio_video_flv'][0] . '" ' . ( isset( $custom['_portfolio_video_flv_hd'] ) ? ' flvhd="' . $custom['_portfolio_video_flv_hd'][0] . '"' : '' ) . ' /]');
                                } else { // HTML5
                                    echo unisphere_run_shortcode('[video_lightbox ' . ( isset( $custom['_portfolio_video_lightbox_width'] ) ? ' width="' . $custom['_portfolio_video_lightbox_width'][0] . '"' : '' ) . ( isset( $custom['_portfolio_video_lightbox_height'] ) ? ' height="' . $custom['_portfolio_video_lightbox_height'][0] . '"' : '' ) . ' type="html5" mp4="' . $custom['_portfolio_video_html5_mp4'][0] . '" ' . ( isset( $custom['_portfolio_video_html5_mp4_hd'] ) ? ' mp4hd="' . $custom['_portfolio_video_html5_mp4_hd'][0] . '"' : '' ) . ' ' . ( isset( $custom['_portfolio_video_html5_webm'] ) ? ' webmogg="' . $custom['_portfolio_video_html5_webm'][0] . '"' : '' ) . ' ' . ( isset( $custom['_portfolio_video_html5_webm_hd'] ) ? ' webmogghd="' . $custom['_portfolio_video_html5_webm_hd'][0] . '"' : '' ) . ' /]');
                                }
                            } ?>
                            <div class="long-bar"></div>
                            <div class="portfolio-info">
                                <a class="portfolio-title" href="<?php echo !empty( $custom['_portfolio_link'][0] ) ? $custom['_portfolio_link'][0] : the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>
                        </li>
                    <?php endif; ?> 
                <?php endwhile; ?>                
            </ul>
        
            <?php
            // If the user has set a number of items per page, then display pagination
            if( $items_per_page != 9999 ) {
                // Show the pagination from navigation.php
                get_template_part( 'navigation' );
            } ?>
            <?php wp_reset_query(); ?>
    </div>
<?php } ?>

<?php get_footer(); ?>