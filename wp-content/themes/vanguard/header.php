<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<?php // Add the iframe YouTube API if not using IE
	$browser = $_SERVER[ 'HTTP_USER_AGENT' ];
	if ( !preg_match( "/MSIE/", $browser ) ) : ?>
		<script src='http://www.youtube.com/player_api'></script>
	<?php endif; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />

	<?php if( !empty($unisphere_options['favicon']) ) : ?>
		<link rel="icon" href="<?php echo $unisphere_options['favicon']; ?>">
	<?php endif; ?>

	<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'unisphere' ), max( $paged, $page ) );

	?></title>

	<meta name="viewport" content="width=device-width,initial-scale=1">

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    
    <style>
    	<?php // Display the custom CSS defined in the admin panel
		if( !empty( $unisphere_options['custom_css'] ) )
			echo $unisphere_options['custom_css']; ?>

		<?php // Show/Hide blog posts meta information	
		if( empty( $unisphere_options['show_meta_author'] ) ) // Author
			echo '.post-meta .author { display: none!important; }';
		
		if( empty( $unisphere_options['show_meta_date'] ) ) // Date
			echo '.post-meta .published { display: none!important; }';
			
		if( empty( $unisphere_options['show_meta_categories'] ) ) // Categories
			echo '.post-meta .post-categories { display: none!important; }';
			
		if( empty( $unisphere_options['show_meta_tags'] ) ) // Tags
			echo '.post-meta .post-tags { display: none!important; }';
			
		if( empty( $unisphere_options['show_meta_comment_count'] ) ) // Comment Count
			echo '.post-meta .post-comment-link { display: none!important; }';
		?>

		<?php // Show/Hide portfolio item meta information
		if( empty( $unisphere_options['show_portfolio_meta_date'] ) ) // Date
			echo '.portfolio-detail .post-meta .published { display: none!important; }';
			
		if( empty( $unisphere_options['show_portfolio_meta_comment_count'] ) ) // Comment Count
			echo '.portfolio-detail .post-meta .post-comment-link { display: none!important; }';
		?>
	</style>
	
	<?php // PHP values needed for JavaScript ?>
	<script type='text/javascript'>
		/* <![CDATA[ */  
		var unisphere_globals = {
	 		searchLocalized: '<?php _e( 'To search type and hit enter', 'unisphere' ); ?>', 
		 	jsFolderUrl: '<?php echo UNISPHERE_JS; ?>',
		 	themeVersion: '<?php $theme_data = unisphere_wp_get_theme(); echo $theme_data['Version']; ?>',
		 	cufonFonts: '<?php echo unisphere_get_cufon_selectors(); ?>'
	 	}; 
		/* ]]> */ 
	</script>
	
	<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
	?>
<!--END head-->
</head>

<!--BEGIN body-->
<body <?php body_class(); ?>>

	<?php // Get the full page background image from full-page-bg.php
	get_template_part( 'full-page-bg' ); ?>

	<!--BEGIN #container-->
	<div id="container">

		<!--BEGIN #superior-header-container-->
		<div id="superior-header-container">

			<!--BEGIN #superior-header-->
			<div id="superior-header">
				<div id="superior-header-left">
					<?php	// Superior-header bar Widgetized area
	             		if ( !function_exists( 'unisphere_dynamic_sidebar' ) || !unisphere_dynamic_sidebar('superior-header-bar-left') ) : ?>
	            	<?php endif; ?>
	            </div>
	            <div id="superior-header-right">
					<?php	// Superior-header bar Widgetized area
		             	if ( !function_exists( 'unisphere_dynamic_sidebar' ) || !unisphere_dynamic_sidebar('superior-header-bar-right') ) : ?>
		            <?php endif; ?>
	            </div>
    		<!--END #superior-header-->
    		</div>
		
		<!--END #superior-header-container-->
		</div>		

		<!--BEGIN #header-container-->
		<div id="header-container">

			<!--BEGIN #header-->
			<div id="header">
			
				<!--BEGIN #logo-->
				<div id="logo">
					<a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
						<?php if( isset( $unisphere_options['logo'] ) ) : ?>
							<img src="<?php echo $unisphere_options['logo'] ?>" alt="<?php bloginfo( 'name' ) ?>" />
						<?php else : ?><a href="http://nyxcapital.com/wp-content/uploads/2012/04/NYX_portfolio1.jpg"><img src="http://nyxcapital.com/wp-content/uploads/2012/04/NYX_portfolio1-300x109.jpg" alt="NYX_portfolio" width="150" height="69" class="alignleft size-medium wp-image-2059" /></a>
							<?php bloginfo( 'name' ) ?>
						<?php endif; ?>
					</a>
				<!--END #logo-->
				</div>
				
				<!--BEGIN #menu-bg-->
				<div id="menu-bg">
					<?php wp_nav_menu( array(
						'show_home' => '0',
						'sort_column' => 'menu_order',
						'container_class' => 'menu',
						'menu_class' => 'nav',
						'theme_location' => 'header_menu',
						'fallback_cb' => 'unisphere_header_navigation',
						'walker' => new unisphere_menu_walker() ) ); ?>
				<!--END #menu-bg-->
				</div>
		
			<!--END #header-->
			</div>
			
		<!--END #header-container-->
		</div>

		<?php // Get the sub-header from sub-header.php
			get_template_part( 'sub-header' ); ?>
			
		<!--BEGIN #content-container -->
		<div id="content-container">
		
			<!--BEGIN #content-->
			<div id="content" class="clearfix">
