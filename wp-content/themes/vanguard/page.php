<?php
/**
 * The default template for displaying all pages with a right sidebar.
 */

get_header(); ?>

	<div class="clearfix right-sidebar">

		<!--BEGIN #primary-->
		<div id="primary">
            
			<?php the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

			<?php //comments_template( '', true); ?>

		<!--END #primary-->
		</div>

		<?php get_sidebar(); ?>
            
	</div>

<?php get_footer(); ?>