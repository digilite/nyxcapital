<?php
/**
 * The Sidebar containing the widget areas.
 */
?>

		<!--BEGIN #secondary-->
        <div id="secondary">

            <?php	/* Main Widgetized Area */
                if ( !function_exists( 'unisphere_dynamic_sidebar' ) || !unisphere_dynamic_sidebar('main-sidebar') ) : ?>
            
            <?php endif; ?>
            
        <!--END #secondary-->
        </div>