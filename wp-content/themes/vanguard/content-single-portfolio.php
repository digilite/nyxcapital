<?php
/**
 * The template for displaying content in the single-portfolio_cpt.php template
 */ 

global $unisphere_options; ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="portfolio-detail-media">
    <?php 
    $custom = get_post_custom($post->ID);

    // Display Video
    if( isset( $custom['_portfolio_video'][0] ) && $custom['_portfolio_video'][0] != 'disabled' ) {
        $rand = Rand();
        $autoplay = ( isset($custom['_portfolio_autoplay']) && $custom['_portfolio_autoplay'][0] == '1' ? 'false' : 'true' );
        $thumbnail = ( isset($custom['_portfolio_no_autoplay_thumbnail']) ? $custom['_portfolio_no_autoplay_thumbnail'][0] : '' );
        $data_attribute = '';
        switch ($custom['_portfolio_video'][0]) {
            case 'youtube':
                echo unisphere_run_shortcode('[video type="youtube" url="' . $custom['_portfolio_video_youtube'][0] . '" autoplay="' . $autoplay . '" /]');
                break;
            case 'vimeo':
                echo unisphere_run_shortcode('[video type="vimeo" url="' . $custom['_portfolio_video_vimeo'][0] . '" autoplay="' . $autoplay . '" /]');
                break;
            case 'html5_flv':
                if( isset( $custom['_portfolio_video_flv'] ) ) { // FLV
                    echo unisphere_run_shortcode('[video type="flv" flv="' . $custom['_portfolio_video_flv'][0] . '" ' . ( isset( $custom['_portfolio_video_flv_hd'] ) ? ' flvhd="' . $custom['_portfolio_video_flv_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                } else { // HTML5
                    echo unisphere_run_shortcode('[video type="html5" mp4="' . $custom['_portfolio_video_html5_mp4'][0] . '" ' . ( isset( $custom['_portfolio_video_html5_mp4_hd'] ) ? ' mp4hd="' . $custom['_portfolio_video_html5_mp4_hd'][0] . '"' : '' ) . ' ' . ( isset( $custom['_portfolio_video_html5_webm'] ) ? ' webmogg="' . $custom['_portfolio_video_html5_webm'][0] . '"' : '' ) . ' ' . ( isset( $custom['_portfolio_video_html5_webm_hd'] ) ? ' webmogghd="' . $custom['_portfolio_video_html5_webm_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                }
                break;
        }
    }
    
    // Images
    $photos = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID') );

    if ($photos) : 
        
        $portfolio_exclude_featured_image = ( !empty($custom['_portfolio_exclude_featured_image']) && $custom['_portfolio_exclude_featured_image'][0] == '1' ? '1' : '' );

        // Check how the user as selected to display images
        switch ( $custom['_portfolio_detail_big_images'][0] ) {
            
            case 'slider': // display images in a Slider 
                
                $slider_height = intval( isset( $custom['_portfolio_slider_height'][0] ) && $custom['_portfolio_slider_height'][0] != '' ? $custom['_portfolio_slider_height'][0] : ( !empty( $unisphere_options['advanced_image_size_slider_shortcode_height'] ) ? $unisphere_options['advanced_image_size_slider_shortcode_height'] : 540 ) );
                $aspect_ratio = 960 / $slider_height;
                ?>

                <div class="slider-container">
                    <ul class="slider" id="slider-portfolio-detail" data-aspect-ratio="<?php echo $aspect_ratio; ?>">
                    
                    <?php                                
                        foreach ($photos as $photo) : 
                            if( ( $photo->ID != get_post_thumbnail_id($post->ID) && $portfolio_exclude_featured_image == '1' ) || $portfolio_exclude_featured_image != '1' ) : 
                            $image = unisphere_resize( $photo->ID, null, UNISPHERE_SLIDER_SHORTCODE_W, $slider_height, UNISPHERE_SLIDER_SHORTCODE_CROP ); ?>
                            <li class="no-description">
                                <div class="slider-image">
                                    <img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" alt="<?php echo get_post_meta($photo->ID, '_wp_attachment_image_alt', true); ?>" title="<?php echo $photo->post_title; ?>" />
                                </div>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    </ul>
                </div>
        <?php   
            break; // end slider images
                
            case 'big': // display big images
            
                foreach ($photos as $photo) : 
                    if( ( $photo->ID != get_post_thumbnail_id($post->ID) && $portfolio_exclude_featured_image == '1' ) || $portfolio_exclude_featured_image != '1' ) :
                        $image = unisphere_resize( $photo->ID, null, UNISPHERE_PORTFOLIO_DETAIL_W, UNISPHERE_PORTFOLIO_DETAIL_H, UNISPHERE_PORTFOLIO_DETAIL_CROP ); ?>
                        <p class="portfolio-detail-big-image">
                        <a href="<?php $portfolio_full = wp_get_attachment_image_src($photo->ID, 'full'); echo $portfolio_full[0]; ?>" title="<?php echo $photo->post_title; ?>" data-rel="lightbox[<?php echo $post->ID; ?>]">
                        <img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" alt="<?php echo get_post_meta($photo->ID, '_wp_attachment_image_alt', true); ?>" />
                        </a>                                
                        <?php echo '<span class="caption">' . $photo->post_excerpt . '</span>'; ?>
                        </p>
                <?php endif; 
                endforeach; 
                
                break; // end big images

            case 'featured': // display featured image only
            
                foreach ($photos as $photo) : 
                    if( $photo->ID == get_post_thumbnail_id($post->ID) ) :
                        $image = unisphere_resize( $photo->ID, null, UNISPHERE_PORTFOLIO_DETAIL_W, UNISPHERE_PORTFOLIO_DETAIL_H, UNISPHERE_PORTFOLIO_DETAIL_CROP ); ?>
                        <p class="portfolio-detail-big-image">
                        <a href="<?php $portfolio_full = wp_get_attachment_image_src($photo->ID, 'full'); echo $portfolio_full[0]; ?>" title="<?php echo $photo->post_title; ?>" data-rel="lightbox[<?php echo $post->ID; ?>]">
                        <img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" alt="<?php echo get_post_meta($photo->ID, '_wp_attachment_image_alt', true); ?>" />
                        </a>                                
                        <?php echo '<span class="caption">' . $photo->post_excerpt . '</span>'; ?>
                        </p>
                <?php endif; 
                endforeach; 
                
                break; // featured image only
            
            case 'gallery': // display images in a gallery
            ?>
            
                <div class="gallery-portfolio-detail"><?php echo do_shortcode('[gallery ' . ( $portfolio_exclude_featured_image == '1' ? 'exclude="' . get_post_thumbnail_id($post->ID) . '" ' : '' ) . '/]'); ?></div>
            
        <?php 
            break; // end gallery
        } // end switch ?>

    <?php endif; ?>
    </div>

    <?php if( $unisphere_options['show_portfolio_share_buttons'] == '1' ) : ?>
        <?php get_template_part( 'social-buttons' ); ?>
    <?php endif; ?>

    <div class="post-text">
        <?php the_content(); ?>
        <?php wp_link_pages('before=<div class="wp-pagenavi post_linkpages">&after=</div><br />&link_before=<span>&link_after=</span>'); ?>            
    </div>

    <?php if( isset($unisphere_options['show_portfolio_navigation_links_below']) && $unisphere_options['show_portfolio_navigation_links_below'] == '1' ) : ?>
    <h3 id="single-nav-below-title"><?php _e( 'Other Work', 'unisphere' ); ?></h3>
    <div id="single-nav-below">
        <?php if( function_exists('next_post_link_plus') && function_exists('previous_post_link_plus') ) : ?>
            <div id="single-nav-below-prev"><?php previous_post_link_plus( array( 'format' => '<span>&#171; ' . __('Previous', 'unisphere') . '</span>' . '%link', 'in_cats' => get_post_meta($parent_page_id, "_page_portfolio_cat", $single = true) ) ); ?></div>
            <div id="single-nav-below-next"><?php next_post_link_plus( array( 'format' => '<span>' . __('Next', 'unisphere') . ' &#187;</span>' . '%link', 'in_cats' => get_post_meta($parent_page_id, "_page_portfolio_cat", $single = true) ) ); ?></div>
        <?php else : ?>
            <div id="single-nav-below-prev"><?php previous_post_link('<span>&#171; ' . __('Previous', 'unisphere') . '</span>' . '%link'); ?></div>
            <div id="single-nav-below-next"><?php next_post_link('<span>' . __('Next', 'unisphere') . ' &#187;</span>' . '%link'); ?></div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    
    <?php // Related Portfolio Items 
    if( !empty( $unisphere_options['show_portfolio_related_work'] ) ) :
        foreach (get_the_terms($post->ID, 'portfolio_category') as $term)
            $terms[] = $term->term_id;

        $related_posts_to_query = get_objects_in_term( $terms, 'portfolio_category' );
        $related_posts_to_query = remove_item_by_value( $related_posts_to_query, $post->ID, true );
        remove_all_filters('posts_orderby');
        $wp_query = new WP_Query( array( 'post_type' => 'portfolio_cpt', 'post__in' => $related_posts_to_query, 'showposts' => 4, 'orderby' => 'rand' ) ); 

        if ( $wp_query->have_posts() ) : ?>
            <div id="portfolio-4-columns" class="related-work clearfix">
                <h3 id="related-work-title"><?php _e('Related Work', 'unisphere'); ?></h3>
                <ul class="portfolio-4-columns-list portfolio-scale clearfix">
                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); 
                    $custom = get_post_custom($post->ID); ?>
                    <li class="portfolio-item">
                        <a class="portfolio-image" href="<?php echo !empty( $custom['_portfolio_link'][0] ) ? $custom['_portfolio_link'][0] : the_permalink(); ?>">
                            <?php echo unisphere_get_post_image("portfolio4"); ?>
                        </a>
                        <div class="long-bar"></div>
                        <div class="portfolio-info">
                            <a class="portfolio-title" href="<?php echo !empty( $custom['_portfolio_link'][0] ) ? $custom['_portfolio_link'][0] : the_permalink(); ?>"><?php the_title(); ?></a>                            
                        </div>
                    </li>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
                </ul>            
            </div>
            <?php if ( have_comments() || comments_open() ) : ?>            
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>

</div>