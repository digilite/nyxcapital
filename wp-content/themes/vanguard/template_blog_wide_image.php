<?php 
/*
Template Name: Blog (wide image)
*/

get_header(); 

// The current page template being used
global $current_page_template;
$current_page_template = explode( '/', get_page_template() );
$current_page_template = $current_page_template[count($current_page_template)-1]; ?>

	<div class="clearfix">

		<?php // If the user as set a number of items per page...
		if( get_post_meta($post->ID, "_page_blog_num_items_page", $single = true) != '' ) { 
			$items_per_page = get_post_meta($post->ID, "_page_blog_num_items_page", $single = true);
		} else { // else don't paginate
			$items_per_page = 9999;
		}

		// The selected categories to show
		$cats = get_post_meta($post->ID, "_page_blog_cat", $single = true);

		if( get_the_content() != '' ) echo do_shortcode( get_the_content() ); 

		if( $cats == '' ) {
			echo '<p>No categories have been selected for this blog page. Please login to your Wordpress Admin area and set the categories you want to show by editing this page and setting one or more categories in the multi checkbox field "Blog Categories".</p>';
		} else { ?>

			<!--BEGIN #primary-->
			<div id="primary" class="blog-wide-image">
	        
				<?php if ( get_option( 'permalink_structure' ) == '' ) { // Default permalink structure
					$unisphere_page = $_GET['paged'] != '' ? $_GET['paged'] : '1';
				} else { // Custom permalink structure which ends with /page/xx/
					$pageURL = 'http';
					//check what if its secure or not
					if ( isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
						$pageURL .= "s";
					}
					//add the protocol
					$pageURL .= "://";
					//check what port we are on
					if ($_SERVER["SERVER_PORT"] != "80") {
						$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
					} else {
						$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					}
					//cut off everything on the URL except the last 3 characters
					$urlEnd = substr($pageURL, -4);
				
					$urlEnd = substr( $urlEnd, strpos($urlEnd, "/") );
					
					//strip off the two forward shashes
					$unisphere_page = str_replace("/", "", $urlEnd);
				}

				$temp = $wp_query;
				$wp_query = null;
				$wp_query = new WP_Query();
				
				// The custom query for displaying blog posts in a page template
				$wp_query->query( array( 'post_type' => 'post', 'paged' => $unisphere_page, 'category__in' => array_map( 'intval', explode( ",", $cats )), 'showposts' => $items_per_page ) );
				
				global $more; $more = 0; ?>

				<?php if ( $wp_query->have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

						<?php get_template_part( 'content' ); ?>

					<?php endwhile; ?>

					<?php // If the user has set a number of items per page, then display pagination
	    			if( $items_per_page != 9999 ) {
	        			// Show the pagination from navigation.php
	        			get_template_part( 'navigation' );
					} ?>

				<?php else : ?>

					<div id="post-0" class="post no-results not-found">
						<h2 class="post-title"><?php _e( 'Not Found', 'unisphere' ); ?></h2>
						<div class="post-text">
							<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'unisphere' ); ?></p>
							<?php get_search_form(); ?>
						</div>
					</div>

				<?php endif; ?>

				<?php $wp_query = null; $wp_query = $temp; wp_reset_query(); ?>

	        <!--END #primary-->
			</div>
	        
	        <?php get_sidebar(); ?>

	    <?php } ?>
        
	</div>

<?php get_footer(); ?>