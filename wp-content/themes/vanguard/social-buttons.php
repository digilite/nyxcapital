<?php
/**
 * Social icons used in some templates of the theme
 */

global $unisphere_options;

if( isset($unisphere_options['addthis_profile_id']) ) : ?>

<div class="post-share-buttons">

	<div class="post-share-title"><?php _e('Share this:', 'unisphere'); ?></div>

	<!-- AddThis Button BEGIN -->
	<div class="addthis_toolbox addthis_default_style">
	<a class="addthis_button_facebook_like" fb:like:layout="button_count" style="width: 83px;"></a>
	<a class="addthis_button_tweet" style="width: 83px;"></a>
	<a class="addthis_button_google_plusone" g:plusone:size="medium" style="width: 72px;"></a> 
	<a class="addthis_counter addthis_pill_style" style="width: 50px;"></a>
	</div>
	<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
	<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo $unisphere_options['addthis_profile_id']; ?>"></script>
	<!-- AddThis Button END -->
    
</div>

<?php endif; ?>