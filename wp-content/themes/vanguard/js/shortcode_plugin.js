(
	function(){

		var icon_url = '';

		tinymce.create(
			'tinymce.plugins.UniSphereShortcodes', 
			{
				init : function(d,e) {
					icon_url = e + '/shortcode.png';
				},
				createControl : function(d,e) 
				{

					if(d=="unisphere_shortcodes_button"){
						
						d=e.createMenuButton( "unisphere_shortcodes_button",{
							title:"Insert Shortcode",
							image:icon_url,
							icons:false
							});
							
							var a=this;d.onRenderMenu.add(function(c,b){
								
								c=b.addMenu({title:"Blockquotes"});
									a.addImmediate(c,"Floated Left", "[blockquote align=\"left\"]Lorem ipsum dolor sit amet....[/blockquote]");
									a.addImmediate(c,"Floated Right", "[blockquote align=\"right\"]Lorem ipsum dolor sit amet....[/blockquote]");
									a.addImmediate(c,"Full-width", "[blockquote]Lorem ipsum dolor sit amet....[/blockquote]");

								c=b.addMenu({title:"Buttons"});
									a.addImmediate(c,"Small", "[button text=\"button text\" size=\"small\" url=\"#\" /]");
									a.addImmediate(c,"Medium", "[button text=\"button text\" size=\"medium\" url=\"#\" /]");
									a.addImmediate(c,"Large", "[button text=\"button text\" size=\"large\" url=\"#\" /]");
									c.addSeparator();
									a.addImmediate(c,"Opens link in new window/tab", "[button text=\"open in new window\" url=\"#\" newWindow=\"true\" /]");
									c.addSeparator();
									a.addImmediate(c,"Custom text and background colors", "[button text=\"Button\" url=\"http://www.google.com\" txtColor=\"#fff\" bgColor=\"#0697d6\" txtColorHover=\"#b0d5e5\" bgColorHover=\"#026894\" /]");

								c=b.addMenu({title:"Call to action"});
									a.addImmediate(c,"Big", "[call_to_action_big title=\"Vanguard, for WordPress\" excerpt=\"Do you want to make a lasting impression with your website?<br/>Vanguard is your new theme, you just don't know it yet.\" buttonText=\"Download the Theme →\" buttonUrl=\"#\" buttonNewWindow=\"true\" buttonType=\"fancy\" buttonCorners=\"rounded\" /]");
									a.addImmediate(c,"Big with custom button colors", "[call_to_action_big title=\"Vanguard, for WordPress\" excerpt=\"Do you want to make a lasting impression with your website?<br/>Vanguard is your new theme, you just don't know it yet.\" buttonText=\"Download the Theme →\" buttonUrl=\"#\" buttonNewWindow=\"true\" buttonType=\"fancy\" buttonCorners=\"rounded\" buttonTxtColor=\"#fff\" buttonBgColor=\"#0697d6\" buttonTxtColorHover=\"#b0d5e5\" buttonBgColorHover=\"#026894\" /]");
									c.addSeparator();
									a.addImmediate(c,"Small", "[call_to_action_small title=\"Vanguard, for WordPress.\" excerpt=\"Do you want to make a lasting impression with your website?\" buttonText=\"Download the Theme →\" buttonUrl=\"#\" buttonNewWindow=\"true\" buttonType=\"fancy\" buttonCorners=\"rounded\" /]");
									a.addImmediate(c,"Small with custom button colors", "[call_to_action_small title=\"Vanguard, for WordPress.\" excerpt=\"Do you want to make a lasting impression with your website?\" buttonText=\"Download the Theme →\" buttonUrl=\"#\" buttonNewWindow=\"true\" buttonType=\"fancy\" buttonCorners=\"rounded\" buttonTxtColor=\"#fff\" buttonBgColor=\"#0697d6\" buttonTxtColorHover=\"#b0d5e5\" buttonBgColorHover=\"#026894\" /]");
									c.addSeparator();
									a.addImmediate(c,"Bar", "[call_to_action_bar buttontext=\"Learn More\" buttonurl=\"#\" title=\"<strong>Ready to start building your site?</strong> Click to learn more.\" excerpt=\"cenas faucibus orci a lorem rhoncus vel mollis enim posuere. Nullam ut diam id orci pretium.\" /]");
									a.addImmediate(c,"Bar with custom colors", "[call_to_action_bar title=\"Do you want to make a lasting impression with your website?\" excerpt=\"cenas faucibus orci a lorem rhoncus vel mollis enim posuere. Nullam ut diam id orci pretium.\" txtColor=\"#fff\" bgColor=\"#bb1f05\" buttonText=\"Let's Talk\" buttonUrl=\"http://www.google.com\" buttonTxtColor=\"#fff\" buttonBgColor=\"#00687e\" buttonTxtColorHover=\"#fff\" buttonBgColorHover=\"#004452\" buttonNewWindow=\"true\" /]");

								c=b.addMenu({title:"Columns"});
									a.addImmediate(c,"2 Columns", "[one_half]<p>Your content here...</p>[/one_half]<br />[one_half_last]<p>Your content here...</p>[/one_half_last]");
									a.addImmediate(c,"3 Columns", "[one_third]<p>Your content here...</p>[/one_third]<br />[one_third]<p>Your content here...</p>[/one_third]<br />[one_third_last]<p>Your content here...</p>[/one_third_last]");
									a.addImmediate(c,"4 Columns", "[one_fourth]<p>Your content here...</p>[/one_fourth]<br />[one_fourth]<p>Your content here...</p>[/one_fourth]<br />[one_fourth]<p>Your content here...</p>[/one_fourth]<br />[one_fourth_last]<p>Your content here...</p>[/one_fourth_last]");
									a.addImmediate(c,"5 Columns", "[one_fifth]<p>Your content here...</p>[/one_fifth]<br />[one_fifth]<p>Your content here...</p>[/one_fifth]<br />[one_fifth]<p>Your content here...</p>[/one_fifth]<br />[one_fifth]<p>Your content here...</p>[/one_fifth]<br />[one_fifth_last]<p>Your content here...</p>[/one_fifth_last]");
									a.addImmediate(c,"6 Columns", "[one_sixth]<p>Your content here...</p>[/one_sixth]<br />[one_sixth]<p>Your content here...</p>[/one_sixth]<br />[one_sixth]<p>Your content here...</p>[/one_sixth]<br />[one_sixth]<p>Your content here...</p>[/one_sixth]<br />[one_sixth]<p>Your content here...</p>[/one_sixth]<br />[one_sixth_last]<p>Your content here...</p>[/one_sixth_last]");					
									a.addImmediate(c,"1/3 + 2/3 Columns", "[one_third]<p>Your content here...</p>[/one_third]<br />[two_third_last]<p>Your content here...</p>[/two_third_last]");
									a.addImmediate(c,"2/3 + 1/3 Columns", "[two_third]<p>Your content here...</p>[/two_third]<br />[one_third_last]<p>Your content here...</p>[/one_third_last]");					
									a.addImmediate(c,"1/4 + 1/4 + 1/2 Columns", "[one_fourth]<p>Your content here...</p>[/one_fourth]<br />[one_fourth]<p>Your content here...</p>[/one_fourth]<br />[one_half_last]<p>Your content here...</p>[/one_half_last]");
									a.addImmediate(c,"1/2 + 1/4 + 1/4 Columns", "[one_half]<p>Your content here...</p>[/one_half]<br />[one_fourth]<p>Your content here...</p>[/one_fourth]<br />[one_fourth_last]<p>Your content here...</p>[/one_fourth_last]");					
									a.addImmediate(c,"1/4 + 3/4 Columns", "[one_fourth]<p>Your content here...</p>[/one_fourth]<br />[three_fourth_last]<p>Your content here...</p>[/three_fourth_last]");					
									a.addImmediate(c,"3/4 + 1/4 Columns", "[three_fourth]<p>Your content here...</p>[/three_fourth]<br />[one_fourth_last]<p>Content here...</p>[/one_fourth_last]");					
									a.addImmediate(c,"2/5 + 1/5 + 2/5 Columns", "[two_fifth]<p>Content here...</p>[/two_fifth]<br />[one_fifth]<p>Your content here...</p>[/one_fifth]<br />[two_fifth_last]<p>Content here...</p>[/two_fifth_last]");					
									a.addImmediate(c,"3/5 + 1/5 + 1/5 Columns", "[three_fifth]<p>Your content here...</p>[/three_fifth]<br />[one_fifth]<p>Your content here...</p>[/one_fifth]<br />[one_fifth_last]<p>Your content here...</p>[/one_fifth_last]");
									a.addImmediate(c,"2/5 + 3/5 Columns", "[two_fifth]<p>Content here...</p>[/two_fifth]<br />[three_fifth_last]<p>Your content here...</p>[/three_fifth_last]");
									a.addImmediate(c,"1/5 + 4/5 Columns", "[one_fifth]<p>Content here...</p>[/one_fifth]<br />[four_fifth_last]<p>Your content here...</p>[/four_fifth_last]");
									a.addImmediate(c,"4/5 + 1/5 Columns", "[four_fifth]<p>Your content here...</p>[/four_fifth]<br />[one_fifth_last]<p>Content here...</p>[/one_fifth_last]");
									a.addImmediate(c,"1/6 + 5/6 Columns", "[one_sixth]<p>Content here...</p>[/one_sixth]<br />[five_sixth_last]<p>Content here...</p>[/five_sixth_last]");
									a.addImmediate(c,"5/6 + 1/6 Columns", "[five_sixth]<p>Content here...</p>[/five_sixth]<br />[one_sixth_last]<p>Content here...</p>[/one_sixth_last]");

								a.addImmediate(b,"Dropcap", "[dropcap]A[/dropcap]");

								a.addImmediate(b,"Flickr", "[flickr title=\"From Flickr\" numberImages=\"9\" flickrID=\"SET_FLICKRID_HERE\" lightbox=\"true\" /]");

								c=b.addMenu({title:"Highlight"});
									a.addImmediate(c,"Default", "[highlight]Lorem ipsum dolor sit amet...[/highlight]");
									a.addImmediate(c,"Custom text and background colors", "[highlight txtcolor=\"#333\" bgcolor=\"#A6CECE\"]Lorem ipsum dolor sit amet...[/highlight]");

								c=b.addMenu({title:"Horizontal Separator"});
									a.addImmediate(c,"Default", "[hr toTop=\"false\" /]");
									a.addImmediate(c,"With \"To Top\" link", "[hr /]");

								a.addImmediate(b,"Icon", "[icon id=\"001\" floatLeft=\"true\" /]");

								c=b.addMenu({title:"Images"});
									a.addImmediate(c,"Single Image", "[image img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" alt=\"title\"/]");
									a.addImmediate(c,"Full Image in Lightbox", "[image img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" alt=\"title\"/]");
									a.addImmediate(c,"Video in Lightbox", "[image img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" url=\"http://vimeo.com/8245346\" alt=\"title\"/]");
									a.addImmediate(c,"Custom destination URL", "[image img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" url=\"http://www.google.com\" lightbox=\"false\" alt=\"title\"/]");
									a.addImmediate(c,"Aligned Left", "[image align=\"left\" img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" alt=\"Left aligned image\" /]");
									a.addImmediate(c,"Aligned Center", "[image align=\"center\" img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" alt=\"Center aligned image\" /]");
									a.addImmediate(c,"Aligned Right", "[image align=\"right\" img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" alt=\"Right aligned image\" /]");

								c=b.addMenu({title:"Info Boxes"});
									a.addImmediate(c,"Default", "[info_box title=\"Title...\"]<br />Content...<br />[/info_box]");
									a.addImmediate(c,"Custom text and background colors", "[info_box title=\"Title...\" titleColor=\"#fff\" titleBgColor=\"#bb1f05\" txtColor=\"#fff\" txtBgColor=\"#00687e\"]<br />Content...<br />[/info_box]");

								c=b.addMenu({title:"Lightbox"});
									a.addImmediate(c,"Single Lightbox Image from Text Link", "[lightbox url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" title=\"Image title\"]single image[/lightbox]");
									a.addImmediate(c,"Grouped Lightbox Images from Text Links", "[lightbox url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" group=\"1\" title=\"Image 1\"]Grouped Image 1[/lightbox]<br />[lightbox url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" group=\"1\" title=\"Image 2\"]Grouped Image 2[/lightbox]<br />[lightbox url=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/960x350.jpg\" group=\"1\" title=\"Image 3\"]Grouped Image 3[/lightbox]");
									a.addImmediate(c,"ThemeForest.net in Lightbox from Image Link", "[lightbox url=\"http://www.themeforest.net\" width=\"100%\" height=\"100%\" iframe=\"true\"][image img=\"http://vanguard.unispheredesign.com/wp-content/uploads/placeholders/210x125.jpg\" /][/lightbox]");
									a.addImmediate(c,"Inline Content from Text Link", "[lightbox url=\"#inline-content\"]Inline Content[/lightbox]<br /><div id=\"inline-content\" style=\"display: none;\"><br /><p><strong>What is Lorem Ipsum?</strong></p><br /><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><br /></div>");

								c=b.addMenu({title:"List"});
									a.addImmediate(c,"Black", "[list bullet=\"black\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Blue", "[list bullet=\"blue\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Cross", "[list bullet=\"cross\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Disk", "[list bullet=\"disk\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Error", "[list bullet=\"error\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Go", "[list bullet=\"go\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Green", "[list bullet=\"green\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Orange", "[list bullet=\"orange\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Picture", "[list bullet=\"picture\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Pink", "[list bullet=\"pink\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Purple", "[list bullet=\"purple\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Red", "[list bullet=\"red\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Star", "[list bullet=\"star\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Tick", "[list bullet=\"tick\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"White", "[list bullet=\"white\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
									a.addImmediate(c,"Yellow", "[list bullet=\"yellow\"]<li>list item...</li><li>list item...</li><li>list item...</li><li>list item...</li>[/list]");
								
								c=b.addMenu({title:"Maps"});
									a.addImmediate(c,"Terrain map from Lat/Long", "[map width=\"710\" height=\"400\" zoom=\"3\" type=\"TERRAIN\" latitude=\"39.045923\" longitude=\"-8.085937\" /]");
									a.addImmediate(c,"Roadmap map from Address with popup", "[map width=\"710\" height=\"400\" zoom=\"16\" type=\"ROADMAP\" popuptext=\"This is a popup\" address=\"Avenida da Boavista, Porto, Portugal\" /]");
									a.addImmediate(c,"Hybrid map without controls", "[map width=\"710\" height=\"400\" zoom=\"14\" type=\"HYBRID\" address=\"Avenida da Boavista, Porto, Portugal\" showMapTypeControl=\"false\" showZoomControl=\"false\" showPanControl=\"false\" showScaleControl=\"false\" showStreetViewControl=\"false\" /]");
									a.addImmediate(c,"Satellite map in full-width page", "[map width=\"960\" height=\"500\" zoom=\"17\" type=\"SATELLITE\" address=\"Avenida da Boavista, Porto, Portugal\" /]");

								c=b.addMenu({title:"Popular Posts"});
									a.addImmediate(c,"With excerpt", "[popular_posts title=\"Popular Posts\" numberPosts=\"4\" /]");
									a.addImmediate(c,"Without excerpt", "[popular_posts title=\"Popular Posts (no excerpt)\" numberPosts=\"4\" numberWords=\"0\" /]");

								c=b.addMenu({title:"Popular Portfolio Items"});
									a.addImmediate(c,"With excerpt", "[popular_portfolio title=\"Popular Portfolio Items\" numberPosts=\"4\" /]");
									a.addImmediate(c,"Without excerpt", "[popular_portfolio title=\"Popular Portfolio Items (no excerpt)\" numberPosts=\"4\" numberWords=\"0\" /]");

								c=b.addMenu({title:"Portfolio"});
									a.addImmediate(c,"1 Column", "[portfolio columns=\"1\" numberPosts=\"4\" cats=\"\" /]");
									c.addSeparator();
									a.addImmediate(c,"2 Columns", "[portfolio columns=\"2\" numberPosts=\"6\" cats=\"\" /]");
									a.addImmediate(c,"2 Columns without Gutter", "[portfolio columns=\"2\" numberPosts=\"6\" withgutter=\"false\" cats=\"\" /]");
									c.addSeparator();
									a.addImmediate(c,"3 Columns", "[portfolio columns=\"3\" numberPosts=\"9\" cats=\"\" /]");
									a.addImmediate(c,"3 Columns without Gutter", "[portfolio columns=\"3\" numberPosts=\"9\" withgutter=\"false\" cats=\"\" /]");
									c.addSeparator();
									a.addImmediate(c,"4 Columns", "[portfolio columns=\"4\" numberPosts=\"8\" cats=\"\" /]");
									a.addImmediate(c,"4 Columns without Gutter", "[portfolio columns=\"4\" numberPosts=\"8\" withgutter=\"false\" cats=\"\" /]");
													            
								c=b.addMenu({title:"Price Tables"});
									a.addImmediate(c,"3 Columns", "[price_table columns=\"3\"]<br />[price_column title=\"First\"]<br /><li>[price_tag value=\"Free!\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column featured=\"true\" title=\"Second\"]<br /><li>[price_tag value=\"$10\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Third\"]<br /><li>[price_tag value=\"$20\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[/price_table]");
									a.addImmediate(c,"4 Columns", "[price_table columns=\"4\"]<br />[price_column title=\"First\"]<br /><li>[price_tag value=\"Free!\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column featured=\"true\" title=\"Second\"]<br /><li>[price_tag value=\"$10\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Third\"]<br /><li>[price_tag value=\"$20\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Fourth\"]<br /><li>[price_tag value=\"$30\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[/price_table]");
									a.addImmediate(c,"5 Columns", "[price_table columns=\"5\"]<br />[price_column title=\"First\"]<br /><li>[price_tag value=\"Free!\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column featured=\"true\" title=\"Second\"]<br /><li>[price_tag value=\"$10\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Third\"]<br /><li>[price_tag value=\"$20\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Fourth\"]<br /><li>[price_tag value=\"$30\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Fifth\"]<br /><li>[price_tag value=\"$40\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[/price_table]");
									a.addImmediate(c,"6 Columns", "[price_table columns=\"6\"]<br />[price_column title=\"First\"]<br /><li>[price_tag value=\"Free!\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column featured=\"true\" title=\"Second\"]<br /><li>[price_tag value=\"$10\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Third\"]<br /><li>[price_tag value=\"$20\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Fourth\"]<br /><li>[price_tag value=\"$30\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Fifth\"]<br /><li>[price_tag value=\"$40\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[price_column title=\"Sixth\"]<br /><li>[price_tag value=\"$50\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\"]</li>[/price_column]<br />[/price_table]");
									c.addSeparator();
									a.addImmediate(c,"5 Columns with custom column colors", "[price_table columns=\"5\"]<br />[price_column title=\"First\" color=\"#89BF3D\"]<br /><li>[price_tag value=\"Free!\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\" corners=\"subtle\" type=\"fancy\"]</li>[/price_column]<br />[price_column title=\"Second\" color=\"#0697D6\"]<br /><li>[price_tag value=\"$10\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\" corners=\"subtle\" type=\"fancy\"]</li>[/price_column]<br />[price_column title=\"Third\" featured=\"true\" color=\"#F93A9D\"]<br /><li>[price_tag value=\"$20\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\" corners=\"subtle\" type=\"fancy\"]</li>[/price_column]<br />[price_column title=\"Fourth\" color=\"#EA0028\"]<br /><li>[price_tag value=\"$30\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\" corners=\"subtle\" type=\"fancy\"]</li>[/price_column]<br />[price_column title=\"Fifth\" color=\"#EDBE00\"]<br /><li>[price_tag value=\"$40\" period=\"per month\" /]</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li><strong>dolor</strong> sit</li><li>[button text=\"Sign Up\" url=\"http://www.google.com/\" corners=\"subtle\" type=\"fancy\"]</li>[/price_column]<br />[/price_table]");

								c=b.addMenu({title:"Recent Posts"});
									a.addImmediate(c,"With excerpt", "[recent_posts title=\"Recent Posts\" numberPosts=\"4\" /]");
									a.addImmediate(c,"Without excerpt", "[recent_posts title=\"Recent Posts (no excerpt)\" numberPosts=\"4\" numberWords=\"0\" /]");

								c=b.addMenu({title:"Recent Portfolio Items"});
									a.addImmediate(c,"With excerpt", "[recent_portfolio title=\"Recent Portfolio Items\" numberPosts=\"4\" /]");
									a.addImmediate(c,"Without excerpt", "[recent_portfolio title=\"Recent Portfolio Items (no excerpt)\" numberPosts=\"4\" numberWords=\"0\" /]");

								a.addImmediate(b,"Slider", "[slider animSpeed=\"500\" pauseTime=\"5000\" numberSlides=\"10\" cats=\"x,y,z\" /]");

								c=b.addMenu({title:"Tables"});
									a.addImmediate(c,"Default", "<table><thead><tr><th>Table header</th><th>Table header</th><th>Table header</th></tr></thead><tbody><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tbody></table>");
									a.addImmediate(c,"With stripes", "<table class=\"stripes\"><thead><tr><th>Table header</th><th>Table header</th><th>Table header</th></tr></thead><tbody><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tbody></table>");
									a.addImmediate(c,"With border", "<table class=\"border\"><thead><tr><th>Table header</th><th>Table header</th><th>Table header</th></tr></thead><tbody><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tbody></table>");
									a.addImmediate(c,"With stripes and border", "<table class=\"stripes border\"><thead><tr><th>Table header</th><th>Table header</th><th>Table header</th></tr></thead><tbody><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tr><tr><td>Cell data</td><td>Cell data</td><td>Cell data</td></tbody></table>");

								a.addImmediate(b,"Tabs", "[tabs titles=\"Tab 1, Tab 2, Tab 3\"]<br />[tab]Tab 1 content...[/tab]<br />[tab]Tab 2 content...[/tab]<br />[tab]Tab 3 content...[/tab]<br />[/tabs]");

								c=b.addMenu({title:"Testimonials"});
									a.addImmediate(c,"Default", "[testimonial person=\"John Doe\"]<br />Sagittis fringilla, massa et nunc. Fusce sollicitudin eros non mauris convallis gravida. Aenean fringilla magna eu nulla euismod id tincidunt tortor adipiscing. Vestibulum elementum quam ac lacus mollis.<br />[/testimonial]");
									a.addImmediate(c,"Custom text and background colors", "[testimonial person=\"John Doe\" bgcolor=\"#403438\" txtcolor=\"#f0eced\"]<br />Sagittis fringilla, massa et nunc. Fusce sollicitudin eros non mauris convallis gravida. Aenean fringilla magna eu nulla euismod id tincidunt tortor adipiscing. Vestibulum elementum quam ac lacus mollis.<br />[/testimonial]");

								c=b.addMenu({title:"Titles"});
									a.addImmediate(c,"Big", "[title_big title=\"Recent <strong>Work.</strong>\" /]");
									a.addImmediate(c,"Big with Sub-title", "[title_big title=\"Awesome <strong>Work.</strong>\" subTitle=\"Please fill out the form below and we will get back to you within 1-2 business days. We look forward to serving you!\" /]");
									a.addImmediate(c,"Small", "[title_small title=\"We <strong>Love</strong> Our Clients\" /]");

								c=b.addMenu({title:"Toggles"});
									a.addImmediate(c,"Closed", "[toggle title=\"Title...\"]<p>Content...</p>[/toggle]");
									a.addImmediate(c,"Opened", "[toggle title=\"Title...\" open=\"true\"]<p>Content...</p>[/toggle]");

								c=b.addMenu({title:"Twitter"});
									a.addImmediate(c,"With replies and retweets", "[twitter title=\"Latest Tweets\" numberTweets=\"3\" username=\"SET_USERNAME_HERE\" /]");
									a.addImmediate(c,"Without replies and retweets", "[twitter title=\"Latest Tweets\" numberTweets=\"3\" username=\"SET_USERNAME_HERE\" excludeReplies=\"true\" includeretweets=\"false\" /]");

								c=b.addMenu({title:"Videos"});
									a.addImmediate(c,"YouTube", "[video type=\"youtube\" url=\"http://www.youtube.com/watch?v=B0ky-VMi9fI\" autoplay=\"false\" /]");
									a.addImmediate(c,"Vimeo", "[video type=\"vimeo\" url=\"http://vimeo.com/8245346\" autoplay=\"false\" /]");
									a.addImmediate(c,"HTML5", "[video type=\"html5\" thumbnail=\"http://vanguard.unispheredesign.com/wp-content/uploads/2012/04/html5_thumbnail.jpg\" mp4=\"http://media.jilion.com/videos/demo/midnight_sun_sv1_360p.mp4\" mp4Hd=\"http://media.jilion.com/videos/demo/midnight_sun_sv1_720p.mp4\" webmOgg=\"http://media.jilion.com/videos/demo/midnight_sun_sv1_360p.webm\" webmOggHd=\"http://media.jilion.com/videos/demo/midnight_sun_sv1_720p.webm\" autoplay=\"false\" /]");
									a.addImmediate(c,"FLV", "[video type=\"flv\" flv=\"http://download.gametrailers.com/gt_vault/12821/t_portal2_pax10_coop_hd.flv\" thumbnail=\"http://vanguard.unispheredesign.com/wp-content/uploads/assets/misc/flv_video_thumbnail_big.jpg\" autoplay=\"false\" /]");

								c=b.addMenu({title:"Wide Bar"});
									a.addImmediate(c,"Default", "[wide_bar paddingTop=\"20\" paddingBottom=\"20\" marginBottom=\"0\"]<p>Content...</p>[/wide_bar]");
									a.addImmediate(c,"Dark solid background color", "[wide_bar paddingTop=\"20\" paddingBottom=\"20\" marginBottom=\"0\" whiteText=\"true\" bgcolor=\"#333\"]<p>Content...</p>[/wide_bar]");
									a.addImmediate(c,"Big background image", "[wide_bar paddingTop=\"20\" paddingBottom=\"20\" marginBottom=\"0\" bgImage=\"http://vanguard.unispheredesign.com/wp-content/uploads/2012/12/wide_bar_bg.jpg\" bgRepeat=\"no-repeat\" bgPosition=\"center center\" ]<p>Content...</p>[/wide_bar]");
									a.addImmediate(c,"Repeatable background pattern", "[wide_bar paddingTop=\"20\" paddingBottom=\"20\" marginBottom=\"0\" bgImage=\"http://vanguard.unispheredesign.com/wp-content/uploads/2012/11/noisy_grid.png\" bgRepeat=\"repeat\" bgPosition=\"center center\" whiteText=\"true\"]<p>Content...</p>[/wide_bar]");
							});
						return d

					} // End IF Statement

					return null
				},

				addImmediate:function(d,e,a){d.add({title:e,onclick:function(){tinyMCE.activeEditor.execCommand( "mceInsertContent",false,a)}})}

			}
		);

		tinymce.PluginManager.add( "UniSphereShortcodes", tinymce.plugins.UniSphereShortcodes);
	}
)();