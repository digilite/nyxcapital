/* Replace "no-js" class with "js" in the HTML element */
var htmlElement = document.getElementsByTagName('html')[0];
htmlElement.className = htmlElement.className.replace("no-js","js");

/* Cufon fonts */
if( unisphere_globals.cufonFonts != '') {
	var cufonFonts = eval( "(" + unisphere_globals.cufonFonts + ")" );
	jQuery.each(cufonFonts.fonts, function(k, v) {
		Cufon.replace( v.selector, { fontFamily: v.fontFamily, hover: true });
	});
	window.setInterval( function() {Cufon.refresh();}, 100 );
}

jQuery(document).ready(function() {

	// Adds a browser specific CSS class to the body tag
	jQuery('body').addClass( jQuery.uaMatch(navigator.userAgent).browser ).addClass( jQuery.uaMatch(navigator.userAgent).browser + '-' + parseInt(jQuery.browser.version, 10));

	// Adds mobile devices classes to the body tag
	if( navigator.userAgent.indexOf('iPad') != -1 )
		jQuery('body').addClass('ipad');
	if( navigator.userAgent.indexOf('iPhone') != -1 )
		jQuery('body').addClass('iphone');
	if( navigator.userAgent.indexOf('Android') != -1 )
		jQuery('body').addClass('android');
	
	mobileNav();
	
	/* Dropdown menu using superfish */
	jQuery('.nav').supersubs({
		minWidth: 12,
		maxWidth: 25,
		extraWidth: 1
	}).superfish({
		delay: 0,
		animation: { opacity: 'show', height: 'show' },
		speed: 'fast',
		autoArrows: false 
	});

	/* Quicksand animation and filtering of the Portfolio */
	var data = jQuery(".portfolio-sortable").clone();

	if(window.location.hash != '') {
	    var filterClass = window.location.hash.substring(1);

	    jQuery(".portfolio-filters li a").removeClass("active");

	    var filteredData = data.find('.portfolio-item[data-type*=' + filterClass + ']');

	    jQuery('.portfolio-sortable').quicksand(filteredData, {
	        duration: 500,
	        easing: 'easeInOutExpo',
	        adjustHeight: 'dynamic'
	    }, function(){
	        // end callback
	        SetPortfolioItemHover();
			SetLightbox();

		    // Check for the existence of the SublimeVideo object
			if (typeof sublimevideo !== 'undefined' && typeof sublimevideo !== false) {			
				jQuery('video.zoom').each(function() {						
					sublimevideo.prepare(jQuery(this).attr('id'));
				});
			} 
	    });

	    jQuery(".portfolio-filters li." + filterClass).children('a').addClass("active");
	}

	jQuery('.portfolio-filters li').click(function(e) {

		jQuery(".portfolio-filters li a").removeClass("active");

		var filterClass = jQuery(this).attr('class');

		if (filterClass == 'all') {
			var filteredData = data.find('.portfolio-item');
		} else {
			var filteredData = data.find('.portfolio-item[data-type*=' + filterClass + ']');
		}

		jQuery('.portfolio-sortable').quicksand(filteredData, {
			duration: 500,
			easing: 'easeInOutExpo',
			adjustHeight: 'dynamic'
		}, function(){
			// end callback
			SetPortfolioItemHover();
			SetLightbox();

		    // Check for the existence of the SublimeVideo object
			if (typeof sublimevideo !== 'undefined' && typeof sublimevideo !== false) {			
				jQuery('video.zoom').each(function() {						
					sublimevideo.prepare(jQuery(this).attr('id'));
				});
			} 
		});

		jQuery(this).children('a').addClass("active");

		return false;
	});

	/* Ajax Contact form validation and submit */
	jQuery('form#contactForm').submit(function() {
		jQuery(this).find('.error').remove();
		var hasError = false;
		jQuery(this).find('.requiredField').each(function() {
			if(jQuery.trim(jQuery(this).val()) == '') {
				jQuery(this).addClass('input-error');
				hasError = true;
			} else if(jQuery(this).hasClass('email')) {
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim(jQuery(this).val()))) {
					jQuery(this).addClass('input-error');
					hasError = true;
				}
			}
		});
		if(!hasError) {
			jQuery(this).find('#unisphere-submit').fadeOut('normal', function() {
				jQuery(this).parent().parent().find('.sending-message').show('normal');
			});
			var formInput = jQuery(this).serialize();
			var contactForm = jQuery(this);
			jQuery.ajax({
				type: "POST",
				url: jQuery(this).attr('action'),
				data: formInput,
				success: function(data){
					contactForm.parent().fadeOut("normal", function() {
						jQuery(this).prev().prev().show('normal'); // Show success message
					});
				},
				error: function(data){
					contactForm.parent().fadeOut("normal", function() {
						jQuery(this).prev().show('normal');  // Show error message
					});
				}
			});
		}
		
		return false;
		
	});
	
	jQuery('.requiredField').blur(function() {
		if(jQuery.trim(jQuery(this).val()) != '' && !jQuery(this).hasClass('email')) {
			jQuery(this).removeClass('input-error');
		} else {
			jQuery(this).addClass('input-error');
		}
	});
	
	jQuery('.email').blur(function() {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if(emailReg.test(jQuery.trim(jQuery(this).val())) && jQuery(this).val() != '') {
			jQuery(this).removeClass('input-error');
		} else {
			jQuery(this).addClass('input-error');
		} 
	});	
	
	/* Search Watermark */
	var watermark = unisphere_globals.searchLocalized;
	jQuery(".search").each(function() {
		if (jQuery(this).val() == '') {
			jQuery(this).val(watermark);
		}	
	});	
	
	jQuery(".search")
		.focus(	function() {
			if (this.value == watermark) {
				this.value = "";
			}
		})
		.blur(function() {
			if (this.value == "") {
				this.value = watermark;
			}
		});

	/* Remove empty .portfolio-detail-media div */
	if(jQuery.trim(jQuery(".portfolio-detail-media").html()) == "") {
		jQuery(".portfolio-detail-media").remove();
	}

	/* Remove empty #comments div */
	if(jQuery.trim(jQuery("#comments").text()) == "") {
		jQuery("#comments").remove();
	}

	/* Tabs */
	jQuery(".tab-content").hide(); //Hide all content
	jQuery(".tab-container > br").remove();

	// Select the first tabs by default
	jQuery("ul.tabs").each( function() {
		jQuery(this).find("li:first").addClass("active"); //Activate first tab
	});
	
	jQuery(".tab-container").each( function() {
		jQuery(this).find(".tab-content:first").show(); //Show first tab content
	});

	// Check if we have a hash in the querystring in order to show a particular tab on page load
	if( window.location.hash != '') {
		var tab_id = window.location.hash.substring(1, window.location.hash.indexOf('-'));
		var tab_to_show = window.location.hash.substring(window.location.hash.indexOf('-tab') + 4);

		jQuery('ul.tabs[data-tab-id=' + tab_id + '] li').each( function() {
			jQuery(this).removeClass("active"); //Deactivate first tab

			if( jQuery(this).find('a').attr('href') == '#tab' + tab_to_show )
				jQuery(this).addClass("active"); // Activate selected tab
		});
		
		jQuery('ul.tabs[data-tab-id=' + tab_id + ']').next().find(".tab-content").hide(); //Hide all tab content
		jQuery('ul.tabs[data-tab-id=' + tab_id + ']').next().find("div.tab-content:nth-child(" + tab_to_show + ")").show();
	}

	jQuery("ul.tabs li").click(function() {
		jQuery(this).parent().find('li.active').removeClass("active");
		jQuery(this).addClass("active"); //Add "active" class to selected tab
		jQuery(this).parent().next().find(".tab-content").hide(); //Hide all tab content

		var activeTabIndex = (jQuery(this).find("a").attr("href")).substring((jQuery(this).find("a").attr("href")).indexOf('#tab') + 4); //Find the href attribute value to identify the active tab + content
		
		var activeTabContent = jQuery(this).parent().next().find("div.tab-content:nth-child(" + activeTabIndex + ")");
		activeTabContent.fadeIn(); //Fade in the active ID content
		
		return false;
	});
	
	/* Toggle */
	jQuery(".toggle-container .toggle-content").hide(); //Hide (Collapse) the toggle containers on load
	jQuery(".toggle-container .toggle-sign").addClass('closed'); //Add the "closed" class on load

	jQuery(".toggle-container-open .toggle-content").show(); //Show the default opened toggle containers on load	
	jQuery(".toggle-container-open .toggle-sign").addClass('opened'); //Add the "opened" class on load

	jQuery(".toggle-container .toggle").click(function(e) {
		e.preventDefault();
		if(jQuery(this).find('.toggle-sign').hasClass('opened')) {
			jQuery(this).find('.toggle-sign').removeClass('opened');
			jQuery(this).find('.toggle-sign').addClass('closed');
		} else {
			jQuery(this).find('.toggle-sign').removeClass('closed');
			jQuery(this).find('.toggle-sign').addClass('opened');
		}
		jQuery(this).next(".toggle-content").slideToggle();
	});
	
	/* Required to style post page links similar to the wp-pagenavi ones */
	jQuery('.post_linkpages a span').each( function() {                                  
        jQuery(this).parent().html(jQuery(this).html());
    });
    jQuery('.post_linkpages span').addClass('current');
	
	/* Pricing Table */
	jQuery(".price-table > br").remove();
	jQuery(".price-table .price-column:nth-child(even)").addClass('price-column-even');
	jQuery(".price-column li:nth-child(even)").addClass('even');
	
	jQuery(".price-table").each( function() { 
        jQuery(this).find('.price-column:first').addClass('price-column-first');
    });
	
	jQuery(".price-table").each( function() { 
        jQuery(this).find('.price-column:last').addClass('price-column-last');
    });
    
    /* Buttons */
    // Add target="_blank" to buttons that have the data-newwindow="true" attribute
	jQuery('.custom-button[data-newwindow=true]').each( function() {                                  
		jQuery(this).attr('target', '_blank').removeAttr('data-newwindow');
	});
	
	// Add custom hover background color to buttons that have the data-bgcolorhover attribute
	jQuery('.custom-button').each( function() {
		if( jQuery(this).attr('data-bgcolor') != '' ) { jQuery(this).css('backgroundColor', jQuery(this).attr('data-bgcolor')); }
		if( jQuery(this).attr('data-txtcolor') != '' ) { jQuery(this).css('color', jQuery(this).attr('data-txtcolor')); }
		jQuery(this).hover(
			function () { // on hover
				jQuery(this).css('backgroundColor', '');
				jQuery(this).css('color', '');
				if( jQuery(this).attr('data-bgcolorhover') != '' ) { jQuery(this).css('backgroundColor', jQuery(this).attr('data-bgcolorhover')); }
				if( jQuery(this).attr('data-txtcolorhover') != '' ) { jQuery(this).css('color', jQuery(this).attr('data-txtcolorhover')); }
			}, 
			function () { // off hover
				jQuery(this).css('backgroundColor', '');
				jQuery(this).css('color', '');
				if( jQuery(this).attr('data-bgcolor') != '' ) { jQuery(this).css('backgroundColor', jQuery(this).attr('data-bgcolor')); }
				if( jQuery(this).attr('data-txtcolor') != '' ) { jQuery(this).css('color', jQuery(this).attr('data-txtcolor')); }
			}
		);
	});

	// Add custom color to a price column
	jQuery('.price-column').each( function() {		
		if( jQuery(this).attr('data-color') != '' ) { 
			var columnColor = jQuery(this).attr('data-color');
			jQuery(this).find('.price-tag').css('color', jQuery(this).attr('data-color')); 
			var customButton = jQuery(this).find('li .custom-button');
			customButton.css('backgroundColor', jQuery(this).attr('data-color')); 

			customButton.hover(
				function () { // on hover
				}, 
				function () { // off hover
					customButton.css('backgroundColor', columnColor); 
				}
			);
		}
	});

    /* Hide the footer widgets area if there's no widgets present */
	if( jQuery('.footer-column .widget').size() == 0 ) {
    	jQuery('#footer-widgets-container').css('display', 'none');
	};

	/* Remove the bar if the widget titles aren't present */
	jQuery('#secondary .widget').each(function(){
		if( jQuery(this).find('.widget-title').size() == 0 ) {
    		jQuery(this).find('.bar').remove();
		};
	});

	/* Set all footer widget columns to the same height */
	if( jQuery('.footer-column .widget').size() != 0 ) {
		var maxHeight = 0;
		jQuery('.footer-column').each(function() {
			if( jQuery(this).height() > maxHeight )
				maxHeight = jQuery(this).height();
		});

		jQuery('.footer-column').height(maxHeight);
	}

	/* Set the submit button class */
	jQuery('input.submitbutton').each( function() {		
		jQuery(this).attr('class', 'custom-button');
	});

	/* Make "to top" links on separators scroll to the top of the page */
	jQuery('.to-top').click(function() {
        jQuery('html, body').animate({scrollTop:0}, 'normal');
    });

	/* Set social icons hover effect */
    jQuery('.widget-social a').hover(function() {
		jQuery(this).stop().transition({
		    rotate: '25deg'
		}, 750, 'ease');
	},function(){
		jQuery(this).stop().transition({
		    rotate: '0'
		}, 750, 'ease');
	});

	SetPortfolioItemHover();
	SetLightbox();
	SetVideoPlayer();	

	jQuery("#content-container, #footer-widgets-container").fitVids();
});

/* Portfolio images hover effect */
function SetPortfolioItemHover() {	
	
	jQuery('.portfolio-scale .portfolio-image').hover(function() {
		jQuery(this).find('img').stop().transition({
		    opacity: 0.9,
    		scale: 1.05
		}, 250, 'ease');
	},function(){
		jQuery(this).find('img').stop().transition({
		    opacity: 1,
    		scale: 1
		}, 250, 'ease');
	});

	jQuery('.portfolio-fade .portfolio-item').hover(function() {
		jQuery(this).find('img').stop().transition({
		    opacity: 0.9,
    		scale: 1.05
		}, 250, 'ease');
		jQuery(this).find('.portfolio-info').stop().show().transition({
		    opacity: 1
		}, 250, 'ease');
	},function(){
		jQuery(this).find('img').stop().transition({
		    opacity: 1,
    		scale: 1
		}, 250, 'ease');
		jQuery(this).find('.portfolio-info').stop().transition({
		    opacity: 0
		}, 250, 'ease');
	});
}

function SetLightbox() {

	/* This fixes inline content lightboxes in some WP installations */
	jQuery('a.lightbox').each(function(){
		if(jQuery(this).attr('href').indexOf('#') != -1) {
			jQuery(this).attr('href', jQuery(this).attr('href').substr(jQuery(this).attr('href').indexOf('#')));
		}
	});
	
	/* PrettyPhoto */
	jQuery("a[data-rel^='lightbox']").prettyPhoto({overlay_gallery: false});
	jQuery("a[data-rel^='lightbox']").each(function(){
		jQuery(this).css('overflow', 'hidden');
	});

	/* PrettyPhoto images hover effect */
	jQuery("a[data-rel^='lightbox'], .widget-posts .post-image").hover(function() {
		jQuery(this).find('img').stop().transition({
		    opacity: 0.9,
    		scale: 1.05
		}, 250, 'ease');
	},function(){
		jQuery(this).find('img').stop().transition({
		    opacity: 1,
    		scale: 1
		}, 250, 'ease');
	});
}

function SetVideoPlayer() {

	/* Sub-header Slider or Shortcode Slider init */
    if( jQuery('#slider-container').length > 0 || jQuery('.slider-container').length > 0 || jQuery('.embedded-video-flv').length > 0 || jQuery('.embedded-video-html5').length > 0 ) {
	    // Check for the existence of the SublimeVideo object
		if (typeof sublimevideo !== 'undefined' && typeof sublimevideo !== false) {			
			sublimevideo.ready(function(){
				// Start sub-header slider
				if( jQuery('#slider').length > 0 ) {
					DisplaySubHeaderSlider( jQuery('#slider') );
					SetSubHeaderSliderNavigation();
				}
				// Start slider shortcodes
				jQuery('.slider').each(function() {					
					DisplayShortcodeSlider(jQuery(this));
					SetShortcodeSliderNavigation(jQuery(this));
				});

				// Display embedded FLV videos
				jQuery('.embedded-video-flv').each( function() {
					DisplayShortcodeFlvVideo(jQuery(this));
				});

				// Display embedded FLV videos
				jQuery('.embedded-video-html5').each( function() {
					DisplayShortcodeHtml5Video(jQuery(this));
				});
			});

			// Initialize sublimevideo API if not already initialized
			if( !jQuery.isFunction( sublimevideo.getState ) ) {
				sublimevideo.load();
			}
		} else {
			if( jQuery('#slider').length > 0 ) {
				DisplaySubHeaderSlider( jQuery('#slider') );
				SetSubHeaderSliderNavigation();
			}
			jQuery('.slider').each(function() {
				DisplayShortcodeSlider(jQuery(this));
				SetShortcodeSliderNavigation(jQuery(this));
			});
		}
	}
}

function DisplaySubHeaderSlider(element) {	
	jQuery(element)	
	.after('<span id="slider-prev"/>') 
	.after('<span id="slider-next"/>') 
	.cycle({ 
		fx: 'fade', 
		easing: 'easeInOutExpo', 
		cleartype: 1,
		speed: element.attr('data-speed'),
		timeout: element.attr('data-timeout'),
		prev: '#slider-prev',
		next: '#slider-next',
		before: onCycleBefore,
		after: onCycleAfter
	});

	var aspectRatio = element.attr('data-aspect-ratio');	
	var newheight = parseInt(Math.round(parseFloat(element.width()/aspectRatio)), 10);
	
	if( element.parent().hasClass('fullwidth') ) {
		newheight = parseInt(Math.round(parseFloat(element.parent().outerWidth()/aspectRatio)), 10);
	}

   	element.css('height', newheight + 'px');
   	element.parent().css('height', newheight + 'px');
   	element.children('li').children('.slider-video').each(function(){
		jQuery(this).css('height', newheight + 'px');
	});

	jQuery(element).touchwipe({
		wipeLeft: function() {
			jQuery(element).cycle("next");
		},
		wipeRight: function() {
			jQuery(element).cycle("prev");
		},
		preventDefaultEvents: false
	});
}

function DisplayShortcodeSlider(element) {	
	jQuery(element)
	.after('<span id="slider-prev-' + element.attr('id') + '" class="slider-prev"/>') 
	.after('<span id="slider-next-' + element.attr('id') + '" class="slider-next"/>') 
	.cycle({ 
		fx: 'fade', 
		easing: 'easeInOutExpo', 
		cleartype: 1,
		speed: element.attr('data-speed'),
		timeout: element.attr('data-timeout'),
		prev: '#slider-prev-' + element.attr('id'),
		next: '#slider-next-' + element.attr('id'),
		before: onCycleBefore,
		after: onCycleAfter
	});

	var aspectRatio = element.attr('data-aspect-ratio');	
	var newheight = parseInt(element.width()/aspectRatio);
   	element.css('height', newheight + 'px');
   	element.parent().css('height', newheight + 'px');
   	element.children('li').children('.slider-video').each(function(){
		jQuery(this).css('height', newheight + 'px');
	});

	jQuery(element).touchwipe({
		wipeLeft: function() {
			jQuery(element).cycle("next");
		},
		wipeRight: function() {
			jQuery(element).cycle("prev");
		},
		preventDefaultEvents: false
	});
}

function DisplayShortcodeFlvVideo(element) {
	var player_id = element.find('div').attr('id');
	var autoplay = element.attr('data-autoplay');
	var video_thumbnail = element.attr('data-video-thumbnail');
	var videourl_flv = element.attr('data-videourl-flv');

	var videourl_flv_hd;
	if (typeof element.attr('data-videourl-flv_hd') !== 'undefined' && element.attr('data-videourl-flv_hd') !== false)
		videourl_flv_hd = element.attr('data-videourl-flv_hd');

	// Create a <video> DOM element 
	var video = jQuery('<video/>').attr('id', player_id).attr('class', 'sublimevideo').attr('preload', 'none').attr('poster', video_thumbnail);

	// Create the <source> DOM elements
	video.append( jQuery('<source/>').attr('src', videourl_flv) );
	if (videourl_flv_hd) { video.append( jQuery('<source/>').attr('src', videourl_flv_hd).attr('data-quality', 'hd') ); }

	element.empty().append(video);

	if(autoplay == 'true') {
		sublimevideo.prepareAndPlay(player_id); // start the video
	} else { 
		sublimevideo.prepare(player_id);
	}

	sublimevideo.onStart(function(sv){
		jQuery('.sublimevideo').each(function(){
			sublimevideo.unprepare(jQuery(this));
		});
	});

	var aspectRatio = element.width() / element.height();
	element.attr('data-aspect-ratio', aspectRatio);	
	element.css('width', '100%');
 		
	var newWidth = element.width();
  	sublimevideo.resize(player_id, newWidth, newWidth/aspectRatio);
  	element.css('height', parseInt(newWidth/aspectRatio) + 'px');
}

function DisplayShortcodeHtml5Video(element) {
	var player_id = element.find('div').attr('id');
	var autoplay = element.attr('data-autoplay');
	var video_thumbnail = element.attr('data-video-thumbnail');
	var videourl_mp4 = element.attr('data-videourl-mp4');

	var videourl_mp4_hd;
	if (typeof element.attr('data-videourl-mp4-hd') !== 'undefined' && element.attr('data-videourl-mp4-hd') !== false)
		videourl_mp4_hd = element.attr('data-videourl-mp4-hd');

	var videourl_webm;
	if (typeof element.attr('data-videourl-webm') !== 'undefined' && element.attr('data-videourl-webm') !== false)
		videourl_webm = element.attr('data-videourl-webm');

	var videourl_webm_hd;
	if (typeof element.attr('data-videourl-webm-hd') !== 'undefined' && element.attr('data-videourl-webm-hd') !== false)
		videourl_webm_hd = element.attr('data-videourl-webm-hd');

	// Create a <video> DOM element 
	var video = jQuery('<video/>').attr('id', player_id).attr('class', 'sublimevideo').attr('preload', 'none').attr('poster', video_thumbnail);

	// Create the <source> DOM elements
	video.append( jQuery('<source/>').attr('src', videourl_mp4) );
	if (videourl_mp4_hd) { video.append( jQuery('<source/>').attr('src', videourl_mp4_hd).attr('data-quality', 'hd') ); }
	if (videourl_webm) { video.append( jQuery('<source/>').attr('src', videourl_webm) ); }
	if (videourl_webm_hd) { video.append( jQuery('<source/>').attr('src', videourl_webm_hd).attr('data-quality', 'hd') ); }

	element.empty().append(video);

	if(autoplay == 'true' && !isIOS()) {
		sublimevideo.prepareAndPlay(player_id); // start the video
	} else {
		sublimevideo.prepare(player_id);
	}

	sublimevideo.onStart(function(sv){
		jQuery('.sublimevideo').each(function(){
			sublimevideo.unprepare(jQuery(this));
		});
	});

	var aspectRatio = element.width() / element.height();
	element.attr('data-aspect-ratio', aspectRatio);	
	element.css('width', '100%');
 		
	var newWidth = element.width();
  	sublimevideo.resize(player_id, newWidth, newWidth/aspectRatio);
  	element.css('height', parseInt(newWidth/aspectRatio) + 'px');
}

function SetSubHeaderSliderNavigation() {

	var triggered = false;
	jQuery("#slider-container").mousemove(function(e) {
		if (jQuery(this).is(':hover') && !triggered) {
        	jQuery(this).trigger('mouseenter');
        	triggered = true;
    	}
	});

	jQuery('#slider-container').hover(
		function (e) { // on hover	
			if(jQuery(e.target).is('#slider-prev')) {
				e.stopPropagation();
				jQuery(this).find('#slider-next').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			} else if(jQuery(e.target).is('#slider-next')) {
				e.stopPropagation();
				jQuery(this).find('#slider-prev').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			} else {	
				jQuery(this).find('#slider-prev').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
				jQuery(this).find('#slider-next').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			}
		},
		function (e) { // off hover
			jQuery(this).find('#slider-prev').stop().animate({'opacity':'0'}, 250);
			jQuery(this).find('#slider-next').stop().animate({'opacity':'0'}, 250);
		}
	);

	// Sub-header Slider navigation hover effect
	jQuery('#slider-container #slider-prev, #slider-container #slider-next').hover(
		function () { // on hover
			jQuery(this).stop().animate({'opacity':'1'}, 250);
		},
		function () { // off hover
			jQuery(this).stop().animate({'opacity':'.5'}, 250);
		}
	);
}

function SetShortcodeSliderNavigation(element) {

	element.parent().hover(
		function (e) { // on hover	
			if(jQuery(e.target).is('.slider-prev')) {
				e.stopPropagation();
				jQuery(this).find('.slider-next').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			} else if(jQuery(e.target).is('.slider-next')) {
				e.stopPropagation();
				jQuery(this).find('.slider-prev').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			} else {	
				jQuery(this).find('.slider-prev').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
				jQuery(this).find('.slider-next').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			}
		},
		function (e) { // off hover
			jQuery(this).find('.slider-prev').stop().animate({'opacity':'0'}, 250);
			jQuery(this).find('.slider-next').stop().animate({'opacity':'0'}, 250);
		}
	);

	// Sub-header Slider navigation hover effect
	element.parent().find('.slider-prev, .slider-next').hover(
		function () { // on hover
			jQuery(this).stop().animate({'opacity':'1'}, 250);
		},
		function () { // off hover
			jQuery(this).stop().animate({'opacity':'.5'}, 250);
		}
	);
}


function SetLayerSliderNavigationEffect() {

	var triggeredLayerSlider = false;
	jQuery(".ls-container").mousemove(function(e) {
		if (jQuery(this).is(':hover') && !triggeredLayerSlider) {
        	jQuery(this).trigger('mouseenter');
        	triggeredLayerSlider = true;
    	}
	});

	jQuery('.ls-container').hover(
		function (e) { // on hover	
			if(jQuery(e.target).is('.ls-nav-prev')) {
				e.stopPropagation();
				jQuery(this).find('.ls-nav-next').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			} else if(jQuery(e.target).is('.ls-nav-next')) {
				e.stopPropagation();
				jQuery(this).find('.ls-nav-prev').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			} else {	
				jQuery(this).find('.ls-nav-prev').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
				jQuery(this).find('.ls-nav-next').stop().css({'display':'block','opacity':'0'}).animate({'opacity':'.5'}, 250);
			}
		},
		function (e) { // off hover
			jQuery(this).find('.ls-nav-prev').stop().animate({'opacity':'0'}, 250);
			jQuery(this).find('.ls-nav-next').stop().animate({'opacity':'0'}, 250);
		}
	);

	jQuery('.ls-container .ls-nav-prev, .ls-container .ls-nav-next').unbind('mouseenter mouseleave');
	// Sub-header Slider navigation hover effect
	jQuery('.ls-container .ls-nav-prev, .ls-container .ls-nav-next').hover(
		function () { // on hover
			jQuery(this).stop().animate({'opacity':'1'}, 250);
		},
		function () { // off hover
			jQuery(this).stop().animate({'opacity':'.5'}, 250);
		}
	);
}

function onCycleAfter() {
	jQuery(this).find('.slider-title').delay(100).animate({'bottom':'30px','opacity':'1'}, 200);
}

/* Event triggered before every slider transition */
function onCycleBefore() {
	jQuery(this).find('.slider-title').css({'bottom':'0','opacity':'0'});

	var sliderContainer = jQuery(this).parent().parent();
	var slider = jQuery(this).parent();

	// By default resume the slider, if a video exists the slider will pause when autoplay is on
	slider.cycle('resume'); // go to next slider item

	// Unprepare any sublime videos in the slider
	slider.find('.slider-video').each(function() {
		if( jQuery(this).attr('data-videotype') == 'html5_flv' ) {
			sublimevideo.unprepare('object' + jQuery(this).attr('id'));
		}
	});

	// Search for an embeded video and remove it
	var element = slider.find('object, iframe, .sublimevideo');
	element.wrap(function() {
		return '<div id="object' + element.parent().attr('id') + '" />';
	}).remove();

	// CSS class to display the slider navigation on different slider item styles.
	// Adds a "with-description" or "no-description" class to the slider container
	sliderContainer.removeClass('with-description no-description video image').addClass(jQuery(this).attr('class'));

	// Now create a video if it's present on this slider item
	var video_element = jQuery(this).find('.slider-video');	
	if(video_element.length) {
		sliderContainer.addClass('video');
		createVideo(video_element);
	} else {
		sliderContainer.addClass('image');
	}
}

/* Function to embed a video in the slider */
var yt_player;
function createVideo(element) {
	if(element.length) { // if element exists
		var slider = element.parent().parent();
		var parent_li = element.parent();
		slider.cycle('pause'); // pause the slider until video finishes

		var video_id;
		if (typeof element.attr('data-videourl') !== 'undefined' && element.attr('data-videourl') !== false)
			video_id = GetVideoId(element.attr('data-videourl'));
		var player_id = element.find('div').attr('id');					
		var video_type = element.attr('data-videotype');
		var unisphere_js = unisphere_globals.jsFolderUrl;
		var autoplay = element.attr('data-autoplay');		

		if(video_type == 'youtube') {
			if(isIE()) {
				var params = { allowScriptAccess: "always", allowfullscreen: "true", wmode: 'transparent' };
				var atts = { id: player_id, unisphere_autoplay: autoplay };
				swfobject.embedSWF("http://www.youtube.com/v/" + video_id + "?enablejsapi=1&fs=1&wmode=transparent&autohide=1&showinfo=0&playerapiid=" + player_id, player_id, "100%", "100%", "8", unisphere_js + "/expressinstall.swf", null, params, atts);
			} else {
				yt_player = new YT.Player(player_id, { 
					height: '100%',
					width: '100%',
					videoId: video_id,
					playerVars: {
						wmode: 'transparent',
						autohide: 1,
						showinfo: 0,
						autoplay: (autoplay == 'true' && !isIOS() ? 1 : 0)
					},
					events: {
						'onReady': onYouTubePlayerReady,
						'onStateChange': onYouTubePlayerStateChange
	          		}
	        	});

	        	if( autoplay != "true" )
	        		slider.cycle('resume');
	        }
		} else if(video_type == 'vimeo') {
			if(isIE()) {
				var flashvars = {
					clip_id: video_id,
					fullscreen: 1,
					show_portrait: 0,
					show_byline: 0,
					show_title: 0,
					js_api: 1, // required in order to use the Javascript API
					js_onLoad: 'onVimeoPlayerReady', // moogaloop will call this JS function when it's done loading (optional)
					js_swf_id: player_id // this will be passed into all event methods so you can keep track of multiple moogaloops (optional)
				};
				var params = { allowscriptaccess: 'always', allowfullscreen: 'true', wmode: 'transparent' };
				var attributes = { unisphere_autoplay: autoplay };
				
				// For more SWFObject documentation visit: http://code.google.com/p/swfobject/wiki/documentation
				swfobject.embedSWF("http://vimeo.com/moogaloop.swf", player_id, "100%", "100%", "9.0.0", unisphere_js + "/expressinstall.swf", flashvars, params, attributes);
			} else {
				jQuery('#' + player_id).append('<iframe id="' + player_id + '" type="text/html" width="100%" height="100%" src="http://player.vimeo.com/video/' + video_id + '?api=1&amp;show_portrait=0&amp;show_byline=0&amp;show_title=0&amp;player_id=' + player_id + '" frameborder="0" unisphere_autoplay="' + autoplay + '"></iframe>');
				jQuery('#' + player_id).find('iframe').unwrap();
				var vimeoPlayer = document.getElementById(player_id);
	            Froogaloop(vimeoPlayer).addEvent('ready', onVimeoPlayerReady);
            }
		} else { // All other video types: FLV, MP4, WEBM
			var videourl_mp4 = false;
			if (typeof element.attr('data-videourl-mp4') !== 'undefined' && element.attr('data-videourl-mp4') !== false)
				videourl_mp4 = element.attr('data-videourl-mp4');

			var videourl_mp4_hd = false;
			if (typeof element.attr('data-videourl-mp4-hd') !== 'undefined' && element.attr('data-videourl-mp4-hd') !== false)
				videourl_mp4_hd = element.attr('data-videourl-mp4-hd');

			// If is iOS and no MP4 video format is provided stop right here
			if( isIOS() && !videourl_mp4 && !videourl_mp4_hd ) {
				return;
			}

			var videourl_webm;
			if (typeof element.attr('data-videourl-webm') !== 'undefined' && element.attr('data-videourl-webm') !== false)
				videourl_webm = element.attr('data-videourl-webm');

			var videourl_webm_hd;
			if (typeof element.attr('data-videourl-webm-hd') !== 'undefined' && element.attr('data-videourl-webm-hd') !== false)
				videourl_webm_hd = element.attr('data-videourl-webm-hd');

			var videourl_flv;
			if (typeof element.attr('data-videourl-flv') !== 'undefined' && element.attr('data-videourl-flv') !== false)
				videourl_flv = element.attr('data-videourl-flv');

			var videourl_flv_hd;
			if (typeof element.attr('data-videourl-flv_hd') !== 'undefined' && element.attr('data-videourl-flv_hd') !== false)
				videourl_flv_hd = element.attr('data-videourl-flv_hd');
			
			var video_thumbnail = element.attr('data-video-thumbnail');

			// Create a <video> DOM element 
			var video = jQuery('<video/>').attr('id', player_id).attr('class', 'sublimevideo').attr('preload', 'none').attr('poster', video_thumbnail);

			// Create the <source> DOM elements
			if (videourl_mp4) { video.append( jQuery('<source/>').attr('src', videourl_mp4) ); }
			if (videourl_mp4_hd) { video.append( jQuery('<source/>').attr('src', videourl_mp4_hd).attr('data-quality', 'hd') ); }
			if (videourl_webm) { video.append( jQuery('<source/>').attr('src', videourl_webm) ); }
			if (videourl_webm_hd) { video.append( jQuery('<source/>').attr('src', videourl_webm_hd).attr('data-quality', 'hd') ); }
			if (videourl_flv) { video.append( jQuery('<source/>').attr('src', videourl_flv) ); }
			if (videourl_flv_hd) { video.append( jQuery('<source/>').attr('src', videourl_flv_hd).attr('data-quality', 'hd') ); }

			element.empty().append(video);

			if(autoplay == 'true') {
				sublimevideo.prepareAndPlay(player_id); // start the video
			} else { // else resume the slider
				sublimevideo.prepare(player_id);
				slider.cycle('resume');
			}

			if( slider.parent().hasClass('fullwidth') )	{
				sublimevideo.resize(player_id, slider.parent().width(), slider.parent().height());
			} else {
				sublimevideo.resize(player_id, parent_li.width(), element.height());
			}

			sublimevideo.onEnd(function(sv){
				if( jQuery('#' + sv.element.id).parents('#slider').size() != 0 ) {
					jQuery('#' + sv.element.id).parents('#slider').cycle('next'); // go to next slider item
				} else {
					jQuery('#' + sv.element.id).parents('.slider').cycle('next'); // go to next slider item
				}
			});

			sublimevideo.onStart(function(sv){
				jQuery('.sublimevideo').each(function(){
					if( jQuery(this).parents('#slider').size() != 0 ) {
						jQuery(this).parents('#slider').cycle('resume'); // resume the slider until video finishes
					} else if( jQuery(this).parents('#slider').size() == 0 ) {
						jQuery(this).parents('.slider').cycle('resume'); // resume the slider until video finishes
					}
					sublimevideo.unprepare(jQuery(this));
				});
				if( jQuery('#' + sv.element.id).parents('#slider').size() != 0 ) {
					jQuery('#' + sv.element.id).parents('#slider').cycle('pause'); // pause the slider until video finishes
				} else {
					jQuery('#' + sv.element.id).parents('.slider').cycle('pause'); // pause the slider until video finishes
				}
			});

		}
	}
}

var youTubePlayerLoggers = {}; 
function onYouTubePlayerReady(event) {
	var playerId;	
	if(isIE()) {
		playerId = event;
		ytplayer = document.getElementById(playerId); // Get the reference to the video			
		youTubePlayerLoggers[playerId] = function (state) {
	        var slider = jQuery('#'+(playerId)).parent().parent().parent();
			if(state > 0) { // Video as started
				slider.cycle('pause'); // pause the slider until video finishes
			} else if(state == 0) { // Video has ended
				slider.cycle('next'); // go to next slider item
			}
	    }; 
	    ytplayer.addEventListener("onStateChange", "youTubePlayerLoggers."+playerId); 

	    // If autoplay is set, then start the video
		if(jQuery('#'+playerId).attr('unisphere_autoplay') == 'true') {
			ytplayer.playVideo(); // start the video
		} else { // else resume the slider
			var slider = jQuery('#'+playerId).parent().parent().parent();
			slider.cycle('resume');
		}
	} else {
		playerId = jQuery(event.target.getVideoEmbedCode()).attr('id');	
	}
}

function onYouTubePlayerStateChange(event) {
	state = event.data;	
	var slider = jQuery('#'+jQuery(event.target.getVideoEmbedCode()).attr('id')).parent().parent().parent();

	if(state > 0) { // Video as started
		slider.cycle('pause'); // pause the slider until video finishes
	} else if(state == 0) { // Video has ended
		slider.cycle('next'); // go to next slider item
	}
}

function onVimeoPlayerReady(playerId) {
	if(isIE()) {
		vmplayer = document.getElementById(playerId); // Get the reference to the video			
		vmplayer.api_addEventListener('onPlay', 'onVimeoPlay'); // add onPlay event listener
		vmplayer.api_addEventListener('onFinish', 'onVimeoFinish'); // add onFinish event listener	
	} else {
		// Keep a reference to Froogaloop for this player
		var container = document.getElementById(playerId).parentNode.parentNode,
			froogaloop = Froogaloop(playerId);
		froogaloop.addEvent('play', onVimeoPlay );
		froogaloop.addEvent('finish', onVimeoFinish );
	}

	// If autoplay is set, then start the video and pause the slider
	if(jQuery('#'+playerId).attr('unisphere_autoplay') == 'true' && !isIOS()) {		
		if(isIE()) {
			vmplayer.api_play(); // start the video
		} else {
			froogaloop.api('play'); // start the video
		}
	} else { // else resume the slider
		var slider = jQuery('#'+playerId).parent().parent().parent();
		slider.cycle('resume');
	}
}

function onVimeoPlay(playerId) {
	var slider = jQuery('#'+playerId).parent().parent().parent();
	slider.cycle('pause'); // pause the slider until video finishes
}

function onVimeoFinish(playerId) {
	var slider = jQuery('#'+playerId).parent().parent().parent();
	slider.cycle('next'); // go to next slider item
}

/* Extract video id */
function GetVideoId(url) {
	var videoId;
	if(url.match(/(youtube)/)) {
		videoId = url.replace(/^[^v]+v.(.{11}).*/,"$1"); // Youtube Video
	} else if(url.match(/(vimeo)/)) {
		var re = new RegExp('/[0-9]+', "g");
		var match = re.exec(url);
		videoId = match[0].substring(1);
	} else {
		videoId = false;
	}
	return videoId;
}

function isIE() {
	if (jQuery.browser.msie) {
		return true;
	}

	return false;
}

function isIOS() {
	var deviceAgent = navigator.userAgent.toLowerCase();
	return deviceAgent.match(/(iphone|ipod|ipad)/);
}

function removeCufon(selector) {
	jQuery(selector).each(function() {
		var g = '';
	 	jQuery(this).find('cufon cufontext').each(function() {
	 	    g = g + jQuery(this).html();
	 	}); 
	 	if( g != '') {
	 		jQuery(this).html( jQuery.trim(g) );
	 	}
	});
}

function mobileNav() {
  /* Clone our navigation */
  var mainNavigation = jQuery('#header .menu').clone();

  /* Add a "select" element to be populated with options, and create a variable to select our new empty option menu */
  jQuery('#header #menu-bg .mobile-menu').remove();
  jQuery('#header #menu-bg').append('<select class="mobile-menu"></select>');
  var selectMenu = jQuery('select.mobile-menu');

  /* Navigate our nav clone for information needed to populate options */
  jQuery(mainNavigation).children('ul').children('li').each(function() {

     /* Get top-level link and text */
     var href = jQuery(this).children('a').attr('href');
     var text = jQuery(this).children('a').text();
     var isCurrent = ( jQuery(this).hasClass('current_page_item') || jQuery(this).hasClass('current-menu-item') );

     /* Append this option to our "select" */
     jQuery(selectMenu).append('<option value="'+href+'"'+(isCurrent?'selected="selected"':'')+'>'+text+'</option>');

     /* Check for "children" and navigate for more options if they exist */
     if (jQuery(this).children('ul').length > 0) {
        jQuery(this).children('ul').children('li').each(function() {

           /* Get child-level link and text */
           var href2 = jQuery(this).children('a').attr('href');
           var text2 = jQuery(this).children('a').text();
           var isCurrent2 = ( jQuery(this).hasClass('current_page_item') || jQuery(this).hasClass('current-menu-item') );

           /* Append this option to our "select" */
           jQuery(selectMenu).append('<option value="'+href2+'"'+(isCurrent2?'selected="selected"':'')+'>- '+text2+'</option>');

           	/* Check for "children" and navigate for more options if they exist */
	        if (jQuery(this).children('ul').length > 0) {
	            jQuery(this).children('ul').children('li').each(function() {

	               /* Get child-level link and text */
	               var href3 = jQuery(this).children('a').attr('href');
	               var text3 = jQuery(this).children('a').text();
	               var isCurrent3 = ( jQuery(this).hasClass('current_page_item') || jQuery(this).hasClass('current-menu-item') );

	               /* Append this option to our "select" */
	               jQuery(selectMenu).append('<option value="'+href3+'"'+(isCurrent3?'selected="selected"':'')+'>-- '+text3+'</option>');

	               	/* Check for "children" and navigate for more options if they exist */
			        if (jQuery(this).children('ul').length > 0) {
			            jQuery(this).children('ul').children('li').each(function() {

			               /* Get child-level link and text */
			               var href4 = jQuery(this).children('a').attr('href');
			               var text4 = jQuery(this).children('a').text();
			               var isCurrent4 = ( jQuery(this).hasClass('current_page_item') || jQuery(this).hasClass('current-menu-item') );

			               /* Append this option to our "select" */
			               jQuery(selectMenu).append('<option value="'+href4+'"'+(isCurrent4?'selected="selected"':'')+'>--- '+text4+'</option>');
			            });
			        }
	            });
	        }
        });
     }
  });

   /* When our select menu is changed, change the window location to match the value of the selected option. */
   jQuery(selectMenu).change(function() {
      location = this.options[this.selectedIndex].value;
   });
}

// Gets called when video needs resizing
function resizeSlidersAndVideo() {
	
	// Add responsiveness to the sub-header bar with background image 
	if( jQuery('#sub-header-container').length > 0 && jQuery('#sub-header-container').hasClass('image-bg') ) {
		var subHeaderContainer = jQuery('#sub-header-container');
		var subHeader = jQuery('#sub-header');
		var aspectRatio = subHeaderContainer.attr('data-aspect-ratio');

		if( jQuery('#sub-header').width() < 960 ) {
			var newWidth = subHeader.width();
			var newheight = parseInt(Math.round(parseFloat(newWidth/aspectRatio)), 10);
		} else {
			var newheight = parseInt(Math.round(parseFloat(960/aspectRatio)), 10);
		}

		subHeaderContainer.css('height', newheight + 'px').css('background-size', 'auto ' + newheight + 'px');
		subHeader.css('height', newheight + 'px');
	}

	// Resize shortcode videos
  	jQuery('.sublimevideo').each(function() {
  		var slider = jQuery(this).parent().parent().parent();
  		var aspectRatio = slider.attr('data-aspect-ratio');
  		var newWidth = slider.width();
  		sublimevideo.resize(jQuery(this).attr('id'), newWidth, newWidth/aspectRatio);
  		slider.css('height', parseInt(newWidth/aspectRatio) + 'px');
	});

	// Resize slider videos and the slider itself
	jQuery('.slider, #slider').each(function() {
		var slider = jQuery(this);
		var aspectRatio = slider.attr('data-aspect-ratio');			
		var newWidth = slider.width();
		if( slider.parent().hasClass('fullwidth') ) { 
			newWidth = slider.parent().outerWidth(); 
		}
		var newheight = parseInt(Math.round(parseFloat(newWidth/aspectRatio)), 10);
		

	   	slider.css('height', newheight + 'px');
	   	slider.parent().css('height', newheight + 'px');

	   	slider.children('li').each(function(){
			jQuery(this).css('width', newWidth + 'px');
		});

	   	if( newWidth < 960 ) {
			var newTitleSize = parseInt(Math.round(parseFloat(newWidth*0.027)), 10);
			var newTitlePadding = parseInt(Math.round(parseFloat(newWidth*0.0177)), 10);
			if( newTitlePadding < 5 ) newTitlePadding = 5;
			var newTitleLineheight = parseInt(Math.round(parseFloat(newWidth*0.0219)), 10);
			slider.find('div.slider-title').each(function(){
				jQuery(this).css('font-size', newTitleSize + 'px');
				jQuery(this).css('padding', newTitlePadding + 'px');
				jQuery(this).css('line-height', newTitleLineheight + 'px');
			});
		}

	   	slider.children('li').children('.slider-video').each(function(){
			jQuery(this).css('height', newheight + 'px');
			if( jQuery(this).attr('data-videotype') == 'html5_flv' ) {
	  			var parent_li = jQuery(this).parent();
				sublimevideo.resize('object' + jQuery(this).attr('id'), newWidth, newWidth/aspectRatio);
	  		}
		});
	});
};

jQuery(window).load(function() {    
	
	SetLayerSliderNavigationEffect();
	
	var deviceAgent = navigator.userAgent.toLowerCase();
	if( deviceAgent.match(/(iphone|ipod|ipad)/) ) {		
		if( jQuery("#full-bg").length == 0 && jQuery('body').css('background-image') != '' && jQuery('body').css('background-repeat') == 'no-repeat' && jQuery('body').css('background-attachment') == 'fixed' ) {
			var bgImage = jQuery('body').css('background-image').replace(/"/g,"").replace(/url\(|\)$/ig, "");
			jQuery('body').css('background-image', '');

			jQuery('<div />', {
				id    : 'full-bg-wrapper'
			}).append(jQuery('<img />', {
					src   : bgImage,
					id    : 'full-bg',
					alt   : ''
			})).prependTo('body');
		}
	}

	var theWindow        = jQuery(window),
	    $bg              = jQuery("#full-bg"),
	    aspectRatio      = $bg.width() / $bg.height();

	function resizeBg() {
		if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
		    $bg.removeClass().addClass('full-bg-height');
		} else {
		    $bg.removeClass().addClass('full-bg-width');
		}
	}

	resizeBg();
	
	theWindow.resize(function() {
		resizeBg();
	});

	resizeSlidersAndVideo();
});

var rtime = new Date(1, 1, 2000, 12,00,00);
var timeout = false;
var delta = 200;
jQuery(window).resize(function() {
	// Bind the onresizeend event
	rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
    }

    var deviceAgent = navigator.userAgent.toLowerCase();
	if( !deviceAgent.match(/(iphone|ipod|ipad)/) ) {
		resizeSlidersAndVideo();
		SetPortfolioItemHover();		
	}
});

jQuery(window).bind('orientationchange', function(event){
	SetPortfolioItemHover();
	resizeSlidersAndVideo();
});

function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        timeout = false;
        var deviceAgent = navigator.userAgent.toLowerCase();
		if( !deviceAgent.match(/(iphone|ipod|ipad)/) ) {
			/* Dropdown menu using superfish */
			jQuery('.nav').supersubs({
				minWidth: 12,
				maxWidth: 25,
				extraWidth: 1
			}).superfish({
				delay: 250,
				animation: { opacity: 'show', height: 'show' },
				speed: 'fast',
				autoArrows: false 
			});
		}
    }               
}
