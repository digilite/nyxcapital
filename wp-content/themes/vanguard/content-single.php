<?php
/**
 * The template for displaying content in the single.php template
 */ 

global $unisphere_options;

$featured_image = unisphere_get_post_image("blog-detail"); 
$custom = get_post_custom($post->ID);
$post_type = get_post_type($post->ID); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-media-wrapper<?php echo ( $featured_image == '' || ( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] != 'image' ) || ( isset( $custom['_blog_post_media_overlay_off'][0] ) && $custom['_blog_post_media_overlay_off'][0] == '1' ) ) ? ' no-image' : ''; ?>">

        <?php /* Image */
        if( $featured_image != '' && ( ( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'image' ) || !isset( $custom['_blog_post_media'][0] ) ) ) : ?>
        <div class="post-image">
            <?php echo $featured_image; ?>
        </div>
        <?php endif; ?>

        <?php /* Video */
        if( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'video' ) : ?>
        <div class="post-video">
            <?php $rand = Rand();
            $autoplay = ( isset($custom['_blog_post_autoplay']) && $custom['_blog_post_autoplay'][0] == '1' ? 'false' : 'true' );
            $thumbnail = ( isset($custom['_blog_post_no_autoplay_thumbnail']) ? $custom['_blog_post_no_autoplay_thumbnail'][0] : '' );
            $data_attribute = '';
            switch ($custom['_blog_post_video'][0]) {
                case 'youtube':
                    echo unisphere_run_shortcode('[video type="youtube" url="' . $custom['_blog_post_video_youtube'][0] . '" autoplay="' . $autoplay . '" /]');
                    break;
                case 'vimeo':
                    echo unisphere_run_shortcode('[video type="vimeo" url="' . $custom['_blog_post_video_vimeo'][0] . '" autoplay="' . $autoplay . '" /]');
                    break;
                case 'html5_flv':
                    if( isset( $custom['_blog_post_video_flv'] ) ) { // FLV
                        echo unisphere_run_shortcode('[video type="flv" flv="' . $custom['_blog_post_video_flv'][0] . '" ' . ( isset( $custom['_blog_post_video_flv_hd'] ) ? ' flvhd="' . $custom['_blog_post_video_flv_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                    } else { // HTML5
                        echo unisphere_run_shortcode('[video type="html5" mp4="' . $custom['_blog_post_video_html5_mp4'][0] . '" ' . ( isset( $custom['_blog_post_video_html5_mp4_hd'] ) ? ' mp4hd="' . $custom['_blog_post_video_html5_mp4_hd'][0] . '"' : '' ) . ' ' . ( isset( $custom['_blog_post_video_html5_webm'] ) ? ' webmogg="' . $custom['_blog_post_video_html5_webm'][0] . '"' : '' ) . ' ' . ( isset( $custom['_blog_post_video_html5_webm_hd'] ) ? ' webmogghd="' . $custom['_blog_post_video_html5_webm_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                    }
                    break;
            } ?>
        </div>
        <?php endif; ?>

        <?php /* Slider */
        if( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'slider' ) : ?>
        <div class="post-slider">
            <?php // Images attached to the post
            $slider_images = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID') );
            if ($slider_images) :
                $slider_height = intval( isset( $custom['_blog_post_slider_height'][0] ) && $custom['_blog_post_slider_height'][0] != '' ? $custom['_blog_post_slider_height'][0] : ( !empty( $unisphere_options['advanced_image_size_slider_shortcode_height'] ) ? $unisphere_options['advanced_image_size_slider_shortcode_height'] : 540 ) );
                $aspect_ratio = 960 / $slider_height;  
                ?>
                <div class="slider-container">
                    <ul class="slider" id="slider-portfolio-detail" data-aspect-ratio="<?php echo $aspect_ratio; ?>">
                    <?php foreach ($slider_images as $slider_image) : 
                        $image = unisphere_resize( $slider_image->ID, null, UNISPHERE_SLIDER_SHORTCODE_W, $slider_height, UNISPHERE_SLIDER_SHORTCODE_CROP ); ?>
                        <li class="no-description">
                            <div class="slider-image">
                                <img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" alt="<?php echo get_post_meta($slider_image->ID, '_wp_attachment_image_alt', true); ?>" title="<?php echo $slider_image->post_title; ?>" />
                            </div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <div class="post-title-meta-wrapper">

            <div class="bar"></div>
            <h1 class="post-title"><?php the_title(); ?></h1>

            <div class="post-meta">

                <?php printf( __( '<span class="published">%2$s</span><span class="author">by %1$s</span>', 'unisphere' ),
                        sprintf( '<a class="url fn n" href="%1$s" title="%2$s">%3$s</a>',
                            get_author_posts_url( get_the_author_meta( 'ID' ) ),
                            sprintf( esc_attr__( 'View all posts by %s', 'unisphere' ), get_the_author() ),
                            get_the_author()
                        ),
                        sprintf( '<abbr class="published-time" title="%1$s">%2$s</abbr>',
                            esc_attr( get_the_time() ),
                            get_the_date()
                        )
                    ); ?>

                <?php if ( count( get_the_category() ) ) : ?>
                    <span class="post-categories"><?php printf( __( 'in %s', 'unisphere' ), get_the_category_list( ', ' ) ); ?></span>
                <?php endif; ?>
                
                <?php
                    $tags_list = get_the_tag_list( '', ', ' );
                    if ( $tags_list ) : ?>
                    <span class="post-tags"><?php printf( __( 'tagged %s', 'unisphere' ), $tags_list ); ?></span>
                <?php endif; ?>
                
            </div>

        </div>

    </div>

    <?php if( $post_type == 'post' && $unisphere_options['show_blog_share_buttons'] == '1' ) : ?>
        <?php get_template_part( 'social-buttons' ); ?>
    <?php endif; ?>

    <div class="post-text">
        <?php the_content(); ?>
        <?php wp_link_pages('before=<div class="wp-pagenavi post_linkpages">&after=</div><br />&link_before=<span>&link_after=</span>'); ?>            
    </div>


    <?php // Post author info
    if( !empty( $unisphere_options['show_blog_author_info'] ) && get_the_author_meta( 'description' ) && $post_type == 'post' ) : ?>
        <div id="author-info-wrapper" class="clearfix">
            <h3 id="author-info-title"><?php printf( esc_attr__( 'About %s', 'unisphere' ), '<a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '">' . get_the_author() . '</a>' ); ?></h3>
            <div id="author-info">
                <div id="author-avatar">
                    <?php echo get_avatar( get_the_author_meta( 'user_email' ), 60 ); ?>
                </div>
                <div id="author-description">
                    <?php the_author_meta( 'description' ); ?><br />
                    <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                        <?php printf( __( 'View all posts by %s', 'unisphere' ), get_the_author() ); ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if( isset($unisphere_options['show_post_navigation_links_below']) && $unisphere_options['show_post_navigation_links_below'] == '1' && get_post_type() == 'post' ) : ?>
    <h3 id="single-nav-below-title"><?php _e( 'Other Posts', 'unisphere' ); ?></h3>
    <div id="single-nav-below">
        <?php if( function_exists('next_post_link_plus') && function_exists('previous_post_link_plus') ) : ?>
            <div id="single-nav-below-prev"><?php previous_post_link_plus( array( 'format' => '<span>&#171; ' . __('Previous', 'unisphere') . '</span>' . '%link', 'in_cats' => get_post_meta($parent_page_id, "_page_blog_cat", $single = true) ) ); ?></div>
            <div id="single-nav-below-next"><?php next_post_link_plus( array( 'format' => '<span>' . __('Next', 'unisphere') . ' &#187;</span>' . '%link', 'in_cats' => get_post_meta($parent_page_id, "_page_blog_cat", $single = true) ) ); ?></div>
        <?php else : ?>
            <div id="single-nav-below-prev"><?php previous_post_link('<span>&#171; ' . __('Previous', 'unisphere') . '</span>' . '%link'); ?></div>
            <div id="single-nav-below-next"><?php next_post_link('<span>' . __('Next', 'unisphere') . ' &#187;</span>' . '%link'); ?></div>
        <?php endif; ?>
    </div>
    <?php endif; ?>

    <?php // Related Blog Posts
    if( !empty( $unisphere_options['show_blog_related_posts'] ) && $post_type == 'post' ) :

        $backup = $post;  // backup the current object
        $posts_tags = array();
        $posts_cats = array();
        
        // Get posts with the same tags as the current post
        $tags = wp_get_post_tags($post->ID);
        if( $tags ) {
            $tagcount = count($tags);
            $tagIDs = array();
        
            for( $i = 0; $i < $tagcount; $i++ ) {
                $tagIDs[$i] = $tags[$i]->term_id;
            }
            
            remove_all_filters('posts_orderby');
            $posts_tags = get_posts(array(
                'tag__in' => $tagIDs,
                'post__not_in' => array($post->ID),
                'showposts' => 3,
                'orderby' => 'rand',
                'ignore_sticky_posts' => 1
            ));
        }

        // Get posts with the same categories as the current post
        $cats = get_the_category($post->ID);
        if( $cats ) {
            $catIDs = array();
            foreach($cats as $individual_cat) {
                $catIDs[] = $individual_cat->term_id;
            }

            remove_all_filters('posts_orderby');
            $posts_cats = get_posts(array(
                'category__in' => $catIDs,
                'post__not_in' => array($post->ID),
                'showposts' => 3, 
                'orderby' => 'rand',
                'ignore_sticky_posts' => 1
            ));
        }

        $merged_posts = array_merge( $posts_tags, $posts_cats ); //combine queries

        if( count($merged_posts) > 0 ) {
            $post_ids = array();
            foreach( $merged_posts as $item ) {
                $post_ids[]=$item->ID; //create a new query only of the post ids
            }
            $merged_posts = array_unique($post_ids); //remove duplicate post ids

            remove_all_filters('posts_orderby');
            $posts = get_posts(array(
                'post__in' => $merged_posts, //new query of only the unique post ids on the merged queries from above
                'orderby' => 'rand',
                'showposts' => 3
            ));
            
            echo '<div class="related-posts">';
            echo '<h3 id="related-posts-title">' . __('Related Posts', 'unisphere') . '</h3>';
            if ( have_comments() || comments_open() ) {
                echo '<ul>';
            } else {
                echo '<ul style="border-bottom: 0; padding-bottom: 0;">';
            }
            foreach( $posts as $post ) {
                setup_postdata($post);            
                
                $image = unisphere_get_post_image( 'sidebar-post' );
                echo '<li class="no-image">';
                if( $image )
                    echo '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-image">' . $image . '</a>';
                echo '<a href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '" class="post-title">' . get_the_title() . '</a>';
                echo '</li>';
            }
            echo '</ul>';
            echo '</div>';
        }
        $post = $backup;  // copy it back
        wp_reset_query(); // to use the original query again ?>
    <?php endif; ?>

</div>