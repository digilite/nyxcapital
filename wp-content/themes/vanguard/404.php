<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

	<div class="clearfix full-width">

		<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching, or one of the links below, can help.', 'unisphere' ); ?></p>

		<?php get_search_form(); ?>

		<div class="hr"><hr /></div>

		<div class="one-third">
			<?php the_widget( 'WP_Widget_Recent_Posts', array( 'number' => 10 ), array( 'widget_id' => '404', 'before_title' => '<h3>', 'after_title' => '</h3>', 'before_widget' => '', 'after_widget' => '' ) ); ?>
		</div>

		<div class="one-third">
			<h3><?php _e( 'Most Used Categories', 'unisphere' ); ?></h3>
			<ul>
			<?php wp_list_categories( array( 'orderby' => 'count', 'order' => 'DESC', 'show_count' => 1, 'title_li' => '', 'number' => 10 ) ); ?>
			</ul>
		</div>

		<div class="one-third last">
			<?php the_widget( 'WP_Widget_Tag_Cloud', null, array( 'before_title' => '<h3>', 'after_title' => '</h3>', 'before_widget' => '', 'after_widget' => '' ) ); ?>
		</div>

	</div>

		
<?php get_footer(); ?>