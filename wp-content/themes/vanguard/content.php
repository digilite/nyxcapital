<?php
/**
 * The default template for displaying content
 */ 

global $unisphere_options, $current_page_template;

switch( $current_page_template ) {
    
    case 'template_blog_wide_image.php': ?>

        <?php // Variables
        $featured_image = unisphere_get_post_image("blog-wide-image");
        $post_type = get_post_type();
        $custom = get_post_custom($post->ID); ?>

        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="post-media-wrapper<?php echo ( !('post' == $post_type || 'portfolio_cpt' == $post_type) || $featured_image == '' || ( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] != 'image' ) || ( isset( $custom['_blog_post_media_overlay_off'][0] ) && $custom['_blog_post_media_overlay_off'][0] == '1' ) ? ' no-image' : '' ); ?>">
            
                <?php if ( ('post' == $post_type || 'portfolio_cpt' == $post_type) && $featured_image != '' && ( ( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'image' ) || !isset( $custom['_blog_post_media'][0] ) ) ) : // Hide featured image for pages on Search ?>
                <div class="post-image">
                    <a href="<?php the_permalink(); ?>"><?php echo $featured_image; ?></a>
                </div>
                <?php endif; ?>

                <?php /* Video */
                if( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'video' ) : ?>
                <div class="post-video">
                    <?php $rand = Rand();
                    $autoplay = 'false';
                    $thumbnail = ( isset($custom['_blog_post_no_autoplay_thumbnail']) ? $custom['_blog_post_no_autoplay_thumbnail'][0] : '' );
                    $data_attribute = '';
                    switch ($custom['_blog_post_video'][0]) {
                        case 'youtube':
                            echo unisphere_run_shortcode('[video type="youtube" url="' . $custom['_blog_post_video_youtube'][0] . '" autoplay="' . $autoplay . '" /]');
                            break;
                        case 'vimeo':
                            echo unisphere_run_shortcode('[video type="vimeo" url="' . $custom['_blog_post_video_vimeo'][0] . '" autoplay="' . $autoplay . '" /]');
                            break;
                        case 'html5_flv':
                            if( isset( $custom['_blog_post_video_flv'] ) ) { // FLV
                                echo unisphere_run_shortcode('[video type="flv" flv="' . $custom['_blog_post_video_flv'][0] . '" ' . ( isset( $custom['_blog_post_video_flv_hd'] ) ? ' flvhd="' . $custom['_blog_post_video_flv_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                            } else { // HTML5
                                echo unisphere_run_shortcode('[video type="html5" mp4="' . $custom['_blog_post_video_html5_mp4'][0] . '" ' . ( isset( $custom['_blog_post_video_html5_mp4_hd'] ) ? ' mp4hd="' . $custom['_blog_post_video_html5_mp4_hd'][0] . '"' : '' ) . ' ' . ( isset( $custom['_blog_post_video_html5_webm'] ) ? ' webmogg="' . $custom['_blog_post_video_html5_webm'][0] . '"' : '' ) . ' ' . ( isset( $custom['_blog_post_video_html5_webm_hd'] ) ? ' webmogghd="' . $custom['_blog_post_video_html5_webm_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                            }
                            break;
                    } ?>
                </div>
                <?php endif; ?>

                <?php /* Slider */
                if( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'slider' ) : ?>
                <div class="post-slider">
                    <?php // Images attached to the post
                    $slider_images = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID') );
                    if ($slider_images) :
                        $slider_height = intval( isset( $custom['_blog_post_slider_height'][0] ) && $custom['_blog_post_slider_height'][0] != '' ? $custom['_blog_post_slider_height'][0] : ( !empty( $unisphere_options['advanced_image_size_slider_shortcode_height'] ) ? $unisphere_options['advanced_image_size_slider_shortcode_height'] : 540 ) );
                        $aspect_ratio = 960 / $slider_height;  
                        ?>
                        <div class="slider-container">
                            <ul class="slider" id="slider-portfolio-detail" data-aspect-ratio="<?php echo $aspect_ratio; ?>">
                            <?php foreach ($slider_images as $slider_image) : 
                                $image = unisphere_resize( $slider_image->ID, null, UNISPHERE_SLIDER_SHORTCODE_W, $slider_height, UNISPHERE_SLIDER_SHORTCODE_CROP ); ?>
                                <li class="no-description">
                                    <div class="slider-image">
                                        <img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" alt="<?php echo get_post_meta($slider_image->ID, '_wp_attachment_image_alt', true); ?>" title="<?php echo $slider_image->post_title; ?>" />
                                    </div>
                                </li>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>

                <div class="post-title-meta-wrapper">

                    <div class="bar"></div>
                    <h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'unisphere' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

                    <?php if ( 'post' == $post_type ) : // Hide meta information for portfolio and pages on Search ?>
                    <div class="post-meta">

                        <?php printf( __( '<span class="published">%2$s</span><span class="author">by %1$s</span>', 'unisphere' ),
                                sprintf( '<a class="url fn n" href="%1$s" title="%2$s">%3$s</a>',
                                    get_author_posts_url( get_the_author_meta( 'ID' ) ),
                                    sprintf( esc_attr__( 'View all posts by %s', 'unisphere' ), get_the_author() ),
                                    get_the_author()
                                ),
                                sprintf( '<abbr class="published-time" title="%1$s">%2$s</abbr>',
                                    esc_attr( get_the_time() ),
                                    get_the_date()
                                )
                            ); ?>

                        <?php if ( count( get_the_category() ) ) : ?>
                            <span class="post-categories"><?php printf( __( 'in %s', 'unisphere' ), get_the_category_list( ', ' ) ); ?></span>
                        <?php endif; ?>
                        
                        <?php
                            $tags_list = get_the_tag_list( '', ', ' );
                            if ( $tags_list ) : ?>
                            <span class="post-tags"><?php printf( __( 'tagged %s', 'unisphere' ), $tags_list ); ?></span>
                        <?php endif; ?>
                        
                    </div>
                    <?php endif; ?>

                </div>

            </div>

            <div class="post-text">
                <?php if( !is_search() && !empty($post->post_excerpt) ) :
                    the_excerpt();
                    $link = '<a class="custom-button more-link" href="' . get_permalink(get_the_ID()) . '">' . __( 'Read more', 'unisphere' ) . '</a>';
                    if ( comments_open() && !post_password_required() ) {
                        ob_start();
                        comments_popup_link( __( 'Leave a comment', 'unisphere' ), __( '1 Comment', 'unisphere' ), __( '% Comments', 'unisphere' ), 'custom-button alt comment-count' );
                        $link .=  ob_get_contents();
                        ob_end_clean();
                    }
                    echo '<p class="more-link-wrapper">' . $link . '</p>'; ?>
                <?php else : ?>
                    <?php if(is_search()) the_excerpt(); else the_content(); ?>
                <?php endif; ?>
                <?php wp_link_pages('before=<div class="wp-pagenavi post_linkpages">&after=</div><br />&link_before=<span>&link_after=</span>'); ?>            
            </div>

            <div class="hr"><hr /></div>

        </div>

        <?php break; ?>

    <?php case 'template_blog_half_image.php': ?>

        <?php // Variables
        $featured_image = unisphere_get_post_image("blog-half-image");
        $post_type = get_post_type();
        $custom = get_post_custom($post->ID); ?>

        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php /* Image */
            if ( ('post' == $post_type || 'portfolio_cpt' == $post_type) && $featured_image != '' && ( ( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'image' ) || !isset( $custom['_blog_post_media'][0] ) ) ) : // Hide featured image for pages on Search ?>
            <div class="post-image">
                <a href="<?php the_permalink(); ?>"><?php echo $featured_image; ?></a>
            </div>
            <?php endif; ?>

            <?php /* Video */
            if( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'video' ) : ?>
            <div class="post-video">
                <?php $rand = Rand();
                $autoplay = 'false';
                $thumbnail = ( isset($custom['_blog_post_no_autoplay_thumbnail']) ? $custom['_blog_post_no_autoplay_thumbnail'][0] : '' );
                $data_attribute = '';
                switch ($custom['_blog_post_video'][0]) {
                    case 'youtube':
                        echo unisphere_run_shortcode('[video type="youtube" url="' . $custom['_blog_post_video_youtube'][0] . '" autoplay="' . $autoplay . '" /]');
                        break;
                    case 'vimeo':
                        echo unisphere_run_shortcode('[video type="vimeo" url="' . $custom['_blog_post_video_vimeo'][0] . '" autoplay="' . $autoplay . '" /]');
                        break;
                    case 'html5_flv':
                        if( isset( $custom['_blog_post_video_flv'] ) ) { // FLV
                            echo unisphere_run_shortcode('[video type="flv" flv="' . $custom['_blog_post_video_flv'][0] . '" ' . ( isset( $custom['_blog_post_video_flv_hd'] ) ? ' flvhd="' . $custom['_blog_post_video_flv_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                        } else { // HTML5
                            echo unisphere_run_shortcode('[video type="html5" mp4="' . $custom['_blog_post_video_html5_mp4'][0] . '" ' . ( isset( $custom['_blog_post_video_html5_mp4_hd'] ) ? ' mp4hd="' . $custom['_blog_post_video_html5_mp4_hd'][0] . '"' : '' ) . ' ' . ( isset( $custom['_blog_post_video_html5_webm'] ) ? ' webmogg="' . $custom['_blog_post_video_html5_webm'][0] . '"' : '' ) . ' ' . ( isset( $custom['_blog_post_video_html5_webm_hd'] ) ? ' webmogghd="' . $custom['_blog_post_video_html5_webm_hd'][0] . '"' : '' ) . ' autoplay="' . $autoplay . '" thumbnail="' . $thumbnail . '" /]');
                        }
                        break;
                } ?>
            </div>
            <?php endif; ?>

            <?php /* Slider */
            if( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'slider' ) : ?>
            <div class="post-slider">
                <?php // Images attached to the post
                $slider_images = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID') );
                if ($slider_images) :
                    $slider_height = intval( isset( $custom['_blog_post_slider_height'][0] ) && $custom['_blog_post_slider_height'][0] != '' ? $custom['_blog_post_slider_height'][0] : ( !empty( $unisphere_options['advanced_image_size_slider_shortcode_height'] ) ? $unisphere_options['advanced_image_size_slider_shortcode_height'] : 540 ) );
                    $aspect_ratio = 960 / $slider_height;  
                    ?>
                    <div class="slider-container">
                        <ul class="slider" id="slider-portfolio-detail" data-aspect-ratio="<?php echo $aspect_ratio; ?>">
                        <?php foreach ($slider_images as $slider_image) : 
                            $image = unisphere_resize( $slider_image->ID, null, UNISPHERE_SLIDER_SHORTCODE_W, $slider_height, UNISPHERE_SLIDER_SHORTCODE_CROP ); ?>
                            <li class="no-description">
                                <div class="slider-image">
                                    <img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" alt="<?php echo get_post_meta($slider_image->ID, '_wp_attachment_image_alt', true); ?>" title="<?php echo $slider_image->post_title; ?>" />
                                </div>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <div class="bar<?php echo $featured_image == '' && ( ( isset( $custom['_blog_post_media'][0] ) && $custom['_blog_post_media'][0] == 'image' ) || !isset( $custom['_blog_post_media'][0] ) ) ? ' no-image' : ''; ?>"></div>
            <h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'unisphere' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

            <?php if ( 'post' == $post_type ) : // Hide meta information for portfolio and pages on Search ?>
            <div class="post-meta">

                <?php printf( __( '<span class="published">%2$s</span><span class="author">by %1$s</span>', 'unisphere' ),
                        sprintf( '<a class="url fn n" href="%1$s" title="%2$s">%3$s</a>',
                            get_author_posts_url( get_the_author_meta( 'ID' ) ),
                            sprintf( esc_attr__( 'View all posts by %s', 'unisphere' ), get_the_author() ),
                            get_the_author()
                        ),
                        sprintf( '<abbr class="published-time" title="%1$s">%2$s</abbr>',
                            esc_attr( get_the_time() ),
                            get_the_date()
                        )
                    ); ?>

                <?php if ( count( get_the_category() ) ) : ?>
                    <span class="post-categories"><?php printf( __( 'in %s', 'unisphere' ), get_the_category_list( ', ' ) ); ?></span>
                <?php endif; ?>
                
                <?php
                    $tags_list = get_the_tag_list( '', ', ' );
                    if ( $tags_list ) : ?>
                    <span class="post-tags"><?php printf( __( 'tagged %s', 'unisphere' ), $tags_list ); ?></span>
                <?php endif; ?>
                
            </div>
            <?php endif; ?>

            <div class="post-text">
                <?php if( !is_search() && !empty($post->post_excerpt) ) :
                    the_excerpt();
                    $link = '<a class="custom-button more-link" href="' . get_permalink(get_the_ID()) . '">' . __( 'Read more', 'unisphere' ) . '</a>';
                    if ( comments_open() && !post_password_required() ) {
                        ob_start();
                        comments_popup_link( __( 'Leave a comment', 'unisphere' ), __( '1 Comment', 'unisphere' ), __( '% Comments', 'unisphere' ), 'custom-button alt comment-count' );
                        $link .=  ob_get_contents();
                        ob_end_clean();
                    }
                    echo '<p class="more-link-wrapper">' . $link . '</p>'; ?>
                <?php else : ?>
                    <?php if(is_search()) the_excerpt(); else the_content(); ?>
                <?php endif; ?>
                <?php wp_link_pages('before=<div class="wp-pagenavi post_linkpages">&after=</div><br />&link_before=<span>&link_after=</span>'); ?>            
            </div>

            <div class="hr"><hr /></div>

        </div>

        <?php break; ?>

<?php } ?>